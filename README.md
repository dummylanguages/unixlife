## unixlife
This repo contains mostly a bunch of [reStructuredText][1] files used by [sphinx][2] a Static Site Generator that generates documentation in several formats, being HTML one of them. Once generated, the documentation can be browsed locally, or hosted almost anywhere, since it's just a **static site**.

* This project, is **hosted for free** at: https://dummylanguages.gitlab.io/unixlife/

* The **source files** can be found at: https://gitlab.com/dummylanguages/unixlife

To learn about setting up a **Sphinx documentation project**, hosted for free at [GitLab][4], check what we wrote [here][3].

[1]: http://docutils.sourceforge.net/rst.html
[2]: http://www.sphinx-doc.org/en/master
[3]: https://dummylanguages.gitlab.io/unixlife/sphinx_projects/index.html
[4]: https://gitlab.com/
