.. #unixlife documentation master file, created by
   sphinx-quickstart on Sat Oct 27 21:13:24 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#unixlife
=========

.. figure:: /_images/UNIX.jpg

Notes about... well `UNIX`_. UNIX (or Unix) is a lot of things:

* A philosophy.
* The `standard`_ for family of operating systems.

We'll also cover stuff related to `Linux`_ based operating systems, since nowadays these have become the **most popular** Unix-like OSes, and doesn't seem that's gonna change in the nearer future.

.. note:: `Here`_ I keep the repo with the **source files** of these notes. If you want to keep adding and improving these docs, you may want to read our notes about how to use the `Sphinx Documentation Generator`_.

.. toctree::
   :maxdepth: 4
   :hidden:
   :caption: A bit of UNIX history
   
   01_history/01_unix_history
   01_history/02_unix_standards
   01_history/macos_history
   01_history/linux_history

.. toctree::
   :maxdepth: 4
   :hidden:
   :caption: Users and Groups

   02_users_groups/index

.. toctree::
   :maxdepth: 4
   :hidden:
   :caption: Assorted topics

   macOS <02_macos/index>
   Linux <03_linux/index>
   Locale <locale/index>
   Bluetooth <bluetooth/index>
   Wi-Fi <wifi/index>
   Keyboard <keyboard/index>
   Fonts <fonts/index>
   Systemd <systemd/index>
   Terminal Emulators <terminal_emulators/index>
   Utilities <04_utilities/index>
   packages/index
   X11 <x11/index>
   Docker <07_docker/index>

.. toctree::
   :maxdepth: 3
   :hidden:
   :caption: the file system

   Files <file_system/file_types/index>
   file_system/finding_files/index

.. toctree::
   :maxdepth: 3
   :hidden:
   :caption: encryption

   encryption/index

.. toctree::
   :maxdepth: 3
   :hidden:
   :caption: appendices
   
   appendices/arch_installation/index
   appendices/arch_virtualbox/index
   appendices/debian_virtualbox/index
   appendices/debian_external_ssd
   appendices/UEFI_and_boot_managers/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _`UNIX`: https://en.wikipedia.org/wiki/Unix
.. _`standard`: https://www.opengroup.org/membership/forums/platform/unix
.. _`Linux`: https://en.wikipedia.org/wiki/Linux
.. _`Here`: https://gitlab.com/dummylanguages/unixlife
.. _`Sphinx Documentation Generator`: https://gitlab.com/dummylanguages/unixlife
