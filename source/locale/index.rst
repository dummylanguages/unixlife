.. _`locale-label`:

######
Locale
######

.. toctree::
	:maxdepth: 3
	:hidden:

	draft


According to `Wikipedia`_:

	In computing, a locale is a set of parameters that defines the user's language, region and any special variant preferences that the user wants to see in their user interface. Usually a locale identifier consists of at least a language code and a country/region code. 

Also according to Wikipedia:

	`Localization`_ is the process of adapting a product's translation to a specific country or region. Generally speaking localization, is the translation and cultural adaptation of software, video games, and websites, as well as audio/voiceover, video, or other multimedia content, and less frequently to any written translation.

On `POSIX`_ platforms, locale identifiers are defined by `ISO/IEC 15897`_ standard, 

In practice, a **locale** is just a language code that indicate things such as the language, dialect, currency, date-time format, etc of a particular country or region.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Locale_(computer_software)
.. _`Localization`: https://en.wikipedia.org/wiki/Language_localisation
.. _`POSIX`: https://en.wikipedia.org/wiki/POSIX
.. _`ISO/IEC 15897`: https://en.wikipedia.org/wiki/ISO/IEC_15897
