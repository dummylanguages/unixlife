**********************
Configuring the locale
**********************
First of all, I was having an issue in Arch. The output of ``locale`` was something like::

    locale: Cannot set LC_CTYPE to default locale: No such file or directory   
    locale: Cannot set LC_MESSAGES to default locale: No such file or directory   
    locale: Cannot set LC_ALL to default locale: No such file or directory
    ...

I solved it running::

    $ sudo localedef -f UTF-8 -i en_US en_US.UTF-8

Current Settings
================
Displaying the Current Settings::

    $ locale
    LANG=en_US.UTF-8
    LANGUAGE=
    LC_CTYPE="en_US.UTF-8"
    LC_NUMERIC="en_US.UTF-8"
    LC_TIME="en_US.UTF-8"
    LC_COLLATE="en_US.UTF-8"
    LC_MONETARY="en_US.UTF-8"
    LC_MESSAGES="en_US.UTF-8"
    LC_PAPER="en_US.UTF-8"
    LC_NAME="en_US.UTF-8"
    LC_ADDRESS="en_US.UTF-8"
    LC_TELEPHONE="en_US.UTF-8"
    LC_MEASUREMENT="en_US.UTF-8"
    LC_IDENTIFICATION="en_US.UTF-8"
    LC_ALL=

The default settings are stored in the ``/etc/default/locale`` file::

    $ cat /etc/default/locale 
    LANG=en_US.UTF-8

This file can either be adjusted manually or updated using the tool, ``update-locale``::

    # update-locale LANG=de_DE.UTF-8


Available Locales
=================
Displaying the Available Locales::

    $ locale -a
    C
    C.UTF-8
    de_AT.utf8
    de_BE.utf8
    de_CH.utf8
    de_DE.utf8
    de_LI.utf8
    de_LU.utf8
    en_AG
    en_AG.utf8
    ...
    POSIX

If a locale does not appear in the list, it will have to be additionally installed::

    # locale-gen fr_FR.UTF-8

The following file contains a list of all available locales: ``/usr/share/i18n/SUPPORTED``

Arch Linux
----------
To list available locales which have been previously generated, run::

    $ localedef --list-archive

Alternatively, using localectl::

    $ localectl list-locales


.. _`systemd`: https://systemd.io/

