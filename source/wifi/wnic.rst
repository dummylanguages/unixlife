****************
Wi-Fi interfaces
****************
A term that pops up quite often when fixing our WiFi, it's **Wi-Fi interface**, which, intuitively, we know is referred to our Wi-Fi card, but let's get a bit more technical. 

A **Wi-Fi interface** is a `Wireless network interface controller (WNIC)`_ that works over any of the versions of the Wi-Fi protocol, defined in the `IEEE 802.11`_ family of standards. So a Wi-Fi card is a `network interface controller`_ that works wirelessly which connects to a wireless radio-based computer network. But what is a **network interface controller**?

Network interface controller
============================
According to `Wikipedia`_:

    A network interface controller (**NIC**, also known as a network interface card, network adapter, LAN adapter or physical network interface, and by similar terms) is a computer **hardware** component that connects a computer to a computer network.

Early network interface controllers were commonly implemented on expansion cards that plugged into a computer bus. Check the image below of an Ethernet nic.

.. figure:: /_images/ethernet_nic.jpg
   :scale: 35%
   :align: center

Nowadays, most computers (except super thin laptops) include an Ethernet interface built into the motherboard.

The network controller implements the electronic circuitry required to communicate among computers on the same local area network (LAN) and large-scale network communications through routable protocols, such as Internet Protocol (IP).

Its place in the OSI
--------------------
The following diagram contains a refresher of the `OSI model`_:

.. figure:: /_images/osi.png
   :scale: 35%
   :align: center

The NIC is both a `physical layer`_ and `data link layer`_ device, as it provides physical access to a networking medium and, for IEEE 802 and similar networks, provides a low-level addressing system through the use of MAC addresses that are uniquely assigned to network interfaces. 

Wireless network interface controller
=====================================
A wireless network interface controller (WNIC) is just a **NIC** that works over a wireless network, rather than a wired one. Apart from the medium, all that we said above applies here. The image below is the `Intel Dual Band Wireless-AC 8265 adapter`_ (adapter is another name for WNIC)

.. figure:: /_images/wifi_wnic.jpg
   :scale: 35%
   :align: center

It supports both **Wi-Fi** and **Bluetooth** connections. Physically speaking, it's an `expansion card`_ with an `M.2`_ format, and **2230** size (22 x 30mm).

Chipset
-------
The electronics of a WNIC are implemented as an `integrated circuit`_ (aka **chip**), are obviously required to implement its physical layer functions in the the OSI model. `PHY`_ (short for "physical layer") is a more technical term to refer to the chipset.

More often than not, we can find the same chipset in cards from different manufacturers. The chipset used by our card is what is gonna determine the driver we need to use.


Modes of operation
------------------

FullMAC and SoftMAC cards
-------------------------

.. _`Wireless network interface controller (WNIC)`: https://en.wikipedia.org/wiki/Wireless_network_interface_controller
.. _`IEEE 802.11`: https://en.wikipedia.org/wiki/IEEE_802.11
.. _`network interface controller`: https://en.wikipedia.org/wiki/Network_interface_controller
.. _`Wikipedia`: https://en.wikipedia.org/wiki/Network_interface_controller
.. _`physical layer`: https://en.wikipedia.org/wiki/Physical_layer
.. _`data link layer`: https://en.wikipedia.org/wiki/Data_link_layer
.. _`OSI model`: https://en.wikipedia.org/wiki/OSI_model
.. _`Intel Dual Band Wireless-AC 8265 adapter`: https://www.intel.com/content/dam/www/public/us/en/documents/product-briefs/dual-band-wireless-ac-8265-brief.pdf
.. _`expansion card`: https://en.wikipedia.org/wiki/Expansion_card
.. _`M.2`: https://en.wikipedia.org/wiki/M.2
.. _`integrated circuit`: https://en.wikipedia.org/wiki/Integrated_circuit
.. _`PHY`: https://en.wikipedia.org/wiki/PHY
