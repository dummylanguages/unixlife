************
Driver stuff
************
The Linux kernel is modular, meaning many of the **drivers** reside on the hard drive and are available as **modules**. To check if the module for my Wi‑Fi card was loaded I run::

  $ lspci -k | grep -i wireless
  04:00.0 Network controller: Intel Corporation Wireless 8265 / 8275 (rev 78)
          Subsystem: Intel Corporation Dual Band Wireless-AC 8265
          Kernel driver in use: iwlwifi

That was quite easy, wasn't it? It seems that the **Arch Linux ISO** that I used, installed the proper driver for my card. But that's not always the case: in my other laptop I was using **Debian 10 (Buster)**, and the installer didn't include the driver. The output of the command above was the same, except for the last line::

  $ lspci -k | grep -i wireless
  04:00.0 Network controller: Intel Corporation Wireless 8265 / 8275 (rev 78)
          Subsystem: Intel Corporation Dual Band Wireless-AC 8265

So I had to use another laptop to download the package. In the Debian packages site I searched for **iwlwifi** and found that the packages was named `firmware-iwlwifi`_, downloaded it, place it in a pendrive and pasted it in the Debian laptop. 

Installing a driver/firmware
============================
Once I got the package with the driver in my filesystem, I installed it with::

    $ sudo apt install ./firmware-iwlwifi.deb

.. note:: When installing from the local filesystem do not forget the ``./`` before the debian package, so ``apt install`` won't go looking for it to the repositories. Or use the old ``dpkg -i``.

But the ``lspci`` command still wasn't showing that the kernel driver was in use. At that point I had two options:

* Reboot.
* Manually load the module (another word for driver).

Reloading a module
------------------
A two step process, first we remove the module::

  $ sudo modprobe -r iwlwifi
  
The we load it again (hence, reload)::

  $ sudo modprobe iwlwifi

That did the trick, although for some reason the ``lspci`` command wasn't giving me the right info, whatever.

Getting the name of the interface
=================================
To interact with the driver, the system needs an interface, and that interface is identified by a name that we can get running::

  $ ip link
  [...]
  3: wlp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DORMANT group default qlen 1000
      link/ether ac:ed:5c:b6:5f:49 brd ff:ff:ff:ff:ff:ff

Now we know that the interface to my Wi‑Fi card is named ``wlp4s0``. And we also know that the interface is ``UP``, and we know that from ``<BROADCAST,MULTICAST,UP,LOWER_UP>`` and not from ``state UP``; but if it wasn't, we would run::

  $ sudo ip link set wlp4s0 up

Once we know the name of our Wi‑Fi interface, another command we could run to check if it's up is::

  $ ip link show wlp4s0

The ``ip`` command is not the only way of getting the name of our interface, in the next section we'll see how to use the command line utilities provided by Network Manager.

.. _`firmware-iwlwifi`: https://packages.debian.org/buster/firmware-iwlwifi
.. _`iw`: https://wireless.wiki.kernel.org/en/users/documentation/iw
