**********************************
Network Manager and wpa_supplicant
**********************************
To fix my connection I used only two pieces of software:

* `Network Manager`_
* `wpa_supplicant`_

I didn't used:

* Not the ``iwconfig`` utility included in the now deprecated `wireless-tools`_.
* Neither the ``iw`` command, included in the `iw package`_.

.. note:: ``iw`` is a new **nl80211** based command-line utility for wireless devices. It supports all new drivers that have been added to the kernel recently. (`nl80211`_ is the new 802.11 netlink interface public header. Together with cfg80211 it is intended to replace Wireless-Extensions.)

Checking the services are up (systemd)
======================================
The first thing we should do is to check whether the **NetworkManager service** is running::
    
  $ systemctl status NetworkManager

If not we can **start** it with::

  $ sudo systemctl start NetworkManager

Or **enable** it so it's loaded when the system boots up::

  $ sudo systemctl enable NetworkManager --now

And the same goes for the **wpa_supplicant service**::
  
  $ systemctl status wpa_supplicant

If it's down::

  $ sudo systemctl enable wpa_supplicant --now

.. note:: Apart from that nothing else is needed in order to deal with ``wpa_supplicant``; it's a low-maintenance piece of software.

NetworkManager tools
====================
NetworkManager manager offers several command-line tools to get the job done.

The ``nmtui`` graphical interface
---------------------------------
``nmtui`` is curses-based **text user interface** (hence TUI) for NetworkManager. We can launch it by running ``nmtui`` and follow instructions, but I couldn't make it work with a **hidden ssid**.

The ``nmcli device`` subcommand
-------------------------------
The first thing we can do with this subcommand, it's listing the available interfaces::

    $ nmcli device show
    wlp4s0          wifi      connected     asdf-hidden

We can also use it to connect to our WiFi network::

  $ sudo nmcli dev wifi connect asdf password 'f!12abcDIrtw*'
  Error: No network with SSID 'asdf' found.

In the command above, we've created a **temporary connection** to WiFi network named ``asdf``, using the password ``f!12abcDIrtw``. Don't mind the ``Error``: it didn't find the SSID because it was a **hidden** WiFi network, but after 2 seconds it connected successfully.You can confirm that running::

  $ nmcli con
  NAME                UUID                                  TYPE      DEVICE
  asdf                703ea447-7833-4194-880d-b14a654da755  wifi      wlp4s0
  Wired connection 1  8ca9bc4c-dfe2-3bac-aebf-73ac2f22f5a2  ethernet  --

As you can see the connection is there regardless of the error. The problem is that this connection won't survive a reboot; in the next section we'll see how to create permanent connections.

.. note:: I think that once I made the connection permanent, just by runnning ``nmcli connection edit asdf`` and type ``save``, and maybe ``activate``. We may be asked for a network password. But I'm not sure at all.

The ``connection add`` subcommand
---------------------------------
We can add a new connection using the ``connection add`` subcommand. The general form of this command is as follows::

  $ nmcli connection add [COMMON_OPTIONS] [TYPE_SPECIFIC_OPTIONS] [IP_OPTIONS]

1. The **common options** are:

* ``type``, specifies the connection type, such as: ethernet, wifi, wimax, pppoe, gsm, cdma, infiniband, bluetooth, vlan, bond, bond-slave, team, team-slave, bridge, bridge-slave, vpn, and olpc-mesh.
* ``ifname``: Short for **Interface Name**. Here we can use the ``*`` special value, for interface-independent connections.
* ``con-name`` stands for **connection name**. This setting is optional; when not provided, a default name is generated.
* ``autoconnect``: Whether the connection is automatically enabled after a reboot. Also optional, and ``yes`` by **default**.
* ``save``: Whether the connection is **persistent**. Also optional and by **default**, ``yes``.

2. The **type specific** options will depend on the type of our connection. In this case, for ``wifi`` connections we can set:

* ``ssid`` : SSID
* ``mac`` for the MAC address of the WiFi interface.device this connection is locked to
* ``cloned-mac`` in case of cloned MAC addresses.
* ``mtu`` for whatever MTU is.

3. The **IP options** are ``ip4`` or ``ip6`` followed by the **IP address** we want to use.

For example::

  $ nmcli connection add con-name temp-delete-me ifname wlp4s0 type wifi ssid asdf
  Connection 'temp-delete-me' (cc43d504-0728-4a6b-8d3e-7a5adfa931c6) successfully added.

The command above creates a connection to a WiFi network named ``asdf``. The name of our interface is ``wlp4s0`` and we named the connection ``temp-delete-me``. At any point we can list all the existent connections (not only WiFi, also wired) by running::

  $ nmcli connection

We can also confirm we've added a connection by checking the contents of the directory::

  $ sudo ls /etc/NetworkManager/system-connections/
  temp-delete-me.nmconnection

All the configuration for existing connections is kept in a separate file under the aforemention directory. That file could be manually edited, but to avoid typos we could edit our configuration settings using a couple of different commands:

* The ``nmcli connection edit`` command provides an interactive interface.
* The ``nmcli connection modify`` command allows to modify a connection once we know exactly the setting we need.

Editing existing connections interactively
------------------------------------------
The problem with the ``connection add`` subcommand is that there's no way of specifying a **hidden** WiFi network. But not to worry, the trick here is that we can edit an existing connection using the command::

  $ nmcli connection edit asdf
  
  ===| nmcli interactive connection editor |===

  Editing existing '802-11-wireless' connection: 'asdf'

  Type 'help' or '?' for available commands.
  Type 'print' to show all the connection properties.
  Type 'describe [<setting>.<prop>]' for detailed property description.

  You may edit the following settings: connection, 802-11-wireless (wifi), 802-11-wireless-security (wifi-sec), 802-1x, ethtool, match, ipv4, ipv6, tc, proxy
  nmcli>

.. note:: This command can also be used to **create a new connection**; in that case, omit the name of the existing connection when you invoke it, as in ``nmcli connection edit``.

Getting help
^^^^^^^^^^^^
First thing is to type ``help`` or ``?`` to get help about what's available::

  nmcli> ?
  ------------------------------------------------------------------------------
  ---[ Main menu ]---
  goto     [<setting> | <prop>]        :: go to a setting or property
  remove   <setting>[.<prop>] | <prop> :: remove setting or reset property value
  set      [<setting>.<prop> <value>]  :: set property value
  describe [<setting>.<prop>]          :: describe property
  print    [all | <setting>[.<prop>]]  :: print the connection
  verify   [all | fix]                 :: verify the connection
  save     [persistent|temporary]      :: save the connection
  activate [<ifname>] [/<ap>|<nsp>]    :: activate the connection
  back                                 :: go one level up (back)
  help/?   [<command>]                 :: print this help
  nmcli    <conf-option> <value>       :: nmcli configuration
  quit                                 :: exit nmcli
  ------------------------------------------------------------------------------

In my case I knew I wanted to set the network as ``hidden`` so I did::

  nmcli> set connection.interface-name wlp4s0
  nmcli> set wifi.ssid asdf
  nmcli> set wifi.hidden yes
  nmcli> set wifi-sec.key-mgmt wpa-psk
  nmcli> save
  nmcli> activate
  Monitoring connection activation (press any key to continue)
  Passwords or encryption keys are required to access the wireless network 'asdf'.
  Password (802-11-wireless-security.psk): •••••••••••••
  nmcli> save
  nmcli> quit


Note that, after activating the connection we'll be asked for the **password**; after introducing it, we'll have to ``save`` again. By default, this will save our connection as ``persistent`` (the default value) so we should have it available the next reboot. The ``activate`` command should do what it promises.

Deleting connections
--------------------
What happens when you want to get rid of all these Hotel WiFi connections that have been piling up over time? Easy, just use::

  $ nmcli connection delete temp-delete-me

Where ``temp-delete-me`` is just the **name of the connection** we want to delete.

.. note:: You can use the short version:: ``nmcli con del temp-delete-me``.

We can also see what networks are found in range you can use the following simple command::

  $ nmcli dev wifi list

Worth mentioning
================
Here we'll see additional stuff we can do mainly with the ``nmcli device`` subcommand.

Get information about a particular interface
--------------------------------------------
The title is self-explanatory::

  $ nmcli device show wlp4s0

That would show the connection settings used by the ``wlp4s0`` interface.

Activate/disactivate a device
-----------------------------
The ``nmcli device`` subcommand allows us to enable or disable an interface::

  $ nmcli device disconnect wlp4s0
  Device 'wlp4s0' successfully disconnected.

To activate the device back we would use the ``connect`` argument.

List available networks
-----------------------
To see the available networks, (at least the not hidden ones), we would run::

  $ nmcli device wifi

To issue a rescan::

  $ nmcli device rescan

Disconnect from a network
-------------------------
At the beginning we saw how to use the ``nmcli device`` command to **connect** to a network. Needless to say, it can be used to **disconnect** from one::

  $ nmcli device disconnect wlp4s0

We have to specify just the name of the interface.

The ``nmcli radio`` subcommand
-------------------------------
In case things don't go as expected, we may want to check if our Wi‑Fi card is off::

    $ nmcli radio wifi on

.. _`Network Manager`: https://wireless.wiki.kernel.org/en/users/documentation/networkmanager
.. _`wpa_supplicant`: https://w1.fi/
.. _`iw package`: https://www.archlinux.org/packages/core/x86_64/iw/
.. _`wireless-tools`: https://www.hpl.hp.com/personal/Jean_Tourrilhes/Linux/Tools.html
.. _`nl80211`: https://wireless.wiki.kernel.org/en/developers/documentation/nl80211
