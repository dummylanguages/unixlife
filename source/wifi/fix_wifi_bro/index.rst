###############
Fix my WiFi bro
###############

.. toctree::
  :maxdepth: 3
  :hidden:

  driver
  network_manager

In this section we'll go over how to get our Wi-Fi connection up and running after a fresh installation. Configuring a Wi‑Fi connection is a two-part process:

1. The first part is to identify and ensure the correct **driver** for your Wi‑Fi card is installed.
2. The second is configuring the connection, which we'll do using command-line tools:

  * The `Network Manager`_.
  * `wpa_supplicant`_, for authentication.

.. _`Network Manager`: https://wireless.wiki.kernel.org/en/users/documentation/networkmanager
.. _`wpa_supplicant`: https://wireless.wiki.kernel.org/en/users/documentation/wpa_supplicant

