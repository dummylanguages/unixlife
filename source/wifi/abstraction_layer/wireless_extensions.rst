***********************
The Wireless Extensions
***********************
There are several Wi-Fi chipsets in the market, each of them requiring a specific driver. If the tools to configure our Wi-Fi connection had to interact with the drivers directly, their code should include low-level instructions to deal with the specifics of each driver. On top of that, every time a new chipset/driver were introduced, the code of the configuration tools would have to be extended to support it.

Fortunately, reality differs from the maintenance nightmare described above, which used to exist back then, when `ioctl`_ with vendor dependent APIs was used to control Wi-Fi cards. Nowadays, in between the kernel space drivers and the userland configuration tools, there is a `hardware abstraction`_ layer, that provides a stable API for the Wi-Fi configuration tools.

That happened for the first time in 1996, when Jean Tourrilhes created the `Wireless Extension(WE)`_, a generic API allowing a driver to expose to the user space configuration and statistics specific to common Wireless cards. Along with the extension, there were provided a set of tools to manipulate them, known as the `Wireless Tools(WT)`_

.. note:: Sometimes the **Wireless Extension** are referred to also in plural as the **Wireless Extensions**.

The Wireless Tools for Linux
============================
According to `Wikipedia`_:

    `Wireless tools for Linux`_ is a collection of user-space utilities written for Linux kernel-based operating systems to support and facilitate the configuration of device drivers of wireless network interface controllers and some related aspects of networking using the Linux Wireless Extension. The Wireless tools for Linux and Linux Wireless Extension are maintained by Jean Tourrilhes and sponsored by Hewlett-Packard. 

Most Linux distributions, include this package by default, or based on whether a wireless card is present. If it is not automatically installed by the distribution, it is usually in the package repositories. (In Debian it's in the `wireless-tools package`_)

Included tools
--------------
The Wireless Tools for Linux include the following command line utilities:

* ``iwconfig`` manipulate the basic wireless parameters
* ``iwlist`` allow to initiate scanning and list frequencies, bit-rates, encryption keys...
* ``iwspy`` allow to get per node link quality
* ``iwpriv`` allow to manipulate the Wireless Extensions specific to a driver (private)
* ``ifrename`` allow to rename interfaces.
* ``wpa_supplicant`` and ``hostapd`` come as a pair of complementary client and host for wireless access points. 


.. _`ioctl`: https://en.wikipedia.org/wiki/Ioctl
.. _`hardware abstraction`: https://en.wikipedia.org/wiki/Hardware_abstraction
.. _`Wireless Tools for Linux`: https://www.hpl.hp.com/personal/Jean_Tourrilhes/Linux/Tools.html
.. _`Wireless Extension(WE)`: https://www.hpl.hp.com/personal/Jean_Tourrilhes/Linux/Tools.html#wext
.. _`Wireless Tools(WT)`: https://www.hpl.hp.com/personal/Jean_Tourrilhes/Linux/Tools.html
.. _`Wikipedia`: https://en.wikipedia.org/wiki/Wireless_tools_for_Linux
.. _`wireless-tools package`: https://packages.debian.org/buster/wireless-tools