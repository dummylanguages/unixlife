#######################
Wi-Fi abstraction layer
#######################

.. toctree::
	:maxdepth: 3
	:hidden:

	new_abstraction_layer
	wireless_extensions

Once the driver for our Wi-Fi card is installed and running, it's time to configure our Wi-Fi connection. In Linux there are plenty of tools to configure our WNIC, but beore going over them, we'll start by explaining the `abstraction layer`_ between the **drivers** and the Wi-Fi configuration tools.

.. note:: This abstraction layer is known also as configuration system or a subsystem.

.. _`abstraction layer`: https://en.wikipedia.org/wiki/Hardware_abstraction
