*****************************************
A new abstraction layer for Wi-Fi devices
*****************************************
`cfg80211`_ is the new **configuration API** for 802.11 devices in Linux. It's intended to replace the Wireless-Extensions which are now in maintenance mode (no new features will be added to it, only fixing bugs.)

.. note:: `IEEE 802.11`_ is the family of standards that specify set of media access control (MAC) and physical layer (PHY) protocols for implementing Wi-Fi networks in various frequencies.

`nl80211`_ is used to configure a **cfg80211 device** and is used for kernel/userspace communication.


is the new 802.11 netlink interface public header. 

`mac80211`_ is a framework which driver developers can use to write drivers for SoftMAC wireless devices. 
::

    +------------------------------+
    |   Configuration tools        |
    |   (NetworkManager, iw, etc)  |
    +------------------------------+
                ^  
                |           USERLAND
            +-----------+
    --------|  nl80211  |-----------
            +-----------+
                |           KERNEL
                v
    +------------------------------+
    |          cfg80211            |
    +------------------------------+
            ^                 ^
            |                 |
            |           +-----------+
            |           |  mac80211 |
            |           +-----------+
            |                 | 
            v                 v          
    +-------------+    +----------------+
    |  Full-MAC   |    |    Soft-MAC    |
    |   drivers   |    |     drivers    |
    | (brcm, etc) |    | (iwlwifi, etc) |
    +-------------+    +----------------+
            ^                 ^
            |                 |
    -------------------------------------
            |    HARDWARE     |
            v                 v      
    +-------------+    +----------------+
    |  Full-MAC   |    |    Soft-MAC    |
    |  hardware   |    |    hardware    |
    +-------------+    +----------------+




.. _`mac80211`: https://wireless.wiki.kernel.org/en/developers/Documentation/mac80211
.. _`cfg80211`: https://01.org/linuxgraphics/gfx-docs/drm/driver-api/80211/cfg80211.html
.. _`nl80211`: https://wireless.wiki.kernel.org/en/developers/Documentation/nl80211
.. _`IEEE 802.11`: https://en.wikipedia.org/wiki/IEEE_802.11
.. _`ioctl`: https://en.wikipedia.org/wiki/Ioctl
