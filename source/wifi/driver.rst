*************
Wi-Fi drivers
*************
Most of the times, our Linux distro installer takes care of detecting the different hardware used in our system and installs the drivers accordingly. Even during the installation process, it's quite common to get a working Wi-Fi connection that can be used to go crazy and install software from remote repositories.

Sometimes though, we have to work with installation media that lacks the proper **driver** for our Wi-Fi card. Here we'll see what can we do to solve it.

What card do I have?
====================
The first step should be to find out what card do we have to find a driver for. If we know the brand and model of our card, an option is to use another device to find information on the Internet about the suitable driver for that brand and model.

PCI Utilities
-------------
Another option is to ask our system. Most laptop computers built after 2005 use `PCI Express`_ for their expansion cards, and most Linux distributions (if not all) include a package named the `PCI Utilities`_, wich is a collection of programs for inspecting and manipulating configuration of PCI devices. One of these utilities is the ``lspci`` command, which displays detailed information about all PCI buses and devices in the system.

One of the **options** of this command is ``-nn``, which shows codes that identify the vendor and device of our PCI device::

    $ lspci -nn | grep -i wireless
    03:00.0 Network controller [0280]: Intel Corporation Wireless 8265 / 8275 [8086:24fd] (rev 78)

The pair of hexadecimal numbers inside the second set of square brackets identify our PCI device in the `PCI ID Database`_. Our system should have a copy of the database that we could check::

    $ cat /usr/share/misc/pci.ids | less

Then we can do a search in the pager for the ``24fd`` number. If for some reason we don't get a match, we can update the database running::

    $ update-pciids

That downloads and installs an updated file to the aforementioned location.

Other programs
--------------
There are other software that can help us identifying our Wi-Fi card, such as:

* Under Gnome : `hardinfo`_
* Under KDE : KInfocenter
* In Debian, the `discover package`_ provides a useful command to idenfify PCI thingies. It uses its own files: /lib/discover/pci-busclass.xml, /lib/discover/pci-device.xml, /lib/discover/pci-vendor.xml

.. note:: Stole almost all this info from https://wiki.debian.org/HowToIdentifyADevice

Installing the driver
=====================
Once I identified my Wi-Fi card as a I made a couple Google searches to find that:

* Surprisingly, Intel offers `Linux drivers for its Wireless adapters`_.
* There's a Debian packaged version of the drivers named `firmware-iwlwifi`_, **Non-free** though.

.. note:: Non-free software is software non compliant with the `Debian Free Software Guidelines(DFSG)`_

Anyways, I had to download the package manually (in another computer) copy it to the Thinkpad using a pendrive. Once there I installed it with::

    $ sudo apt install ./firmware-iwlwifi.deb

.. note:: When installing from the local filesystem do not forget the ``./`` before the debian package, so ``apt install`` won't go looking for it to the repositories. Or use the old ``dpkg -i``.

Reloading the module
--------------------
The Debian Wiki page about `iwlwifi`_ recommends to reinsert this module to access installed firmware: 

    $ sudo modprobe -r iwlwifi; sudo modprobe iwlwifi

Using the ``-r`` option, **removes** the module. After that I can load it again.

Checking if the driver was in use
---------------------------------
After installing, I run again::

    $ lspci -nn | grep -i wireless
    03:00.0 Network controller [0280]: Intel Corporation Wireless 8265 / 8275 [8086:24fd] (rev 78)

Then I used the options ``-vv`` for **very verbose**, and ``-s`` to select the device, followed by the numbers ``03:00``, which refer to ``[domain]:bus:device.function`` (the domain in this case wasn't there)::

    $ lspci -vv -s 03:00
    ...
    Kernel driver in use: iwlwifi
    Kernel modules: iwlwifi

Other commands to show info about a driver/module
-------------------------------------------------
There's::

    $ sudo modinfo iwlwifi | head

Or::

    $ lshv -C network

Or::

    $ lsmod | grep -i wifi

.. _`PCI Express`: https://en.wikipedia.org/wiki/PCI_Express
.. _`PCI Utilities`: https://mj.ucw.cz/sw/pciutils/
.. _`PCI ID Database`: http://pci-ids.ucw.cz/
.. _`hardinfo`: https://github.com/lpereira/hardinfo
.. _`discover package`: https://packages.debian.org/buster/discover
.. _`Linux drivers for its Wireless adapters`: https://www.intel.com/content/www/us/en/support/articles/000005511/network-and-i-o/wireless-networking.html
.. _`firmware-iwlwifi`: https://packages.debian.org/stable/firmware-iwlwifi
.. _`Debian Free Software Guidelines(DFSG)`: https://en.wikipedia.org/wiki/Debian_Free_Software_Guidelines
.. _`dmesg`: https://en.wikipedia.org/wiki/Dmesg
.. _`iwlwifi`: https://wiki.debian.org/iwlwifi
