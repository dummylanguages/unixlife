#####
Wi-Fi
#####

.. toctree::
	:maxdepth: 3
	:hidden:

	wnic
	driver
	abstraction_layer/index
	userland_tools/index
	fix_wifi_bro/index

According to `Wikipedia`_:

	**Wi-Fi** is a family of wireless network protocols, based on the `IEEE 802.11`_ family of standards, which are commonly used for local area networking of devices and Internet access. Wi‑Fi is a trademark of the non-profit `Wi-Fi Alliance`_, which restricts the use of the term Wi-Fi Certified to products that successfully complete interoperability certification testing.

In this section we'll try to get familiar with the management of WiFi interfaces from Linux operating systems.

.. note:: There has been a lot of recent development and advances on Linux wireless. The `Linux Wireless`_ page contains first-hand information about it.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Wi-Fi
.. _`IEEE 802.11`: https://en.wikipedia.org/wiki/IEEE_802.11
.. _`Wi-Fi Alliance`: https://www.wi-fi.org/
.. _`Linux Wireless`: https://wireless.wiki.kernel.org/en/users
