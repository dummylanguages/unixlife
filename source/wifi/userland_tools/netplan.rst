*******
Netplan
*******
`Netplan`_ is a utility for easily configuring networking on a linux system. We just have to create a YAML file with the configuration of our network interfaces (WiFi or ethernet). This configuration file is read by ``netplan`` during early boot, and backend specific configuration files are generated under the ``/run`` folder for different network daemons.

At the moment, Netplan can be used to generate configurations for:

* The GNOME `Network Manager`_.
* `systemd-networkd`_

Check this link: https://www.linuxjournal.com/content/have-plan-netplan

Configuration files
===================

Configuration files can exist in three different locations with the precedence from most important to least as follows::

    /run/netplan/*.yaml
    /etc/netplan/*.yaml
    /lib/netplan/*.yaml

Although the documentation recommends that users should save configurations under ``/etc/netplan`` with a ``.yaml`` extension (e.g. /etc/netplan/config.yaml).

After configuring an interface and save the changes, we must run::

    $ sudo netplan apply

As a result, the configuration is parsed, written, and applied to the system. Since the file is written to disk the the configuration will persist between reboots.

Another option is to run::

    $ sudo netplan generate

Which will validate that the configuration is correctly parsed by netplan and written to disk. However, be warned that once generated the new configuration is written to disk the configuration is activated on next boot. Therefore, backing up a current working configuration is highly recommended.

Deconfigure an interface
========================
To deconfigure an interface, remove the configuration for the device from the netplan ``.yaml`` file and run::

    $ sudo netplan apply.

If the **interface** is not configured in a .yaml file in ``/etc/netplan``, it will not be configured at boot.

.. _`Netplan`: https://netplan.io/
.. _`Network Manager`: https://wireless.wiki.kernel.org/en/users/documentation/networkmanager
.. _`systemd-networkd`: http://manpages.ubuntu.com/manpages/focal/man5/systemd.network.5.html

