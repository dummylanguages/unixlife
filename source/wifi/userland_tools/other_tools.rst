***********
Other tools
***********

iw
===
`iw`_ is a new nl80211 based CLI configuration utility for wireless devices. 

The old tool iwconfig, which uses Wireless Extensions interface, is deprecated and it's strongly recommended to switch to iw and nl80211.

iproute2
========
https://en.wikipedia.org/wiki/Iproute2
https://wiki.linuxfoundation.org/networking/iproute2


.. _`iw`: https://wireless.wiki.kernel.org/en/users/documentation/iw
