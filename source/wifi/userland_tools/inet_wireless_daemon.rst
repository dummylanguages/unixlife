********************
iNet Wireless daemon
********************
The `iNet Wireless Daemon (iwd)`_ project aims to provide a comprehensive Wi-Fi connectivity solution for Linux based devices. The core goal of the project is to optimize resource utilization: storage, runtime memory and link-time costs. This is accomplished by not depending on any external libraries and utilizes features provided by the Linux Kernel to the maximum extent possible. The result is a self-contained environment that only depends on the Linux Kernel and the runtime C library.

.. note:: It doesn't depend on the ``wpa_supplicant`` to handle


.. _`iNet Wireless Daemon (iwd)`: https://iwd.wiki.kernel.org/
