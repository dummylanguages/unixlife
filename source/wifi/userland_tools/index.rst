#####################
Wi-Fi: Userland tools
#####################

.. toctree::
	:maxdepth: 3
	:hidden:

	network_manager
	wpa_supplicant
	inet_wireless_daemon
	netplan
	connman
	other_tools

In the sections before we've explained how to get the driver for our Wi-Fi card installed and running, and the `abstraction layer`_ that sits between the **kernel space** and the tools that run in `user space`_. Now it's time to configure our Wi-Fi connection, and in Linux there are plenty of tools to do that, so many that the landscape gets a bit confusing.

On one hand we have what is known as **connection managers**, which are programs that take care of multiple aspects of a network connection. That connection doesn't have to be only **WiFi**, it can also be **ethernet**. Some of these tools, have a GUI or a GUI frontend:

* The `Network Manager`_, which is a project run by Gnome.
* `Netplan`_, which is a project run by Ubuntu.
* `ConnMan`_
* `wicd`_

There's also the `iNet Wireless Daemon (iwd)`_, which is a daemon that offers commands to configure and manage multiple aspects of our WiFi connection. It offers a command line interface, but it can also be used as a backend by other front-end tools as the aforemention **Network Manager** or **ConnMan**.

We can control the **iNet Wireless Daemon** with commands such as:

* `iw`_ is a tool for manipulating the Linux Wireless stack via cfg80211/nl80211. 
* `iwctl`_ is a tool for manipulating the Linux Wireless stack via cfg80211/nl80211. 

.. note:: The **iNet Wireless Daemon** is a project that is still evolving and it's a bit difficult to find documentation about it.

Then we have several other smaller and more command line tools that take care of specific aspects of the connection, such as:

* `wpa_supplicant`_

Finally, we have to mention the `Wireless Tools for Linux`_, which are in legacy mode and their use is being not recommended.

.. _`abstraction layer`: https://en.wikipedia.org/wiki/Hardware_abstraction
.. _`user space`: https://en.wikipedia.org/wiki/User_space
.. _`Network Manager`: https://wireless.wiki.kernel.org/en/users/documentation/networkmanager
.. _`Netplan`: https://netplan.io/
.. _`ConnMan`: https://01.org/connman
.. _`wicd`: https://wireless.wiki.kernel.org/en/users/documentation/wicd
.. _`wpa_supplicant`: https://wireless.wiki.kernel.org/en/users/documentation/wpa_supplicant
.. _`Wireless Tools for Linux`: https://www.hpl.hp.com/personal/Jean_Tourrilhes/Linux/Tools.html
.. _`iNet Wireless Daemon (iwd)`: https://iwd.wiki.kernel.org/
.. _`iw`: https://iwd.wiki.kernel.org/interface_lifecycle?s[]=iw
.. _`iwctl`: https://wiki.archlinux.org/index.php/Iwd
