***************
Network Manager
***************
The `Network Manager`_ is a connection manager that can be used to configure and manage our Ethernet/PPP/Wireless connection. It is a project run by GNOME, although I couldn't find much information in the `official site`_. Instead, they provide several links to different documentation projects from several distros:

* `Red Hat Networking Guide`_
* `Fedora Networking Guide`_
* `Network Manager in Debian`_
* `Network Manager on Arch`_
* `Network Manager in Ubuntu`_, this was wasn't included but I found useful info in there too.

Even though the software was originally targeted at desktops, recently it has been chosen as the default network management software for some server-oriented distros such as **Red Hat**.

Installation
============
All major distros include the package preinstalled, otherwise it's in the repos. For Debian and derivatives can be installed with::

  $ sudo apt install network-manager

The `network-manager package`_  includes:

* The ``network-manager`` **daemon**.
* A couple of **front-end** command line tools to manage it: ``nmcli`` and ``nmtui``.

.. note:: Depending on the distro, the package may be named differently: **NetworkManager**, **networkmanager**, etc.

There are also available GUIs for GNOME and plasma desktop environments, and additional interfaces such as a system tray applet. We won't cover any of these here.

Usage
=====
The first thing we should do is to check whether the NetworkManager service is running::
    
  $ systemctl status NetworkManager

If not we can **start** it with::

  $ sudo systemctl start NetworkManager

Or **enable** it so it's loaded when the system boots up::

    $ sudo systemctl enable NetworkManager

Unmanaged interfaces
--------------------
By default, **Network Manager** does **not** manage any interface defined in ``/etc/network/interfaces`` file. This can be changed in the ``NetworkManager.conf`` file, that lives in the ``/etc/NetworkManager`` folder. We just have to set::

    [ifupdown]
    managed=true

And restart the daemon running::

    /etc/init.d/network-manager restart

Or using ``systemctl`` as explained in the section before.

``nmtui``
=========
``nmtui`` is curses-based **text user interface** (hence TUI) for NetworkManager. Just run ``nmtui`` and follow instructions, not much to explain.

``nmcli``
=========
The Network Manager main package includes the ``nmcli`` command, which allows us to do several things. If we use the command by itself (just run ``nmcli``), it will show basic information about all the networking interfaces. If we need complete information about all known devices::

    $ nmcli device show

Turn the WiFi card on and off::

    $ nmcli radio wifi on

And connect to a WiFi network::

    $ sudo nmcli dev wifi connect <wifinetworkname> password <wifipass>

We can also see what networks are found in range you can use the following simple command::

    $ nmcli connection


.. _`Network Manager`: https://wireless.wiki.kernel.org/en/users/documentation/networkmanager
.. _`official site`: https://wiki.gnome.org/Projects/NetworkManager
.. _`Red Hat Networking Guide`: https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Networking_Guide/
.. _`Fedora Networking Guide`: https://docs.fedoraproject.org/en-US/Fedora/24/html/Networking_Guide/index.html
.. _`Network manager in Debian`: https://wiki.debian.org/NetworkManager
.. _`Network manager on Arch`: https://wiki.archlinux.org/index.php/NetworkManager
.. _`Network manager in Ubuntu`: https://help.ubuntu.com/community/NetworkManager
.. _`network-manager package`: https://packages.debian.org/buster/network-manager
