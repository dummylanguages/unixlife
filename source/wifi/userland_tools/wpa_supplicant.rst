**************
wpa_supplicant
**************
Nowadays, most *protected* WiFi networks use any of the `Wi-Fi Protected Access (WPA)`_ authentication and encrytion protocols.  **WPA** and **WPA2** are the successors to the old and simpler **Wired Equivalent Privacy (WEP)**.

.. note:: It seems that **WPA3** is on its way.

According to `Wikipedia`_:

    `wpa_supplicant`_ is is a free software implementation of an `IEEE 802.11i`_ supplicant for Linux and other Unix operating systems.

In computer networking, a `supplicant`_  refers to an entity at one end of a point-to-point LAN segment that seeks to be authenticated by an **authenticator** attached to the other end of that link. In practice, a **supplicant** is a software application installed on an end-user's computer. The user invokes the program in order to submit credentials to connect to a secure network. If the authentication succeeds, the authenticator typically allows the computer to connect to the network. 

``wpa_supplicant`` is a "daemon" program that runs in the background and acts as the backend component controlling the wireless connection. ``wpa_supplicant`` supports separate frontend programs and a text-based frontend (``wpa_cli``) and a GUI (``wpa_gui``) are included with ``wpa_supplicant``.

Configuration
=============
``wpa_supplicant`` is configured using a text file that lists all accepted networks and security policies, including pre-shared keys. (Check this example `wpa_supplicant.conf`_ online.)

In addition, wpa_supplicant can use OpenSSL engine to avoid need for exposing private keys in the file system.

Usage
=====
We can query the current **status** of WPA/WPA2 with the shell command::

    $ sudo wpa_cli status


.. _`Wi-Fi Protected Access (WPA)`: https://en.wikipedia.org/wiki/Wi-Fi_Protected_Access
.. _`Wikipedia`: https://en.wikipedia.org/wiki/Wpa_supplicant
.. _`wpa_supplicant`: https://w1.fi/
.. _`IEEE 802.11i`: https://en.wikipedia.org/wiki/IEEE_802.11i-2004
.. _`supplicant`: https://en.wikipedia.org/wiki/Supplicant_(computer)
.. _`wpa_supplicant.conf`: http://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf
