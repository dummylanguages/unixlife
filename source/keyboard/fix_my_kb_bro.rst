####################
Fix my keyboard bro!
####################
Ok, so I have a Linux fresh install in front of me, and I'd like to set up my keyboard. This is what I want to achieve:

* I want three layouts: Spanish, English and Russian. And I want to toggle between them by pressing both ``Control`` keys.
* ``CapsLock`` will behave like ``Control``.
* Pressing both ``Shift`` keys simultaneously will toggle ``CapsLock``.
* Finally, I want to be able to kill the **X Window Manager** with the ``Ctrl+Alt+Backspace`` shortcut.

.. note:: Ideally, ``CapsLock`` would produce ``Escape`` when pressed alone, and ``Control`` when pressed in combination with some other key, but we won't cover that here.

What are my options
===================
I don't remember the exact names of the **layouts** or **options** I have to set, so let's refresh my memory::

    $ sudo localectl list-x11-keymap-layouts
    [...]

According to the output of the command above, I have to set up ``es`` for Spanish, ``us`` for American English, and ``ru`` for Russian. Now let's check the other **options**::

    $ sudo localectl list-x11-keymap-options
    [...]

From the output of the command above, I'm pretty sure that the **options** I have to set up are ``caps:ctrl_modifier``, ``shift:both_capslock``, and ``grp:ctrls_toggle``. So let's set everything up on the fly to make sure that's what I'm after::

    $ setxkbmap -model pc104 -layout us,es,ru -variant ,phonetic -option caps:ctrl_modifier,shift:both_capslock,grp:ctrls_toggle

.. warning:: If you specify more than one option at the same time, don't forget to separate them with a **comma**, with **no space** preceding or following it.

Yes, it worked as expected, so let's set them up **permanently**::

    $ sudo localectl set-x11-keymap us,es,ru pc104 "" caps:ctrl_modifier,shift:both_capslock,grp:ctrls_toggle

And if we check our ``/etc/default/keyboard`` file (in Debian) or ``/etc/X11/xorg.conf.d/00-keyboard.conf`` (in Arch), our changes must be there.

.. warning:: For some reason I had to **reboot** for the changes to take place using ``localectl``.

.. _`scancode`: https://en.wikipedia.org/wiki/Scancode
