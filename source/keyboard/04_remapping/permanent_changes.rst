***********************
Permanent configuration
***********************
The proper place to set a permanent keyboard configuration is in the `XOrg configuration files`_.

Location
========
The location of these files varies among distributions. In Debian and Debian-based distros, the XOrg server configuration can be found in:

* The **single file** in ``/usr/share/X11/xorg.conf``
* In **separate files** in the ``/usr/share/X11/xorg.conf.d/`` directory.

Other distributions: use the ``/etc/X11`` base directory instead of ``/usr/share/X11``.

Restarting the XOrg server
==========================
These files are parsed by the X server at start-up, so any time we modify them, the server must be restarted for the changes to take effect. Nowadays, the shortcut for **restarting X** is:

    ``Alt + PrtScn/SysReq + K``

But there are option to use the old shortcut.

    ``Ctrl + Alt + Backspace``

Example
=======
This is what we should put in the configuration file (later we'll see what files)::

    Section "InputClass"
        Identifier "keyboard defaults"
        MatchIsKeyboard "on"

        Option "XkbModel" "pc104"
        Option "XkbLayout" "us"
        Option "XKbOptions" ""
    EndSection

Note that in ``XKbOptions`` we have remap the **Caps Lock** key to **Escape**.

We have to restart the server for the changes to take effect, so hit ``Alt + PrtScn/SysReq + K`` to **stop** the XOrg server, and start it again.

Options
=======
The `XKB Configuration Guide`_ is a good play to start reading about configuring a keyboard for the X Window System. According to this guide, there are five options we can configure:

* XkbRules - files of rules to be used for keyboard mapping composition
* XkbModel - name of the model of your keyboard type
* XkbLayout - layout(s) you intend to use
* XkbVariant - variant(s) of the layout you intend to use
* XkbOptions - extra xkb configuration options

But what values can we assign? We can use **rules** files. In Debian, these files can be found under the directory::

    /usr/share/X11/xkb/rules

There are several set of rules, named:

* base
* evdev
* xfree86
* xfree98
* xorg

Each set of rules is made up of three files:

* The one without extension (``evdev`` or ``base`` for example) is where we define the rule.
* The files with ``.xml`` and ``.lst`` extensions contain just **descriptions of the rules** contained in each file.

Listing the rules
=================
To see a full list of keyboard models, layouts, variants and options, along with a short description, open /usr/share/X11/xkb/rules/base.lst. Alternatively, you may use one of the following commands to see a list without a description::

    $ localectl list-x11-keymap-models
    $ localectl list-x11-keymap-layouts
    $ localectl list-x11-keymap-variants [layout]
    $ localectl list-x11-keymap-options



.. _`IBM PC keyboard`: https://en.wikipedia.org/wiki/IBM_PC_keyboard
.. _`XKB Configuration Guide`: https://www.x.org/releases/X11R7.5/doc/input/XKB-Config.html
.. _`IBM Model M keyboard`: https://en.wikipedia.org/wiki/Model_M_keyboard
.. _`Xlib`: https://en.wikipedia.org/wiki/Xlib
.. _`libx11-6`: https://packages.debian.org/buster/libx11-6
.. _`XKB wiki`: https://www.x.org/wiki/XKB/
.. _`x11-xserver-utils`: https://packages.debian.org/buster/x11-xserver-utils
.. _`xmodmap`: https://manpages.debian.org/buster/x11-xserver-utils/xmodmap.1.en.html
.. _`xev`: https://manpages.debian.org/buster/x11-xserver-utils/xev.1.en.html
.. _`setxkbmap`: https://manpages.debian.org/buster/x11-xkb-utils/setxkbmap.1.en.html
.. _`xkbcomp`: https://manpages.debian.org/buster/x11-xkb-utils/xkbcomp.1.en.html
.. _`xkeyboard-config`: https://manpages.debian.org/buster/xkb-data/xkeyboard-config.7.en.html
.. _`XOrg configuration files`: https://manpages.debian.org/buster/xserver-xorg-core/xorg.conf.5.en.html

