***************
Basic remapping
***************
Let's say we want to remap the **Caps Lock** key to **Control**. Easily done with::

    $ setxkbmap -option caps:ctrl_modifier

Great, but we've lost the functionality of locking caps. We can get it back, if we set::

    $ setxkbmap -option shift:both_capslock

Thanks to the command above, we can **toggle CapsLock** by pressing both **Shift keys** simultaneously.

Permanent setting
=================
What we just did is great for testing, but it won't survive a reboot. To fix the rule in the ``/etc/default/keyboard`` file::

    $ sudo localectl set-x11-keymap us pc105 '' 'caps:ctrl_modifier,shift:both_capslock'

The ``set-x11-keymap`` subcommand, allows us to set the system keyboard mapping for **X11** and the **virtual console**. This subcommand takes:

* At least one **keyboard mapping name**, such as ``us`` for American English, or ``de`` for German. We can add a second one, so we can toggle between them.
* A **model** such as ``pc105``.
* A **variant** such as ``dvorak``. In our example, we had to pass an empty string ``""``.
* Finally the **options** to remap keys. Note that the options are passed as a **single string**, separated by a comma.

.. note:: If we only wanted to apply these settings to **X11** and not the **console**, we could have used the ``--no-convert`` option.

We can check the **keyboard** file, to see that the options are there::

    $ cat /etc/default/keyboard
    XKBMODEL=pc105
    XKBLAYOUT=us
    XKBOPTIONS=caps:ctrl_modifier,shift:both_capslock
    BACKSPACE=guess

A good idea is to **restart** the **XOrg** server, login out or rebooting didn't work for me.

Listing up the options with ``localectl``
=========================================
We can list all the available options using ``localectl``::

    $ sudo localectl list-x11-keymap-options

Some interesting options:

* ``terminate:ctrl_alt_bksp`` is the **old shortcut** for restarting the XOrg server. (In case you don't like the new one, ``Alt + PrtScn/SysReq + K``)
* ``grp:alt_space_toggle`` will switch among the mappings for ``us`` and ``ru``, for example.

GNOME overwrites my configuration
=================================
Usually Desktop managers overwrite our configurations. We have two options here:

1. Find some way of disable that behaviour.
2. Not to fight it, and set up our remappings using the GNOME GUI.


Remapping using GNOME
---------------------
In **GNOME 3.30.2**, we can remap the **Caps Lock** key if we open the **Tweaks** panel, and under **Keyboard & Mouse**:

1. Caps Lock behaviour : Caps Lock is also a Ctrl.
2. Miscellaneous compatibility options: Both Shift together enable Caps Lock.

For changing the input sources, under **Settings**, the **Keyboard** section under the **Typing** category we can set the *Switch to next input source* to whatever shortcut we want. It will also warn us of conflicting shortcuts.



.. _`xkb-data`: https://packages.debian.org/buster/xkb-data

