***********
Experiments
***********
First approach: Not working
===========================
Let's say we have to remap a key. First of all, we'd need to find out what's that key's keycode. We can do that dumping our keymap configuration into a file::

    $ xkbcomp $DISPLAY ~/keymap.dump

Then we can display the contents of the file and look for the keycodes we are interesting in. In my case I was trying to remap **Scroll Lock** key, so inside the ``xkb_keycodes`` section, I found the line::

    default xkb_keycodes "evdev" {
    ...
    <SCLK> = 78;
    ...

It seems **78** is the keycode produced when the **Scroll Lock** key is pressed, and I wanted to change it to the keycode of any of the Windows/Super keys::

    ...
    <LWIN> = 133;
    <RWIN> = 134;
    ...

Save the current keymap to a file
---------------------------------
Then we would print just the **components** of the current keymap with::

    $ setxkbmap -print > ~/components.dump

Let's display the components of the original keymap::

    $ cat ~/original.xkb
    xkb_keymap {
        xkb_keycodes  { include "evdev+aliases(qwerty)" };
        xkb_types     { include "complete"      };
        xkb_compat    { include "complete"      };
        xkb_symbols   { include "pc+us+inet(evdev)"     };
        xkb_geometry  { include "pc(pc105)"     };
    };

Note the ``.xkb`` extension.

Edit the keymap
---------------
Now we have to edit this file and add the modification. In this case we want the **Scroll lock** key to generate the keycode of the **Super_R**:: 

    xkb_keymap {
        xkb_keycodes  { 
            include "evdev+aliases(qwerty)"	
            // Remap Scroll-lock to the keycode of Super_R(134)
            <SCLK> = 134;
        };
        xkb_types     { include "complete"	};
        xkb_compat    { include "complete"	};
        xkb_symbols   { include "pc+us+inet(evdev)"	};
        xkb_geometry  { include "pc(pc105)"	};
    };

We can save the files as ``~/.xkb/keymap/scroll_lock_remapped.xkb``.

Compiling the keymap
--------------------
Now we have to use this modified keymap as the input keymap for the ``xkbcomp`` application, that will compile our keymap and output an ``.xmp`` file. The command::

    $ xkbcomp -I$HOME/.xkb ~/.xkb/keycode/scroll_lock_remapped.xkm $DISPLAY

Note that there's no space between the ``-I`` option and the base directory where we store the ``keymap`` folder.


.. _`man X`: https://manpages.debian.org/buster/xorg-docs-core/X.7.en.html
