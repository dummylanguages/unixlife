*********
Remapping
*********
Let's say we want to remap the **Scroll Lock** key so that when we press it, it produces the **Super R** keysymbol. We have to take three steps:

1. Create a file in the symbols directory.
2. Add the rule definition and descriptions.
3. Enable the rule.

Create the mapping
==================
Created a new file named ``sclkfile`` in the ``/usr/share/X11/xkb/symbols/`` directory::

    // Make the Scroll Lock key a right Super.
    xkb_symbols "sclk_super" {
        replace key <SCLK> { [ Super_R ] };
        modifier_map  Mod4 { <SCLK> };
    };

Adding the rule definition
==========================
Added the new **rule** in ``/usr/share/X11/xkb/rules/evdev``::

    sclkoption:sclk_super = +sclkfile(sclk_super)

Adding the rule descriptions
============================
And **description** of the rule in ``/usr/share/X11/xkb/rules/evdev.lst``::

    ! option
    sclkoption           Scroll Lock behaviour
    sclkoption:sclk_super Scroll Lock is Super

And **description** of the rule in ``/usr/share/X11/xkb/rules/evdev.xml``::
    
    <configItem>
        <name>sclkoption</name>
        <description>Scroll Lock behaviour</description>
    </configItem>    
    <option>
        <configItem>
            <name>sclkoption:sclk_super</name>
            <description>Scroll Lock is Super</description>
        </configItem>
    </option>

Backup your modifications
=========================
Make backups of all of these evdev files, (or create a patch file). Your changes will be overwritten whenever the `xkb-data`_ package is updated. We'll also keep a backup of the ``sclk`` file.

.. note:: The **xkb-data** package contains configuration data used by the X Keyboard Extension (XKB), which allows selection of keyboard layouts when using a graphical interface. Every X11 vendor provides its own XKB data files, so keyboard layout designers have to send their layouts to several places. The xkeyboard-config project has been launched at FreeDesktop in order to provide a central repository that could be used by all vendors.

I think that also means that if we mess up these files badly, we can always **reinstall** the package::

    $ sudo apt-get install --reinstall xkb-data

Enable the rule on the fly
==========================
To start using this remapping inmediately::

    $ setxkbmap -option sclkoption:sclk_super

No errors, and when I print the components of my layout::

    xkb_keymap {
        xkb_keycodes  { include "evdev+aliases(qwerty)"	};
        xkb_types     { include "complete"	};
        xkb_compat    { include "complete"	};
        xkb_symbols   { include "pc+us+inet(evdev)+sclkfile(sclk_super)"	};
        xkb_geometry  { include "pc(pc105)"	};
    };

The **Scroll Lock** key works like a **Super R**. When I run ``xev``::

    keycode 78 (keysym 0xffeb, Super_R)

The **keycode** is still **78** but the **keysym** is **Super_R**. Great.

Set it up permanently
=====================
To fix the rule in the ``/etc/default/keyboard`` file::

    $ sudo localectl --no-convert set-x11-keymap us pc104 "" sclkoption:sclk_super

A good idea is to **restart** the **XOrg** server, login out or rebooting didn't work for me.

Listing up the options with ``localectl``
=========================================
For some reason, the new ``sclkoption:sclk_super`` wasn't listed among the list of symbols by ``localectl``::

    $ sudo localectl list-x11-keymap-options


Based on these answers:
https://askubuntu.com/questions/968806/remap-caps-lock-to-ctrl-b/969232#969232
https://web.archive.org/web/20170825051821/http://madduck.net/docs/extending-xkb/

Gotta check this one too:
https://askubuntu.com/questions/876005/what-file-is-the-setxkbmap-option-rules-meant-to-take-and-how-can-i-add-keyboa
https://askubuntu.com/questions/896822/custom-keyboard-layout-behaves-like-last-selected


.. _`xkb-data`: https://packages.debian.org/buster/xkb-data

