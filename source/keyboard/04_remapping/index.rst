#########
Remapping
#########

.. toctree::
   :maxdepth: 4
   :hidden:

   basic_remapping
   custom_remapping
   permanent_changes
   zz_experiments


.. _`keyboard layouts`: https://en.wikipedia.org/wiki/Keyboard_layout
.. _`scancode`: https://en.wikipedia.org/wiki/Scancode
.. _`Kcancode`: https://www.win.tue.nl/~aeb/linux/kbd/scancodes.html