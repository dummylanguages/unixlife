*************************
The console-setup package
*************************
The `console-setup package`_ package provides the console with the same keyboard configuration scheme as the **X Window System**. It includes:

    * The `setupcon`_ command, which sets up the keyboard **only** in the **console**.
    * The `keyboard file`_.
    * The `console-setup file`_ can also be used to host keyboard configuration, but it's not advisable to be used that way. Its intended use is for **font configuration**. Read more about this topic here.

.. note:: The package also installs console fonts supporting many of the world's languages. It provides an unified set of font faces - the classic VGA, the simplistic Fixed, and the cleaned Terminus, TerminusBold and TerminusBoldVGA.

Thanks to this package, we just have to worry about the keyboard configuration for **X11**, and then run a simple command to extend the same configuration to the **console**::

    $ setupcon

The ``setupcon`` command
========================
This command is used to extend the configuration of the keyboard in X Window System to the **Linux console**, but not the other way around. Apart from that, it can also be used for setting up:

* The console **font**.
* The console **keyboard**.

Here we'll focus on the keyboard.

Several keyboard files
----------------------
It's possible to add several keyboard files that can be switched using the setupcon command. For example, in our user's home directory we could keep:

* A file named ``~/.keyboard`` with the **default** configuration for an English keyboard layout.
* And an additional configuration in the file ``~/.keyboard.russian``.

Then, if we want to change to the Russian keyboard layout files we just have to run::

    $ setupcon russian

And to go back to the default configuration we would run the command without any arguments::

    $ setupcon

Check the manual pages of `setupcon`_ for additional options.

The keyboard file
=================
The **keyboard file** describes the properties of the keyboard. These properties are based on `X keyboard extension`_. The following variables can be set:

* ``XKBMODEL`` specifies the keyboard **model name** (default: pc105 on most platforms).
* ``XKBLAYOUT`` specifies the keyboard's layout. (default: us on most platforms).
* ``XKBVARIANT`` specifies the variant components, such as ``dvorak``. (default: not set).
* ``XKBOPTIONS`` specifies the XKB keyboard **option** components, super interesting for remapping some keys. (default: not set).
* ``BACKSPACE`` determines the behavior of <BackSpace> and <Delete> keys on the console. Allowed values: bs, del and guess. In most cases you can specify ``guess`` here, in which case the current terminal settings and the kernel of your operating system will be used to determine the correct value. 
* ``KMAP`` usually this variable will be unset but if you don't want to use a XKB layout on the console, you can specify an alternative keymap here. The keymap must be suitable as input for `loadkeys`_ on Linux or for `kbdcontrol`_ on FreeBSD.

Location
--------
The location of the the **system wide configuration** is:

* ``/etc/default/keyboard``

But users can keep a default **per-user configuration** in their home directrory:

* ``~/.keyboard``

In **Debian** systems this default keyboard layout is shared between the X Window System and the console.

Configuring tools
-----------------
Although it is possible to manually edit this file and set up the aforementioned variables, it's advisable to use some configuration tool. Any of the tools that can be used to set up the keyboard in X11 can be use; the whole point of the package is to extend the X11 keyboard configuration to the console automatically.

Some tool examples:

* Linux systems that use systemd, can use `localectl`_, which was discussed in the previous section.
* All Desktop environments offer some graphic tool to deal with basic keyboard configuration. In Debian and derivatives there's a command line tool named `dpkg-reconfigure`_, which offers a Ncurses interface.

Basic configuration: dpkg-reconfigure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Using `dpkg-reconfigure`_ we can achieve a basic keyboard configuration, we just have to run::

    $ sudo dpkg-reconfigure keyboard-configuration

That would start an ncurses graphic that will allow us to choose options to configure our keyboard. In my case, this was the ``keyboard`` file::

    # KEYBOARD CONFIGURATION FILE

    # Consult the keyboard(5) manual page.

    XKBMODEL="pc104"
    XKBLAYOUT="us"
    XKBVARIANT=""
    XKBOPTIONS="terminate:ctrl_alt_bksp"

    BACKSPACE="guess"

Tweaking the configuration manually
-----------------------------------
We can use the configuration that results from a graphical tool as a starting point for further tweaking. For example, we could use the file above and add a couple layouts for **Russian** and **Spanish**, and a shortcut to switch between them::

    $ cat /etc/default/keyboard
    # KEYBOARD CONFIGURATION FILE
    # Consult the keyboard(5) manual page.

    XKBMODEL="pc105"
    XKBLAYOUT="us,es,ru"
    XKBVARIANT=""
    XKBOPTIONS="terminate:ctrl_alt_bksp,grp:alt_shift_toggle"

    BACKSPACE="guess"

After applying the settings, we should be able of toggling between languages using ``Alt + Shift``.

.. note:: We can achieve something similar for the **console** using separate ``keyboard`` files in conjunction with the ``setupcon`` command.

Applying settings
-----------------
To apply new settings, we have to either **reboot** the system, or just restart the **kernel input system** via udev::

    $ sudo udevadm trigger --subsystem-match=input --action=change 

.. warning:: In order to activate the changes on the **console**, we just have to run ``setupcon``, to make it pick up the default configuration.


.. _`Linux console`: https://en.wikipedia.org/wiki/Linux_console
.. _`console-setup package`: https://packages.debian.org/buster/console-setup
.. _`console-setup file`: https://manpages.debian.org/buster/console-setup/console-setup.5.en.html
.. _`keyboard file`: https://manpages.debian.org/buster/keyboard-configuration/keyboard.5.en.html
.. _`X keyboard extension`: https://en.wikipedia.org/wiki/X_keyboard_extension
.. _`loadkeys`: https://manpages.debian.org/buster/kbd/loadkeys.1.en.html
.. _`kbdcontrol`: https://www.freebsd.org/cgi/man.cgi?kbdcontrol(1)
.. _`setupcon`: https://manpages.debian.org/buster/console-setup/setupcon.1.en.html
.. _`dpkg-reconfigure`: https://manpages.debian.org/buster/debconf/dpkg-reconfigure.8.en.html
.. _`localectl`: https://www.freedesktop.org/software/systemd/man/localectl.html#

