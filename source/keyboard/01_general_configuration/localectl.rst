*********
localectl
*********
One of the services included in `systemd`_ is called `systemd-localed.service`_, which is used to control:

1. The system **locale settings**.
2. And **keyboard settings**, for both:
    
    * The **Linux console**.
    * And **X Windows system**.

This service is automatically activated on request and terminates itself when it is unused. `localectl`_ is a command line client to control this service, and we can use it for querying and changing both:

1. The **system locale**.
2. And the **keyboard layout** settings, for both the **Linux console** and **X11**.

Here we are going to focus on its **keyboard-related** capabilities, but you can check our :ref:`section about locale <locale-label>`, where we explain how to use ``localectl`` in that regard.

Different target files
======================
Depending on your Linux distro, ``localectl`` will store the settings in a couple of different files:

* In **Debian** and other distros (derivatives and not), it modifies the ``/etc/default/keyboard`` file.
* In **Arch**, the changes are made to the ``00-keyboard.conf`` file, which can be found under the ``/etc/X11/xorg.conf.d/`` folder. (The file may not exist if you haven't run ``localectl`` yet.)

In both cases, ``localectl`` allows us to modify the contents of any of these files, without having to manually edit them. If you want to learn more about these files check:

* The Debian's man page about the `keyboard file`_.
* The Arch Wiki about `Linux console/keyboard configuration`_.

Next we'll explain how ``localectl`` works, regardless of the file it modifies. In any case, this method creates system-wide configuration which is persistent across reboots.

.. warning:: In **Debian** there's a folder in ``/usr/share/X11/xorg.conf.d/``, where you can place `Xorg configuration files`_. But **manually** adding/editing your **keyboard configuration file** there, it's useless. The location of the file that ``localectl`` generates and updates is the one we mentioned before. That file (``/etc/default/keyboard``) you can manually edit it if you want. Just be aware that running ``localectl`` again would overwrite your manually added settings.

Querying: available options
===========================
When configuring the keyboard we may want to know what are the available options. According to the `XKB Configuration Guide`_, there are five parameters:

* XkbRules - files of rules to be used for keyboard mapping composition
* XkbModel - name of the model of your keyboard type
* XkbLayout - layout(s) you intend to use
* XkbVariant - variant(s) of the layout you intend to use
* XkbOptions - extra xkb configuration options

Using ``localectl`` we can get the whole list of valid values for setting up these parameters.

Keyboard models
----------------
We can get the list of valid values to set the ``XkbModel`` parameter running (as root)::

    $ sudo localectl list-x11-keymap-models

The default value for this parameter is ``pc104``, which corresponds `IBM PC keyboard`_, with the American layout with 104 keys, which out of curiosity looks like this:

.. figure:: /_images/pc104.png
   :scale: 35%
   :align: center

.. note:: In case you are a keyboard aficionado, this layout was mostly introduced with the `IBM Model M keyboard`_, with the later addtions of the Windows, and Fn keys. Nowadays, the Fn key is usually labeled with the Windows logo.

Keyboard layouts
----------------
We can get the list of valid values to set the ``XkbLayout`` parameter running(as root)::

    $ sudo localectl list-x11-keymap-layouts

We would choose the ``us`` value for and English (US) layout or ``es`` for Spanish.

Keyboard variants
-----------------
We can get the list of valid values to set the ``XkbVariant`` parameter running (as root)::

    $ sudo localectl list-x11-keymap-variants

Typical values here may be ``colemak``, ``dvorak`` or ``dvorak-intl``.

Keyboard options
----------------
Here things start to get interesting. To get the list of valid values to set the ``XkbOptions`` parameter run (as root)::

    $ sudo localectl list-x11-keymap-options

The output will show us values such as:

* ``ctrl:nocaps`` for mapping **Caps Lock** as **Ctrl**
* Or ``terminate:ctrl_alt_bksp Ctrl+Alt+Backspace`` wich is the old shortcut to **kill the X server**.

The interesting part about this, it's that we can create our own options that will show up in the output of this command.

Custom options
^^^^^^^^^^^^^^
If you create your options, make sure you add the descriptions in the right places. For example, imagine that:

1. I created a file in the ``/usr/share/X11/xkb/symbols`` folder
2. Added the **rule** (aka options) in ``/usr/share/X11/xkb/rules/evdev``
3. You have to add the **descriptions** in the ``evdev.{lst,xml}`` AND in the ``base.{lst,xml}``. Why? I don't know since when we print our XKB components::

    $ setxkbmap -print
    xkb_keymap {
        xkb_keycodes  { include "evdev+aliases(qwerty)" };
    ...

Apparently only the rules defined in ``evdev`` are included! Anyways, apparently ``localectl`` read the rule descriptions from ``base``, even though XKB does it from ``evdev``. Does it also read the rules themselves from ``base``? Gonna check that too ;)

Adding temporal settings
========================
If we want to try some keyboard configuration settings before committing them permanently, you could use ``localectl`` in combination with the ``setxkbmap`` command. The former examines all the available settings, and the latter enables them on the fly.

.. warning:: Be aware that these settings won't survive a reboot, they are valid only for the **session**.

Check the :ref:`section about setxkbmap` for more information.

Setting a permanent keyboard configuration
==========================================
Once we have tested out keyboard configuration settings as mentioned above, ``localectl`` can be used for setting our preferences permanently. The **general syntax** of this command is as follows:: 

    $ localectl [--no-convert] set-x11-keymap layout [model [variant [options]]]

So, for example, if we wanted to set keyboard **layouts** for three languages (American English, Spanish, and Russian), for a standard keyboard **model**, with **options** for toggling the layouts and terminating the X.Org server we would run (as root)::

    $ sudo localectl --no-convert set-x11-keymap us,es,ru pc104 "" grp:alt_shift_toggle,terminate:ctrl_alt_bksp

* It's important to pass an empty string for the fields that we don't need to set, in this case the ``variant`` field.
* Also, note how the ``options`` are separated by commas.

Checking the generated file
---------------------------
Once we have run the command, we can check the generated files, which location will vary according to our Linux distro.For the sake of the example, if we were in **Arch** we would run::

    $ cat /etc/X11/xorg.conf.d/00-keyboard.conf
    Section "InputClass"
        Identifier "filco"
        MatchIsKeyboard "on"

        Option "XkbModel" "pc104"
        Option "XkbLayout" "us"
        Option "XKbOptions" ""
    EndSection

Again, in **Debian** or other distros we'd run::

    $ cat /etc/default/keyboard

Even though is not advisable, we may edit this file manually and modify the configuration, adding or deleting settings.

Checking the active configuration
---------------------------------
At least in **Arch** we can use the following command to see the actual XKB settings::

    $ setxkbmap -print -verbose 10

The ``--no-convert`` option
===========================
The ``--no-convert`` avoids to apply the configuration to the **Linux console**, so the when it's present the setting doesn't affect the keyboard configuration for the console.

Depending on the system, the console keyboard configuration is maintained in different locations:

1. In **Arch Linux** systems, on the ``/etc/vconsole.conf`` file.
2. In **Debian**, derivatives and other Linux systems, the console configuration can be found in:

    * The ``/etc/default/keyboard`` file (Console **keyboard** configuration)
    * And the ``/etc/default/console-setup`` (Console **font** configuration)

.. warning:: Is the ``--no-convert`` option is useless and the configuration we set with ``localectl`` also affects the console? (Forgot to mention where did I experience this problem)

.. _`localectl`: https://www.freedesktop.org/software/systemd/man/localectl.html
.. _`systemd`: https://systemd.io/
.. _`systemd-localed.service`: https://www.freedesktop.org/software/systemd/man/systemd-localed.html
.. _`keyboard file`: https://manpages.debian.org/buster/keyboard-configuration/keyboard.5.en.html
.. _`Linux console/keyboard configuration`: https://wiki.archlinux.org/index.php/Linux_console/Keyboard_configuration
.. _`IBM PC keyboard`: https://en.wikipedia.org/wiki/IBM_PC_keyboard
.. _`IBM Model M keyboard`: https://en.wikipedia.org/wiki/Model_M_keyboard
.. _`XKB Configuration Guide`: https://www.x.org/releases/X11R7.5/doc/input/XKB-Config.html
.. _`Xorg configuration files`: https://manpages.debian.org/stretch/xserver-xorg-core/xorg.conf.d.5.en.html

