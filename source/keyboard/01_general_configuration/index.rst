#####################
General configuration
#####################

.. toctree::
   :maxdepth: 3
   :hidden:

   localectl
   console_setup_package

In this section we are gonna examine tools, to set up the **keyboard configuration** in both:

* The `Linux console`_.
* And the `X Window System`_.

We'll see how to do that:

1. Using the `localectl`_ command.
2. Using tools included in the `console-setup package`_, which is available for a lot of distros, such as Debian, `Ubuntu`_ and `Fedora`_.

.. note:: This package may not be available in your distro repositories. At the time of this writing, it wasn't available in **Arch**, not even in the **AUR** repos.


.. _`Linux console`: https://en.wikipedia.org/wiki/Linux_console
.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`localectl`: https://www.freedesktop.org/software/systemd/man/localectl.html
.. _`console-setup package`: https://packages.debian.org/buster/console-setup
.. _`Fedora`: http://rpms.remirepo.net/rpmphp/zoom.php?rpm=console-setup
.. _`Ubuntu`: https://packages.ubuntu.com/focal/console-setup
