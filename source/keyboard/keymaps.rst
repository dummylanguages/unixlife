*******
Keymaps
*******
Let's say we want to configure a classic 104 keys `IBM PC keyboard`_, with the American layout with 104 keys, which out of curiosity looks like this:

.. figure:: /_images/pc104.png
   :scale: 35%
   :align: center

This layout was mostly introduced with the `IBM Model M keyboard`_, with the later addtions of the Windows, and Fn keys. Nowadays, the Fn key is usually labeled with the Windows logo.

An XKB keymap is a file that combines
https://unix.stackexchange.com/questions/240566/what-is-wrong-with-these-xkb-rules-files/355404#355404



The `XKB Configuration Guide`_ is a good play to start reading about configuring a keyboard for the X Window System. According to this guide, there are five parameters are:

* XkbRules - files of rules to be used for keyboard mapping composition
* XkbModel - name of the model of your keyboard type
* XkbLayout - layout(s) you intend to use
* XkbVariant - variant(s) of the layout you intend to use
* XkbOptions - extra xkb configuration options

But what values can we assign? We can use **rules** files. In Debian, these files can be found under the directory::

    /usr/share/X11/xkb/rules

There are several rules files, all ended with the ``.lst`` extension. The proper rules file depends on your vendor, and if we open the ``xorg.lst`` file, we'll see a long list of values under four sections:

* ``model``: with values such as ``pc104`` for a Generic 104-key PC.
* ``layout``: with ``us`` for English (US) or ``es`` for Spanish.
* ``variant``: such as ``colemak``, ``dvorak`` or ``dvorak-intl``.
* ``option``: like ``ctrl:nocaps`` for mapping **Caps Lock** as **Ctrl** or ``terminate:ctrl_alt_bksp Ctrl+Alt+Backspace`` wich is the key sequence to kill the X server.


To see a full list of keyboard models, layouts, variants and options, along with a short description, open /usr/share/X11/xkb/rules/base.lst. Alternatively, you may use one of the following commands to see a list without a description::

    $ localectl list-x11-keymap-models
    $ localectl list-x11-keymap-layouts
    $ localectl list-x11-keymap-variants [layout]
    $ localectl list-x11-keymap-options




.. _`IBM PC keyboard`: https://en.wikipedia.org/wiki/IBM_PC_keyboard
.. _`XKB Configuration Guide`: https://www.x.org/releases/X11R7.5/doc/input/XKB-Config.html
.. _`IBM Model M keyboard`: https://en.wikipedia.org/wiki/Model_M_keyboard
.. _`Xlib`: https://en.wikipedia.org/wiki/Xlib
.. _`libx11-6`: https://packages.debian.org/buster/libx11-6
.. _`XKB wiki`: https://www.x.org/wiki/XKB/
.. _`x11-xserver-utils`: https://packages.debian.org/buster/x11-xserver-utils
.. _`xmodmap`: https://manpages.debian.org/buster/x11-xserver-utils/xmodmap.1.en.html
.. _`xev`: https://manpages.debian.org/buster/x11-xserver-utils/xev.1.en.html
.. _`setxkbmap`: https://manpages.debian.org/buster/x11-xkb-utils/setxkbmap.1.en.html
.. _`xkbcomp`: https://manpages.debian.org/buster/x11-xkb-utils/xkbcomp.1.en.html
.. _`xkeyboard-config`: https://manpages.debian.org/buster/xkb-data/xkeyboard-config.7.en.html
.. _`configuration files`: https://manpages.debian.org/buster/xserver-xorg-core/xorg.conf.5.en.html

