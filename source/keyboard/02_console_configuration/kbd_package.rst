*******************
The ``kbd`` package
*******************
The `kbd project`_ aka the **Linux keyboard tools**, contains tools for managing Linux console, mainly loading console fonts and keyboard maps. This distribution contains no binaries because the sources depend on the kernel version, but almost every Linux distro includes a compiled and packaged version of the tools.

In Debian we can find them in the `kbd`_ package. Some of the utilities included in this package are:

* The  `keymaps`_ command, keyboard table descriptions for loadkeys and dumpkeys.
* The  `loadkeys`_ command, load keyboard translation tables.
* The  `dumpkeys`_ command, dump keyboard translation tables.
* The  `showkey`_ command, examine the codes sent by the keyboard.
* The  `kbd_mode`_ command, report or set the keyboard mode.

showkey
=======
This utility dump the **scancodes** or **keycodes** of keys. We can run it with the options:

* ``-k`` which is short for ``--keycodes``. It starts showkey in **keycode** dump mode. This is the default, when no command line options are present.
* ``-s`` which is short for ``--scancodes``. Starts showkey in scan code dump mode.

Once the utility starts running, the program will keep dumping either keycodes or scancodes of the keys we press. It stops 10 seconds after the last keypress. 

For example::

   $ sudo showkey
    keycode  70 press
    keycode  70 release
    ...

Note that we get a **70** keycode, and also that ``showkey`` must be run with root privileges.

By the way, as they warn at the bottom of `this page`_, the keycodes reported by ``showkey`` are not the same that the ones we get when using ``xev``. The same key(Scroll Lock) will produce the keycode **78** in ``xev``.


.. _`kbd project`: http://kbd-project.org
.. _`kbd`: http://kbd-project.org
.. _`keymaps`: https://manpages.debian.org/buster/kbd/keymaps.5.en.html
.. _`loadkeys`: https://manpages.debian.org/buster/kbd/loadkeys.1.en.html
.. _`dumpkeys`: https://manpages.debian.org/buster/kbd/dumpkeys.1.en.html
.. _`showkey`: https://manpages.debian.org/buster/kbd/showkey.1.en.html
.. _`kbd_mode`: https://manpages.debian.org/buster/kbd/kbd_mode.1.en.html
.. _`this page`: https://www.x.org/wiki/XKBLayoutCreationNotes/