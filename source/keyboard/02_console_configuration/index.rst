#################################
The keyboard in the Linux console
#################################

.. toctree::
   :maxdepth: 3
   :hidden:

   kbd_package

The `Linux console`_ is the system console which outputs all kernel messages and warnings when the system is booting and which allows us to login in single user mode. Just to be clear, the Linux console is implemented inside the kernel and does not depend on the X Window System. That means that most of the utilities available in **X11** for managing they keyboard are not useful when dealing with its behaviour in the **console**.

There are two packages that deal with the **keyboard** configuration of the **Linux console**:

* The `console-setup package`_, which extends to the **console** the keyboard settings that exists for **X11**.
* The `kbd`_ package, whose settings are specific for the **Linux console**.

We already reviewed the console-setup package in the section before, so we'll focus on the **kbd package** here.

.. _`Linux console`: https://en.wikipedia.org/wiki/Linux_console
.. _`console-setup package`: ttps://packages.debian.org/buster/console-setup
.. _`kbd`: http://kbd-project.org
