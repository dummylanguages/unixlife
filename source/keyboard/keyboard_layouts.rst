################
Keyboard Layouts
################
Generally speaking, when talking about `keyboard layouts`_ we have to differentiate between:

* **Mechanical layout** or physical layout is the placements and keys of a keyboard. Here we have to make a distinction between the form factor of a keyboard (full size, tenkeyless, etc), and its version (ANSI for America, ISO for Europe, and JIS for Japan). For example, a **full size ANSI** keyboard may have between 104 keys, whereas a **Tenkeyless** has 87 keys.

* **Visual layout** is the arrangement of the legends (labels, markings, engravings) that appear on the keys of a keyboard.

* **Functional layout** is the software mapping of the mechanical keys to the symbols they produce.

Modern computer keyboards are designed to send a `scancode`_ to the operating system when a key is pressed, rather than send it the specific character engraved on that key. Scancodes tables depend on the manufacturer, and there are `websites which contain tables`_ for lots of them.

.. note:: Scancodes don't change unless you have a programmable keyboard.

The OS converts the scancode into a specific binary character code using a conversion table, called the keyboard mapping table, or **keymap** for short. Users can change keyboard mapping in system settings to modify or extend keyboard functionality. Thus the symbol shown on the physical key-top need not be the same as what goes into a document being typed.

In the X Window System, every individual key has an associated keycode. Keycode tables contain mappings of scancodes to keycodes. A keycode may be mapped to more than one **keysym**. Why more than one? If we press the key with the printed ``A``, it can produce at least two keysyms:

* ``a``
* ``A`` if we are also pressing the Shift modifier.

The idea is that these three concepts work together. For example, when we press the key labeled as ``Q``, the keyboard generates:

1. The **scancode** of ``24``.
2. Which is mapped to the **keycode** ``AD01``.
3. Which is map to the **keysym** ``q``.



.. _`keyboard layouts`: https://en.wikipedia.org/wiki/Keyboard_layout
.. _`scancode`: https://en.wikipedia.org/wiki/Scancode
.. _`websites which contain tables`: https://www.win.tue.nl/~aeb/linux/kbd/scancodes.html