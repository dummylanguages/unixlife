########
Keyboard
########


.. toctree::
   :maxdepth: 4
   :hidden:

   01_general_configuration/index
   02_console_configuration/index
   03_X11_configuration/index
   04_remapping/index
   keymaps
   keyboard_layouts
   fix_my_kb_bro

In this section we'll talk about keyboards in Linux, mostly about how to configure them. The configuration can be done:

* Just for the **Linux console**.
* Just for the **X Window System**.

Although we'll see how it's possible to apply a keyboard configuration for the console that is also applied to X, and the other way around. That allows us to configure the keyboard at a **general level**, for both the Linux console and the X Window System.

We'll also see how to make **temporal configuration changes** for just the **session**, as well as **permanent modifications** that will survive a reboot.
