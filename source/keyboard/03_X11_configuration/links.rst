*****************
Some useful links
*****************
When I started this section I couldn't imagine how complex was the `X keyboard extension`_ topic. At the same time the information in the web is disperse, and some useful resources can't be reached or are in the time machine. Here I'll try to keep links with useful information about it.

* The `XKB Configuration Guide`_ contains information about how to make our keyboard configuration permanent with the help of `xorg.configuration files`_.
* The `Madduck guide on extending XKB`_, archived at Wayback Machine.
* The `Unreliable guide to XKB`_ by Doug Palmer.
* The `Ivan Pascal XKB docs`_ in English are nowhere to be found, although I found something `in Russian`_. :-(
* The `XKB wiki`_ at the X.Org foundation contain a few **useful links** with information about XKB.
* Also, at Freedesktop.org it's hosted `XKB Configuration Database`_, which contains frequently released open source of X keyboard configuration data for X Window System implementations.
* `How to further enhance XKB configuration`_



.. _`X keyboard extension`: https://en.wikipedia.org/wiki/X_keyboard_extension
.. _`XKB Configuration Guide`: https://www.x.org/releases/current/doc/xorg-docs/input/XKB-Config.html
.. _`Madduck guide on extending XKB`: https://web.archive.org/web/20170825051821/http://madduck.net:80/docs/extending-xkb/
.. _`xorg.configuration files`: https://manpages.debian.org/stretch/xserver-xorg-core/xorg.conf.d.5.en.html
.. _`Unreliable guide to XKB`: https://www.charvolant.org/doug/xkb/html/xkb.html
.. _`Ivan Pascal XKB docs`: pascal.tsu.ru/en/xkb/
.. _`in Russian`: http://rus-linux.net/MyLDP/x/xkb/xkb.html
.. _`XKB wiki`: https://www.x.org/wiki/XKB/
.. _`XKB Configuration Database`: https://www.freedesktop.org/wiki/Software/XKeyboardConfig/
.. _`How to further enhance XKB configuration`: https://www.xfree86.org/current/XKB-Enhancing.html

