**********************
XKB configuration data
**********************
The `X Keyboard Extension (XKB) configuration data`_ contains mostly data but also an utility named `xkeyboard-config`_ which is pretty useful to list all the possible values for our keyboard configuration. We run it just like::

    $ xkeyboard-config

If you get a ``command not found`` warning, you can install it::

    $ sudo apt install xkb-data

Later we can feed these options to the ``setxkbmap`` command, or check out that a custom option added by us appear in the list. It basically list:

* MODELS
* LAYOUTS/VARIANTS
* OPTIONS

These values are pulled from the files under ``/usr/share/X11/xkb``.

.. _`X Keyboard Extension (XKB) configuration data`: https://packages.debian.org/buster/xkb-data
.. _`xkeyboard-config`: https://manpages.debian.org/buster/xkb-data/xkeyboard-config.7.en.html
