*********************
XOrg server utilities
*********************
The **X server utilities** (in Debian packaged as `x11-xserver-utils`_) provides a miscellaneous assortment of X Server utilities that ship with the X Window System, including one useful for the keyboard:

* `xmodmap`_, a utility for modifying keymaps and pointer button mappings in X.

This utility was used quite often but now for some reason its use is deprecated.

xmodmap
=======
`xmodmap`_, is a utility for modifying keymaps and pointer button mappings in X.

.. note:: ``xmodmap`` is part of the **X11 utilities** (in Debian packaged as `x11-utils`_)

Feel free to run `man xmodmap`_ to see the whole list of available options.


.. _`x11-xserver-utils`: https://packages.debian.org/buster/x11-xserver-utils
.. _`man xmodmap`: https://manpages.debian.org/buster/x11-xserver-utils/xmodmap.1.en.html
.. _`xmodmap`: https://www.x.org/releases/X11R7.7/doc/man/man1/xmodmap.1.xhtml
.. _`x11-utils`: https://packages.debian.org/buster/x11-utils