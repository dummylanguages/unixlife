***********************
Other utilities for XKB
***********************

.. toctree::
   :maxdepth: 3
   :hidden:

   X_server_utilities
   X11_utilities
   xkb_config_data

We can find utilities for dealing with the `X keyboard extension`_ in packages such as:

* The **X server utilities** (in Debian packaged as `x11-xserver-utils`_) also contain some.
* The **X11 utilities**, which in Debian is packaged as `x11-xkb-utils`_.
* The `X Keyboard Extension (XKB) configuration data`_ contains mostly data but some utility.
* Even `localectl`_ can provide super useful information about **XKB**.

.. _`X keyboard extension`: https://en.wikipedia.org/wiki/X_keyboard_extension
.. _`x11-xserver-utils`: https://packages.debian.org/buster/x11-xserver-utils
.. _`x11-xkb-utils`: https://packages.debian.org/buster/x11-xkb-utils
.. _`X Keyboard Extension (XKB) configuration data`: https://packages.debian.org/buster/xkb-data
.. _`localectl`: https://www.freedesktop.org/software/systemd/man/localectl.html
