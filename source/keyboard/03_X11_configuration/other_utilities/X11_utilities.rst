*************
X11 utilities
*************
The **X11 utilities** (in Debian packaged as `x11-utils`_) contain:

* `xev`_, it prints the contents of X events, such as mouse clicks or keypresses.

xev
===
`xev`_ prints the contents of X events, such as mouse clicks or keypresses.

.. note:: ``xev`` is part of the **X11 utilities** (in Debian packaged as `x11-utils`_)

By the way, the keycodes that we would get using ``xev`` are different from the ones we'd get using ``showkey``. For example, with ``xev`` the **Scroll Lock** key produces they keycode number **78**, but if we run::

    $ sudo showkey
    keycode  70 press
    keycode  70 release

Note that we get a **70** keycode, and also that ``showkey`` must be run with root privileges.

The ``-k`` which is short for ``--keycodes`` starts showkey in **keycode** dump mode. This is the default, when no command line options are present.


.. _`X11 XKB utilities`: https://packages.debian.org/buster/x11-xkb-utils
.. _`x11-xserver-utils`: https://packages.debian.org/buster/x11-xserver-utils
.. _`x11-utils`: https://packages.debian.org/buster/x11-utils
.. _`man xmodmap`: https://manpages.debian.org/buster/x11-xserver-utils/xmodmap.1.en.html
.. _`xmodmap`: https://www.x.org/releases/X11R7.7/doc/man/man1/xmodmap.1.xhtml
.. _`xev`: https://manpages.debian.org/buster/x11-xserver-utils/xev.1.en.html