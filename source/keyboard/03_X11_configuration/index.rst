###################
The keyboard in X11
###################

.. toctree::
   :maxdepth: 3
   :hidden:

   X11 XKB utilities <X11_XKB_utilities/index>
   other_utilities/index
   links

The `X keyboard extension`_ aka **XKB**, is an extension of the X Window System that extends the ability to control the keyboard over what is offered by the X Window System core protocol. This extension is included in modern versions of the **X11 client-side library**, also known as `Xlib`_ or **libX11**, and is active by default.

.. note:: In Debian, the **X11 client-side library** library is packaged as `libx11-6`_

There are several packages that contain keyboard related utilities to control the **XKB extension**:

* The most important ones are in **X11 XKB utilities**, which in Debian is packaged as `x11-xkb-utils`_.

But there are other ones in:

* The **X server utilities** (in Debian packaged as `x11-xserver-utils`_) also contain some.
* The **X11 utilities**, which in Debian is packaged as `x11-utils`_ (``xev``).
* The `X Keyboard Extension (XKB) configuration data`_ contains mostly data but some utility.
* Even `localectl`_ can provide super useful information about **XKB**.


.. _`X keyboard extension`: https://en.wikipedia.org/wiki/X_keyboard_extension
.. _`Xlib`: https://en.wikipedia.org/wiki/Xlib
.. _`libx11-6`: https://packages.debian.org/buster/libx11-6
.. _`x11-xkb-utils`: https://packages.debian.org/buster/x11-xkb-utils
.. _`x11-xserver-utils`: https://packages.debian.org/stretch/x11-xserver-utils
.. _`x11-utils`: https://packages.debian.org/stretch/x11-utils
.. _`X Keyboard Extension (XKB) configuration data`: https://packages.debian.org/buster/xkb-data
.. _`localectl`: https://www.freedesktop.org/software/systemd/man/localectl.html
