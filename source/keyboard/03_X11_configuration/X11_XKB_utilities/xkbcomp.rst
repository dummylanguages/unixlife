xkbcomp
=======
One of the utilities included in `Xlib`_ is `xkbcomp`_

Using xkbcomp
-------------
The command::

    $ xkbcomp -xkb $DISPLAY

Creates the ``server-0_0.xkb`` file which is a source description of the keyboard as output. If you're curious, you can check the value of the ``$DISPLAY`` variable::

    $ echo $DISPLAY
    :0.0

Once we have a dump of the whole keyboard layout we can modify it. To compile it and load it into the XOrg server::

    $ xkbcomp server-0_0.xkb $DISPLAY

From components
---------------
I found easier to generate an ``.xkb`` file with just the components of the layout (easier to edit)::

    $ setxkbmap -print > components

After we edit them, we can::

    $ xkbcomp components.xkb $DISPLAY




.. _`xkbcomp`: https://manpages.debian.org/buster/x11-xkb-utils/xkbcomp.1.en.html
.. _`Xlib`: https://en.wikipedia.org/wiki/Xlib
