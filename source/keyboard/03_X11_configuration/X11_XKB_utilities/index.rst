*****************
X11 XKB utilities
*****************

.. toctree::
   :maxdepth: 3
   :hidden:

   setxkbmap
   xkbcomp

The **X11 XKB utilities**, which in Debian is packaged as `x11-xkb-utils`_, contains a number of client-side utilities for XKB, the `X keyboard extension`_.

The three most important ones are:

* `setxkbmap`_, which is a utility to set the keyboard using the X Keyboard Extension 
* `xkbcomp`_ The  keymap compiler to create keymap files
* `xkeyboard-config`_ is a file that provides description files for the X Keyboard Extension (XKB)


.. _`x11-xkb-utils`: https://packages.debian.org/buster/x11-xkb-utils
.. _`X keyboard extension`: https://en.wikipedia.org/wiki/X_keyboard_extension
.. _`setxkbmap`: https://manpages.debian.org/buster/x11-xkb-utils/setxkbmap.1.en.html
.. _`xkbcomp`: https://manpages.debian.org/buster/x11-xkb-utils/xkbcomp.1.en.html
.. _`xkeyboard-config`: https://manpages.debian.org/buster/xkb-data/xkeyboard-config.7.en.html
