.. _section about setxkbmap:

setxkbmap
=========
One of the utilities included in `Xlib`_ is `setxkbmap`_, which is super useful for a lot of things.

.. note:: All configuration changes set with this command won't survive a reboot!

The general form of the command is:

    $ setxkbmap [ args ] [ layout [ variant [ option ... ] ] ]

Checking the active configuration
---------------------------------
To see the actual XKB settings::

    $ setxkbmap -print -verbose 10

Setting layouts
---------------
It is useful to set the keyboard layout::

    $ setxkbmap -layout us,es,ru

Setting rules
-------------
$ setxkbmap -rules evdev

Setting options
---------------
When remapping keys, it's a good idea to start testing out how the remapping feel, and when we are satisfied, set them up permanently. For example, imagine I decide to disable the **Caps Lock** key::

    $ setxkbmap -option "caps:none"
    $ setxkbmap -option "shift:both_capslock"

Now, pressing the Caps Lock key will do nothing; the second line toggle the Caps Lock when we press both Shift keys. If after experimenting for a while I decide I don't like it, I can get rid of the changes with::

    $ setxkbmap -option ""

Be warned, the command above will get rid of ALL your temporal and permanent customizations you may have (if any), not only the last one we just made.

Checking settings
-----------------
To check the active settings of our keyboard configuration in X, we would run::

    $ setxkbmap -query
    rules:      evdev
    model:      pc105
    layout:     us

``evdev`` is the kernel’s input driver

    $ setxkbmap -print




.. _`setxkbmap`: https://manpages.debian.org/buster/x11-xkb-utils/setxkbmap.1.en.html
.. _`Xlib`: https://en.wikipedia.org/wiki/Xlib
