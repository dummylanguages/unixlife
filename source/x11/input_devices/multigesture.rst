.. _multigesture-label:

**********************
Touchpad Multi-gesture
**********************
There are several packages to enjoy touchpad multi-gestures in Linux:

* `libinput-gestures`_, which works in `XOrg`_ and somewhat incompletely in `Wayland`_
* `fusuma`_

Installation
============
Before installing anything we have to make sure that our user is a member of the ``input`` group to have permission to read the touchpad device::

    $ sudo usermod -aG input bob

Debian
------
We need several packages:

* `xdotool`_, which is a package to fake input from the mouse and keyboard.
* `wmctrl`_, a command line tool to interact with an `EWMH`_/NetWM compatible X Window Manager.
* `libinput`_, which is a library to handle input devices in Wayland compositors and to provide a generic **X.Org input driver**. In Debian this library is installed with the `libinput-tools`_ package.

So let's install these packages::

    $ sudo apt-get install xdotool wmctrl libinput-tools

Arch
----
If you are running Arch linux, in the AUR you can find the `libinput-gestures package`_ and save quite a bit of time running::

    $ sudo yay libinput-gestures

Install manually
----------------
The usual steps::

    $ git clone https://github.com/bulletmark/libinput-gestures.git
    $ cd libinput-gestures
    $ sudo make install
    
We can replace the last command with ``sudo ./libinput-gestures-setup install``, to make use of the included installation script.

Uninstalling
------------
To **uninstall** the app::

    $ libinput-gestures-setup stop
    $ libinput-gestures-setup autostop
    $ sudo libinput-gestures-setup uninstall

Configuration
=============
The default gestures are in ``/etc/libinput-gestures.conf``. If you want to create your own custom gestures then copy that file to ``~/.config/libinput-gestures.conf`` and edit it.

For example, consider the following default configuration::

    gesture swipe right     xdotool key alt+Right
    gesture swipe left      xdotool key alt+Left

The first line means that if we **swipe right**, we'll generate the ``alt + Right`` shortcut, which usually is recognized by browsers to go to the last visited page. The way is defined, it will be triggered with a swipe, no matter how many fingers we use. But we could fine tune it, specifying **three fingers** for example::

    gesture swipe right     3   xdotool key alt+Right
    gesture swipe left      3   xdotool key alt+Left

.. warning:: Specifying **2 fingers** for **swiping** is not supported. Plus you gotta **log out** after changes to the configuration.

.. note:: Apparently, someone decided to patch `libinput`_ and add support for **3 finger drag**, like in macOs. Read more about it `here`_.

Commands
========
For starting/stoping the service:

* ``libinput-gestures-setup autostart``
* ``libinput-gestures-setup start``
* ``libinput-gestures-setup stop``
* ``libinput-gestures-setup status``

To print out commands that would be executed:

* ``libinput-gestures -d`` (press ``Ctrl + c`` to stop)

PROBLEM RESUMING FROM SUSPEND
=============================
Some touchpads have a problem which causes ``libinput-gestures`` to **stop** after resuming from a system **suspend**. You can use a companion program named `dbus-action`_, which listens to `D-Bus`_ and actions configured commands on specified messages.

Installing
----------
``dbus-action`` is a Python program dependent on version **3.6** or later. It also depends on:

* `PyGObject`_, can be installed with pip3 (usually preinstalled).
* `python3-dbus`_, which is available as a Debian package (usually preinstalled).
* `ruamel.yaml`_, can be installed with pip3.

Once we verify we have all the dependencies, we can install ``dbus-action``::

    $ git clone https://github.com/bulletmark/dbus-action.git
    $ cd dbus-action
    $ sudo make install

Instead of the last line, we could use the included script::

    $ sudo ./dbus-action-setup install

Configuration
-------------
The default configuration file is in ``/etc/dbus-action.conf``. We need to create your own **custom triggers and actions** so we gotta copy that file to ``~/.config/dbus-action.conf`` and edit it::

    $ cp /etc/dbus-action.conf ~/.config/dbus-action.conf

We should add the following config::

    triggers:
      - bus: system
        interface: org.freedesktop.login1.Manager
        member: PrepareForSleep
        values:
          0: "libinput-gestures-setup restart"
          1: "libinput-gestures-setup stop"

The lines above will trigger one command when our machine suspends, and another command when it resumes. Once the configuration is done, **start** the application immediately in the background using::

    $ dbus-action-setup start

To enable the app to start automatically in the background when you **log in** (on an XDG compliant DE such as GNOME and KDE) we run::

    $ dbus-action-setup autostart

REMOVAL
-------
To **uninstall** the app::

    $ dbus-action-setup stop
    $ dbus-action-setup autostop
    $ sudo dbus-action-setup uninstall

.. _`XOrg`: https://www.x.org/wiki/
.. _`Wayland`: https://wayland.freedesktop.org/
.. _`libinput-gestures`: https://github.com/bulletmark/libinput-gestures
.. _`fusuma`: https://github.com/iberianpig/fusuma
.. _`libinput`: https://www.freedesktop.org/wiki/Software/libinput/
.. _`xdotool`: https://github.com/jordansissel/xdotool
.. _`wmctrl`: http://tripie.sweb.cz/utils/wmctrl/
.. _`EWMH`: https://specifications.freedesktop.org/wm-spec/wm-spec-1.3.html
.. _`libinput-tools`: https://packages.debian.org/buster/libinput-tools
.. _`libinput-gestures package`: https://aur.archlinux.org/packages/libinput-gestures/
.. _`dbus-action`: https://github.com/bulletmark/dbus-action/
.. _`D-Bus`: https://en.wikipedia.org/wiki/D-Bus
.. _`PyGObject`: https://pypi.org/project/PyGObject/
.. _`python3-dbus`: https://packages.debian.org/buster/python3-dbus
.. _`ruamel.yaml`: https://pypi.org/project/ruamel.yaml/
.. _`libinput`: https://www.freedesktop.org/wiki/Software/libinput/
.. _`here`: https://medium.com/@dakshin.k1/enable-3-finger-gesture-for-click-and-drag-on-windows-and-linux-cd7165b66851
