#############
Input devices
#############

.. toctree::
	:maxdepth: 3
	:hidden:

	touchpad
	multigesture

At the beginning, `Weston`_ (the reference inmplementation of a Wayland compositor) used to handle **input devices** (keyboards, pointers, touch screens, etc.), but later on the code providing this functionality was split into a separate library named `libinput`_. Now, **libinput** not only handles input devices for multiple **Wayland compositors**, but also provides a generic **X.Org Server input driver**.

From **libinput 1.16**, the X.Org Server started using the libinput library in form of a wrapper called `xf86-input-libinput`_.

.. note:: In Debian, the **libinput driver** is provided by the package `xserver-xorg-input-libinput`_. 

The new **libinput driver** supersedes the old `xf86-input-synaptics`_ driver, but is currently lacking some features found in the older driver. 

.. note:: In Debian, the **xf86-input-synaptics** driver is provided by the package `xserver-xorg-input-synaptics`_. 

If I'm not mistaken, the old **xf86-input-synaptics** is no longer being updated, so it seems **libinput** is the way to go.

.. _`Weston`: https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)#Weston
.. _`libinput`: https://www.freedesktop.org/wiki/Software/libinput/
.. _`xf86-input-libinput`: https://gitlab.freedesktop.org/xorg/driver/xf86-input-libinput
.. _`xserver-xorg-input-libinput`: https://packages.debian.org/buster/xserver-xorg-input-libinput
.. _`xf86-input-synaptics`: https://gitlab.freedesktop.org/xorg/driver/xf86-input-synaptics
.. _`xserver-xorg-input-synaptics`: https://packages.debian.org/xserver-xorg-input-synaptics
