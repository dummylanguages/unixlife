.. _touchpad-label:

********
Touchpad
********
In modern Linux distributions the trackpad usually works out of the box.

Installing packages (usually not needed)
========================================
In case it doesn't we would need to install several packages, which vary according to the distro.

Debian
------
In my case, after installing **Debian 10** the touchpad didn't need anything to work. If that's not your case, you'll need:

* The `libinput`_ driver, which in **Debian** is included in the `xserver-xorg-input-libinput`_ package.
* The `libinput-tools`_, which is a package that contains several **command line utilities** to interact with the **libinput library**.

These two packages were installed in my case, so getting basic functionality out of my trackpads wasn't an issue.

Listing devices
===============
One of the utilities included in the **libinput-tools** is the ``libinput-list-devices`` command, which lists all the input devices recognized by **libinput**. In my case, I was getting a ``command not found`` error, so I had to run the command providing the full path::

    $ /usr/lib/x86_64-linux-gnu/libinput/libinput-list-devices

Temporary configuration
=======================
The ``xinput`` tool is used to **view** or **change** options available for a particular device at **runtime**. For example::

    $ xinput list

to view all devices and determine their names and **id numbers**. Once we have the device id, we can check its available properties:: 

    $ xinput list-props 10

Let's imagine I want to set up an Apple Magic Trackpad whose id is **10**, to register a left-click every time I tap on it. We just have to locate the property in the output of the command above::

    ...
    libinput Tapping Enabled (290): 1
    ...

To set up the property we can use either the **number** (290 in this case) or the **name** of the option, ``"libinput Tapping Enabled"``::

    $ xinput set-prop 10 "libinput Tapping Enabled" 1

These setting are fine for testing purposes, but they won't survive a reboot.

.. warning:: Do **not** copy/paste this line, because your touchpad may not be number ``10``.

Permanent configuration
=======================
We have two ways of configuring the trackpad:

1. Using the GUI of our desktop environment.
2. Place a configuration file in the ``xorg.conf.d`` folder.

xorg.conf.d
-----------
We can supply X server **user-specific configuration** by adding files in the ``/etc/X11/xorg.conf.d`` directory::
    
    Section "InputClass"
        Identifier "touchpad"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
        Option "Tapping" "on"
        Option "Tapping Drag Lock" "on"
    EndSection

Careful with ``MatchDevicePath "/dev/input/event*"``, if we specify here a specific device, the config only will apply to that touchpad and not any additional. (Bluetooth touchpad vs laptop trackpad) That's why we used a ``event*``.

.. note:: Note also the ``Option "Tapping Drag Lock" "on"``, which is quite convenient.


Disable a touchpad
==================
An interesting thing to do is to create a shortcut (xbindkeys or sxhkd) to disable the laptop integrated touchpad while we type, we may be using a mouse or an external touchpad. The triggered script could look something like this::

    #!/bin/sh
    # Toggle touchpad status
    # Using libinput and xinput

    # Use xinput list and do a search for touchpads. Then get the first one and get its name.
    device="$(xinput list | grep -P '(?<= )[\w\s:]*(?i)(touchpad|synaptics)(?-i).*?(?=\s*id)' -o | head -n1)"

    # If it was activated disable it and if it wasn't disable it
    [[ "$(xinput list-props "$device" | grep -P ".*Device Enabled.*\K.(?=$)" -o)" == "1" ]] &&
        xinput disable "$device" ||
        xinput enable "$device"

.. _`libinput`: https://www.freedesktop.org/wiki/Software/libinput/
.. _`xf86-input-libinput`: https://gitlab.freedesktop.org/xorg/driver/xf86-input-libinput
.. _`xserver-xorg-input-libinput`: https://packages.debian.org/buster/xserver-xorg-input-libinput
.. _`libinput-tools`: https://packages.debian.org/buster/libinput-tools
