**************
The clipboards
**************

ICCCM
=====
According to Wikipedia, the communication between the clients (applications) of a common X server is defined by the `ICCCM`_ protocol.

.. note:: **ICCCM** stands for **Inter-Client Communication Conventions Manual**. For more information check `ICCCM Version 2.0`_.

The ICCCM specifies things such as:

* cut and paste buffers
* window manager interaction
* session management
* how to manipulate shared resources
* how to manage device colours

These low-level functions are generally implemented within widget toolkits or desktop environments. This isolates application programmers from working directly with the ICCCM itself, as this functionality is delegated to the implementing toolkit. 

Freedesktop specification
=========================
According to the `clipboards-spec`_, the ICCCM defines three **standard selections**:

1. PRIMARY selection
2. SECUNDARY selection
3. CLIPBOARD selection

PRIMARY
-------


* Copy: We can **write** to this selection just by highlighting text, without having to explicitely copying it.
* Paste: We can **read** from this selection clicking with the mouse **middle-button**. Some applications, offer also a keyboard shortcut for pasting, like ``Shift + Insert`` in most terminal emulators.

SECUNDARY
---------
This selection is not really used these days.

CLIPBOARD
---------
There's another important selection called the **CLIPBOARD**. This buffer has to be filled explicitely with some **action**, for example, an user selects some text and press:

* Use a shortcut, either ``Ctrl + c`` to **copy** or ``Ctrl + x`` to **cut**.
* Right-click with the mouse. 
* Choose any of these options from a menu.

A small experiment
------------------
There's a small command line utility named ``xclip``, that allows us to access the contents of the PRIMARY, SECUNDARY and CLIPBOARD. Once installed we can do a small test, open a terminal and another app in two windows side by side. 

1. Then in the window of the app, let's say Firefox select some text and **copy** it (``Ctrl + c``), right after just **highlight** a different text small section.

2. In the terminal window run::

    $ xclip -selection clipboard -o
    the text that you copied
    $ xclip -selection primary -o
    the text that is highlighted in Firefox 

The PRIMARY buffer is immediately replaced with new content as soon as another piece of text is highlighted. The CLIPBOARD keeps its contents until new text is explicitely **copied** (``Ctrl + C``) or **cut** (``Ctrl + Delete``).

Usability of the PRIMARY
------------------------
Here we have 3 situations:

1. Inside **xterm** we **copy** just by highlighting some text, and **paste** it using:

    * The shortcut ``Shift + insert``.
    * Click with the mouse **middle-button**.

2. There is not problem bringing text from other apps into **xterm** using the 2 methods described above.

3. The problem is taking text from **xterm** out to other apps. Even though these apps also **write** to the PRIMARY, as we showed in our experiment, pasting its contents can't be done, because usually apps don't **read** from it: all you end up pasting is the contents of the CLIPBOARD, even using ``Shift + insert``.

.. note:: Even if you click with the **middle-button** somewhere else to paste the PRIMARY selection, a new selection is created and the old selection is gone. (I have to test this)

Since most of the apps use the CLIPBOARD, and don't offers ways to use the PRIMARY, there is no way to take text out of **xterm** using the PRIMARY road.

A solution: Making xterm use the CLIPBOARD
------------------------------------------
Xterm has an option named **Select to Clipboard** in its **#VT Options menu**. There's also an **X resource** named ``selectToClipboard`` to achieve the same. So if we put this on our ``~/.Xresources``:

    Xterm.vt100.selectToClipboard: true

What happens? 3 things:

1. **xterm** will put its highlighted selection(used to go in the PRIMARY) always to the CLIPBOARD, and we can paste it with ``Shift + insert`` as we used to do before.

2. In other apps, we can still highlight text, which still puts it into the PRIMARY, but we won't be able to paste it into **xterm**, since now the shortcut ``Shift + insert`` pastes the contents of the CLIPBOARD.

3. If we want to paste something from other apps into **xterm**, we'll have to put it in the CLIPBOARD, since **xterm** now uses it for both copy and paste.

So this setting allows us to interchange our selections between **xterm** and other apps using only the CLIPBOARD. Let's summarize it:

* Inside **xterm** we copy just by highliting text (CLIPBOARD). And **paste** with ``Shift + insert``. We are pasting always the CLIPBOARD.
* In other apps we use the normal shortcuts for copy/paste.

.. _`ICCCM`: https://en.wikipedia.org/wiki/Inter-Client_Communication_Conventions_Manual
.. _`ICCCM Version 2.0`: https://specifications.freedesktop.org/clipboards-spec/clipboards-latest.txt
.. _`clipboards-spec`: https://specifications.freedesktop.org/clipboards-spec/clipboards-latest.txt
.. _`framebuffer`: https://en.wikipedia.org/wiki/Framebuffer

https://www.uninformativ.de/blog/postings/2017-04-02/0/POSTING-en.html