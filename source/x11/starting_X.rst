**********
Starting X
**********
To start the X.Org server we can use the command ``startx``, which is a front-end script for the `xinit`_ command. 

.. note:: Check the man pages of both commands for more information: `man startx`_ and `man xinit`_

Configuration files
===================
Both ``xinit`` and ``startx`` take an optional **client program** as an argument, and if you do not provide one they will look for the files:

* If exists  the user's ``~/.xinitrc``
* Otherwise the default ``/etc/X11/xinit/xinitrc``

If none of these files exist, the terminal emulator `xterm`_ is the default **client program** that will run.

Customizing ``~/.xinitrc``
--------------------------
Users customize the list of client programs in their ``~/.xinitrc`` files. As a starting point, it's a good idea to copy the **default configuration** to the file in the home directory::

    $ cp /etc/X11/xinit/xinitrc ~/.xinitrc

Then we can use this file to start a different window manager or desktop environment. It's also useful to add keyboard remappings done by the `xmodmap`_ program.

.. note:: For some reason ``xmodmap`` remappings set in this file don't work, maybe because when using a display manager ``.xinitrc`` is not read.

An alternative method for starting an X display server is using a **display manager**, which on desktop Linux systems, is the most commonly used method.


.. _`xinit`: https://en.wikipedia.org/wiki/Xinit
.. _`xterm`: https://invisible-island.net/xterm/
.. _`man startx`: https://manpages.debian.org/buster/xinit/startx.1.en.html
.. _`man xinit`: https://manpages.debian.org/buster/xinit/xinit.1.en.html
.. _`xmodmap`: https://manpages.debian.org/buster/x11-xserver-utils/xmodmap.1.en.html

