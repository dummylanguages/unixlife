################
Display managers
################


.. toctree::
	:maxdepth: 3
	:hidden:

	lightdm

In the X Window System, a `display manager`_ is a graphical login manager which starts a login session on an X server from the same or another computer.

.. _`display manager`: https://en.wikipedia.org/wiki/X_display_manager
