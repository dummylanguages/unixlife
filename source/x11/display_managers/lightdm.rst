*******
Lightdm
*******
According to Wikipedia, `LightDM`_ is a free and open-source **X display manager** that aims to be lightweight, fast, extensible and multi-desktop. It can use various front-ends to draw User Interface, also called **Greeters**. It also supports Wayland.

Configuration
=============
LightDM configuration is provided by the following files:

1. ``/usr/share/lightdm/lightdm.conf.d/*.conf``
2. ``/etc/lightdm/lightdm.conf.d/*.conf``
3. ``/etc/lightdm/lightdm.conf``

Files are read in the above order and combined together to make the LightDM configuration.

startup services
----------------
If we are using LightDM with a window manager that doesn't take care of starting up services, we may use ``lightdm.conf`` to start the services by adding the line::

    display-setup-script=~/bin/startup.sh

Don't forget to ``chmod +x`` the ``startup.sh`` file, where you can add as many programs as you want::

    bob@debiancito


.. _`LightDM`: https://en.wikipedia.org/wiki/LightDM
