*********
Bluetooth
*********
The bluetooth was a little easier to set up, mostly because it worked out of the box: pressing the multimedia key with the Bluetooth logo actually toggled on and off the Bluetooth connection. So I just created a script to produce notifications of the Bluetooth state everytime it was toggled::

    #!/usr/bin/env bash

    main()
    {
    local state=$(sudo rfkill list 0 -n -o SOFT)
    notify-send 'Wi-Fi' "$state"
    }

    main $@

I'm using a ``0`` to refer more precisely to the Bluetooth interface, but I could have used ``bluetooth`` too. Then to create the shortcut, added the following to my``.xbindkeysrc``::

    "kill-wifi"
        XF86Bluetooth

To test out that everything was working fine everytime I press the key::

    $ sudo rfkill list bluetooth






.. _`rfkill`: https://wireless.wiki.kernel.org/en/users/documentation/rfkill
.. _`urfkill`: https://packages.debian.org/buster/urfkill

