**************
Sound Controls
**************
For the audio controls I used a command line utility named ``amixer`` included as a packaged version in the repositories of any distro.

.. note:: There's a `alsa-utils in Debian`_ and an `alsa-utils in Arch`_, etc.

After checking that all the commands achieved the desired results, I created the shortcuts in ``~/.config/sxhkd/sxhkdrc``::

    XF86AudioMute
        amixer set Master toggle
    XF86AudioRaiseVolume
        amixer set Master 5%+
    XF86AudioLowerVolume
        amixer set Master 5%-
    XF86AudioMicMute
        amixer set Capture toggle

.. note:: I didn't create any notifications for adjusting the volume, only updated the percentage on the status bar.

.. _`alsa-utils in Debian`: https://packages.debian.org/buster/alsa-utils
.. _`alsa-utils in Arch`: https://www.archlinux.org/packages/?name=alsa-utils
