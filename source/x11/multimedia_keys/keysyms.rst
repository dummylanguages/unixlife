****************
Keyboard Symbols
****************
The first question we have to solve is to find the `KeySyms`_ produced when we press the ``Fn`` key combined with any of the function keys. For that there's a small utility named `xev`_ that comes in handy, we just have to invoke the command from our terminal::

    $ xev

.. note:: In **Debian**, the ``xev`` command is available in the `X11-utils`_ package, in **Arch** is in `xorg-xev`_.

Once ``xev`` is running, a white window appears next to the terminal, we just have to hover over it, and press the **function keys** we're interested in.

.. note:: To exit ``xev``, while **not** hovering over the **white window**, press ``Ctrl + c`` to kill the process.

In my case I got the KeySyms for the following keys:

+-----+-----------------------+
| Key |        KeySym         |
+=====+=======================+
| Fn  | XF86WakeUp            |
+-----+-----------------------+
| F1  | XF86AudioMute         |
+-----+-----------------------+
| F2  | XF86AudioLowerVolume  |
+-----+-----------------------+
| F3  | XF86AudioRaiseVolume  |
+-----+-----------------------+
| F4  | XF86AudioMicMute      |
+-----+-----------------------+
| F5  | XF86MonBrightnessDown |
+-----+-----------------------+
| F6  | XF86MonBrightnessUp   |
+-----+-----------------------+
| F7  | XF86Display           |
+-----+-----------------------+
| F8  | XF86WLAN              |
+-----+-----------------------+
| F9  | XF86Tools             |
+-----+-----------------------+
| F10 | XF86Bluetooth         |
+-----+-----------------------+
| F11 | **None**              |
+-----+-----------------------+
| F12 | XF86Favorites         |
+-----+-----------------------+

The only key I didn't get a KeySym for was **F11**, so I won't be able to remap that one, no biggie.

.. note:: A full list of symbols can be found doing some Internet searching, for example in `XF86 keyboard symbols`_

Once we have the KeySyms we can use some utility such as `xbindkeys`_ or `sxhkd`_ to map them to commands that will perform the functions.

.. _`KeySyms`: https://tronche.com/gui/x/xlib/input/keyboard-encoding.html
.. _`xev`: https://www.x.org/releases/X11R7.7/doc/man/man1/xev.1.xhtml
.. _`x11-utils`: https://packages.debian.org/buster/x11-utils
.. _`xorg-xev`: https://www.archlinux.org/packages/extra/x86_64/xorg-xev/
.. _`XF86 keyboard symbols`: http://wiki.linuxquestions.org/wiki/XF86_keyboard_symbols
.. _`xbindkeys`: https://www.nongnu.org/xbindkeys/
.. _`sxhkd`: https://github.com/baskerville/sxhkd
