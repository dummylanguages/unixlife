*****************
Screen brightness
*****************
The brightness of the screen can often be controlled using the ACPI kernel module for video. An interface to this module is provided via a sysfs directory::

    $ ls /sys/class/backlight/
    intel_backlight

In this case, the backlight is managed by an **Intel graphics card**. This directory contains the following files and subdirectories::

    $ ls -1F /sys/class/backlight/intel_backlight
    actual_brightness
    bl_power
    brightness
    device@
    max_brightness
    power/
    scale
    subsystem@
    type
    uevent

We can display the **maximum brightness** with::

    $ cat /sys/class/backlight/intel_backlight/max_brightness
    1515

And the **actual brightness** too::

    $ cat /sys/class/backlight/intel_backlight/actual_brightness
    489

To change it we have to be **root** tho::
    
    $ su root
    Password:
    # echo 700 > /sys/class/backlight/intel_backlight/brightness
    exit
    $ cat /sys/class/backlight/intel_backlight/actual_brightness
    700

Installing some utility
=======================
We could create shortcuts to issue commands that change the brightness, but as we've seen, only the root user can do it. We could go around that by creating a `udev`_ rule that allowed users in the ``video`` group to change the brightness.

A simple approach is to install any of the available utilities that allow normal users managing the screen's backlight. One of the most commonly used is the ``xbacklight`` command, but it doesn't hurt to know about more options.

.. note:: For example, due to a bug in Debian, I struggled a bit to make ``xbacklight`` work in Buster(Debian 10).



that in Arch is packaged in the ``xorg-xbacklight`` package, so::

    $ sudo pacman -S xorg-backlight

.. note:: You may have to restart your system after installing, although I'm not sure if that's because right before I had installed the drivers for my Intel video card.

.. warning:: At some point I had to install the ``xf86-video-intel`` driver for my **Intel video card**, and I'm not sure if that's the reason for ``xbacklight`` not responding.

To control the brightness of the screen, the Debian repositories contain a package named `brightnessctl`_, so installing it was trivial::

    $ sudo apt install brightnessctl

The source code is available at its `github project page`_ where they mention several ways of how can we adjust the brightness without using ``sudo``, but in my case everything was done automatically, I just had to reboot the system and was able of adjusting the brightness::

    $ brightnessctl s +10%

Once I was sure the command was working properly, I added the following shortcuts in ``~/.config/sxhkd/sxhkdrc``::

    XF86MonBrightnessDown
        brightnessctl s 10%-
    XF86MonBrightnessUp
        brightnessctl s +10%

.. note:: I didn't create any notifications for adjusting the brightness, only updated the percentage on the status bar.

.. _`udev`: https://wiki.archlinux.org/index.php/Udev
.. _`brightnessctl`: https://packages.debian.org/buster/brightnessctl
.. _`github project page`: https://github.com/Hummer12007/brightnessctl
