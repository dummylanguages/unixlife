*************
Notifications
*************
Popular desktop managers keep users up to date through notifications that pop up when we receive a new email, turn on/off the Wi-Fi and things like that. Main stream desktop managers have the notification system baked in, but tiling window managers usually lack these type of conveniences. That doesn't mean we can add them easily.

A notification system has two parts:

* A notification daemon.
* A notification utility.

A notification daemon
=====================
For the notification daemon we'll use `dunst`_, which is lightweight, customizable and doesn't depend on any widget toolkit (also known as GUI frameworks) such as `GTK`_ or `Qt`_.

Installing Dunst
----------------
Dunst is quite popular and available in most of the distro repositories. Even the conservative Debian offers a `dunst package`_; bw we can install it with::

    $ sudo apt install dunst

Launch and configure
--------------------
To easily launch the daemon when the X11 server starts, we can add the following line to our ``.xinitrc``::

    dunst &

Dunst supports a configuration file located in ``~/.config/dunst/dunstrc``

Notifier
========
Now that we have a notification daemon listening for notifications, we need software to send notifications, and `libnotify`_ is such a program.

Fortunately the Debian repositories offer a packaged version named `libnotify-bin`_ that we can install with::

    $ sudo apt install libnotify-bin

We can easily create notifications in the command-line::

    $ notify-send 'Hey' 'You have new mail'

.. _`dunst`: https://dunst-project.org/
.. _`GTK`: https://en.wikipedia.org/wiki/GTK
.. _`Qt`: https://en.wikipedia.org/wiki/Qt_(toolkit)
.. _`dunst package`: https://packages.debian.org/buster/dunst
.. _`libnotify`: https://developer.gnome.org/libnotify/
.. _`libnotify-bin`: https://packages.debian.org/buster/libnotify-bin
