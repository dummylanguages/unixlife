.. _`multimedia_keys-label`:

###############
Multimedia keys
###############


.. toctree::
	:maxdepth: 3
	:hidden:

	keysyms
	sound_controls
	screen_brightness
	wifi
	bluetooth
	notifications

A common issue with laptops is getting the `Multimedia keys`_ to work properly. In my case I was working with a couple of **Thinkpads** in particular:

* `T480`_
* `T470`_

But the process described here should work with other models and brands. The goal here is make the keys behave as the printed logos on the keycaps indicate.

.. _`Fn keys`: https://en.wikipedia.org/wiki/Fn_key
.. _`T470`: https://www.lenovo.com/us/en/laptops/thinkpad/thinkpad-t-series/ThinkPad-T470/p/22TP2TT4700
.. _`T480`: https://www.lenovo.com/us/en/laptops/thinkpad/thinkpad-t-series/ThinkPad-T480/p/22TP2TT4800
