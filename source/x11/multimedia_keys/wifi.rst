****
WiFi
****

.. warning:: I think that the problem was that the **WiFi key** was working all the time, so when I bound the shortcut to toggle the WiFi I was toggling twice: once because it was already working and again because of the ``rfkill`` command the shortcut was triggering. 

**RFKill** is a subsystem in the Linux kernel that provides an API to be used by any Radio Frequency device, such as Wi-Fi and Bluetooth cards, NFC, WAN, FM, etc. If a driver implements this API, the status of the device can be queried, activated, and deactivated(killed).

There are two ways of killing an RF device:

* A **soft block** is when the transmitters can be reactivated by software, like an option in a GUI menu, or a command line utility.
* A **hard block** is for those devices come with a hard switch, like the FN buttons in some laptops. Once we block the device using the buttons (a hard block) the only way to unblock them is to press the button again. They can't be unblocked by software. 

The RFKill interface is located at /dev/rfkill which contains the current state of all radio transmitters on the system. Each device has its current RFKill state registered in sysfs. Additionally, RFKill issues uevents for each change of state in an RFKill-enabled device.

The ``rfkill`` utility
----------------------
On top of that, `rfkill`_ is also a small userspace command-line tool to query the state of the RF interfaces (soft blocked or hard blocked), as well as blocking/unblocking softly. In my case this tool wasn't installed on my system, so I had to::

    $ sudo apt install rfkill

To **list** RF devices::

    $ sudo rfkill list

To **block/unblock** the Wi-Fi or Bluetooth::

    $ sudo rfkill block wifi
    $ sudo rfkill block bluetooth

Disable prompting for password
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
I had to add the following line in the ``/etc/sudoers`` file, so the ``sudo rfkill`` wouldn't prompt the user ``bob`` for a password when running ``rfkill``::

    bob ALL=NOPASSWD: /usr/sbin/rfkill

Another slightly more complicated approach is:

1. Create a **system group** named ``rfkill``::

    $ sudo groupadd -r rfkill

2. Add my user to the group::

    $ sudo usermod -aG rfkill bob

3. Grant this group ``sudo`` privileges to run the ``rfkill`` command. This way, we'll still need to use ``sudo`` before ``rfkill`` but we won't be prompted for a password. Run ``sudo visudo`` and add the following line::

    %rfkill ALL=NOPASSWD: /usr/sbin/rfkill

Small script
^^^^^^^^^^^^
So I had to create a shell script to **toggle** the Wi-Fi or Bluetooth based on their current state::

    #!/usr/bin/env bash

    local state=$(sudo rfkill list wifi -n -o SOFT)

    if [[ $state == 'blocked' ]]
    then
        sudo rfkill unblock wifi
    else
        sudo rfkill block wifi
    fi

    notify-send 'WiFi' "$state"

The script worked flawlessly when run from the shell, but there was something not working when invoked with a press of the ``XF86WLAN`` key. I had to press repeatedly to get the Wi-Fi interface to actually toggle, most of the time it was stuck in one state, either ``blocked`` or ``unblocked``.

.. note:: Apparently there is some race condition or something strange going on, but I couldn't pinpoint the reason.

``urfkill``
^^^^^^^^^^^
So my solution was to install a utility named `urfkill`_, and modify my script so it would only produce the notification::

    #!/usr/bin/env bash

    local state=$(sudo rfkill list wifi -n -o SOFT)
    notify-send 'WiFi' "$state"

It always block or unblock the wifi. There must be something else binding that key.

.. _`rfkill`: https://wireless.wiki.kernel.org/en/users/documentation/rfkill
.. _`urfkill`: https://packages.debian.org/buster/urfkill

