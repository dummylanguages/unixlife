*********************
Environment variables
*********************
The most important environment variable for X Window System clients is ``DISPLAY``. If we check `man X`_, every X server has a display name of the form:

    hostname:displaynumber.screennumber

* The ``hostname`` specifies the name of the machine to which the display is physically connected.
* 

In my system::

    $ echo $DISPLAY
    :0.0

As you can see, before the ``:`` there's no value. An omitted hostname means the localhost.

.. _`man X`: https://manpages.debian.org/buster/xorg-docs-core/X.7.en.html#DISPLAY_NAMES

