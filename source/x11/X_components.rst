*********************************
Components of the X Window System
*********************************
The `X Window System`_ is made up of several parts:

* The **main component** of any windowing system is generically known as the `display server`_ (aka window server or compositor), and in X is called the `X.Org server`_.
* A graphical `login manager`_ named `XDM`_.
* A `window manager`_ named `twm`_, which is short for **Tabbed Window Manager**.
* A terminal emulator named `xterm`_, which is standard in the X Window System.

But most of the times, Unix systems use a windowing system that include just a few components of the X Window System, frequently just the display server.

The display server
==================
Since X was specifically designed to be used over network connections, it uses a **client–server** model: an X server communicates with various client programs. The server accepts requests for graphical output (windows) and sends back user input (from keyboard, mouse, or touchscreen).

a single server controls the input/output hardware, such as the screen, the keyboard, and the mouse; all application programs act as clients, interacting with the user and with the other clients via the server.

The display server and its clients communicate with each other over a communications protocol, which is usually called display server protocol, the display server being the mediator between the clients and the user

.. note:: The display server is the only essential part when creating a windowing system based on X.

The X Window System core protocol
---------------------------------
All the interaction between the display server and its clients is regulated by the `X Window System core protocol`_. 

The X display manager
=====================
The `X display manager`_ is a graphical login manager which starts a login session on an X server from the same or another computer. A display manager presents the user with a login screen. A session starts when a user successfully enters a valid combination of username and password.

X11 Release 3 was the first one to introduce display managers. **X11R4** introduced the `X Display Manager Control Protocol`_ (XDMCP) in 1989 to fix problems in the **X11R3** implementation.

The display manager included in the X Window System is called `XDM`_, but it's usually replaced by a number of more modern display managers such as:


X window managers
=================
An `X window manager`_ is a system software that controls the placement and appearance of windows within a windowing system. In the X Window System, the default window manager is named `twm`_, which is short for **Tabbed Window Manager**, but this is an optional component.

In the X Window System, there is a clear distinction between the window manager and the windowing system in general. Strictly speaking, an X window manager does not directly interact with video hardware, mice, or keyboards – that is the responsibility of the display server. This separation of concerns offered by the X Window System allows modularity, so that users can choose between many different window managers.

Most window managers are designed to help provide a desktop environment.

.. _`X Window System`: https://www.x.org/wiki/
.. _`display server`: https://en.wikipedia.org/wiki/Display_server
.. _`login manager`: https://en.wikipedia.org/wiki/Login_manager
.. _`XDM`: https://en.wikipedia.org/wiki/XDM_(display_manager)
.. _`X.Org server`: https://en.wikipedia.org/wiki/X.Org_Server
.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
.. _`twm`: https://en.wikipedia.org/wiki/Twm
.. _`xterm`: https://en.wikipedia.org/wiki/Xterm
.. _`X window manager`: https://en.wikipedia.org/wiki/X_window_manager
.. _`X display manager`: https://en.wikipedia.org/wiki/X_display_manager
.. _`X Display Manager Control Protocol`: https://en.wikipedia.org/wiki/X_display_manager#X_Display_Manager_Control_Protocol
.. _`X Window System core protocol`: https://en.wikipedia.org/wiki/X_Window_System_core_protocol

