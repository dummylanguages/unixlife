***************
Battery scripts
***************
As of Linux kernel 2.6, we can get battery information from the filesystem by checking the contents of the folder::

    $ ls /sys/class/power_supply
    AC BAT0 BAT1 hid-xx:xx:xx:xx:xx:dc-battery

The output of the command above shows several **directories** that contains simple text files with information about:

* ``AC`` contains directories and files with information about our power supply. The ``online`` file contains a ``1`` when the **AC adapter** is connected, and a ``0`` when is disconnected.
* ``BAT0`` contains information about one of my batteries(my laptop has two of them), the internal one. 
* ``BAT1`` contains information about the external battery. If we disconnected this battery, the ``BAT1`` folder simply dissappears.
* ``hid-xx:xx:xx:xx:xx:dc-battery`` is an external touchpad I have also connected.

From these files we can get some useful information such as the **battery's capacity**::

    $ cat /sys/class/power_supply/BAT0/capacity
    89

The output above shows the percentage of the battery that is charged. Another piece we may need is the battery's **status**, which will show when the battery is ``Charging`` or ``Discharging``::

    $ cat /sys/class/power_supply/BAT0/status
    Discharging

As we mention at the beginning, another way to check if the **AC adapter** is plugged in is by::

    $ cat /sys/class/power_supply/AC/online
    0



.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
