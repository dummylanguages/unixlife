###########
Status bars
###########


.. toctree::
	:maxdepth: 3
	:hidden:

	dwm_status_bar
	battery_scripts
	bluetooth_scripts
	polybar
	customizing_polybar

As usual, `Wikipedia`_ has the answer for what a **status bar** is: a `widget`_ to display information about the current state of its window, although some status bars have extra functionality.

In mainstream operating systems or even popular desktop environments, status bars are taken for granted. But usually window managers leave to the users the set up and configuration of the status bar. In some cases, the status bar is not even included, so we're in charge of finding a suitable one and adapt it to our system and preferences.

At the time of writing this, some popular status bars for Linux and Unix operating systems are:

* `Polybar`_.
* `Lemonbar`_.
* `Yabar`_.

But there are plenty more of them. Truth be told, we don't even need to install anything to have our own status bar. We just have to create a bunch of scripts that will take care of the different pieces of information we want to see in our bar:

* Date and time.
* Battery status.
* Wi-Fi and Bluetooth connections.
* Volume of our audio interface and microphone.
* System statistics: CPU and RAM, used filesystem.
* Some people even show the weather, currencies, etc.

We'll see how to create some of those scripts too ;-)


.. _`Wikipedia`: https://en.wikipedia.org/wiki/Status_bar
.. _`widget`: https://en.wikipedia.org/wiki/Graphical_widget
.. _`Polybar`: https://polybar.github.io/
.. _`Lemonbar`: https://github.com/LemonBoy/bar
.. _`Yabar`: https://github.com/geommer/yabar
