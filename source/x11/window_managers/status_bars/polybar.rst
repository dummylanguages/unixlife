.. _polybar-label:

*******
Polybar
*******
At the time of writing this, `Polybar`_ is the most popular **status bar** for Linux. The project aims to help users build beautiful and highly customizable status bars for their desktop environment. It offers a starting point with some default scripts, but the user can  roll up his own scripts to adapt to their own needs or customize further.

Installation
============
I'll cover the two distros I'm more familiar with.

Debian installation
-------------------
If you are in Debian, at the time I'm writing this there's not a official package either. So manual compilation/installation is the only option. I should probably mention that the list of **dependencies** is long:

    $ sudo apt-get install cmake cmake-data libcairo2-dev libxcb1-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev pkg-config python-xcbgen xcb-proto libxcb-xrm-dev i3-wm libasound2-dev libmpdclient-dev libiw-dev libcurl4-openssl-dev libpulse-dev

Update
^^^^^^
Debian offers now a `packaged version`_ of this bar in the bullseye repositories::

    $ sudo apt install polybar

Arch installation
-----------------
If you use Arch Linux, **Polybar** is in the **AUR** repositories, so you can install it via your favourite **AUR helper**:

    $ yay -Sy polybar

Or, if you prefer you can download it and install it manually:

    $ cd ~/Builds
    $ git clone https://aur.archlinux.org/polybar.git
    $ cd polybar
    $ makepkg -si

.. note:: Go brew yourself a cup of your favourite tea, the build takes a while.

Once the build and the installation it's finished, we are informed of a list of **optional dependencies**, some of them were not installed:

    alsa-lib: alsa module support
    pulseaudio: pulseaudio module support
    libmpdclient: mpd module support
    ttf-unifont: Font used in example config
    siji-git: Font used in example config
    xorg-fonts-misc: Font used in example config

Testing the installation
------------------------
Ideally we would launch polybar from the same scripts our window manager starts, but for testing purposes we could launch the bar in the background to see it runs::

    $ polybar -r example &

The ``-r`` option is for **restarting** the bar automatically everytime the configuration file changes. To stop it::

    $ killall -q polybar

Configuration
=============
We need to create a configuration folder for polybar::

    $ mkdir ~/.config/polybar

And copy an included configuration example to this folder::

    $ sudo cp /usr/share/doc/polybar/config ~/.config/polybar/config

Make sure that its **permissions** allow the owner to read/write it, and group and others can read::

    $ chmod 644 ~/.config/polybar/config

* Or if you prefer, you can use the super useful ``install`` command::

    $ install -Dm644 /usr/share/doc/polybar/config ~/.config/polybar/config-example

Once we have the configuration file in place, we can start adding our customizations there.

Launching script
----------------
It's recommended to create a dedicated script for launching the bar. We could keep it in the same directory as the configuration files::

    $ touch ~/.config/polybar/launch.sh
    $ chmod +x ~/.config/polybar/launch.sh

In your ``~/.xinitrc`` or ``~/.config/bspwm/bspwmrc`` add::

    $HOME/.config/polybar/launch.sh

.. warning:: If we're using **Bspwm** we have to launch the ``launch.sh`` from ``~/.config/bspwm/bspwmrc``.

And the bar will be launched automatically when our window manager starts up. The script is quite minimal::

    #!/usr/bin/env bash

    # Terminate already running bar instances before launching a new one
    killall -q polybar

    # If all your bars have ipc enabled, you can also use 
    # polybar-msg cmd quit

    # Launch bar
    echo "---" | tee -a /tmp/polybar-example.log
    polybar -r example >>/tmp/polybar-example.log 2>&1 &

.. note:: The ``polybar`` command has a lot of interesting **options** that must be checked out in its **man page**. The ``-r`` option (short for ``--reload``) **reloads** polybar when changes to the configuration are detected.

In the script provided we've named the bar as ``example``; this **name** is optional, but it's important that it matches the name of the bar in the **configuration file**.

.. warning:: Don't forget to name your bar the same as in the configuration file.

Customizing the bar
===================
The first thing we have to make sure is that the **bar section** of our configuration file, the bar is named as in the **launching script**::

    [bar/example]

Again, change the name ``example`` to whatever you want, but make sure that the name is the same both in the launching script and the configuration file.

.. warning:: If for some reason you see the bar, but not the **desktop numbers** (it happened to me in Bspwm) make sure the ``height`` is enough to show the numbers: I had to gave it a 4%.

That f..king gap at the top
---------------------------
The default configuration was responsible of a small gap at the top of the bar and to the sides. After a couple of hours looking for the setting where the gap was produced, I found that it was in::

    border-size = 2
    border-color = #00000000

I was looking for **margin** or **padding** but as it happens it was a **border** whose color was set to **transparent**. Comment or delete both lines above to get rid of the gap.

.. warning:: Mind the gap.

Fonts
-----
There are three fonts specified:

* ``font-0`` is for the characters and numbers.
* ``font-1`` is for the controls (sliders, etc).
* ``font-2`` is for the icons.

The default configuration was using a font named `siji`_, which is not in the Debian repositories, but can be downloaded and installed manually::

    $ git clone https://github.com/stark/siji && cd siji
    $ ./install.sh

By default, the font ``siji.pcf`` will be installed in your ``$HOME/.local/share/fonts`` directory, it will be created if the directory is non-existent. If you wish to install Siji in another directory then run the install.sh script with the ``-d`` flag and specify the font directory as an argument::

    $ ./install.sh -d ~/.fonts

Whatever path you choose, you can prepend it to the **fontpath** if it's not there already::

    $ xset +fp /home/bob/.local/share/fonts

.. note:: To see the **font path** (and other useful information) run: ``xset q``.

Finally, we may have to enable **bitmap fonts** in our system by running::

    $ sudo dpkg-reconfigure fontconfig-config

Then enable bit maps font in the third or fourth screen. 

.. _`Polybar`: https://polybar.github.io/
.. _`packaged version`: https://packages.debian.org/bullseye/polybar
.. _`siji`: https://github.com/stark/siji
