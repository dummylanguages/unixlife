*******************
Customizing the bar
*******************
The first thing we have to make sure is that in the **bar section** of our configuration file, the bar is named as in the **launching script**::

    [bar/example]

Again, change the name ``example`` to whatever you want, but make sure that the name is the same both in the launching script and the configuration file.

.. warning:: If for some reason you see the bar, but not the **desktop numbers** (it happened to me in Bspwm) make sure the ``height`` is enough to show the numbers: I had to gave it a 4%.

The configuration file uses the **INI** format; it uses ``;`` for **comments**.

That f..king gap at the top
===========================
The default configuration was responsible of a small gap at the top of the bar and to the sides. After a couple of hours looking for the setting where the gap was produced, I found that it was in::

    border-size = 2
    border-color = #00000000

I was looking for **margin** or **padding** but as it happens it was a **border** whose color was set to **transparent**. Comment or delete both lines above to get rid of the gap.

.. warning:: Mind the gap.

Fonts
=====
There are three fonts specified:

* ``font-0`` is for the characters and numbers.
* ``font-1`` is for the controls (sliders, etc).
* ``font-2`` is for the icons.

Installing ``siji`` in Debian
-----------------------------
The default configuration was using a font named `siji`_, which is not in the Debian repositories, but can be downloaded and installed manually::

    $ git clone https://github.com/stark/siji && cd siji
    $ ./install.sh

By default, the font ``siji.pcf`` will be installed in your ``$HOME/.local/share/fonts`` directory, it will be created if the directory is non-existent. If you wish to install Siji in another directory then run the install.sh script with the ``-d`` flag and specify the font directory as an argument::

    $ ./install.sh -d ~/.fonts

Whatever path you choose, you can prepend it to the **fontpath** if it's not there already::

    $ xset +fp /home/bob/.local/share/fonts

.. note:: To see the **font path** (and other useful information) run: ``xset q``.

Finally, we may have to enable **bitmap fonts** in our system by running::

    $ sudo dpkg-reconfigure fontconfig-config

Then enable bit maps font in the third or fourth screen. 

Font-naming convention
----------------------
Defining the fonts for our bar is a two step process:

1. **Define** a font using a **font index** tag with the format::

    font-N = <fontconfig pattern>;<vertical offset>

We can define several fonts this way, font-1, font-2, etc.

2. Now, when we want to use any of the fonts we have defined inside some **module**, we'll use the **font index** inside the module, using a tag named ``label-active-font``, for example::

    label-active-font = 2

Regarding the **fontconfig pattern**, this is made out of the:

* name of the font
* weigth: normal, bold...
* size

Next we'll explain how can we get all this information about a font.

Using ``fontconfig``
--------------------
A first place to start would be to list all the available fonts using ``fc-list``::

    $ fc-list : family style | sort
    SFNS Display:style=Bold
    SFNS Display:style=Regular
    SFNS Display:style=Thin
    SFNS Display:style=UltraLight
    xos4 Terminus:style=Bold
    xos4 Terminus:style=Regular

That would mean that in my system I only have a couple of fonts, in several styles. By the way, it's important to write properly the name of the font, spaces included. For example, the last font is named precisely ``xos4 Terminus``, just ``Terminus`` won't cut it. The following could be a polybar font configuration::

    font-0 = SFNS Display:style=Bold:size=14;0
    font-1 = SFNS Display:style=Regular:size=14;0
    font-3 = SFNS Display:style=Thin:size=14;0
    font-4 = xos4 Terminus:style=Regular:size=15;0

To check if our font selection is right, we can use::

    $ fc-match 'SFNS Display:style=Regular:size=14'

A module to show window titles
==============================
Since we want to get rid of the **window title bars** it would be useful to have **Polybar** displaying the title of the window that has the focus. The Polybar configuration already has a module defined to accomplish this, it's named ``xwindow`` and its most simple form looks like this::

    [module/xwindow]
    type = internal/xwindow
    label = %title%

.. note:: This module requires a **window manager** that has support for a **EWMH atom** named ``_NET_ACTIVE_WINDOW``.

We just have to place it in our bar. Inside each bar, we may place our modules in 3 locations:

* Left: ``modules-left = module1``
* Center: ``modules-center = xwindow``
* Right: ``modules-right = module2, module3``


.. _`Polybar`: https://polybar.github.io/
.. _`packaged version`: https://packages.debian.org/bullseye/polybar
.. _`siji`: https://github.com/stark/siji
