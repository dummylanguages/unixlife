***
Dwm
***
`dwm`_ stands for **dynamic window manager** and as its name points out, it's `window manager`_ for the X Window System. One of the main features of this window manager is its **minimalism**, which is why its code base is really small, under 2000 lines of code (`SLOC`_)

Due to the aforementioned minimalism, any customizations and even configuration changes involve recompiling the code and reinstalling. For that reason, installing **dwm** starts by get ahold of its source code, which can be done in a couple ways:

1. We can download the **source code** either from https://dwm.suckless.org/, by clicking on the download link. Once downloaded, we'll change to the Downloads directory and untar the file::

    $ cd ~/Downloads
    $ tar -zxvf dwm-6.2.tar.gz
    $ cd dwm-6.2

2. A more advisable approach is using Git to clone the codebase from the suckless repository::

    $ cd ~/Downloads
    $ git clone git clone https://git.suckless.org/dwm

Compiling and installing
========================
Compiling should be as easy as::

    $ cd ~/Downloads/dwm
    $ make

But in my case I was missing a couple of libraries which in Debian we can find in the following packages::

* `libxft-dev`_: FreeType-based font drawing library for X (development files)
* `libx11-dev`_: X11 client-side library (development headers)
* `libxxinerama-dev`_: This package contains the development headers for the library found in `libxinerama1`_. Non-developers likely have little use for this package.

Let's install the **dependencies**::

    $ sudo apt install libxft-dev libx11-dev libxinerama-dev

Now we can compile (with ``make``), and **install**::

    $ sudo make install

.. note:: Better to get used to ``sudo make clean install``, because after to make effective any changes to the configuration, the source code must be recompiled and the executable reinstalled.

Launching dwm
=============
Once installed, we can start **dwm** from a **tty** (``Ctrl + Alt + F3`` for example) and once there, **log in** and run::

    startx dwm

The `startx`_ script is a **front end** to `xinit`_ that provides a somewhat nicer user interface for running a single session of the X Window System. It is often run with no arguments, but we can pass the name of some client that should be used as the **magic client**: if we close it the X server will exit, no matter if there are other clients running.

.. warning:: You should see the window's manager top bar, and an instance of `xterm`_ running. Even though we passed ``dwm`` as a **client**, the terminal window is still launched, not sure why... 

Once we've checked dwm is properly running, way we can go back to Gnome by changing to **tty 2**.

A minimal ``~/.xinitrc``
------------------------
When no specific client program is given on the command line, ``xinit`` will look for a file in the user's home directory called ``.xinitrc`` to run as a shell script to start up client programs. If no such file exists, xinit will use the following as a default::

   xterm -geometry +1+1 -n login -display :0

To avoid that pesky xterm window, we can create a minimal ``~/.xinitrc`` with the following content::

    #!/bin/sh
    exec dwm

Adding to the display manager
-----------------------------
Next step is creating a ``.desktop`` file in ``/usr/share/xsessions/`` so our `display manager`_ offers us the option to choose to start a session in **DWM**. I named it ``dwm.desktop`` and added the following::

    [Desktop Entry]
    Version=1.0
    Name=DWM Xsession
    Exec=dwm
    Icon=/usr/share/icons/dwm.png
    Type=Application

Configuration settings
======================
According to the official documentation dwm is `customized`_ by editing a couple of different files:

* ``config.h``, a C language header file. 
* ``config.mk``, a Make include file.

These files are located wherever the source code package was unzipped. After we're finished editing them, we must **compile** and **install** again.

.. note:: The source code folder also contains a file called ``config.def.h``, which is a template we can use to create our own ``config.h`` file. To start customising dwm, simply copy ``config.def.h`` into ``config.h`` before you run ``make``.

Exiting DWM
-----------
Since changes to the configuration involve recompiling and reinstalling, we'll obviously have to **quit** the window manager before doing that. We have to ways of doing that:

* ``Alt-Shift-Q`` quits the dwm process.
* ``Ctrl-Alt-BS`` kills the whole **X server**.

If we have launched the X server and dwm from a tty, then quitting dwm also ends your X session, which means that both shortcuts achieve us the same result. That being said, a proper shutdown of our **X session** should be preferred to just killing the X server.

.. note:: The shortcut for killing the **X server** may differ from the old default we've mentioned. Recent versions of X use ``something``, but you may have changed it.

Font
----
Edit the following line in ``config.h``::

    /* appearance */
    static const char *fonts[]      = { "monospace:size=16" };
    static const char dmenufont[]   = "monospace:size=16";

The ``MODKEY``
--------------
The is the modifier key that we must press to issue dwm commands, and by default it's mapped to the **left Alt key**. To modify this key, again, we'll have to edit ``config.h``. , under the comment ``/* key definitions */``, you can find the line::

    #define MODKEY Mod1Mask

In order to change dwm's modifier key to the **Windows key** (or **Command key** in mac), you can simply change its value definition to ``Mod4Mask``::

    #define MODKEY Mod4Mask

Terminal emulator
-----------------
By default, pressing the ``MODKEY + Shift + Return`` will launch `simple terminal`_, another of the **suckless tools** that we may or may not have installed. In any case, if we want to change the default terminal emulator, we'll have to find the line::

    ...
    static const char *termcd[] = {"st", NULL};
    ...

And replace ``st`` with a terminal emulator of our choice (make sure it's installed in your system).

If what we want is to change the **shortcut** that triggers a terminal window, that's done on the line::

    ...
    {MODKEY|ShiftMask, XK_Return, spawn, {.v = termcd} },
    ...

Gradual changes
---------------
Ideally, changes to the "configuration" should be applied gradually, compiling and installing after each of them. This way, we could easily track and revert the last **breaking** change. Remember, to compile and install run::

    $ sudo make install

Source control
--------------
It's a good idea to keep all of our configuration changes under source control using Git. Right after we clone the project for the first time::

    $ git clone git://git.suckless.org/dwm

We can create a **special branch** where all the customizations will be kept. It doesn't matter what the name is, it just needs to be something different than ``master``::

    $ git branch my_config

Then switch to that branch::

    $ git checkout my_config

And only then, we can start writing/adding/commiting our configuration changes::

    $ git add config.h
    $ git commit -m 'Increased font size'

If at any point, one of them mess things up, we can easily revert it using **Git**::

    $ git log
    $ git revert <unwanted commit hash>

Recording the initial configuration changes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You may be regretting not having recorded the initial configuration changes we made at the beginning on a separate branch. If that's the case, don't worry, we can do something about it:

1. Use the `diff`_ command to create a **diff file** containing all the changes and place the file in a dedicated folder outside the ``dwm`` source code folder.
2. Use git to revert all changes.
3. Create the branch ``my_config``, check it out, and use the `patch`_ command to apply the changes contained in the ``diff`` file to the ``config.h`` file. 

1. Create the diff file
"""""""""""""""""""""""
Move to the ``~/Downloads/dwm`` folder and create the **diff file**::

    $ cd ~/Downloads/dwm
    $ diff -u config.def.h config.h > ../config.diff

We have output the diff file outside the **dwm folder** because we are about to revert all the changes we've made.

By the way, we have to edit the first two lines of the ``config.diff`` file the ones that look like this::

    --- config.def.h 2020-06-23 22:03:51
    +++ config.h 2020-06-23 22:03:51

And make it look like this::

    --- config.bak.h 2020-06-23 22:03:51
    +++ config.def.h 2020-06-23 22:03:51

That's because we adding our configuration settings to ``config.h``, then the diff file was generated out of comparing this modified file to the pristine ``config.def.h``. From now on, we'll be adding our changes to ``config.def.h`` which will be used to generate the ``config.h`` during compilation.

.. note:: A bit later, when we run the ``patch`` command, thanks to the lines that we just edited, it will be the ``config.def.h`` file the one patched.

2. Revert all changes
"""""""""""""""""""""
Since we've saved our changes in the diff file, we can do a hard reset to take the repo to its pristine state just after we downloaded it::

    $ git reset --hard
    $ make clean
    $ rm config.h

Now if you run a ``git status`` your branch should be up to date with 'origin/master', with nothing to commit, working tree clean.

3. Branches
"""""""""""
Let's create a branch to keep track of our configurations::

    $ git branch my_config
    $ git checkout my_config

Now we can **patch** and **compile**::

    $ patch < ../config.diff
    $ make && sudo make clean install

If everything works ok after restarting the window manager, we can commit the changes in the ``my_config`` branch and merge it into ``my_dwm-6.2``::

    $ git add config.def.h
    $ git commit -m 'my_config: basic settings'
    $ git checkout my_dwm-6.2
    $ git merge my_config

And another one where we'll merge all the patch branches and the one for configuration (don't forget to clean up before creating the branch)::

    $ make clean && rm -rf config.h && git reset --hard origin/master
    $ git branch my_dwm-6.2

New ``dwm`` release
^^^^^^^^^^^^^^^^^^^
After a **new release** of dwm or when the dwm repository contains a commit fixing some bug, you should checkout the master branch and pull the changes::

    $ git checkout master
    $ git pull

Then we can rebase our customized dwm branch on top of the master branch::

    $ git checkout my_dwm-6.2
    $ git rebase --preserve-merges master


.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
.. _`SLOC`: https://en.wikipedia.org/wiki/Source_lines_of_code
.. _`libxft-dev`: https://packages.debian.org/buster/libxft-dev
.. _`libx11-dev`: https://packages.debian.org/buster/libx11-dev
.. _`libxinerama-dev`: https://packages.debian.org/stable/libxinerama-dev
.. _`libxinerama1`: https://packages.debian.org/buster/libxinerama1
.. _`xterm`: https://invisible-island.net/xterm/
.. _`xinit`: https://www.x.org/archive/X11R6.7.0/doc/xinit.1.html
.. _`startx`: https://www.x.org/releases/X11R7.5/doc/man/man1/startx.1.html
.. _`display manager`: https://en.wikipedia.org/wiki/X_display_manager
.. _`simple terminal`: https://st.suckless.org/
.. _`customized`: https://dwm.suckless.org/customisation/
.. _`diff`: https://en.wikipedia.org/wiki/Diff
.. _`patch`: https://en.wikipedia.org/wiki/Patch_(Unix)