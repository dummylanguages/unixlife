###############
Window managers
###############


.. toctree::
	:maxdepth: 3
	:hidden:

	dwm
	patching_dwm
	bspwm/index
	status_bars/index
	compositor

A `window manager`_



.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
