**********
Compositor
**********
A compositor or `compositing window manager`_ is a window managers that provides each application running in a window with a buffer. Then the compositor creates a composite of all the window buffers into an image representing the screen and writes the result into the display memory. Each frame of each running application goes through the compositor. Compositors can also add effects like transparency, animations, blur, etc.

Some examples of compositing window managers are:

* In Linux/Unix there are some such as `Mutter`_
* In Windows 10 there is the `Desktop Window Manager`_
* In macOS there's `Quartz`_, which is a **display server** and at the same time a **compositing window manager**.

Some window managers such as the minimalistic `dwm`_, don't include a **compositor**, so in order to enjoy the aesthetic of window transparency and blurred backgrounds we need to install an external standalone compositor.

Compton
=======
`Compton`_ is one of the most used lightweight compositors. Installing it is quite easy, since most distros include it in their package repositories. In Debian run::

    $ sudo apt install compton

Once installed, we could set transparency in our terminal, and as a sanity check run::

    $ compton &

Transparency should be applied inmediately, the problem is that once we close our terminal session, the compton process will die with it.

So we need a way of starting Compton when our window manager does, and a simple one is by adding the following line to our ``~/.xinitirc``::

    compton &

That was enough for me, since I was just after a bit of transparency for my terminal windows.

.. note:: I added in ``~/.config/alacritty/alacritty.yml`` the setting ``background_opacity: 0.8``.

Configuration
-------------
According to its man page, when **Compton** is not invoked using the ``--config`` option, it will seek for a configuration file in::

* ``$XDG_CONFIG_HOME/compton.conf``
* ``~/.compton.conf``
* ``XDG_CONFIG_DIRS/compton.conf``, usually under ``/etc/xdg/compton.conf``

We'll create our configuration file at the first location. But what are we gonna put inside? Our installation includes a **configuration example** so let's copy it to our ``~/.config`` folder::

    $ cp /usr/share/doc/compton/examples/compton.sample.conf ~/.config/compton.conf

In this file we can place those settings we want to apply permanently.

.. note:: Not sure if the location is ``~/.config/compton/compton.conf`` or ``~/.config/compton.conf`` 

.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
.. _`compositing window manager`: https://en.wikipedia.org/wiki/Compositing_window_manager
.. _`Mutter`: https://en.wikipedia.org/wiki/Mutter_(window_manager)
.. _`Desktop Window Manager`: https://en.wikipedia.org/wiki/Desktop_Window_Manager
.. _`Quartz`: https://en.wikipedia.org/wiki/Quartz_Compositor
.. _`dwm`: https://en.wikipedia.org/wiki/Dwm
.. _`compton`: https://github.com/chjj/compton