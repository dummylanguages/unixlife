************
Patching dwm
************
The community around `dwm`_ has contributed with a considerable amount of `patches`_ that add functionality to the window manager. In this section we'll discuss a sustainable approach to maintain an evolving **patched dwm** in our system.

Add a remote
============
We need to add a remote repository where we'll be pushing/pulling all the changes::

    $ git remote add dl git@gitlab.com:dummylanguages/dwm.git
    $ git push -u dl --all
    $ git push -u dl --tags

We haven't deleted the ``origin`` remote, since we will be pulling new releases and official changes from that remote.

.. warning:: Don't forget to use ``dl`` when **pushing** your changes.

Applying a patch
================
A patch I like a lot is `focus adjacent tag`_, which provides the ability to move the focus the tag on the immediate left or right of the currently focused tag.

Creating a dedicated branch
---------------------------
First things first, let's create a dedicated branch for this patch, and check it out::

    $ git branch focus_adjacent_tag
    $ git checkout focus_adjacent_tag

.. note:: To **create** and simultaneously **check out** a new branch we can use ``git checkout -b new_branch``.

Edit the patch
--------------
Before patching, I noticed that this patch modifies ``config.h`` instead of ``config.def.h``, so I just edited the second line of the diff file to retarget it to ``config.def.h``.

Patching
--------
Then applied it::

    $ patch < ../dwm-focusadjacenttag-6.0.diff

One of the hunks failed, so I copy/paste it manually. The next step would be to recompile and install::

    $ make && sudo make clean install

Recompile and reinstall
-----------------------
Then we can exit **dwm** using the ``Mod + Shift + Q`` shortcut, and run ``startx`` again. Once we check everything is working we can clean up and commit our changes::

    $ git add .
    $ git commit -m 'focus adjacent tag'
    $ git checkout my_dwm-6.2
    $ git merge focus_adjacent_tag
    $ git push dl

A mistake along the way
-----------------------
While writing these notes I made the mistake of patching without having checking out the ``focus_adjacent_tag`` branch. To fix it, I just stashed all the **staged changes** and cleaned the patching backup files::

    $ git stash
    $ git clean -f

Checked out the branch, and popped out the stash::

    $ git checkout focus_adjacent_tag
    $ git stash pop

Added and commited the changes::

    $ git add .
    $ git commit -m 'focus adjacent tag'

And merge the patch branch into my dwm build branch::

    $ git checkout my_dwm-6.2
    $ git merge focus_adjacent_tag

Finally, push the changes to the remote::

    $ git push dl

Links
=====
I got most of this workflow from https://github.com/qguv/dwm

.. _`dwm`: https://dwm.suckless.org
.. _`patches`: https://dwm.suckless.org/patches/
.. _`focus adjacent tag`: https://dwm.suckless.org/patches/focusadjacenttag/
