****
Bspc
****
`Bspwm`_ only responds to X events and messages it receives on a dedicated socket. ``bspc`` is the program in charge of writing those messages.

Subcommands
===========
``bspc`` has a lot of subcommands:

* ``node``, for node related stuff (basically a node is a window).

The ``node`` subcommand
-----------------------
For all the following commands we can specify a given node to act upon, but if we don't do so, the ``focused`` node is assumed.

+---------------------------+-----------------------------------------------------+
|          Command          |                       Result                        |
+===========================+=====================================================+
| ``--focus`` (``-v``)      | Put the focus on the selected node.                 |
+---------------------------+-----------------------------------------------------+
| ``--activate`` (``-a``)   | Activate the selected node.                         |
+---------------------------+-----------------------------------------------------+
| ``--to-desktop`` (``-d``) | Send the selected node to the given desktop.        |
+---------------------------+-----------------------------------------------------+
| ``--to-monitor`` (``-m``) | Send the selected node to the given monitor.        |
+---------------------------+-----------------------------------------------------+
| ``--to-node`` (``-m``)    | Send the selected node to the given node.           |
+---------------------------+-----------------------------------------------------+
| ``--swap`` (``-s``)       | Swap the selected node with the given node.         |
+---------------------------+-----------------------------------------------------+
| ``--close`` (``-c``)      | Close the windows rooted at the selected node.      |
+---------------------------+-----------------------------------------------------+
| ``--kill`` (``-k``)       | Kill the windows rooted at the selected node.       |
+---------------------------+-----------------------------------------------------+
| ``--resize`` (``-z``)     | Resize the window using a given handle.             |
+---------------------------+-----------------------------------------------------+
| ``--state`` (``-t``)      | Set the state of the window (tiled, floating, etc). |
+---------------------------+-----------------------------------------------------+

With the ``d``, ``-m``, ``-s`` and ``-n`` options, we can pass an additional ``--follow`` option, so the focused node will stay focused.


.. _`Bspwm`: https://github.com/baskerville/bspwm
