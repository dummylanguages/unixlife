.. _bspwm-label:

#####
Bspwm
#####

.. toctree::
	:maxdepth: 3
	:hidden:

	installation
	sxhkd
	bspc

`Bspwm`_ stands for **binary space partitioning window manager** and as its name points out, it's a `window manager`_ for the X Window System. The curious name is due to the fact that bspwm is a tiling window manager that represents windows as the leaves of a full binary tree.

Closely tied to **Bspwm** but independently maintained is the `sxhkd`_ shortcut manager. This program is what allows the user to drive the window manager using keyboard shortcuts.

.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
.. _`Bspwm`: https://github.com/baskerville/bspwm
.. _`sxhkd`: https://github.com/baskerville/sxhkd
