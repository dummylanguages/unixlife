.. _sxhkd-label:

**********************
Simple X hotkey daemon
**********************
An essential part of using a `window manager`_ is the ability of launching things and controlling the environment using keyboard shortcuts. In some window managers this functionality is integrated in the window manager itself, and can be changed via configuration files. The problem with this approach is that if we decide to change to another window manager, we'll have to create our keybindings from scratch.

A modular approach
==================
Another approach is to delegate this responsability to an external tool, so that if we need/want to switch to another window manager, we can take our configuration file with us without the need of recreate the same bindings. There are several tools that allow us to create and manage shortcuts in X11:

* `xbindkeys`_
* `sxhkd`_

Both of them are quite popular an readily available in most distro repositories. 

.. note:: `sxhkd`_ is the default shortcut manager for `bspwm`_.

Install
=======
As we said, **sxhkd** is popular enough to be in any distro repository, so installing it is trivial. In Debian we just have to do::

    $ sudo apt install sxhkd

Configuration
=============
The configuration file for **sxhkd** should be placed under ``~/.config/sxhkd/sxhkdrc``. It contains a series of bindings that define the associations between the input events and the commands.

The format of the configuration file supports a simple notation for mapping multiple shortcuts to multiple commands in parallel.

Launching the  daemon
=====================
As its name implies, **sxhkd** is an `daemon`_ for the X Window System that reacts to input events by executing commands. To make sure the **daemon** is launched along your window manager we have two options:

1. Add it to your ``~/.xinitrc`` file (or your ``~/.profile`` if you use a display manager).
2. If you use **bspwm** you may prefer to add it in your ``~/.config/bspwm/bspwmrc`` file.

In any case we'd add the line::

    sxhkd &

So that the daemon would run in the background.


.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
.. _`xbindkeys`: https://www.nongnu.org/xbindkeys/
.. _`sxhkd`: https://github.com/baskerville/sxhkd
.. _`bspwm`: https://github.com/baskerville/bspwm
.. _`daemon`: https://en.wikipedia.org/wiki/Daemon_(computing)
