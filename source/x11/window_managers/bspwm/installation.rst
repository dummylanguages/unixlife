************
Installation
************
`Bspwm`_ started as a sort of obscure window manager, but slowly has gained traction among power users and now it's usually available in the package repositories of any distro. Debian also offers a `packaged version`_, which can be installed as usual::

    $ sudo apt install bspwm

We're also gonna need the `sxhkd`_ package, also available in the Debian repos, so::

    $ sudo apt install sxhkd

.. warning:: Without installing and configuring ``sxhkd`` you won't be able to control **Bspwm**.

Configuration files
===================
Both of these programs need configuration files that go under ``$XDG_CONFIG_HOME``, so let's create them::

    $ mkdir ~/.config/bspwm && touch ~/.config/bspwmrc
    $ mkdir ~/.config/sxhkd && touch ~/.config/sxhkdrc

.. note:: The `XDG Base Directory Specification`_ defines the ``$XDG_CONFIG_HOME`` as the base directory where user specific configuration files should be stored. This directory is usually located at ``~/.config``.

Once you have the config files in place, you better open them in your editor, especially ``~/.config/sxhkd/sxhkdrc``, in order to get familiar with the shortcuts and adapt them to your needs.

.. _`packaged version`: https://packages.debian.org/bullseye/polybar
.. _`Bspwm`: https://github.com/baskerville/bspwm
.. _`sxhkd`: https://github.com/baskerville/sxhkd
.. _`XDG Base Directory Specification`: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
