#############
X11 utilities
#############
In this section I'll cover utilities that depend on the X Window System, as opposed to Wayland or any other system.

.. toctree::
    :maxdepth: 3

    rofi/index
    display/index

.. _`entry`: https://en.wikipedia.org/wiki/Unix_philosophy