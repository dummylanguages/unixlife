*************
Screen-savers
*************
According to `Wikipedia`_:

    A screensaver (or screen saver) is a computer program that blanks the screen or fills it with moving images or patterns when the computer has been idle for a long time. The original purpose of screensavers was to prevent phosphor burn-in on `CRT`_ and plasma computer monitors (hence the name). Though modern monitors are not susceptible to this issue, screensavers are still used for other purposes. Screensavers are often set up to offer a basic layer of security, by requiring a password to re-access the device.

From what I've been seeing lately, it seems that screensavers are not trendy anymore and a lot of main stream desktop environments don't include one. The general trend is to just show a different wallpaper after some inactivity period, and optionally lock the screen.

Although for nostalgics, the old classic `XScreenSaver`_ application is still maintained an offers a ton of different dynamic screensavers.

.. note:: Check this `promotional article`_ written by the XScreenSaver author.

A minimalistic approach
=======================
 I don't know of any window manager that include s 
 applications which, usually, also work as **screen lockers** or can be configured to do so. Most of the times, these screensavers are meant to be used in conjunction with the included 

Fortunately, for users of window managers there are other more minimalistic options that don't depend on any display manager or some `widget library`_ such as `GTK`_. They are more specialized tools, that do can do one thing or another:

* Either show some sort of wallpaper.
* Lock the screen.



Before start installing and configuring things, let's go a bit deeper into some technicalities.

Screen saving
=============
The X Window System has built-in support for changing the image on a display screen after a user-settable period of inactivity to avoid burning the cathode ray tube phosphors by showing the same image for long periods of time. However, no interfaces are provided for the user to control the image that is drawn. 

The `X11 Screen Saver Extension`_ was introduced to allow an external **screen-saver client** to detect when the alternate image is to be displayed and to provide the graphics. There's an old classic utility named `XScreenSaver`_ that rides on this mechanism.

.. note:: In the `official site`_ of the project, **XScreenSaver** is described as two things: a large collection of screen savers; and it is also the framework for blanking and locking the screen. 

So as you can see, traditionally, the X Window System has provided support for both blanking and non-blanking **screen savers**:

* Screen blanking is when the screen goes black.
* A non-blanking screensaver is one that shows an image.

In both cases the end goal was to avoid `CRT`_ monitors getting burned by always showing the same image, hence **screensavers**. Though modern monitors are not susceptible to this issue, screensavers are still used for other purposes, such as offering a basic layer of security, by requiring a password to re-access the device. 

To check the screensaver configuration in our system, we can run::

    $ xset q
    ...
    Screen Saver:
        prefer blanking: yes    allow exposures: yes
        timeout:  600   cycle:  600
    ...

Which means that every 600 seconds (10 minutes) of iddle user interaction, the screen saver mechanism will be triggered, and it will be a **blank**, meaning just a black screen. 

.. note:: No idea where these initial default values are configured.

We can change the ``timeout`` and ``cycle`` values with::

    $ xset s 60 120

So the screensaver will start after 60 seconds, and cycle every 2 minutes. The first number is the ``length``, which specifies the length of time the server must be inactive for the screen saver to activate. The second number is the ``period``, which specifies the period in which the background pattern must be changed to avoid burn in. Both values are specified in seconds. If only one numerical parameter is given, it is read as a ``length`` parameter.


XScreenSaver (XSS) and login manager events and call out to an external screen locker in order to lock the screen. 






*   *   *

What we want to learn here is how to automatize:

* After 15 seconds (from the last time laptop was not idle), **dim** the screen to 1%.
* After 60 seconds, turn the screen off(blank?), without locking.
* After 120 seconds, **lock** the screen.
* After 600 seconds (10 min), **suspend**.


Media applications
==================
Most media player applications disable blanking and power saving while they are playing a video. To enable this functionality in MPlayer, start it with mplayer -stop-xscreensaver or add the line stop-xscreensaver=1 to your ~/.mplayer/config . By doing so, MPlayer will simulate user activity (just as if the user has moved the mouse) upon startup and then each 30 seconds. So this will prevent the screen from blanking or going to power saving mode if all your timeouts are larger than 30 seconds. Please note that the name of the flag is confusing, because its effect isn't related to screen savers, it just simulates user activity. If a screen saver is running, it may or may not pick up the simulated activity generated by MPlayer. To prevent screen saver activation in this case, you may have to run mplayer -heartbeat-cmd "xscreensaver-command -deactivate" or mplayer -heartbeat-cmd "gnome-screensaver-command -p" .

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Screensaver

.. _`desktop environment`: https://en.wikipedia.org/wiki/Desktop_environment
.. _`widget library`: https://en.wikipedia.org/wiki/Widget_toolkit
.. _`display manager`: https://en.wikipedia.org/wiki/X_display_manager
.. _`gtk`: https://en.wikipedia.org/wiki/GTK
.. _`XScreenSaver`: https://en.wikipedia.org/wiki/XScreenSaver
.. _`promotional article`: https://www.jwz.org/xscreensaver/versus-xlock.html

.. _`X.Org Server`: https://en.wikipedia.org/wiki/X.Org_Server
.. _`dwm`: https://en.wikipedia.org/wiki/Dwm


.. _`xss-lock`: https://bitbucket.org/raymonad/xss-lock/src/master/
.. _`xss-lock package`: https://packages.debian.org/bullseye/xss-lock
.. _`systemd`: https://systemd.io/
.. _`Energy Star`: https://en.wikipedia.org/wiki/GTK
.. _`X11 Screen Saver Extension`: https://www.x.org/releases/X11R7.7/doc/scrnsaverproto/saver.html
.. _`official site`: https://www.jwz.org/xscreensaver/
.. _`CRT`: https://en.wikipedia.org/wiki/Cathode_ray_tube
.. _`SVGA`: https://en.wikipedia.org/wiki/Super_VGA
.. _`VESA`: https://en.wikipedia.org/wiki/Video_Electronics_Standards_Association