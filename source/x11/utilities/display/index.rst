################
Screen utilities
################

.. toctree::
    :maxdepth: 3

    dpms
    screensaver
    screenlocker

In this section we'll cover utilities related with our monitor screen, such as:

* Screen-savers. 
* Screen-lockers. 
* **Display Power Management Signaling** (or `DPMS`_ for short) is a mechanism to turn off the display when the system is not being used, in order to save energy.

Each major Linux `desktop environment`_ includes their own builtin mechanism to keep our system safe from prying eyes. Usually, after a configurable period of inactivity, the wallpaper changes and optionally the system is locked. Whenever we want to start using the system again, we just have to move the mouse or press any key, which usually brings out the `display manager`_, where we can introduce our password to unlock the screen.

Each inactivity time to trigger each state is configurable. We may decide to:

* Show a wallpaper (or a screensaver) after 5 minutes of inactivity.
* Turn the screen black after 8 minutes.
* Lock it after 10 min.

Apart from privacy concerns, this whole thing is done to save up energy, either to extend our laptop's battery charge, or just to save up energy and protect our environment.

In any case, here we'll explore several minimalistic utilities to accomplish these sort of tasks.

.. _`DPMS`: https://en.wikipedia.org/wiki/VESA_Display_Power_Management_Signaling
.. _`desktop environment`: https://en.wikipedia.org/wiki/Desktop_environment
.. _`display manager`: https://en.wikipedia.org/wiki/X_display_manager
