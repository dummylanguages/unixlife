*************
Screen locker
*************
All main stream Linux desktop environment include a screen locker, which as its name implies, it's a program to lock the screen. The screen locker can be triggered in two ways:

* **Manually** through some icon, usually a lock.
* **Automatically**, after some inactivity period a different wallpaper shows up, covering the screen.

In these systems, the screen locker is meant to be used in conjunction with a `display manager`_, which pops up after the user moves the mouse or press some key. The whole enchilada is usually baked-in into the desktop environment and users take it for granted.

For users of window managers the situation is a bit differente. Althought some window managers include this functionality, such as `i3wm`_ which includes `i3lock`_, more minimalistic wms don't include one, and the user is in charge of getting one installed. This can be seen as a disadvantage, but also as an oportunity to learn how things are wired. 


Lots of options
===============
There are plenty of applications that follow the Unix philosophy of small, specialized tools that do just one thing, in this case locking the screen:

* `slock`_, from **Suckless tools**.
* `i3lock`_, from the people behind the `i3wm`_.
* `xsecurelock`_, created by someone who works at Google, but it's not a Google official project.

These are just three examples, but there are a lot more of them. They are really small tools that don't depend on any **display manager** (which in turn usually depends on some `widget library`_ such as `GTK`_) and that's a good thing, because lots of window managers usually don't include one either.

Whatever choice we make, they work basically in the same way. For example, let's install **i3lock**, almost as lightweight as **slock** but includes a couple more small nice features. Luckily is available in the Debian repositories, so we can install it with::

    $ sudo apt install i3lock

Once installed, we can launch it with::

    $ i3lock

We will see a white screen that can be unlocked by entering our password. A circle in the middle informs us about keys type and failed tries after pressing ``Enter``. If the white background is too dull for us, we can configure the color or even specify a **png image** using the ``-i`` option.

.. note:: Check ``man i3lock`` for more options.

Triggering the screen lock
==========================
In the last section we saw how to launch our screen locker from the command line, but the intended use of any screen locker, is to be lauched when something happens:

* Automatically, when our computer goes into sleep or suspend mode.
* Also automatically, when we close the lid or our laptop.
* Manually, when pressing a shortcut.

All of these are events that will trigger our screen locker. So we need a utility that listens for all these events, and calls the locker when any of them takes place. There are several utilities that can trigger our screen locker:

* `xss-lock`_.
* `xautolock`_.
* `xidle`_.

Note that these are **not** screen-lockers.

``xss-lock``
============
It's a quite old tool that's widely available in any distro repository. In Debian can be found in the `xss-lock package`_, that can be easily installed with::

    $ sudo apt install xss-lock

By default, xss-lock listens for two types of events:

* `systemd`_ events, such as suspend, hibernate, lock-session, and unlock-session.
* `DPMS`_ events.

We can select the action we want xss-lock to perform after any of these events:: 

* Run a screen-locker and wait for user to unlock.
* Kill locker. (don't know how to do that) 

Scripts for dimming the screen
------------------------------
``xss-lock`` includes a couple of scripts that are placed under ``/usr/lib/share/doc/xss-lock``:

* ``dim-screen.sh``
* ``dim-screen2.sh``

I made a copy and place it somewhere in my ``PATH``, and change their permissions to **executable**::

    $ chmod +x dim-screen.sh

These scripts are intended to be called by ``xss-lock`` and dim the screen for a while, before it's locked.

The screen dimming doesn't work
-------------------------------
When I tried the script included in xss-lock, the screen didn't dim. As it happens, it was some bug in Debian that broke the ``xbacklight`` command (used inside the script). I fixed the ``xbacklight`` command adding the following section in ``/etc/X11/xorg.d.conf``::

    Section "Device"
        Identifier  "Card0"
        Driver      "intel"
        Option      "Backlight"  "intel_backlight"
    EndSection

.. note:: The section above inside a file named ``10-monitor.conf`` located in the aforementioned folder.

After that the screen dimming was working as expected.

Launching the whole thing
-------------------------
To make everything work together I added the following to my ``~/.xinitrc`` file::

    xset +dpms s 300 300
    xss-lock -n dim-screen.sh -- i3lock -i ~/Pictures/wps/lock.png &

The ``xset`` command above does three things:

* Enable **dpms**.
* After five minutes(300 seconds) of inactivity the screen gets dimmed.
* Lock the screen five minutes later (after an overall of 600 seconds of inactivity).

Manually
--------
We should also be able of locking the screen with a shortcut. For that, I added the following in my ``~/.config/sxhkd/shxkdrc``::

    super + shift + s
        sleep 1; xset dpms force off

This forces the screen off and at the same time gets locked.

it's recommended to not launch ``xsecurelock`` directly, but to manually lock using .

.. note:: Before I tried other approach, using ``xset s activate``, which locked the screen showing the locked wallpaper for a while.

xsecurelock
===========
Another option was to use `xsecurelock`_, which also includes a script for dimming the script named ``dimmer`` under the ``/usr/libexec/xsecurelock`` folder. It works basically the same way as we've described, just added this to my ``.xinitrc``::

    xset +dpms s 300 600
    xss-lock -n /usr/libexec/xsecurelock/dimmer -l -- xsecurelock &

https://bbs.archlinux.org/viewtopic.php?id=176001
https://www.reddit.com/r/linuxquestions/comments/5pidyj/how_do_i_set_up_i3lock_xset_and_xsslock_to_work/
https://leahneukirchen.org/blog/archive/2020/01/x11-screen-locking-a-secure-and-modular-approach.html


https://github.com/google/xsecurelock/issues/92

Block tty access
================
To block tty access when in an X add the following to ``xorg.conf``::

    Section "ServerFlags"
        Option "DontVTSwitch" "True"
    EndSection

To prevent a user from killing the **X server** when it is running, we'll just have to add the following line to the same section of ``xorg.conf``::

    Section "ServerFlags"
        Option "DontZap"      "True"
    EndSection

.. note:: Read this for more infor: https://wiki.archlinux.org/index.php/Xorg#Block_TTY_access

Closing the lid (laptops)
=========================
Last thing I wanted to do was triggering the screen-locker when the laptop's lid was closed. I found a way to do it using a ``systemd`` service.


.. _`i3lock`: https://i3wm.org/i3lock/
.. _`i3wm`: https://i3wm.org/
.. _`slock`: https://tools.suckless.org/slock/
.. _`xsecurelock`: https://github.com/google/xsecurelock
.. _`display manager`: https://en.wikipedia.org/wiki/X_display_manager
.. _`widget library`: https://en.wikipedia.org/wiki/Widget_toolkit
.. _`GTK`: https://gtk.org/
.. _`xss-lock`: https://bitbucket.org/raymonad/xss-lock/src/master/
.. _`xss-lock package`: https://packages.debian.org/bullseye/xss-lock
.. _`xautolock`: https://github.com/l0b0/xautolock
.. _`systemd`: https://systemd.io/
.. _`DPMS`: https://en.wikipedia.org/wiki/VESA_Display_Power_Management_Signaling
.. _`X11 Screen Saver Extension`: https://www.x.org/releases/X11R7.7/doc/scrnsaverproto/saver.html
.. _`official site`: https://www.jwz.org/xscreensaver/
.. _`CRT`: https://en.wikipedia.org/wiki/Cathode_ray_tube
.. _`screen saver`: https://en.wikipedia.org/wiki/Screensaver
.. _`SVGA`: https://en.wikipedia.org/wiki/Super_VGA
.. _`VESA`: https://en.wikipedia.org/wiki/Video_Electronics_Standards_Association
.. _`xidle`: https://github.com/fgsch/xidle