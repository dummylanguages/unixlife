************
Installation
************
Installing Rofi manually it's a bit unconvenient due to the amount of dependencies. Fortunately, as its popularity increases, it's getting added to more distro repositories. I didn't have much trouble finding it in the repos of such a conservative distro as Debian, so installing it was a matter of::

    $ sudo apt install rofi

Adding a shortcut
=================
As we mentioned, Rofi is intended to be launched with a shortcut, so we have to create one using any of the methods available, such as:

* xbindkeys
* sxhkd
* Or the configuration file of your window manager.

I find convenient sxhkd, which can be used with several window managers, so I added the following shortcuts to my ``~/.config/sxhkd/sxhkdrd``::

    super + @space
            rofi -show drun -show-icons

    super + Tab
            rofi -show window

The ``-show`` option allows us to specify a mode to launch Rofi:

* The ``drun`` mode is for launching apps with ``.desktop`` files, such as Firefox and such. Note that here I'm using the ``-show-icons`` options, since these are programs which most of the time include an icon.
* The ``window`` mode allows us to select any of the programs currently running in a window.

Truth is, once Rofi is running we can **switch between modes** using ``Shift + Left/Right`` or ``Ctrl + Tab``.

.. _`Rofi`: https://github.com/davatorium/rofi
