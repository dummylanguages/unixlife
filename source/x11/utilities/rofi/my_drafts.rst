Rofi
====
An alternative to **dmenu** to launch apps is **rofi**. It has several advantages:

* It's way easier to configurate through configurations files.
* It actually does more things than simply launching programs.
* And as opossed to **dmenu**, it's well documented. 

What it does
============

* switch windows
* launch applications,
* launch .desktop applications,
* run ssh via ~/.ssh/config,
* run scripts

Installing in Arch linux
========================
Install it in Arch it's as easy as::

    $ sudo pacman -S rofi

And we get the last version ( ``rofi-1.5.2-1`` at the moment of writing this)

Installing in Debian
====================
Install it in Debian::

    $ sudo apt install rofi

The problem with this, it's that we are getting an outdated version, **1.3.0** in my case while the latest one was **1.5.2**. So we are gonna have to manually compile/install it. Let's download it first, uncompress and follow the instructions on github.

I had to go through a dependency hell to finally make install. Even had to add **sid** to my repos to satisfy a version of **check**:

    deb http://http.us.debian.org/debian/ sid non-free contrib main

Debian dependency hell. Started installed all the dependencies for rofi:

* libpango1.0-dev
* libcairo-5c0
* libcairo2-dev
* libglib2.0-0
* librsvg2-dev
* librsvg2-bin
* libstartup-notification0 
* libxkbcommon-dev
* libxkbcommon-x11-0
* libxkbcommon-x11-dev
* libstartup-notification0
* libstartup-notification0-dev
* libxcb1:
    * libxcb-xrm0
    * libxcb-xrm-dev
    * libxcb-xkb1
    * libxcb-xkb-dev
    * libxcb-randr0
    * libxcb-randr0-dev
    * libxcb-xinerama0
    * libxcb-xinerama0-dev
    * libxcb-xinerama0-dbg
* libxcb-util0
* xcb-util-wm:
    * libxcb-ewmh2
    * libxcb-ewmh-dev 
    * libxcb-icccm4
    * ibxcb-icccm4-dev
* xcb-util-xrm is actually named **libxcb-xrm-dev** in debian.

Launching it
============
Once installed we can try it out just typing the ``rofi`` command in our shell, it will propose us to choose a mode. That option was not available in an older version.

Let's add it to our **i3 configuration**::

    bindsym $mod+d exec rofi -show run

To launch it press ``mod + d``, and the rofi window will appear in **run** mode. To **switch modes** use ``Shit + arrow keys``. These **modes** are:

* **Run**, to launch apps.
* **Windows**, to switch windows.
* **SSH**, to quickly ``ssh`` into remote machines. Parses ``~/.ssh/config`` to find **hosts**.

But there are more modes:
* **Drun**, to launch apps with ``.desktop`` files, whick allow to show frequently used programs sorted on top and auto starting terminal applications in a terminal.
* search and launch an application from the _freedesktop.org_ Desktop Entries. These are used by most Desktop Environments to populate launchers and menus. Drun mode features:

    favorites list, with 


## Configuration file
There are 3 ways of setting configuration options:

1. Using command line options:

    $ rofi -show run

The above line looks quite nice, but setting options this way become unwieldy very fast. The ``-show`` option allows us to select the **mode** we want to launch rofi.

2. The ``~/.Xresources`` file. We can write the rofi configuration right there, or like I do for all my Xresources configurations, link the rofi file our ``~/.Xresources`` file::

    #include ``~/.config/rofi``

3. Keep it in ``~/.config/rofi/config``, our preferred method. This file also uses the Xresources format.

Configuration options
=====================
Rofi *may* ship with a **default configuration** file located in ``/etc/rofi.conf``. If it's not there, generating an example file with all the possible configuration settings it's easy::

    $ rofi -dump-xresources > ~/.config/rofi/example.config

Now we can modify this file until it suits our needs, or just keep it as reference.

Themes
======
Rofi comes packed with a lot of preinstalled themes. To **select** one of these themes, we just have to run the ``rofi-theme-selector``. We can launch it from rofi itself. Then we can:

* Hit ``Enter`` to previsualize the theme
* And ``Alt+a`` to set it up.

NOTE: It's important to have a configuration file, even if empty, so the theme selection will stick.

* We also can set up a theme using the ``-theme`` command line option:

    $ rofi -show run -theme Monokai

Customizing the themes
======================
Rofi comes packed with a pretty decent selection of themes that can be found in ``/usr/share/rofi/themes``. If we have in mind to do some theme customization, it's probably a good idea to leave the **default location** alone and make a copy of the themes in some other location in our $HOME. That way we will always have the defaults to go back in case our customizations go south.

By the way, in **Debian** the included themes are both in:

* /usr/local/share/rofi/themes/
* /usr/share/rofi/themes/

Whereas in **Arch**, only in:

* /usr/share/rofi/themes/

There are two **locations** where ``rofi-theme-selector`` will look for **user themes**:

1. In ``$XDG_DATA_HOME/rofi/themes``; by default ``$XDG_DATA_HOME``, is set to ``~/.local/share``. Meaning that, if we don't change that variable: ``~/.local/share/rofi/themes``.

2. Also in ``$XDG_CONFIG_HOME/rofi/themes``; ``$XDG_CONFIG_HOME`` defaults to ``~/.config``. So, by default, ``rofi-theme-selector`` will look also in ``~/.config/rofi/themes``. This is my favourite location, so let's make a copy to customize real good::
 
    $ sudo cp -r /usr/local/share/rofi/themes  ~/.config/rofi/themes

Once the themes are there, we can start tinkering with them. Rofi themes use the ``.rasi`` file extension. The format is similar to ``.css``, easily customizable.

NOTE: According to the **XDG Base Directory Specification**:

* If ``$XDG_CONFIG_HOME`` is either not set or empty, a **default** equal to ``$HOME/.config`` should be used.
* If ``$XDG_DATA_HOME`` is either not set or empty, a default equal to ``$HOME/.local/share`` should be used.

Rofi modes
==========
Rofi has several built-in modes, which may be further extended with scripts. The default modes are:


# desktop files
location?
