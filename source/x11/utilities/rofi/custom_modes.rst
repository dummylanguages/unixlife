************
Custom modes
************
`Rofi`_ supports the creation of custom modes using the ``script`` internal mode. This is not a mode we can switch to, instead it allows us to launch scripts as a custom mode using the general syntax:

    <name>:<script>

Let's say we have created a script named ``switch_workspace`` that allows our window manager to switch to another workspace. This is how we would integrate it in a set of modes::

    rofi -show "Workspaces:switch_workspace"

Or if we prefer, we would integrate it in a set of modes::

    rofi -modi "window,Workspaces:switch_workspace" -show Workspaces

The line above would launch Rofi in the custom mode **Workspaces** and also would allow us to switch to ``window`` mode as well.

Creating an example custom mode
===============================
Let's write a sample script, we'll save it with the name ``rofitest``::

    #!/usr/bin/env bash

    echo 'Option 1'
    echo 'Option 2'
    echo 'Option 3'

.. note:: Don't forget to place your script somewhere in the ``PATH`` and make it **executable**.

With that script in place, we can create a shortcut for a Rofi custom mode. We'll use `sxhkd`_, but there are other ways of setting up shortcuts::

    super + t
            rofi -show "Test Custom:rofitest"

.. note:: Don't forget to reload ``sxhkd`` after changes to the configuration.

Now, pressing the ``Super + t`` shortcut should launch Rofi in **Test Custom** mode, where we can select between any of the three options by hitting ``Enter``. Of course, nothing happens because we didn't implement anything in our script other than showing three option lines.

By the way, if you want to run your new custom mode on its own, meaning without having the option of switching to other modes, define the shortcut::

    super + t
            rofi -modi "Test Custom:rofitest" -show 'Test Custom'

Basic flow
==========
When we start launch Rofi as::

    rofi -modi your_mode:your_script -show your_mode
    
The following happens:

1. Rofi calls ``yourscript`` without any positional parameters.
2. Your script needs to write its choices to stdout, separated by line breaks (\n). 
3. Rofi shows these entries and allows the user to choose one.
4. Your script is called again with the chosen entry as its positional parameter.

Let's rewrite our script taken this flow into consideration::

    #!/usr/bin/env bash
 
    if [ -z $@ ]
    then
        echo 'Option 1'
        echo 'Option 2'
        echo 'Option 3'
    else
        echo "You chose $1"
    fi

Now when we hit the ``Super+t`` shortcut, this is the flow:

1. Rofi launches the ``rofitest`` script without any positional parameters, that's why the if statement runs.
2. Our script prints the choices to the user.
3. Rofi shows the choices.
4. When we select a choice and press ``Enter``, Rofi calls ``rofitest`` again passing as positional parameters whatever choice we made. Our script will run the else block, and prints the message inside Rofi menu.

Piping into a command
---------------------
What we did so far doesn't seem very useful, but hopefully was educational. Let's add a small modification:: 

    #!/usr/bin/env bash
 
    if [ -z $@ ]
    then
        echo 'Option 1'
        echo 'Option 2'
        echo 'Option 3'
    else
        echo "$1" | xclip -selection clipboard | rofi -dmenu
    fi

This time the user selection is piped int the ``xclip`` command which is also piped into the ``rofi -dmenu`` command. As a result, we have whatever option the user selected into our system clipboard.

Getting useful input
--------------------
Our script is not really useful offering options that go nowhere. What if we offer the user the list of files and directories of his/her $HOME directory::

    #!/usr/bin/env bash
 
    if [ -z $@ ]
    then
        ls -dp1 ~/*
    else
        echo "$1" | xclip -selection clipboard | rofi -dmenu
    fi

This time when the user selects one of the shown files or folders, its full path is placed into the system's clipboard.

Showing font icons
------------------
Let's say we want to find a way of copying icons from a cool font we've just downloaded, let's say `Font Awesome`_. Usually, once we unzip the file, there should be some folder with icon descriptions. In my case there was a folder named **metadata**, which contained files with information about the fonts. Two of these files named ``icons.json`` and ``icons.yml`` contained information about each and every icon.

I decided to parse the ``icons.json`` file using a popular command line utility named `jq`_. The official site offers a `manual`_ where we can learn about the filters we can use to extract the information we need.

1. After a few attempts I was able to extract the keys using the command::

    $ jq '. | keys' icons.json

Each key corresponds to a JSON object which represents a font icon.

2. I was interested in the Unicodes of each icon, so the following command did the trick::

    $ jq '. [] | {"label": .label, "unicode": .unicode}' icons.json

* In ``. []`` can be used to iterate over the values in an **array** or an **object**, in this case everything is wrapped in a big JSON object.
* We are piping the into a pair or curly braces, ``{}``, which is used to build objects (aka dictionaries, or hashes).
* Inside the object constructor we put the value pairs we need. We could have write this expression as a shortcut, ``{.label , .unicode}``, but the problem is that ``label`` is a keyword in ``jq``, so we have to write ``{"label": .label}`` when trying to look for the ``label`` key in the JSON file. Writing just ``{label}`` wouln't work.

3. On my third(figuratively speaking) try, I crafted the following command::

    $ jq '. [] | .label + "," + .unicode' icons.json
    ...
    "Piggy Bank,f4d3"
    "Pills,f484"
    ...

That's what everything I needed from ``jq``, so I put together a small script::

    #!/usr/bin/env bash

    local quoted_pairs
    local unicode
    local name
    quoted_pairs=$(jq --raw-output '. [] | .label + "," + .unicode' icons.json)

    for p in $quoted_pairs
    do
        temp="${p//\"}" # Clear the double-quotes
        name="${temp
        unicode=""
        pairs+="$name: $unicode"
    done

    if [[ -d "$HOME/.config/rofi" ]]
        echo "$pairs" > font_awesome_icons.txt
    else
        mkdir -p "$HOME/.config/rofi"
        echo "$pairs" > font_awesome_icons.txt
    fi

To show font icons using its Unicode codes in the terminal: echo $'\uf641', where ``f641`` is the Unicode code.

Rofi script
-----------
The last touch is done in the ``rofitest`` script::

    #!/usr/bin/env bash
 
    if [ -z $@ ]
    then
        cat $HOME/.config/rofi/font_awesome_icons
    else
        echo "$1" | xclip -selection clipboard | rofi -dmenu
    fi

.. _`Rofi`: https://github.com/davatorium/rofi
.. _`Suckless dmenu`: https://tools.suckless.org/dmenu/
.. _`sxhkd`: https://github.com/baskerville/sxhkd
.. _`Font Awesome`: https://fontawesome.com/
.. _`jq`: https://stedolan.github.io/jq/
.. _`manual`: https://stedolan.github.io/jq/manual/
