.. _rofi-label:

####
Rofi
####

.. toctree::
    :maxdepth: 3
    :hidden:

    installation
    basic_use
    configuration/index
    custom_modes
 

`Rofi`_ is a small application for the X Window System, intended to be launched with a keyboard shortcut. When we launch it, a small window pops up in the center of our screen, and from there we can select different things to do:

* Launch applications, even ones with ``.desktop`` files. 
* Switch between application windows.
* Run user scripts, which allows the user to extend Rofi to multiple tasks.

.. _`Rofi`: https://github.com/davatorium/rofi
