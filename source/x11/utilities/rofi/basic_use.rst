*********
Basic use
*********
Once `Rofi`_ is installed, we can launch it with whatever shortcuts are more convenient for us.

Available modes
===============
Let's say we have just launched it in ``drun`` mode, we can easily switch between the available modes using the **default shortcut** ``Ctrl+Tab``. These available modes are:

* The ``window`` mode allows us to select any of the programs currently running in a window.
* The ``drun`` mode is for launching apps with ``.desktop`` files, such as Firefox and such.
* The ``run`` mode is for launching utilities within our PATH, usually scripts or command line utilities.
* The ``ssh`` mode is for launching a secure shell connection.
* The ``combi`` mode...

``dmenu`` emulation
-------------------
We have to mention here the ``-dmenu`` option, which allows us to launch Rofi emulating the `Suckless dmenu`_. It's not exactly a **mode**, so don't try to launch it with the ``-show`` or ``-modi`` options.

Limiting the modes
==================
If we want to limit the number of available modes, we can use the ``-modi`` option. For example, if we want to limit the modes to ``run`` and ``ssh``, we would launch Rofi with the command::

    rofi -modi "run,ssh" -show run

That would launch Rofi in ``run`` mode, but would allow us to switch back and forth to ``ssh`` mode only. This approach is quite flexible since we can create several different shortcuts for a limited set of modes.

Selecting a theme
=================
Once we've launched Rofi, we can type the command ``rofi-theme-selector`` and press ``Enter``. We'll be presented with a list of preinstalled themes which we can:

* Preview, by hitting ``Enter`` on any of them.
* Set up as the **default theme**, pressing ``Alt-a`` on a selected theme.

To exit, press ``Escape`` as usual.

Custom modes
============
We can create custom modes using the ``script`` mode, which allows us to launch scripts as a custom mode using the general syntax:

    <name>:<script>

Let's say we have created a script named ``switch_workspace`` that allows our window manager to switch to another workspace. This is how we would integrate it in a set of modes::

    rofi -show "Workspaces:switch_workspace"

Or if we prefer, we would integrate it in a set of modes::

    rofi -modi "window,Workspaces:switch_workspace" -show Workspaces

The line above would launch Rofi in the custom mode **Workspaces** and also would allow us to switch to ``window`` mode as well.



.. _`Rofi`: https://github.com/davatorium/rofi
.. _`Suckless dmenu`: https://tools.suckless.org/dmenu/
