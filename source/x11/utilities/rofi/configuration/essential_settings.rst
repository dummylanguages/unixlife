******************
Essential settings
******************
Here we'll mention several settings to help you keep your sanity.

Default shell
=============
If when you start ``tmux``, some important keys such as **tab**, **escape** or the **arrow keys** send their raw keycodes, such as ``^[[A``, ``^[[B``, etc. The reason for that is that ``tmux`` may not be using your default shell, so we have to tell them to do so adding the following line to the config file::

    set-option -g default-shell /bin/bash

If your prompt doesn't look right either, that may be another clue pointing to this same issue. The setting above should solve it.

Changing prefix key
===================
Another essential setting for me is to modify the default :kbd:`Prefix` key to something more to our taste. In my case I chose to change it to :kbd:`Ctrl + Space`, so this is what I added to my config file::

    # Prefix key: Ctrl+Space
    unbind C-b
    set -g prefix C-Space
    bind Space send-prefix 

.. _`Tmux`: https://github.com/tmux/tmux