#############
Configuration
#############
.. toctree::
    :maxdepth: 3

    essential_settings


There are  three methods for setting `Rofi`_ configuration options:

1. **System configuration** is kept under ``/etc/rofi.conf``.
2. Using **Xresources**.
3. **User configuration** is in a file named ``~/.config/rofi/config``, compliant with the `XDG Base Directory Specification`_, which defines the ``$XDG_CONFIG_HOME`` as the base directory where user specific configuration files should be stored.
4. If our configuration needs are minimal, we can control Rofi's behaviour passing **command-line options** where we define the shortcuts that launch Rofi.

.. note:: ``$XDG_CONFIG_HOME`` equals to ``$HOME/.config``.

If the configuration folder wasn't created during installation, we can easily create it manually::

    $ mkdir ~/.config/rofi





.. _`Rofi`: https://github.com/davatorium/rofi
.. _`XDG Base Directory Specification`: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

