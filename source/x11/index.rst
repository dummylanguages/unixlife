###################
The X Window System
###################

.. toctree::
	:maxdepth: 3
	:hidden:

	X_components
	starting_X
	window_managers/index
	display_managers/index
	utilities/index
	clipboards
	xresources
	variables
	input_devices/index
	multimedia_keys/index

If you've finished a minimal install or your favourite distro (or maybe you are using a headless server) and you haven't installed any **Desktop Environment** or **Window Manager**, when you boot into linux you are faced with a simple **tty**. Text interfaces are cool and powerful, but GUIs are also useful and comfy and you want one. 

Regardless of your choice of **desktop environment** or **window manager**, they all need a **display server protocol** to run with. There are several protocols:

* The **X Window System** was created in MIT in 1984, and in September 1987 reached its 11th version, hence its other name **X11**. **Xorg** is just an implementation of this protocol and it's been the default X11 protocol in Debian since version 4.0, when it replaced **XFree86**.
* `Wayland`_ is a modern protocol which is being adopted by mainstream desktop environments.
* Mir was used by Ubuntu for a while
* Quartz

The `X Window System`_ (X11, or simply X) is the dominant `windowing system`_ on Unix-like operating systems, the other main alternative being `Wayland`_, which is a modern implementation that's being adopted as a default by several main-stream desktop environments.

Even if **X** is on its way out, a detailed study of it would provide valuable information that we can extrapolate to other windowing systems.

.. note:: A **windowing system** is a type of graphical user interface (GUI) which implements the `WIMP`_ paradigm for a user interface. WIMP stands for windows, icons, menus, pointer.



.. _`Wayland`: https://wayland.freedesktop.org/
.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`windowing system`: https://en.wikipedia.org/wiki/Windowing_system
.. _`WIMP`: https://en.wikipedia.org/wiki/WIMP_(computing)
