*********
Shortcuts
*********

Showing hidden files
--------------------
Imagine you are using vscode, and want to open your ``.bash_profile``, but since it's a **hidden file**, you can't see it in your home directory. From the open file window press:

    :kbd:`Command` + :kbd:`Shift` + :kbd:`.`

.. warning:: In recent **macOS** versions the shortcut has been changed to ``Command + FN + .``