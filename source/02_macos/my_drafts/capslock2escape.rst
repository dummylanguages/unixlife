Remapping my Caps lock key to Escape
====================================
In the old terminal days, the **Escape** was the further key to the left in the home row. But in modern keyboards, the escape key is not in our confort zone, that's why, following a popular trend, I decided to remap it. 
For this task, I'm gonna be using a couple of free apps from my main man `Takayama Fumihiko`_:

* `PCKeyboardHack`_
* `KeyRemap4MacBook`_


Disabling Caps lock key in OS X
-------------------------------
The first thing we have to do is disable the Caps Lock key in OS X. We can do that in `System Preferences` click in the `Keyboard icon` and in the bottom right, click the `Modifier Keys...` button. 

> Set Caps Lock to “No Action.”

Installing PCkeyboardHack
-------------------------
We need to download and install `PCKeyboardHack`_. This is a small application that install a kernel extension, nothing to worry about.

Once it's installed, we are gonna set our **Caps Lock** key to **keycode 53**. That's it, no need to restart or anything. Now we have a new **Escape key** more at hand.

Adding a Right Control key to my MacBook Air
--------------------------------------------
Another thing that comes in handy when using the graphic version of **MacVim**, is a second **Control key** for the right hand.

In the terminal version I just have to remap it in [Iterm][], but when I use **Gvim** outside the terminal, I lose that mapping.

We are gonna address that issue in the next point.

## Installing KeyRemap4MacBook
Once we download and install `KeyRemap4MacBook`_, we are gonna start adding our own settings. [Here][] you can find pretty clear instructions of how to do that.

Basicly, what we have to do is:
* Click on **Preferences**.
* Go to the tab **Misc & Uninstall**.
* Click on the **Open private.xml** button, that will open the file in your default text editor.

You can also find it in `Users/yourUserName/Library/Application Support/KeyRemap4MacBook`.

What do I have to write here? Easy, check the `private.xml Reference manual`_, it's pretty well documented.

## Using and external configuration file
What I did, is take advantage of the neat feature of using an external configuration file. To do that we just have to  specify where are we gonna keep our external file, adding the following lines to the **private.xml**::

	<?xml version="1.0"?>
	<root>
	<include path="/Users/yourUserName/Google Drive/core.xml" />
	</root>

As you can see, I like to keep mine in my **Google Drive folder**. You can keep it in your **Dropbox** or anywhere you like.

Adding a 2nd Control key
------------------------
Now we are gonna see the proper configuration. As I said, I'm gonna keep it in my **core.xml**, under my **Google Drive folder**. This is what my **core.xml** looks like::

	<?xml version="1.0"?>
	<root>

	<appdef>
	<appname>VIM</appname>
	<equal>org.vim.MacVim</equal>
	</appdef>

	<item>
		<name>Change Right Option key to Control ;) </name>
		
		<appendix>[Right Alt-Option] key mapped to [Right Control]</appendix>
		
		<identifier>private.app_vim_swap_option_r_control_r</identifier>
		
		<only>VIM</only>
		
		<autogen>__KeyToKey__ 
			KeyCode::OPTION_R, 
			KeyCode::CONTROL_R
		</autogen>
	</item>

	</root>

* Inside `<appdef>` we define the application we are gonna use the mappings:

	* Inside `<appname>` choose a name for your app.
	* In `<equal>` we have to write the 

* Inside `<item>` is where we create the desired mapping:

	* `<name>` Choose a descriptive name for your setting.
	* `<description>` This is optional, you can briefly describe your setting.
	* `<identifier>` Here we 
	* `<only>` This is gonna be a mapping only for Vim.
	* `<autogen>` Here we define the mapping.

.. _`Takayama Fumihiko`: https://pqrs.org/index.html.en			
.. _`KeyRemap4MacBook`:	 https://pqrs.org/macosx/keyremap4macbook/index.html.en			
.. _`PCKeyboardHack`:	 https://pqrs.org/macosx/keyremap4macbook/pckeyboardhack.html.en		
.. _`Here`:				 https://pqrs.org/macosx/keyremap4macbook/document.html.en#usage	
.. _`private.xml Reference manual`:	https://pqrs.org/macosx/keyremap4macbook/xml.html.en#examples
