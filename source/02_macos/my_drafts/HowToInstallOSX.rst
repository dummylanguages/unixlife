# Installing OS X
Here we are gonna describe two ways of make a new installation of OS X:

1. Starting the machine pressing **Option** key (**Alt**) and installing from internet.
2. Installing OS X from USB drive. I couldn't make this one work, after trying several times, there's always some error along the process.

## Installing OS X from Internet
Just boot your computer and pressing **Option** key (**Alt**) and follow instructions.
The only thing we have to do is configure the WIFI connection and all should be fairly simple. 

## Installing OS X from USB drive
These are the steps we must follow:

1. Format the pendrive.
2. Copy the new OSX to the pendrive.
3. Reboot the computer where you want to install OS X, with the pendrive plugged in.

## Format the pendrive
* Plug a pen with at least 8GB of capacity.
* Open **Disk Utilities** and select the **physical drive** (not any of its partitions), and format it (**erase**) with the **Mac OS Extended (journaled)** format.
* Go to the **Partition** tab, and make sure that in the **Partition Layout** drop-down menu, is selected the option **1 Partition**. Also **GUID Partition Table** should be selected as the partition scheme in the **Options** section, with its name set to **Untitled**.

## Copy the new OSX to the pendrive
* Make sure that the pendrive is plugged in, and that its name is **Untitled** (change it if it's not).
* Open the **terminal** and paste the following command:

```
sudo /Applications/Install\ OS\ X\ El\ Capitan.app/Contents/Resources/createinstallmedia --volume /Volumes/Untitled --applicationpath /Applications/Install\ OS\ X\ El\ Capitan.app --nointeraction
```

The following should also work, and you don't have to escape the space characters:
```
sudo "/Applications/Install OS X El Capitan.app/Contents/Resources/createinstallmedia" --volume /Volumes/Untitled --applicationpath "/Applications/Install OS X El Capitan.app" --nointeraction
```

This is the output of the above command:
```
Password:
Erasing Disk: 0%... 10%... 20%... 30%...100%...
Copying installer files to disk...
Copy complete.
Making disk bootable...
Copying boot files...
Copy complete.
Done.
```

Now, your pendrive should have been named as **Install OS X El Capitan**.

## Reboot the computer where you want to install OS X, with the pendrive plugged in.
As the fucking title says:

* Plug the pendrive (the one you used in the process described above) in the computer where you want to install OS X.
* Restart the machine and press the **Option** (Alt) key several times.
* After a while you'll be presented with a screen that offers several hard drives to choose from. So choose **Install OS X El Capitan**.
* Next is the screen **OS X Utilities**, choose install OS X and follow instructions.
* Select the disk where you want to install **El Capitan**, it should take about 15 minutes.
* From there it's all very simple, Accept and Continue all the time.

Once you have a fresh install of OS X, go make a backup of the system using **Time Machine**.
