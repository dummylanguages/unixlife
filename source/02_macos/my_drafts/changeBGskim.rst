How to change the page color to skim
====================================
`Skim`_ is is a PDF reader for OS X. To change the background color of the page of ``.pdf`` files we can run the command::

  $ defaults write -app skim SKPageBackgroundColor -array 0.99, 0.94, 0.64 1

Where the **4 values** passed correspond to the **3 RGB** colors plus an **alpha value**.

Getting the colors
------------------
To choose the color we can use any color picker, such as **colors**, an app developed by Matt Patenaude that we can download for free `here`_.

Converting the colors
---------------------
Since this app, delivers the colors in 0-255 scale, we have to convert it to 0-1 scale. We can do that easily with JavaScript. Open your browser's JavaScript console and run::

  // The colors we got originally
  var rgb = [253, 240, 164];

  // Transforming them to 0-1 scale
  var rgb2 = rgb.map(function (color) {
    return Number((color/255).toFixed(2));
  });

  console.log(rgb2);

Now we just have to use these values in the command we mentioned at the beginning.

## A Ruby version
We can also start an **irb** session and use this code::

  a = [253, 240, 164]
  a.map { |color| (color / 255.0).round(2) }


---
.. _`Skim`: http://skim-app.sourceforge.net/
.. _`here`: http://mattpatenaude.com/software/colors-1.9.zip
