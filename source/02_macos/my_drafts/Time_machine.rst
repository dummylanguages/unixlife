# How to create a backup copy of your OS X
Once you have booted up your fresh new OS X **El Capitan** these are the steps we must follow to create a backup copy of it:

1. Connect the external drive.
2. Open **Time Machine Preferences** and click on **Options**. Exclude items from backup copy until the size of the backup makes sense (about 14GB) and click **Save**.
3. Right click on the **Time Machine icon** on the title bar, and click **Backup Now**. The backup process will start and should be finished in about 10 min.

## How to restore a backup copy of OS X
Plug in the hard drive where the backup copy is, and restart the machine. When we hear the chime tune, press `Command + R` and you'll be presented with a menu screen. Choose the **Restore Backup** option.

Make sure you save all the files you may have in **Macintosh HD** to your **Data** partition (you should have one).
