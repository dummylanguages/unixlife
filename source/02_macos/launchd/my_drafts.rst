*******
launchd
*******
What is Launchd

The launchd Startup Process
===========================
When macOS boots, the first thing that start running is the **kernel**, a process named ``kernel_task`` with the **pid** number **1**. Right after it starts the ``launchd``, a process identified with the number 2, that's how important is ``launchd``.

.. image:: ../img/macos/launchd.png

1. As part of the **system initialization**, ``launchd`` reads the property list files found in:

    * /System/Library/LaunchDaemons/
    * /Library/LaunchDaemons/

And launches all of the processes described in those files. When the **system shuts down**, it sends a ``SIGTERM`` signal to all of the daemons that it started.

2. After, when a **user logs in**, ``launchd`` reads the property list files located in:

   * /System/Library/LaunchAgents
   * /Library/LaunchAgents
   * ~/Library/LaunchAgents/

When the user **logs out**, it sends a ``SIGTERM`` signal to all of the user agents that it started.




launchd replaces
----------------
https://en.wikipedia.org/wiki/Init
https://en.wikipedia.org/wiki/Cron
https://en.wikipedia.org/wiki/At_(command)
https://en.wikipedia.org/wiki/Inetd
https://en.wikipedia.org/wiki/Xinetd

launchd does emulate the old inetd (using various InetD compatibility keys), and actually has many of xinetd's function


https://en.wikipedia.org/wiki/Job_scheduler

Other useful links
------------------
* Wikipedia
* daemon

https://nathangrigg.com/2012/07/schedule-jobs-using-launchd
https://neverendingsecurity.wordpress.com/2015/02/06/mac-os-x-services-daemons-and-agents-where-you-can-find-them-and-how-to-disable-them/
https://www.macobserver.com/tmo/article/apples_changes_to_unix_can_infuriate_mystify_delight
http://sud0man.blogspot.com/2015/05/artefacts-for-mac-os-x.html?m=1
https://askubuntu.com/questions/192058/what-is-technical-difference-between-daemon-service-and-process

.. _`documentation archive`: https://developer.apple.com/library/archive/navigation/

.. _wikipedia: https://en.wikipedia.org/wiki/Launchd



.. _launchd.info: http://www.launchd.info/



Daemons and agents
------------------
In the Apple's `documentation archive`_ we can find the `Technical Note TN2083`_ (no longer updated), titled **Daemons and agents**. There in the **Daemonomicom** section, **background processes** are classified into two categories:

    * **Daemons**, system wide background processes.
    * **Agents**, which work on behalf of a specific user.

A **daemon** is a program that runs in the background as part of the overall system (that is, it is not tied to a particular user). A daemon cannot display any GUI; more specifically, it is not allowed to connect to the window server. A web server is the perfect example of a daemon.

An **agent** is a process that runs in the background on behalf of a particular user. Agents are useful because they can do things that daemons can't, like reliably access the user's home directory or connect to the window server.

The difference between an agent and a daemon is that an agent can display GUI if it wants to, while a daemon can't.

Another classification
----------------------
Also in the Apple's `documentation archive`_ we can find the `Daemons and Services Programming Guide`_ (also no longer updated). There, **background processes** are classified into 4 categories:

    Type     | Managed by launchd? | Run in which context? | Can present UI?
------------ | ------------------- | --------------------- | ---------------
Login item   | No[#f1]_            | User                  | Yes
XPC service  | Yes                 | User                  | No
Daemon       | Yes                 | System                | No
Launch Agent | Yes                 | User                  | Not recommended

.. [#f1] Actually **login items** are also started by the per-user instance of ``launchd``, but it does not take any actions to manage them.

Property list files
-------------------
To manage the different **daemons** and **agents** ``launchd`` uses a special type of configuration files named **property list files**. These files are written in XML format, and are structured the same way for both daemons and agents. We indicate whether they describe a daemon or agent by the directory where we place them.

.. note:: ``.plist`` files are Apple's standard configuration file format.

To describe the properties contained in these files we use XML keys. The man pages of ``launchd.plist`` contain the full list of keys that can be used.

Location of the plist files
---------------------------
The Apple's `terminal user guide`_ and the **man page of launchd.plist** explain where we can find these files:

            Folder             |                                 Usage
------------------------------ | ---------------------------------------------------------------------
~/Library/LaunchAgents/        | Per-user agents provided by the **user**.
/Library/LaunchAgents/         | Per-user agents provided by the **administrator**.
/Library/LaunchDaemons/        | System-wide daemons provided by the **administrator**.
/System/Library/LaunchAgents/  | Per-user agents provided by **OS X**.
/System/Library/LaunchDaemons/ | System-wide daemons provided by **OS X**.

That means that the first three directories listed above can be used by **users** and **administrators** to place their property list files.

The launchd Startup Process
---------------------------
When macOS boots, the first thing that start running is the **kernel**, a process named ``kernel_task`` with the **pid** number **1**. Right after it starts the ``launchd``, a process identified with the number 2, that's how important is ``launchd``.

.. image:: ../img/macos/launchd.png

1. As part of the **system initialization**, ``launchd`` reads the property list files found in:

    * /System/Library/LaunchDaemons/
    * /Library/LaunchDaemons/

And launches all of the processes described in those files. When the **system shuts down**, it sends a ``SIGTERM`` signal to all of the daemons that it started.

2. After, when a **user logs in**, ``launchd`` reads the property list files located in:

   * /System/Library/LaunchAgents
   * /Library/LaunchAgents
   * ~/Library/LaunchAgents/

When the user **logs out**, it sends a ``SIGTERM`` signal to all of the user agents that it started.

The ``mysqld`` daemon
---------------------
For example, by default, the **MySQL installation package** (DMG) on OS X installs a property list file named ``com.oracle.oss.mysql.mysqld.plist`` in the **/Library/LaunchDaemons** directory.

The name of the file is quite revealing in itself, since it follows a naming convention known as **reverse-domain notation** recommended by Apple:

    * Starts with the **domain name** of the company's in reverse, in this case ``com.oracle``.
    * Name of the program ``oss.mysql`` (**oss** stands for Open Source Software. In this case makes sense, because I had installed the community edition)
    * Name of the daemon ``msqld``.
    * These files always use with the ``.plist`` extension.

This are the contents of the file:

.. literalinclude:: ../code/com.oracle.oss.mysql.mysqld.plist
    :language: xml
    :linenos:

The ``launchctl`` command
-------------------------
We don’t interact with ``launchd`` directly; instead we use the ``launchctl`` command. This commands allows us to:
    * load
    * unload
    * start
    * stop

daemons and agents.

launchd replaces
----------------
https://en.wikipedia.org/wiki/Init
https://en.wikipedia.org/wiki/Cron
https://en.wikipedia.org/wiki/At_(command)
https://en.wikipedia.org/wiki/Inetd
https://en.wikipedia.org/wiki/Xinetd

launchd does emulate the old inetd (using various InetD compatibility keys), and actually has many of xinetd's function

Unix concepts
-------------
https://en.wikipedia.org/wiki/Job_(computing)
https://en.wikipedia.org/wiki/Job_scheduler

Other useful links
------------------
* `Wikipedia`_
* `daemon`_
https://nathangrigg.com/2012/07/schedule-jobs-using-launchd
https://neverendingsecurity.wordpress.com/2015/02/06/mac-os-x-services-daemons-and-agents-where-you-can-find-them-and-how-to-disable-them/
https://www.macobserver.com/tmo/article/apples_changes_to_unix_can_infuriate_mystify_delight
http://sud0man.blogspot.com/2015/05/artefacts-for-mac-os-x.html?m=1
https://askubuntu.com/questions/192058/what-is-technical-difference-between-daemon-service-and-process

.. _`documentation archive`: https://developer.apple.com/library/archive/navigation/

.. _wikipedia: https://en.wikipedia.org/wiki/Launchd

.. _Daemons and Agents: https://developer.apple.com/library/archive/technotes/tn2083/_index.html#//apple_ref/doc/uid/DTS10003794-CH1-SECTION1

.. _Daemons and Services Programming Guide: https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/Introduction.html

.. _launchd.info: http://www.launchd.info/