*******************
Property list files
*******************
To manage the different **daemons** and **agents** ``launchd`` uses a special type of configuration files named **property list files**. These files are written in XML format, and are structured the same way for both daemons and agents. We indicate whether they describe a daemon or agent by the directory where we place them.

.. note:: ``.plist`` files are Apple's standard configuration file format.

To describe the properties contained in these files we use XML keys. The man pages of ``launchd.plist`` contain the full list of keys that can be used.

Location of the plist files
===========================
The Apple's `terminal user guide`_ and the **man page of launchd.plist** explain where we can find these files:

+--------------------------------+--------------------------------------------------------+
| Folder                         | Usage                                                  |
+================================+========================================================+
| ~/Library/LaunchAgents/        | Per-user agents provided by the **user**.              |
+--------------------------------+--------------------------------------------------------+
| /Library/LaunchAgents/         | Per-user agents provided by the **administrator**.     |
+--------------------------------+--------------------------------------------------------+
| /Library/LaunchDaemons/        | System-wide daemons provided by the **administrator**. |
+--------------------------------+--------------------------------------------------------+
| /System/Library/LaunchAgents/  | Per-user agents provided by **OS X**.                  |
+--------------------------------+--------------------------------------------------------+
| /System/Library/LaunchDaemons/ | System-wide daemons provided by **OS X**.              |
+--------------------------------+--------------------------------------------------------+

That means that the first three directories listed above can be used by **users** and **administrators** to place their property list files.

The ``mysqld`` daemon
=====================
For example, by default, the **MySQL installation package** (DMG) on OS X installs a property list file named ``com.oracle.oss.mysql.mysqld.plist`` in the **/Library/LaunchDaemons** directory.

The name of the file is quite revealing in itself, since it follows a naming convention known as **reverse-domain notation** recommended by Apple:

* Starts with the **domain name** of the company's in reverse, in this case ``com.oracle``.
* Name of the program ``oss.mysql`` (**oss** stands for Open Source Software. In this case makes sense, because I had installed the community edition)
* Name of the daemon ``msqld``.
* These files always use with the ``.plist`` extension.

This are the contents of the file:

.. literalinclude:: /_code/macos/com.oracle.oss.mysql.mysqld.plist
    :language: xml
    :linenos:



Other useful links
------------------
See the following:

* `Wikipedia`_
* `daemon`_

https://nathangrigg.com/2012/07/schedule-jobs-using-launchd
https://neverendingsecurity.wordpress.com/2015/02/06/mac-os-x-services-daemons-and-agents-where-you-can-find-them-and-how-to-disable-them/
https://www.macobserver.com/tmo/article/apples_changes_to_unix_can_infuriate_mystify_delight
http://sud0man.blogspot.com/2015/05/artefacts-for-mac-os-x.html?m=1
https://askubuntu.com/questions/192058/what-is-technical-difference-between-daemon-service-and-process



.. _`documentation archive`: https://developer.apple.com/library/archive/navigation/

.. _wikipedia: https://en.wikipedia.org/wiki/Launchd
.. _daemon: https://en.wikipedia.org/wiki/Daemon_(computing)

.. _Daemons and Agents: https://developer.apple.com/library/archive/technotes/tn2083/_index.html#//apple_ref/doc/uid/DTS10003794-CH1-SECTION1

.. _Daemons and Services Programming Guide: https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/Introduction.html

.. _launchd.info: http://www.launchd.info/

.. _terminal user guide: https://support.apple.com/guide/terminal/script-management-with-launchd-apdc6c1077b-5d5d-4d35-9c19-60f2397b2369/mac
