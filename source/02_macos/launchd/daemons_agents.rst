******************
Daemons and agents
******************
In the Apple's `documentation archive`_ we can find the `Technical Note TN2083`_ (no longer updated), titled **Daemons and agents**. There in the **Daemonomicom** section, **background processes** are classified into two categories:

    * **Daemons**, system wide background processes.
    * **Agents**, which work on behalf of a specific user.

A **daemon** is a program that runs in the background as part of the overall system (that is, it is not tied to a particular user). A daemon cannot display any GUI; more specifically, it is not allowed to connect to the window server. A web server is the perfect example of a daemon.

An **agent** is a process that runs in the background on behalf of a particular user. Agents are useful because they can do things that daemons can't, like reliably access the user's home directory or connect to the window server.

The difference between an agent and a daemon is that an agent can display GUI if it wants to, while a daemon can't.

Another classification
----------------------
Also in the Apple's `documentation archive`_ we can find the `Daemons and Services Programming Guide`_ (also no longer updated). There, **background processes** are classified into 4 categories:

+--------------+---------------------+-----------------------+-----------------+
|     Type     | Managed by launchd? | Run in which context? | Can present UI? |
+==============+=====================+=======================+=================+
| Login item   | No [#f1]_           | User                  | Yes             |
+--------------+---------------------+-----------------------+-----------------+
| XPC service  | Yes                 | User                  | No              |
+--------------+---------------------+-----------------------+-----------------+
| Daemon       | Yes                 | System                | No              |
+--------------+---------------------+-----------------------+-----------------+
| Launch Agent | Yes                 | User                  | Not recommended |
+--------------+---------------------+-----------------------+-----------------+

.. [#f1] Actually **login items** are also started by the per-user instance of ``launchd``, but it does not take any actions to manage them.

.. _Technical Note TN2083: https://developer.apple.com/library/archive/technotes/tn2083/_index.html
.. _documentation archive: https://developer.apple.com/library/archive/navigation/#section=Platforms&topic=macOS
.. _Daemons and Agents: https://developer.apple.com/library/archive/technotes/tn2083/_index.html#//apple_ref/doc/uid/DTS10003794-CH1-SECTION1
.. _Daemons and Services Programming Guide: https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/Introduction.html