*******
launchd
*******
What is Launchd

The launchd Startup Process
===========================
When macOS boots, the first thing that start running is the **kernel**, a process named ``kernel_task`` with the **pid** number **1**. Right after it starts the ``launchd``, a process identified with the number 2, that's how important is ``launchd``.

.. image:: /_images/macos/launchd.png

1. As part of the **system initialization**, ``launchd`` reads the property list files found in:

    * /System/Library/LaunchDaemons/
    * /Library/LaunchDaemons/

And launches all of the processes described in those files. When the **system shuts down**, it sends a ``SIGTERM`` signal to all of the daemons that it started.

2. After, when a **user logs in**, ``launchd`` reads the property list files located in:

   * /System/Library/LaunchAgents
   * /Library/LaunchAgents
   * ~/Library/LaunchAgents/

When the user **logs out**, it sends a ``SIGTERM`` signal to all of the user agents that it started.

The ``mysqld`` daemon
---------------------
For example, by default, the **MySQL installation package** (DMG) on OS X installs a property list file named ``com.oracle.oss.mysql.mysqld.plist`` in the **/Library/LaunchDaemons** directory.

The name of the file is quite revealing in itself, since it follows a naming convention known as **reverse-domain notation** recommended by Apple:

    * Starts with the **domain name** of the company's in reverse, in this case ``com.oracle``.
    * Name of the program ``oss.mysql`` (**oss** stands for Open Source Software. In this case makes sense, because I had installed the community edition)
    * Name of the daemon ``msqld``.
    * These files always use with the ``.plist`` extension.

This are the contents of the file:

.. literalinclude:: /_code/macos/com.oracle.oss.mysql.mysqld.plist
    :language: xml
    :linenos:



launchd replaces
----------------
https://en.wikipedia.org/wiki/Init
https://en.wikipedia.org/wiki/Cron
https://en.wikipedia.org/wiki/At_(command)
https://en.wikipedia.org/wiki/Inetd
https://en.wikipedia.org/wiki/Xinetd

launchd does emulate the old inetd (using various InetD compatibility keys), and actually has many of xinetd's function


https://en.wikipedia.org/wiki/Job_scheduler

Other useful links
------------------
* Wikipedia
* daemon

https://nathangrigg.com/2012/07/schedule-jobs-using-launchd
https://neverendingsecurity.wordpress.com/2015/02/06/mac-os-x-services-daemons-and-agents-where-you-can-find-them-and-how-to-disable-them/
https://www.macobserver.com/tmo/article/apples_changes_to_unix_can_infuriate_mystify_delight
http://sud0man.blogspot.com/2015/05/artefacts-for-mac-os-x.html?m=1
https://askubuntu.com/questions/192058/what-is-technical-difference-between-daemon-service-and-process

.. _`documentation archive`: https://developer.apple.com/library/archive/navigation/

.. _wikipedia: https://en.wikipedia.org/wiki/Launchd



.. _launchd.info: http://www.launchd.info/


