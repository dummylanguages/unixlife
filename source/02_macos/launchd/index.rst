.. toctree::
    :maxdepth: 3
    :hidden:

    daemons_agents
    launchd
    launchctl
    plist

According to its **man page**, ``launchd`` is the system wide and per-user daemon/agent manager. The Apple's online `terminal user guide`_ , also says that it can be used to run your **shell scripts**. Here we'll shed some light about it, but before, we have to explain some basic concepts.

.. _terminal user guide: https://support.apple.com/guide/terminal/script-management-with-launchd-apdc6c1077b-5d5d-4d35-9c19-60f2397b2369/mac
