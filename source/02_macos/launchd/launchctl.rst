*************************
The ``launchctl`` command
*************************
We don’t interact with ``launchd`` directly; instead we use the ``launchctl`` command. This commands allows us to:

* load
* unload
* start
* stop

daemons and agents.
