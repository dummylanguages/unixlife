#####
macOS
#####

.. toctree::
   :maxdepth: 3
   :hidden:

   launchd/index
   package_managers/index
   shortcuts

Here I'll keep notes about `macOS`_ covering a bit of everything.

 