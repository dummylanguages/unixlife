################
Package managers
################

.. toctree::
    :maxdepth: 3
    :hidden:

    homebrew
    cask

A lot of **UNIX** users, prefer to install new software directly from **source code**. That implies configuring, compiling and installing. 

That's a lot of work, and software authors prefer distribute their programs in **packages**. These packages are compressed files, that include the source code already compiled, and also libraries, documentation and stuff like that in some cases. 

But manually installing these packages still would be a lot of work. During the installation, you may need to resolve some dependencies, meaning installing some needed libraries and stuff like that.

That's why, there are different type of packages, made for different **package managers**. These are programs that make easy the installation, update and uninstallation of packages. They also may throw warnings during the installation process, telling us about some **unresolved dependency**,  and resolve it for you.

.. _terminal user guide: https://support.apple.com/guide/terminal/script-management-with-launchd-apdc6c1077b-5d5d-4d35-9c19-60f2397b2369/mac
