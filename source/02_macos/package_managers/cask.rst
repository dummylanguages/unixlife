*************
Homebrew Cask
*************
`Homebrew Cask`_ is an **extension** of `Homebrew`_ for the installation and management of **GUI applications** for macOS using the command line.

.. warning:: This extension comes already **preinstalled** with **Homebrew**.

So for example, if we wanted to install **Google Chrome**, instead of open the Google's site on our browser, download the installer, and double-click or drag to the applications folder to install it, using the **cask** extension we would just run::

    $ brew install --cask google-chrome

As you can see, using the extension is just a matter of adding the ``--cask`` option to the normal Homebrew commands.

.. note:: The ``--cask`` option must go after the ``brew`` command **and** any of its **subcommands**, such as ``install`` in the example above.

For a **complete list of available casks**, visit https://formulae.brew.sh/cask/, or using the command line::

    $ brew search casks


.. _`Homebrew Cask`: https://github.com/Homebrew/homebrew-cask
.. _`Homebrew`: https://brew.sh
		
