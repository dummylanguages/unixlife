********
Homebrew
********
`Homebrew`_ is a is a package management system that simplifies the installation of software on **Mac OS X**. 

Package managers in macOS
--------------------------
Before `Homebrew`_, there were a couple of popular package managers for macOS, `Fink`_ and `Macports`_, but not everybody was comfortable with them.

That's why `Max Howell`_ decided to create his own open-source **package manager**, and he built it in **Ruby** and since then, **homebrew** has gained huge popularity. In 2012  had the largest number of new contributors on GitHub. In 2013, had both the largest number of contributors and issues closed of any project on Github. 

One characteristic feature of **homebrew** is the installation of all the files of every **formula** under a **single directory**, and then send **symlinks** to the proper directories. 

.. note:: A **formula** is a package created with **homebrew**.

Installing Homebrew
-------------------
Instructions for installing it are very easy. Last time I checked it was a matter of copy-paste the line::

    $ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

.. note:: If you don't have the **Command Line Tools** installed, the system is gonna prompt you to install them. Go ahead and run the line above again

This line, will run a script explaining the course of the installation and giving you options. By default, **homebrew** gets installed in `/usr/local/bin`, but during the installation, you can change its location if you want.

Homebrew commands
-----------------
Let's see some useful commands for managing Homebrew:

* Homebrew's self-diagnose::

    $ brew doctor

If everything is ok, after running this command we will get an answer like::

    Your system is ready to brew.

* Let's update our **formulae**::

    $ brew update

* I want to see how many packages I installed with homebrew::

    $ brew list

* Search for some formulae, **markdown** for example::

    $ brew search markdown

The output of this command, will show us the available packages::

    markdown	multimarkdown	peg-markdown

By the way, you can also go to `Braumeister`_ and browse online all the available **formulae**.

* To install one of the list, let's say **multimarkdown**::

    $ brew install markdown

* If after some experimenting we are not happy with **multimarkdown**::

    $ brew uninstall markdown

Above I just wrote the bare essentials of **homebrew's** commands. There is a lot more, to know about them::

    $ man brew

Location of our installed formulae
----------------------------------
As we said at the beginning, **homebrew** uses a single directory for your packages. By default, all of your **formulae** are installed in ``/usr/local/Cellar``.

.. _`Homebrew`: https://brew.sh
.. _`Fink`:		http://www.finkproject.org/		
.. _`Macports`:	http://www.macports.org/		
.. _`Max Howell`:	http://mxcl.github.io/		
.. _`Braumeister`:	http://braumeister.org/			
