******************
Running containers
******************
Running a container is synonymous with **creating a container** and starting it right after. To create a container we use::

    $ docker container run [OPTIONS] IMAGE [COMMAND] [ARG...]

The **name of the image** from which the container should be created is the only **required argument** for this command. If the image is not present on the local system, it is pulled from the registry.

Run the Container in the foreground
===================================
By default, when no option is provided, containers are created to be run in the **foreground**, for example::

    $ docker container run nginx

But that may be inconvenient for two reasons:

* It would take up our interactive session.
* Once we close the session, the process will die with it.

.. note:: To stop a container running in the **foreground** just kill the process using ``Ctrl + c``.

Run the Container in Detached Mode
==================================
If we need to run servers, databases and these types of containers it's a better idea to use the ``--detach`` option so the container runs as a background process and won't die when our interactive session does.

    $ docker container run -d nginx

Run a container in interactive mode
===================================
If we need an interactive session inside the container we need to run it with the ``-it`` options::

    $ docker container run -it --name ubuntu_cont --hostname magic-box ubuntu_img

As you can see, we've also used the ``--hostname`` option, which allows us to set up a hostname for the container.

Sharing Data
============
We can create containers that share a filesystem with the **host** and with other **containers**. To do that we use the option ``--volume`` option. This option takes the following argument:

    host_src:cont_dest:options

Where:

* ``host_src`` is the absolute path to a file or directory on the host.
* ``cont_dest`` is an absolute path to a file or directory on the container.
* Options can be ``rw`` (read-write) and ``ro`` (read-only).

For example, to share the ``~/Public`` folder in the **host** as ``macOS`` in the container we do::

    $ docker container run -it --name ubuntu_cont -v ~/Public:~/macOS:rw ubuntu_img

We have specified the ``rw`` option, but only for demonstration purposes, since that is the default when no option is specified.

Publishing Container Ports
==========================
By default, if no ports are published, the process running in the container is accessible only from inside the container. Publishing ports means mapping container ports to the host machine ports so that the ports are available to services outside of Docker.

To publish a port use the ``--publish`` option as follows::

    -p host_ip:host_port:container_port/protocol

It should be self-explanatory, for example, to map the ``TCP`` port **80** (nginx) in the container to port **8080** on the host localhost interface, we would run:

    $ docker container run -it -p 8080:80 ubuntu_img

We can verify that the container's port is accessible in our **host** by pointing our browser to ``http://localhost:8080``.

.. _`Docker container`: https://docs.docker.com/glossary/#container
