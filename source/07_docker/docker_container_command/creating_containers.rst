*******************
Creating containers
*******************
Most of the people will use ``docker container run`` when creating containers. This command also starts the container after it's created. We prefer to explain **creating** and **starting** containers separately, to get a better idea of what's going on when using the different available options.

General form
============
To **create** a container we use::

    $ docker container run [OPTIONS] IMAGE [COMMAND] [ARG...]

The **name of the image** from which the container should be created is the only **required argument** for this command. If the image is not present on the local system, it is pulled from the registry.

.. note:: There's also available the old syntax `docker create`_, but the options are the same.

Options
=======
Regarding the **options** there are a lot of them, so check the official documentation about the `docker container create`_ command.

Interactive mode
----------------
If we need an interactive session inside the container we need to create it with the ``-it`` options. These are two options that are usually combined:

* ``--interactive`` means that the container will keep the standard input open even if we are not attached.
* ``--tty`` is used to allocate a pseudo tty in our container.

For example::

    $ docker container create -it --name ubuntu_cont --hostname magic-box ubuntu_img

Setting a hostname
------------------
We can also use the ``--hostname`` option, which allows us to set up a **hostname** for the container::

    $ docker container create -it --name ubuntu_cont -h magic-box ubuntu_img

Here we've chosen ``magic-box`` as the **hostname**, if we don't use this option, the container will use the **container id number** as a hostname.

Sharing Data
------------
We can create containers that share a filesystem with the **host** and with other **containers**. To do that we use the option ``--volume`` option. This option takes the following argument:

    host_src:container_dest:options

Where:

* ``host_src`` is the absolue path to a file or directory on the host.
* ``container_dest`` is an **absolute path** to a file or directory on the container.
* Options can be ``rw`` (read-write) and ``ro`` (read-only).

We don't have to create the directory in the container, it's automatically created and mounted for us at whatever we specify.

By default, `Docker Desktop`_ only allows us to share some default directories:

* /Users
* /Volumes
* /private
* /tmp

But we can go into preferences and add a new one, ``~/Public`` for example. Now we can share this folder in the **host** as ``macOS`` in the container we do::

    $ docker container create -it --name ubuntu_cont -v ~/Public:/home/bob/macOS:rw ubuntu_img

We have used the ``rw`` option, but only for demonstration purposes, since that is the default when no option is specified.

.. warning:: When specifying the paths, careful with ``~`` (tilde expansion). We are using it for the **host** because it will expand to the value ``$HOME`` in our **host**, but we don't use it(or any other expansion) when setting the path to the folder in the container.

Publishing Container Ports
==========================
By default, if no ports are published, the process running in the container is accessible only from inside the container. Publishing ports means mapping container ports to the host machine ports so that the ports are available to services outside of Docker.

To publish a port use the ``--publish`` option as follows::

    -p host_ip:host_port:container_port/protocol

It should be self-explanatory, for example, to map the ``TCP`` port **80** (nginx) in the container to port **8080** on the host localhost interface, we would run:

    $ docker container create -it -p 8080:80 ubuntu_img

We can verify that the container's port is accessible in our **host** by pointing our browser to ``http://localhost:8080``.

.. _`Docker container`: https://docs.docker.com/glossary/#container
.. _`docker create`: https://docs.docker.com/engine/reference/commandline/create/
.. _`docker container create`: https://docs.docker.com/engine/reference/commandline/container_create/
.. _`Docker Desktop`: https://docs.docker.com/desktop/
