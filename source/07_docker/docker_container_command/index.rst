################################
The ``docker container`` command
################################

.. toctree::
	:maxdepth: 3
	:hidden:

	creating_containers
	starting_containers
	running_containers
	draft


According to the docker docs, a `Docker container`_ is a runtime instance of a docker image. In this section we're gonna see Docker commands that relate to containers.

Before version 1.13, the Docker CLI allowed a shortened version of these commands, for example ``docker run``, instead of ``docker container run``. The **old syntax** is still supported but users are encouraged to use the newer and more verbose one.

For more information check the official documentation about the `docker container command`_.

.. _`Docker container`: https://docs.docker.com/glossary/#container
.. _`docker container command`: https://docs.docker.com/engine/reference/commandline/container/
