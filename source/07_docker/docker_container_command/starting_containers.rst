*******************
Starting containers
*******************
To start one or more stopped containers run::

    $ docker container start [OPTIONS] CONTAINER [CONTAINER...]

Use ``stop`` to stop one or more running containers.

Attaching to a running container
================================
Attach local standard input, output, and error streams to a running container. If we are running a container and for some reason close the terminal emulator, we can always open it again and ``attach`` to the container again::

    $ docker container attach ooboo

In the command above ``oobboo`` is the name of our container.

Exporting a container
=====================
If we need to move to another host, we can easily carry our containers with us. We just have to export them first::

    $ docker container export ubuntu_cont -o ubuntu_exported

Here we are using the ``--output`` option, followed by a name for the exported container file. We don't have to use this option if we want to pipe into a compression tool such as::

    $ docker container export ubuntu_cont |  gzip > ubuntu_cont.tar.gz

Removing containers
===================
To remove one or more containers::

    $ docker container rm ooboo

The command above removes a container named ``ooboo``. We can **force** the removal of a **running** container using the ``--force`` options. (uses SIGKILL)

To remove all **stopped** containers run::

    $ docker container prune

.. _`docker ps`: https://docs.docker.com/engine/reference/commandline/ps/
.. _`docker container ls`: https://docs.docker.com/engine/reference/commandline/container_ls/
