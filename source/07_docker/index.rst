######
Docker
######

.. toctree::
	:maxdepth: 3
	:hidden:

	installing_docker
	hello_world
	ubuntu_playground/index
	dockerfiles
	docker_container_command/index
	docker-images

`Docker`_ is a platform that allows to run applications inside containers. A `docker container`_ is an isolated environment that includes all the parts the application needs to run, from libraries to the runtime environment.

.. figure:: /_images/docker/docker.png
	:scale: 90 %
	:align: center

Once we have installed the the Docker platform, the basic steps to run a container are:

1. Writing a `dockerfile`_.
2. Build an `docker image`_ based on the dockerfile.
3. Run the **container** based on the image.

But if we don't want to build our own image, we can **download** a pre-built image from `Docker hub`_. Docker hub is an online repository with thousands of images built by the community around Docker.

.. figure:: /_images/docker/dockerhub.png
	:scale: 75 %
	:align: center

In the next sections we'll explain how to:

* Write a **Dockerfile** (the text document that contains all the commands necessary to build a Docker image).
* Build a **Docker image** (a read-only template that defines how the container will run). The image contains the code of our app, and all the libraries and dependencies our code needs to run. 
* Run a **Docker container** (a runtime instance of a `docker image`_).

The practical reason to use Docker as a software development platform, it's that it provides a consistent development environment for teams. As long as everybody uses the same docker image, they will have available the same system libraries, and the same language runtime no matter the host machine they are using to actually develop.

They don't have to install and manage different versions of a library or runtime environment in order to comply with the needs of different projects they're working on. They can also develop using their favourite code editor or IDE.

.. _`Docker`: https://www.docker.com/
.. _`dockerfile`: https://docs.docker.com/glossary/#dockerfile
.. _`docker container`: https://docs.docker.com/glossary/#container
.. _`docker image`: https://docs.docker.com/glossary/#image
.. _`Docker hub`: https://hub.docker.com/