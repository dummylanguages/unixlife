*************
Docker images
*************


Location in macOS
=================
Since Docker is a Linux application, it is not natively compatible with macOS. For that reason, Docker runs in macOS inside a lightweight **Linux virtual machine**, managed by a hypervisor (virtual machine manager) named `Hyperkit`_.

That means that the images are stored inside the virtual machine::

    ~/Library/Containers/com.docker.docker/Data/vms/0/data

But this location can be easily changed using the graphical interface of `Docker Desktop`_.

In Windows the situation is quite similar, and the images are usually placed in C:\ProgramData\DockerDesktop.


.. _`Hyperkit`: https://github.com/moby/hyperkit
.. _`Docker Desktop`: https://www.docker.com/products/docker-desktop
