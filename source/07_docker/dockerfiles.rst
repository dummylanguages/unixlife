***********
Dockerfiles
***********
Writing a `dockerfile`_ is the first step to containerizing an application. A Dockerfile is a text document that contains all the commands necessary to build a Docker image.

.. note:: You can think of these Dockerfile commands as a step-by-step recipe on how to build up your image.

Once the dockerfile is done, we can **build the image** using the ``docker build`` command, but before that, let's see a small preliminary step: create a directory to work in.

A build directory
=================
When you run the ``docker build`` command, all the contents of the **current working directory** (files and directories) are sent to the Docker daemon as the **build context**.

.. note:: By default, the **Dockerfile** is assumed to be located here, but you can specify a different location with the file flag ``-f``.

We could also place the **source code** for our app in some subdirectory under the build directory, just to keep things organized. Later we'll see `a couple of different ways`_ of making the source code available to the container.

Writing the Dockerfile
======================
Inside your build directory, create a text file and put the following two lines in it::

    FROM scratch
    RUN echo "hello world"

Now we'll use this Dockerfile to build our image::

    $ docker build .

If you get the error::

    Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?

Make sure you have Docker running.

.. _`Dockerfile`: https://docs.docker.com/glossary/#dockerfile
.. _`a couple of different ways`: https://hackernoon.com/how-to-move-code-into-a-docker-container-ab28edcc2901
