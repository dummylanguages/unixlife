*****************
Installing Docker
*****************
Let's go over how to install Docker in macOS and Linux.

In macOS
========
In **macOS** there are two ways of running Docker:

1. Installing the old `Docker Toolbox`_.
2. Installing the new `Docker Desktop`_.

To install the **Docker Desktop** we just have to download a **dmg** file from `Docker Hub`_. They offer a `stable`_ as well as a `edge`_ version.

In Linux
========
In **Linux systems** it's also quite easy to install Docker, since most distributions offer a packaged version of Docker called `Docker Engine`_. As opposed to Docker Desktop, when using Docker Enginge we interact with Docker through the command-line.

.. note:: **Docker Hub** offers several editions of **Docker Engine** for the `most popular Linux distributions`_, unfortunately, Arch Linux is not amongst them.

To install Docker on Arch we just have to run::

    $ sudo pacman -S docker

The `docker`_ package includes the **Docker daemon** (sometimes also called the **Docker Engine**) and a command-line interface to interact with the daemon, the ``docker`` command.

Starting the service
--------------------
Once Docker is installed we have to start the daemon. We'll do so using `systemd`_::

    $ systemctl enable docker --now

We can check out that the service is enabled and active by running::

    $ systemctl status docker

Post-installation steps for Linux
---------------------------------
The Docker daemon always runs as the ``root`` user. On top of that, it binds to a Unix socket which, by default, is also owned by the user ``root``.  If you don’t want to preface the docker command with ``sudo``, we have to do a couple of things:

1. Create a Unix group called ``docker``, which usually is done automatically during installation.
2. Add our user to this group.
3. Log out and log back in so that your group membership is re-evaluated.

We can solve the first two things in just two lines::

    $ sudo groupadd docker
    $ sudo usermod -aG docker $USER

For the last one we can just log out and back in again, or run::

    $ newgrp docker

Finally, you can test everything works without using ``sudo`` running::

    $ docker run hello-world

.. _`Docker Toolbox`: https://docs.docker.com/toolbox/toolbox_install_mac/
.. _`Docker Desktop`: https://docs.docker.com/docker-for-mac/
.. _`Docker Hub`: https://hub.docker.com/editions/community/docker-ce-desktop-mac/
.. _`stable`: https://download.docker.com/mac/stable/Docker.dmg
.. _`edge`: https://download.docker.com/mac/edge/Docker.dmg
.. _`Docker Engine`: https://docs.docker.com/engine/
.. _`most popular Linux distributions`: https://hub.docker.com/search?q=&type=edition&offering=community
.. _`docker`: https://www.archlinux.org/packages/community/x86_64/docker/
.. _`systemd`: https://systemd.io/

