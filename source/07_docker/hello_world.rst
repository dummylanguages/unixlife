***********
Hello world
***********
Let's get familiar with Docker creating a container that will greet us with the message ``Hello world!`` once we run it. A summary of the steps:

1. Create a build directory.
2. Write a Dockerfile
3. Build the image
4. Run the container

A build directory
=================
When you run the ``docker build`` command, all the contents of the **current working directory** (files and directories) are sent to the Docker daemon as the **build context**.

.. note:: By default, the **Dockerfile** is assumed to be located here, but you can specify a different location with the file flag ``-f``.

Let's open a terminal, and somewhere in our home directory let's create a folder for our small project::

    $ mkdir hello_docker
    $ cd hello_docker

You can name it whatever you want, and place it wherever you want.

Writing a Dockerfile
====================
Once we have a build directory, writing a `Dockerfile`_ is the first step to containerizing an application. A Dockerfile is a text document that contains all the commands necessary to build a Docker image.

.. note:: You can think of a Dockerfile as a step-by-step recipe on how to build up your image.

Let's move to our project directory, if we are not already there and create the file::

    $ touch Dockerfile

Now we have to open the file with our favourite code editor and add the following lines to the :download:`Dockerfile </_code/docker/hello_docker/Dockerfile>`:

.. literalinclude:: /_code/docker/hello_docker/Dockerfile
   :language: docker
   :linenos:
   :tab-width: 2

Every valid Dockerfile must start with a ``FROM`` instruction. This directive serves the purpose of setting the base for our image. In this case we are building our image based on the `busybox Docker image`_, which Docker will download for us from the repositories (Docker Hub).

.. note:: `Busybox`_ is a software suite that provides several **Unix utilities** in a **single executable file**. This only file replaces basic functions of more than 300 common commands. It was specifically created for embedded operating systems with very limited resources. Although it was designed to work on top of the Linux kernel, it runs in a variety of POSIX environments such as Linux, Android, and FreeBSD.

The ``CMD`` directive allows us to specify a command to be executed when our container start running. In this case we are using it to print the message ``Hello world!`` with the ``echo`` command, which is included in BusyBox.

Building the image
==================
Now we'll use this Dockerfile to build our image::

    $ docker image build .

The ``docker image build`` command builds our image based on the contents of the current working directory, signified by a dot ``.``. We should specify the path to the build directory in case that wasn't our current working directory. This command will compress all the contents of the specified directory into a ``tar`` file, and will send it to the Docker daemon.

By the way, before running docker commands make sure you have Docker running, otherwise you'll get the error::

    Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?

Listing images
--------------
If everything went according to plan, we must have an image on our system. To list all the images in our system we can run::

    $ docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    <none>              <none>              2be871fcbb9a        6 seconds ago       1.22MB
    busybox             latest              6d5fcfe5ff17        2 months ago        1.22MB

As you can see, there are listed two images:

1. One with the name ``<none>`` is the image we have created.
2. Since our image was based on the prebuilt image named ``busybox``, Docker pulled that image for us. The image is now in our system, so we can use it as a base for other builds without having to be downloaded again.

Note that our image has an **IMAGE ID**, which is what Docker really use to identify images, but it's lacking a name, which should be listed (a bit counterintuitively) under the **REPOSITORY** column.

.. note:: In Docker, **image names** are known are **repositories**.

Renaming images
---------------
Let's give our image a proper name, ``hello_img`` for example::

    $ docker tag 2be871fcbb9a hello_img

The ``docker tag`` command allows us to rename images. The first argument is the **IMAGE ID** and the second one the name. Let's list our images again::

    $ docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    hello_img           latest              2be871fcbb9a        18 minutes ago      1.22MB
    busybox             latest              6d5fcfe5ff17        2 months ago        1.22MB

Now under the **REPOSITORY** column, we can see that our image has a name, ``hello_docker``.

Retagging images
----------------
Also worth noticing that under the **TAG** column it reads ``latest``, even though we didn't assign any tag. The ``latest`` tag is the **default** one that Docker adds when. The naming it's a bit unfortunate since it doesn't have anything to do with the image being the latest version or anything like that. Let's fix it::

    $ docker tag hello_docker hello_img:0.1
    $ docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    hello_img           0.1                 2be871fcbb9a        34 minutes ago      1.22MB
    hello_img           latest              2be871fcbb9a        34 minutes ago      1.22MB
    busybox             latest              6d5fcfe5ff17        2 months ago        1.22MB

Indeed the tagging worked, only that now it seems we have two ``hello_docker`` images. If you check the **IMAGE ID** column you'll notice that these two images have the same id number. An image may have several **name:tag** references, they are just human-readable aliases for an image.

Removing images
---------------
We can remove a reference to an image with the command::

    $ docker rmi hello_img:latest
    Untagged: hello_img:latest
    Deleted: sha256:92d713e93720693307f37634e214d5ff89db9eb4b87347ba8bd2f1c7d7e3b393

Now if we list our images again, ``hello_img:latest`` is not there anymore. Note that we have not deleted the image, just a tag to the image, hence the output ``Untagged: hello_img:latest``. Images are only deleted when their last and only reference is removed.

Create container
================
To **create** a Docker container from a given image we use the command::

    $ docker container create hello_img

Where ``hello_img`` is the name of the **image**.

Listing containers
------------------
Once we've created a container is a good idea to check the container has been indeed created. We can list Docker containers using the command::

    $ docker container ls -a
    CONTAINER ID        IMAGE               COMMAND                   CREATED             STATUS                     PORTS               NAMES
    b48be9a39eec        hello_img         "/bin/sh -c 'echo \"h…"   3 minutes ago       Exited (0) 3 minutes ago                       beautiful_hermann

The column names in the output is self explanatory. The **STATUS** column is interesting, it shows how the container did its thing and then exited. Also the **NAMES** column shows a strange name that we didn't assign, in this case ``beautiful_hermann``. That's automatically done by Docker if we don't assign a name to our container.

.. note:: The old syntax of this command is ``docker ps -a``.

Renaming containers
-------------------
We can rename an existing container using the command::

$ docker container rename beautiful_hermann hello_cont

Creating named containers
-------------------------
If we prefer to run a container under a given name choosen by us, we just have to use the ``--name`` option, followed by a **name** of our choice::

    $ docker container create --name hello_cont_2 hello_img

Now if we list our containers again we should see this last one listed with the name ``hello_cont_2``.

Start container
===============
To **start** the container we need to know the **container name** or its **id**::

    $ docker container start hello_cont

The container is created, we can list them to be sure is there.

Running the container
=====================
We've just seen how running a Docker container consists of two steps:

1. **Creating** the container.
2. **Starting up** the container.

There's a command that allows us to execute both steps simultaneously::

    $ docker container --name hello_cont_3 run hello_img
    Hello world!

Re-starting containers
----------------------
Once a container is created we can run it as many times as we want with the ``docker start`` command::

    $ docker container start hello_cont

This command allows us to start an already existing container without creating a new one.

Cleaning up
===========
Now that we've finished with the example and have a good understanding of the Docker basics, we may want to delete the toy image and containers we have created. 

Removing images
---------------
But if we try to do that::

    $ docker rmi hello_img
    Error response from daemon: conflict: unable to remove repository reference "hello_img" (must force) - container b48be9a39eec is using its referenced image 2965e57688d2
    [...]

We get an **error message**, because the image is referenced by several containers. We have two options here:

1. Use the ``--force`` option (``-f`` for short), which will remove a docker image while its associated container remains in the system.
2. Remove the associated containers and then remove the docker image. If we choose this path, notice that if an image is referenced by several containers, we'll have to delete all of them before being able of getting rid of the image.

Let's take the first option::

    $ docker rmi -f hello_img
    Untagged: hello_img:latest
    Deleted: sha256:2965e57688d2262c8bde62e6ebf6849b3364a5c241b1efb1849f7b1982ea46af

If you list the images you'll see the ``hello_img`` image is gone. But the container is still there though, and we can **start** it as many times we want.

Removing containers
-------------------
Actually we're not gonna need those silly containers either. If you list them, we still have three of them, ``hello_cont``, ``hello_cont_2`` and ``hello_cont_3``. Let's get rid of **all** of them with::

    $ docker container prune

If we list all the containers again, they're all gone. Alternatively, if we have several containers but we want to get rid of only some of them::

    $ docker rm hello_cont_2 hello_cont_3

That would leave ``hello_cont`` hanging around to be used if we need to.

.. _`Dockerfile`: https://docs.docker.com/glossary/#dockerfile
.. _`a couple of different ways`: https://hackernoon.com/how-to-move-code-into-a-docker-container-ab28edcc2901
.. _`Busybox`: https://www.busybox.net/
.. _`busybox Docker image`: https://hub.docker.com/_/busybox
