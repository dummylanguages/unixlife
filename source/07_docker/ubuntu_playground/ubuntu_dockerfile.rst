*****************
Ubuntu Dockerfile
*****************
In this section we're gonna make the same changes we did in the section, but using a Dockerfile. This achieve the same results while at the same time is more portable and repeatable.

A build directory
=================
Let's choose a fancy name for our **build directory**::

    $ mkdir ~/ubuntu_playground && cd $_

Create a Dockerfile::

    $ touch Dockerfile

We're gonna be building the file incrementally, explaining all the steps. As we are gonna be building on top of the `Ubuntu official Docker image`_, the first lines must be::

    FROM ubuntu

Creating a Dockerfile
=====================
At first we are gonna need to perform several things as root user:

1. Install the ``sudo`` utility.
2. Create a normal user and add him to the ``sudo`` group.

The ``RUN`` instruction
-----------------------
The ``RUN`` instruction allows us to execute commands on top of the current image. Let's add the line that ``RUN`` the command to install ``sudo``::

    RUN apt-get update && apt-get -y install sudo

For adding a normal user through a Dockerfile, it's better to choose a command that doesn't run interactively, prompting us for user information and such. The ``useradd`` command does exactly that, so let's add the line to run this command::

    RUN useradd --create-home --groups sudo bob

Every time we use the ``RUN`` directive, it creates a new layer on top of the current image, meaning that in the example we are creating two new layers. We can avoid that by grouping both commands under a single ``RUN`` instruction::

    RUN apt-get update && apt-get -y install sudo \
        apt-get update && apt-get -y install sudo

We can use a ``\`` (backslash) to continue a **single** ``RUN`` instruction onto the next line. That would only create a **single layer**, but note that the use of the ``\`` is only allowed in the **shell form** of the ``RUN`` instruction. Check the excellent documentation about `RUN`_ to know more about it.

The ``ENV`` instruction
-----------------------
This instruction allows us to set **environment variables** in the build stage. We could use this instruction so we don't have to hardcode the name in a lot of places inside the Dockerfile. Since the values will be in the environment for all **subsequent instructions** of the build stage, probably is better to set the variable right after the ``FROM`` instruction. So this is how our Dockerfile would look like::

    FROM ubuntu

    ENV user=bob

    RUN apt-get update && apt-get -y install sudo && \
        useradd --create-home --groups sudo $user && \
        echo "$user ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

We may recognize ``$user`` as an example of as parameter expansion in the Bash shell, but actually inside the Dockerfile it's a feature of Docker known as `environment replacement`_ that has the same syntax.

.. warning:: The environment variables set using ``ENV`` will persist when a container is run from the resulting image, which may cause **unexpected side effects**.

The ``ARG`` instruction
-----------------------
To avoid the unexpected side effects of setting environment values that will live in the container, there's another instruction that allows us to set variables that are not passed down to the environment of the container. The ``ARG`` instruction defines a variable that users can pass at build-time to the builder with the ``docker build`` command using the ``--build-arg <varname>=<value>`` flag.

For example, to set the user name using this instruction we would use::

    ARG user=bob

    RUN apt-get update && apt-get -y install sudo && \
        useradd --create-home --groups sudo $user && \
        echo "$user ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

An additional advantage of using ``ARG`` is that we could set up the user name to any value we want without having to edit the Dockerfile. We do that from the command line when we build the image::

    $ docker image build --build-arg user=lynda .

If we don't make use of the ``--build-arg`` flag during build, the value of ``user`` will be ``bob``, otherwise it will be ``lynda`` or whatever value we pass.

The ``USER`` instruction
------------------------
This instruction sets the user name to use when running the image and for any ``RUN`` instructions that follow it in the Dockerfile. That's exactly what we need for installing stuff as normal users, so let's add::

    USER $user
    RUN sudo apt-get -y install nano

This way we're installing the nano editor as a **normal user**. Also, when the container starts running, we'll be logged in as the user **bob** instead of the **root** user

No man pages
------------
One of the issues you may find in some Linux images is the lack of man pages to keep the images light. In fact, even if we tried to install ``man`` and the man pages it wouldn't work. We have to change some setting in the configuration of `dkpg`_, the package manager for Debian-based systems.

Let's change to ``/etc/dpkg/dpkg.cfg.d/`` and display the contents of a file named ``excludes``::

    $ cd /etc/dpkg/dpkg.cfg.d/
    $ cat excludes
    # Drop all man pages
    path-exclude=/usr/share/man/*
    ...

We have to comment the **second line** so the path to the man pages is not excluded::

    RUN sudo sed -i.bak '2 s/^/#/' /etc/dpkg/dpkg.cfg.d/excludes

The ``sed`` command above is pretty simple:

* The ``-i`` option edits the file in place, but before it creates a **backup file** with the extension ``.bak``. There are no spaces in this **option**, ``-i.bak``.
* The ``'2 s/^/#/'`` part is the **expression** that ``sed`` executes. We are saying, select the ``2`` line, and substitute(``s``) the beginning of the line (``^``) with a comment (``#``).

From now on, all the new programs we install will have their man pages available. Now we have to reinstall the man pages for all the commands of the `coreutils`_ package::

    RUN sudo apt --reinstall install man-db coreutils

The ``WORKDIR`` instruction
---------------------------
The `WORKDIR`_ instruction sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADD instructions that follow it in the Dockerfile. Let's add it to our Dockerfile::

    WORKDIR /home/$user

We need to do this because in the next section we want to do some work in this directory. Also, when the container starts, this will be the directive where we'll be placed in, as long as we don't change it again.

The ``COPY`` instruction
------------------------
The `COPY`_ instruction allows us to copy new files to the container's filesystem. I keep a small file with my prompt customization that I would like to add at the end of the ``.bashrc`` file. So let's copy this file from our **build directory** to our **container**::

    COPY bobs_prompt bobs_prompt

The first argument to ``COPY`` is the **source** relative to the build directory, and the second is the **destination** in the container. Note that since we used the ``WORKDIR`` instruction in the section before, we already are at bob's **home directory**.

Now we'll make a backup copy of the original ``.bashrc`` and add the contents of the ``bobs_prompt`` file at the end of the new ``.bashrc``::

    RUN cp .bashrc .bashrc.bak && \
        cat bobs_prompt >> .bashrc && \
        rm bobs_prompt

In the last line we deleted the file.

Add this file to the ``.dockerignore``?

Final version of the Dockerfile
-------------------------------
This is the complete version of the :download:`Dockerfile </_code/docker/ubuntu_playground/Dockerfile>`:

.. literalinclude:: /_code/docker/ubuntu_playground/Dockerfile
   :language: docker
   :linenos:
   :tab-width: 2

Build the image
===============
Now we are ready to build the image based on our finished Dockerfile, so from our build directory we'll run::

    $ docker image build --tag ubuntu_custom .

Run the container
=================
Use the ``--hostname`` option to set up a nice hostname instead of the default container id::

    $ docker run -it --name bobuntu --hostname magic-box ubuntu_custom


.. _`Ubuntu official Docker image`: https://hub.docker.com/_/ubuntu
.. _`RUN`: https://docs.docker.com/engine/reference/builder/#run
.. _`environment replacement`: https://docs.docker.com/engine/reference/builder/#environment-replacement
.. _`dkpg`: https://help.ubuntu.com/lts/serverguide/dpkg.html
.. _`neovim`: https://neovim.io/
.. _`coreutils`: https://www.gnu.org/software/coreutils/coreutils.html
.. _`WORKDIR`: https://docs.docker.com/engine/reference/builder/#workdir
.. _`COPY`: https://docs.docker.com/engine/reference/builder/#copy