####################
An Ubuntu playground
####################

.. toctree::
	:maxdepth: 3
	:hidden:

	ubuntu_container
	modifying_container
	ubuntu_dockerfile

Docker is meant to be used to containerize apps, but that doesn't mean we can't use it to create an Ubuntu container that we can use to play and learn things from. In this section we're gonna learn how to use Ubuntu Linux in a Docker container.


.. _`Docker`: https://www.docker.com/
.. _`dockerfile`: https://docs.docker.com/glossary/#dockerfile
.. _`docker container`: https://docs.docker.com/glossary/#container
.. _`docker image`: https://docs.docker.com/glossary/#image
.. _`Docker hub`: https://hub.docker.com/