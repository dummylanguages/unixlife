*********************
Ubuntu in a container
*********************
Let's say we're on a macOS or Windows host, and we want to test some Linux commands. We can easily download an Ubuntu Docker image and run a container from it.

Pulling the Ubuntu image
========================
Instead of starting with a Dockerfile, we could just download a prebuilt Docker image of Ubuntu from `Docker Hub`_. Just run::

    $ docker image pull ubuntu

When this command finishes we should have an Ubuntu image in our system. Let's check it out::

    $ docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    ubuntu              latest              72300a873c2c        9 days ago          64.2MB
    busybox             latest              6d5fcfe5ff17        2 months ago        1.22MB

Creating a container
====================
We could go ahead and **run a container** based on this image, so we could start playing a bit with Ubuntu::

    $ docker container create --name ubuntu-cont ubuntu
    378cc3166cd9633eff749d240c6d659d361fbaa03608ca7435cdb6cf1cfa340f

This command always takes an **image name** as an argument, in this case ``ubuntu``. The **name of the container** is optional, but we chose to name it ``ubuntu-cont``.

Starting the container
======================
We can start a container running::

    $ docker container start ubuntu-cont
    ubuntu-cont

What happened here? The output ``ubuntu-cont`` is telling us that the container has started. If we list our containers::

    $ docker container ls -a
    CONTAINER ID      IMAGE         COMMAND           CREATED            STATUS         PORTS           NAMES
    378cc3166cd9      ubuntu        "/bin/bash"       11 seconds ago     Created                        ubuntu-cont

You'll see that under the **COMMAND** column, ``"/bin/bash"`` is listed. That means, that when the container is started, it runs that command and then exits. What we have to do is to create an **interactive container**.

Let's delete the container and create a new one::

    $ docker container prune
    WARNING! This will remove all stopped containers.
    Are you sure you want to continue? [y/N]

Note that I'm using the ``container prune`` subcommand because I have only one container stopped, and I'm sure I want to delete it.

Creating an interactive container
=================================
As the official documentation states, for **interactive processes** (like a shell), we must use ``-i`` ``-t`` together in order to allocate a **pseudo-tty** for the container process:

* ``-i``: Keep STDIN open even if not attached.
* ``-t``: Allocate a pseudo-tty.

These two options are often combined as ``-it``, so let's create a container with them::

    $ docker container create -it --name ubuntu-cont ubuntu
    ubuntu-cont

Starting a container
--------------------
This time the container is created, now we can **start** it::

    $ docker container start ubuntu-cont

Two ways of attaching to a running container
============================================
1. The container is running, but we have to attach our standard streams to it using the `container attach`_ command::

    $ docker container attach ubuntu-cont
    root @ 9832f1c932f8 |/| $ 

This time we are welcomed by an **interactive prompt** that shows were are **root** user inside the Ubuntu container. We can operate inside this container as we please, typing all the commands we want installing programs, create users, etc.

.. note:: We can use only the first digits of the container id to run commands on it.

2. If we need to attach several terminals to a **running container**, we can use the `container exec`_ command. For example::

    $ docker container exec -it ubuntu-cont bash

Then we can open another terminal tab, and run the same command.

Stop a container
================
When we are done using a container we have two options to **stop** it:

1. We can stop the container by typing ``exit``.

2. From another terminal window we can run::

    $ docker container stop ubuntu-cont

Detach from a running container
-------------------------------
If we don't want to stop the container, we can simply **detach** from it using the shortcut ``Ctrl - p`` ``Ctrl - q``. This doesn't stop it, it's still running in the background.

Restart and attach to a container
---------------------------------
Once a container is stopped, if we want start working in the same container again we have to:

1. **Start** the container with ``docker container start container_name``.
2. **Attach** to the container with ``docker container attach container_name``.

In our example::

    $ docker container start ubuntu-cont
    ubuntu-cont
    $ docker container attach ubuntu-cont
    root @ 9832f1c932f8 |/| $


.. _`Docker Hub`: https://hub.docker.com/
.. _`a couple of different ways`: https://hackernoon.com/how-to-move-code-into-a-docker-container-ab28edcc2901
.. _`Busybox`: https://www.busybox.net/
.. _`busybox Docker image`: https://hub.docker.com/_/busybox
.. _`dkpg`: https://help.ubuntu.com/lts/serverguide/dpkg.html
.. _`neovim`: https://neovim.io/
.. _`container attach`: https://docs.docker.com/engine/reference/commandline/container_attach/
.. _`container exec`: https://docs.docker.com/engine/reference/commandline/container_exec/