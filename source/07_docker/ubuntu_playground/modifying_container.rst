*********************
Modifying a container
*********************
We can make changes inside a container such as installing software or creating users, and the changes are **persistent**. The problem with this approach is that the changes are dependent of the container, and if down the line we are gonna need 100 containers, is not practical to change stuff in each of them. We could export the modified container and make as many copies as we needed too, but it's never gonna be as lightweight as a **Dockerfile**.

Still, modifying a container can be useful to try out things, that we can take out later to a Dockerfile, from which we can clone images and spawn as many containers as we wanted to. In order to keep images lightweight, the amount of packages in a Linux distro tends to be kept to a minimum, so let's install some essential packages for our Ubuntu playground.

No man pages
============
One of the issues you may find in some Linux images is the lack of man pages to keep the images light. In fact, even if we tried to install ``man`` and the man pages it wouldn't work. We have to change some setting in the configuration of `dkpg`_, the package manager for Debian-based systems.

If we change to ``/etc/dpkg/dpkg.cfg.d/`` we can edit a file named ``excludes``, to be precise we have to comment the line::

    # Drop all man pages
    path-exclude=/usr/share/man/*

We can comment this line running the command::

    sed -i.bak '2 s/path/# path/' /etc/dpkg/dpkg.cfg.d/excludes

From now on, all the new programs we installed will have their man pages available.

Reinstalling basic tools
------------------------
Now we have to reinstall the man pages for all the commands of the `coreutils`_ package, which is easily accomplished by reinstalling the package::

    # apt-get update && apt-get install --reinstall man-db coreutils

Installing an editor
====================
Let's install neovim::

    # apt-get install -y neovim

Installing ``sudo``
===================
Since we are planning to add a normal user to the ``sudo`` group, let's install ``sudo`` first::

    # apt-get install sudo

Creating a user
===============
By default we are logged in as the **root** user in the container, let's change that by creating a normal user with administrator privileges::

    # adduser bob

We'll be prompted to enter a password and fill out the user information for the new user. Now we have to add ``bob`` to the ``sudoers`` file::

    # usermod -aG sudo bob

Now, if we want, we can continue installing packages as the user bob::

    # su bob -

Caveat
------
Once the changes made to the container are suitable to our needs, we can take these changes to the **Dockerfile**, so we can build as many images as we need to.


.. _`dkpg`: https://help.ubuntu.com/lts/serverguide/dpkg.html
.. _`neovim`: https://neovim.io/
.. _`coreutils`: https://www.gnu.org/software/coreutils/coreutils.html