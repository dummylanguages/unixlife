****************************
Basic ``systemd`` Management
****************************
In this section we'll explain the basics of `systemd`_:

* Starting and stoping services.
* Checking the status of a service.
* Enabling and disabling a service.

Starting and stoping daemons
============================
Starting a daemon is as simple as::

  $ sudo systemctl start whateverd

To stop one::

  $ sudo systemctl stop whateverd

To check their **status**::

  $ sudo systemctl status whateverd

Reloading daemons
=================
To stop and start a daemon we'd do::

  $ sudo systemctl restart whateverd

This is usually done when we want to **reload** a configuration. Some daemons support just reloading the new settings without the need of stopping the process::

  $ sudo systemctl reload whateverd

In case we don't know if the service in question supports reloading, we can always use::

  $ sudo systemctl reload-or-restart whateverd

Enable and disable daemons
==========================
To ensure a daemon starts when the system does, we have to ``enable`` it::

  $ sudo systemctl enable whateverd

To ``disable`` it, we'd do::

  $ sudo systemctl disable whateverd

The same way the ``status`` command tells us if a daemon is running or not, we may want to check if a service is enabled or not, using the ``is-active`` (or ``is-enabled``) command::

  $ sudo systemctl is-active whateverd

.. note:: To refer to a service that starts when the system boots, we can say that is **enabled** or that is **active**.

Listing units
=============
To see the list of all **loaded** daemons on our system we run::

  $ sudo systemctl list-units

.. note:: You can filter units by type using the ``-t`` option followed by the type: ``service``, ``socket``, etc.

If we need to check for services not loaded in memory we'd run::

  $ sudo systemctl list-unit-files

Error starting daemons
======================
Sometimes we may hit a bump when starting a service. In such situations we would run::

  $ sudo journalctl -xe

If for some reason this command fails, make sure the ``systemd-journald`` service is running::

  $ sudo systemctl restart systemd-journald

.. _`systemd`: https://systemd.io/
