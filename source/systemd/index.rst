#######
Systemd
#######

.. toctree::
	:maxdepth: 3
	:hidden:

	basics
	example

According to `Wikipedia`_:

	`Systemd`_ is a software suite that aims to unify service configuration and behavior across Linux distributions that has been adopted across most distros.

``systemctl`` is a command to interface and control the state of the ``systemd`` service manager. Using this command we can:

* Start and stop **daemons**.
* Enable and disable them.

.. note:: A `daemon`_ is a computer program which runs as a **background** process. Traditionally, the process names of a daemon end with the letter ``d``, to differentiate them from normal processes. In other operating systems, daemons are known as **services**.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Systemd
.. _`Systemd`: https://systemd.io/
.. _`daemon`: https://en.wikipedia.org/wiki/Daemon_(computing)
