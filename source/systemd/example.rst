*************************
A systemd service example
*************************
Let's see how to configure a service to be use by `systemd`_.

Creating a service
==================
By that we mean to edit a file with the name of the service, for example, ``sxhkd.service``. The first part before the dot (``sxhkd``) will identify the service itself, and the ``.service`` part is just an extension. This are the contents we'll put in::

    [Unit]
    Description=Simple X Hotkey Daemon
    Documentation=man:sxhkd(1)
    PartOf=graphical-session.target
    After=graphical-session.target

    [Service]
    Restart=always
    RestartSec=2
    ExecStart=/usr/bin/sxhkd
    ExecReload=/usr/bin/kill -SIGUSR1 $MAINPID

    [Install]
    WantedBy=default.target

Services are known as **units**.

User units
==========
**systemd** offers users the ability to manage services under the user's control with a per-user systemd instance, enabling users to start, stop, enable, and disable their own units. This is convenient for daemons and other services that are commonly run for a single user, such as ``sxhkd`` in this case, or to perform automated tasks like fetching mail.

User units are located in the following directories (ordered by ascending precedence):

* ``/usr/lib/systemd/user/`` where units provided by installed packages belong.
* ``~/.local/share/systemd/user/`` where units of packages that have been installed in the home directory belong.
* ``/etc/systemd/user/`` where **system-wide user units** are placed by the system administrator.
* ``~/.config/systemd/user/`` where the user puts their own units; it has the highest precedence.

We're interested in the last location, if it doesn't exist, you must create it::

    $ mkdir -p ~/.config/systemd/user/

Now let's copy/move the ``sxhkd.service`` file there::

    $ cp sxhkd.service ~/.config/systemd/user

.. warning:: If you would like to keep your user services under source control, and symlink them to this folder from your **dotfiles** folder, don't bother; ``systemd`` won't follow your stinky **symlinks**.

Once we have our file service in place, we have to run::

    $ systemctl --user enable sxhkd.service --now

.. note:: Ironically, that creates a folder named ``default.target.wants`` containing a **symbolic link** tou our ``sxhkd.service`` file.

And to confirm everything's good::

  systemctl --user status sxhkd.service

.. warning:: You may receive some error when starting the service if the sxhkd **configuration file** is not in place (``~/.config/sxhkd/sxhkdrc``).

.. _`systemd`: https://systemd.io/
