*************
Installing it
*************
`Powerline`_ is written in Python, so we can install it using ``pip``. I prefer to do it using the `user scheme`_::

    $ pip install --user powerline-status

Beforehand, it's a good idea to make sure of the name of the Python package by running::

    $ pip search powerline
    [...]
    powerline (0.2.4)                      - Simple computer reservations system
    powerline-status (2.7)                 - The ultimate statusline/prompt utility.
        INSTALLED: 2.7 (latest)
    [...]

As you can see, there'a package named ``powerline`` which has nothing to do with the one we want.

Powerline fonts
===============
To make the **status line** look really spiffy, we'll need to install patched fonts in our system. These fonts are available in the `powerline/fonts`_ repository.

Fonts in macOS
--------------
In macOS we just have to **download** a patched font we like, **install** it by double-click on it, and set it up in our terminal profile.

I chose to download the `Source Code Pro`_ in `Medium`_ weight (click the **download button** in the github page). Once in my Downloads folder, double click to add it to the system font collection.

Then chose the font in the macOS official **Terminal app**, or **iTerm2**, whatever you use.

Fonts in Linux
--------------


Configuration
=============
As we said in the beginning, Powerline can provide a status line for several applications. Its configuration will depend on the application where we want to display the status line.

Tmux: essential configuration
-----------------------------
The documentation explains the **bare essentials** we need to do to enjoy a spiffy :ref:`tmux_status_line_label`. First thing is to find out where did ``pip`` install the Powerline executables, which is easily done by asking ``pip``::

    $ pip show powerline-status
    [...]
    Location: /Users/fito/Library/Python/3.7/lib/python/site-packages
    [...]

Now we have to add the following line to our Tmux configuration file, usually located in ``~/.tmux.conf``::

    source "${repository_root}/powerline/bindings/tmux/powerline.conf"

It is advised to run the `powerline-daemon`_ before adding the above line to our Tmux config file. To do so add::

    run-shell "powerline-daemon -q"

So that's what my Powerline configuration in ``~/.tmux.conf`` looks like::

    run-shell "powerline-daemon -q"
    repository_root=/Users/fito/Library/Python/3.7/lib/python/site-packages/powerline
    source "${repository_root}/bindings/tmux/powerline.conf"

.. _`Powerline`: https://github.com/powerline/powerline
.. _`documentation`: https://powerline.readthedocs.io/en/master/
.. _`user scheme`: https://docs.python.org/3/install/index.html#alternate-installation-the-user-scheme
.. _`powerline-daemon`: https://powerline.readthedocs.io/en/master/commands/daemon.html
.. _`powerline/fonts`: https://github.com/powerline/fonts
.. _`Source Code Pro`: https://github.com/powerline/fonts/tree/master/SourceCodePro
.. _`Medium`: https://github.com/powerline/fonts/blob/master/SourceCodePro/Source%20Code%20Pro%20Medium%20for%20Powerline.otf