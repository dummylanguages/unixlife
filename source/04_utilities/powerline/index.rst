#########
Powerline
#########
.. toctree::
    :maxdepth: 3

    install

`Powerline`_ is a program written in Python that provides a **status line** for several other applications, such as:

* Some shells: Bash, Zsh.
* Window managers: Awesome and Qtile.
* Other apps: tmux, IPython.

It has a nice `documentation`_, so here we'll keep it short and to the point.

.. _`Powerline`: https://github.com/powerline/powerline
.. _`documentation`: https://powerline.readthedocs.io/en/master/