***
SSH
***

What is ssh
===========
SSH stands for "secure shell" and is a program that allows us to remotely connect to another computer in a safe way. In other words, it allows us to use the shell of another remote computer, as if we were sitting in front of it.
To start using ssh, obviously you need to have it installed in your computer. Since I'm using OS X, I don't need to install anything, because a version of OpenSSH is already preinstalled in my computer. 
What version is installed::

	$ ssh --version
	OpenSSH_6.2p2, OSSLShim 0.9.8r 8 Dec 2011

Where exactly is installed that piece of software?::

	$ which ssh
	/usr/bin/ssh

Generating ssh keys
===================
To make our work with ssh more comfortable, we can generate a pair of ssh keys:

* Private key: id_rsa
* Public key: id_rsa.pub

.. note:: 
	Before creating a new pair ssh keys, we could check for old keys. They should be in ``~/.ssh``, if we don't care about these old keys, we can delete that directory with: ``$ rmdir -r ~/.ssh``

		ssh-keygen -t rsa -C "your_email@example.com"
		Generating public/private rsa key pair.
		Enter file in which to save the key (/Users/you/.ssh/id_rsa): [Press enter]

Optional passphrase
===================
Next we'll be asked for a **passphrase**. This is optional but advisable. Whithout a passphrase anyone who gains access to your computer has gained access to every system you use that key with. Add a passphrase to the SSH key for an extra layer of security.

When you first try to use the key, you are prompted to enter your passphrase:

That doesn't mean that you have to remember your passphrase everytime you connect to github. There is a tool called **ssh-agent** that can securely save your passphrase, so you don't have to re-enter it. You enter the passphrase once, and after that, ssh-agent keeps your key in its memory and pulls it up whenever it is asked for it.

You have two options here:

* If you are in a mac, the ssh-agent can work together with the OS X Keychain and store our passphrase in it, so everytime we want to use it, we just have to unlock our keychain using our normal user password in our computer.
* If you are not in a mac, or just don't want to use the OS X keychain you can use the **ssh-agent** by itself.

Adding or changing a passphrase
===============================
You can change the passphrase for an existing private key without regenerating the keypair. Just type the following command::

	ssh-keygen -p

If our key already has a passphrase, we will be prompted to enter it before you can change to a new passphrase.

Adding our new private key to the ssh-agent
===========================================
Once we have generated our ssh-keys (with or without passphrase) we have to add the private one to the ssh-agent::

	$ ssh-add ~/.ssh/id_rsa
	Enter passphrase for /Users/javi/.ssh/id_rsa:
	Identity added: /Users/javi/.ssh/id_rsa (/Users/javi/.ssh/id_rsa)

If your in a mac, as I am, you don't have to worry about starting the ssh-agent before adding the key, it's already running since we start our session in the computer.

Adding your publick key to Github
=================================
Run the following code to copy the key to your clipboard.::

	$ pbcopy < ~/.ssh/id_rsa.pub

Or optionally, open the ``id_rsa.pub`` in your favourite text editor, select everything and copy it to the clipboard.
Now, with our public key in our clipboard, we just have to go to github.com, and:

1. In the user bar in the top-right corner of any page, click Account settings
2. Click SSH Keys in the left sidebar.
3. Click Add SSH key.
4. In the Title field, add a descriptive label for the new key. For example, if you're using a personal Mac, you might call this key "Personal MacBook Air".
5. Paste your key into the "Key" field.
6. Click Add key.




