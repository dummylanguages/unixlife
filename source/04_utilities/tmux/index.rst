####
Tmux
####

.. toctree::
    :maxdepth: 3

    installation
    configuration/index
    workflow/index
    copy_paste/index
    tmuxinator

`Tmux`_ stands for **terminal multiplexer** and it's a utility that allows us to control several terminals within a single screen.

It's easier to understand what it is, once you know how it works:

* There's already a great `online book`_ about it.
* Its official `wiki`_
* As well as a cool list of resources compiled at `awesome-tmux`_.
* And let's not forget its `man tmux`_

I'm gonna base my personal notes on those sources.

.. _`Tmux`: https://tmux.github.io
.. _`online book`: https://leanpub.com/the-tao-of-tmux/read
.. _`wiki`: https://github.com/tmux/tmux/wiki
.. _`awesome-tmux`: https://github.com/rothgar/awesome-tmux
.. _`man tmux`: https://manpages.debian.org/buster/tmux/tmux.1.en.html