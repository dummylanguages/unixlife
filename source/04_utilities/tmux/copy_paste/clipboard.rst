*********
Clipboard
*********
According to `Wikipedia`_:

    The clipboard is a **buffer** that some operating systems provide for short-term storage and transfer within and between application programs. The clipboard is usually temporary and unnamed, and its contents reside in the computer's RAM. The clipboard is sometimes called the **paste buffer**.

The `Tmux wiki`_ contains a section dedicated to the `clipboard`_, where they explain how to synchronize the system's **paste buffer** with **Tmux buffer**. The way we solve this need will depend on the system we're working on, since each system offer different tools.

There are two possible methods:

1. Piping the contents of the Tmux buffer into and **external tool** that gives us access to the system clipboard (pbcopy, xclip, etc)
2. Using ``OSC 52``, which is a **escape sequence** implemented by some terminals to set the clipboard.

macOS - iTerm2
==============
If you are in macOS, `iTerm2`_ supports the ``OSC 52`` `ANSI escape sequence`_, we just have to enable it in **Preferences**: Under the **General** tab, make sure the checkbox titled **Applications in terminal may access clipboard** is on.

1. I also made sure that ``set-clipboard`` was set in ``tmux``::

    $ tmux show -s set-clipboard
    set-clipboard external

2. And that ``Ms`` was also set (the ``tmux`` server must be running)::

    $ tmux info|grep Ms:
    180: Ms: (string) \033]52;%p1%s;%p2%s\a

Neovim
------
According to Neovim documentation about `clipboard integration`_, Neovim has no direct connection to the system clipboard. Instead it depends on a `provider`_ which transparently uses shell commands to communicate with the system clipboard. By provider they mean any of the clipboard tool that different operating systems provide:

* In macOS: ``pbcopy``, ``pbpaste``.
* In Linux: ``xclip`` or ``xsel`` (They both need a ``$DISPLAY`` set)

To ALWAYS use the clipboard for ALL operations (instead of interacting with the ``+`` and/or ``*`` registers explicitly), add the following to your ``init.vim``::

    set clipboard+=unnamedplus

This way, whatever we copy in Neovim it is put into the system's clipboard automatically.

.. note:: In **Vim** to copy to the system clipboard, we have to use a special buffer (``"=``). For example ``"+2yy`` would copy two lines to the clipboard, and ``"+dd`` would cut the line to the clipboard; ``"+p`` would paste from the system's clipboard.

Linux
=====
In Linux terminals, support for the ``OSC 52`` escape sequence is either:

* **Non-existent**, in terminals that use the `VTE Terminal Widget Library`_, such as `Gnome terminal`_, `Guake`_ or `Terminator`_.

* Supported, but with certain caveats:

    * `Kitty`_ supports it, but it  it has a bug where it appends to the clipboard each time text is copied rather than replacing it.
    * `urxvt`_ supports it via `Perl extension`_.
    * `Simple Terminal`_ supports it but with a length limit on the amount of text that can be copied, so text may be truncated.

Given the situation, it's better to forget about the ``OSC 52`` approach, and try to use the piping into some external tool solution.

External tools
--------------
The **Tmux wiki** contains a section named `external tools`_ where they explain how to pipe the contents of the **Tmux buffer** to external tools that set up the clipboard system. Depending on the **Tmux** version we have installed, the configuration settings are a bit different. For versions **2.4** to **3.1** the settings are (**Vi mode**)::

    bind -T copy-mode-vi Enter             send-keys -X copy-pipe-and-cancel 'xsel -i --clipboard'
    bind -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'xsel -i --clipboard'

As you can see, when we **end the selection** pressing ``Enter``(default key for that), we are simultaneously setting up the system buffer through ``xsel``. If for some reason, we are using a different key for ending the selection, that's the one we should put here.

.. warning:: The settings recommended in the **Tmux wiki** are a bit different and didn't work for me. The ones above did; I took them from an `awesome article`_.

https://www.reddit.com/r/vim/comments/7tt4ts/painless_copy_paste_between_tmux_vim_and_system/
https://www.devroom.io/2017/03/22/tmux-and-vim-copy-and-paste-on-macos-sierra/
https://www.rushiagr.com/blog/2016/06/16/everything-you-need-to-know-about-tmux-copy-pasting-ubuntu/


Remotely
========
We'll cover several scenarios when we are connected to another machine through `SSH`_, 


.. _`Wikipedia`: https://en.wikipedia.org/wiki/Clipboard_(computing)
.. _`Tmux wiki`: https://github.com/tmux/tmux/wiki
.. _`clipboard`: https://github.com/tmux/tmux/wiki/Clipboard
.. _`SSH`: https://en.wikipedia.org/wiki/Secure_Shell
.. _`xquartz`: https://www.xquartz.org
.. _`iTerm2`: https://www.iterm2.com
.. _`ANSI escape sequence`: https://en.wikipedia.org/wiki/ANSI_escape_code#Escape_sequences
.. _`clipboard integration`: https://neovim.io/doc/user/provider.html#clipboard
.. _`provider`: https://neovim.io/doc/user/provider.html#clipboard-tool
.. _`VTE Terminal Widget Library`: https://wiki.gnome.org/Apps/Terminal/VTE
.. _`Gnome Terminal`: https://wiki.gnome.org/Apps/Terminal/VTE
.. _`Guake`: http://guake-project.org/
.. _`Terminator`: https://terminator-gtk3.readthedocs.io/en/latest/
.. _`Kitty`: https://sw.kovidgoyal.net/kitty/
.. _`urxvt`: http://software.schmorp.de/pkg/rxvt-unicode.html
.. _`Perl extension`: http://anti.teamidiot.de/static/nei/*/Code/urxvt/
.. _`Simple Terminal`: https://st.suckless.org/
.. _`awesome article`: https://www.freecodecamp.org/news/tmux-in-practice-integration-with-system-clipboard-bcd72c62ff7b/