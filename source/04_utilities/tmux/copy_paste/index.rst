##########
Copy-paste
##########
.. toctree::
    :maxdepth: 3

    copy_mode
    tmux_buffer
    clipboard


`Tmux`_ 

.. _`Tmux`: https://github.com/tmux/tmux/wiki
