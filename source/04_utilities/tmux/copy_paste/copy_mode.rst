*********
Copy mode
*********
There are 2 ways of copy-paste in Tmux:

The emacs way
=============
This is the **default** way of copy-paste.

.. note:: I base these explanations on my **Prefix**, ``Ctrl + Space``.

TL;DR
-----
To **copy**:

1. ``Prefix + [`` to enter copy mode.
2. ``Prefix + Space + Space`` to start selection. (``Ctrl + Space + Space + Space``)
3. ``Ctrl + w`` to end selection (``Alt + w`` also works).

To **paste**: ``Prefix + ]``

Explanation
-----------
1. Enter **copy mode** using ``Prefix + [``
2. **Start selection**: Navigate to beginning of text you want to select and hit ``Prefix + Space`` to start selection.

.. note:: If you are using :kbd:`Ctrl + Space` as your ``Prefix`` key, to **start the selection** once you type the **Prefix**, you'll have to **press space twice**, which means that overall, you gotta press space **thrice**: once for the prefix, two times more to start the selection (:kbd:`Ctrl + Space + Space + Space`)

3. **End selection**: Navigate to the end of the wished selection and press either:

    * ``Ctrl + W``
    * ``Alt + W``

4. To **paste** the selection: ``Prefix + ]``

At any point we can **quit** the copy mode pressing ``q``.

The **navigation** keys and shortcuts are simply the **arrow keys**; if your keyboard doesn't have them, you can use:

* ``Ctrl + P`` to go up a line.
* ``Ctrl + N`` to go down a line.
* ``Ctrl + F`` a character to the right.
* ``Ctrl + B`` a character to the left.

The vim way
===========
To enjoy this modus operandi, we have to **enable it** in our tmux configuration (``~/.tmux.conf``) adding the following line::

    set-window-option -g mode-keys vi

Using this mode is a bit easier:

1. Enter copy mode using ``Prefix + [``
2. Navigate to beginning of text you want to select and hit ``Space``
3. Navigate to the end of the wished selection and press ``Enter``.
4. To **paste** the selection ``Prefix + ]``

Tweaking the vim way
--------------------
We can make the vim way of our **tmux copy mode** behave in a more **vim way** adding these settings to our config file::

    unbind [
    bind Escape copy-mode
    bind-key -t vi-copy 'v' begin-selection
    bind-key -t vi-copy 'V' rectangle-toggle
    bind-key -t vi-copy 'y' copy-selection
    unbind p
    bind p paste-buffer

1. The first line just unbind the ``[`` from the **tmux copy mode**, the second rebind this mode to ``Escape``. It feels like changing mode in vim.
3. I used ``v`` to begin the selection (Like visual-line mode in vim), and ``V`` to rectangle-toggle(like visual-block in vim)
4. Finally ``y`` to "yank" the selection, and ``p`` to paste it.

Copy-paste with the mouse
=========================
If we enable the **mouse mode**, we can select text by dragging our mouse, and use :kbd:`Enter` to copy it into the tmux buffer. Add this setting to your configuration::

    set -g mouse on

Once this setting is on, copy-paste is easy:

1. Select text **dragging** with the mouse (make sure you see the yellow indicator)
2. ``Enter`` to put the selection in the buffer.
3. ``Prefix + ]`` to **paste**.

Zoom mode
=========
Copying text from our terminal is ok too (**without** enabling **Tmux copy-mode**), the problem is that when we have two or more horizontal panes, the text selection will extend to all panes. We can easily solve that using the **zoom mode**, which basically maximize the current panel hiding the other ones.

We can **toggle** this mode using:

* ``Prefix + z``

If you use **vim**, it's a good idea to have a **shortcut** to **hide the line numbers**, so they are not included in the selection.

https://www.reddit.com/r/vim/comments/7tt4ts/painless_copy_paste_between_tmux_vim_and_system/
https://www.devroom.io/2017/03/22/tmux-and-vim-copy-and-paste-on-macos-sierra/
https://www.freecodecamp.org/news/tmux-in-practice-scrollback-buffer-47d5ffa71c93/
https://www.rushiagr.com/blog/2016/06/16/everything-you-need-to-know-about-tmux-copy-pasting-ubuntu/


.. _`clipboard`: https://en.wikipedia.org/wiki/Clipboard_(computing)