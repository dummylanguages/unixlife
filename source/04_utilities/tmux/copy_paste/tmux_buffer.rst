***********
Tmux buffer
***********
Whenever we **copy** some text in our system, it's usually put in a **buffer** known as the `clipboard`_. When we **paste**, we are dumping the contents of this buffer to whatever application we are pasting the text on. Almost all applications in the system share this buffer, all of them can copy to it and paste from it; **Tmux** is one of the exceptions.

Tmux has its own buffer, so as long as we are copying and pasting text between applications running inside our tmux session, everything is ok. The problem is when we try to share information between the applications that use the **tmux buffer** and the ones that use the **clipboard**.

https://www.reddit.com/r/vim/comments/7tt4ts/painless_copy_paste_between_tmux_vim_and_system/
https://www.devroom.io/2017/03/22/tmux-and-vim-copy-and-paste-on-macos-sierra/
https://www.freecodecamp.org/news/tmux-in-practice-scrollback-buffer-47d5ffa71c93/
https://www.rushiagr.com/blog/2016/06/16/everything-you-need-to-know-about-tmux-copy-pasting-ubuntu/

Vim: To use system clipboard you have to use ``"+2yy`` – copy two lines to X11 clipboard ``"+dd`` to cut line to X11 clipboard ``"+p`` to paste X11 clipboard 

Accessing the buffers
=====================
To see what's in the **last buffer**::

    tmux show-buffer

Press ``Q`` to quit.

* And we can save it to a file with:
    $ tmux save-buffer foo.txt

* To see a list of all the buffers:
    $ tmux list-buffers

Or use the shortcut ``Prefix + #``

We can show or save also named buffers with the ``-b`` option:
$ tmux save-buffer -n b1 foo.txt


.. _`clipboard`: https://en.wikipedia.org/wiki/Clipboard_(computing)