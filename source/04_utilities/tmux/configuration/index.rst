#############
Configuration
#############
.. toctree::
    :maxdepth: 3

    reload_configuration
    essential_settings
    status_bar
    capslock_control

`Tmux`_ keeps its Configuration in a file named ``.tmux.conf`` located in our ``$HOME`` directory. To see all the available options, we could run::

    $ tmux show-options -g




.. _`Tmux`: https://github.com/tmux/tmux/wiki