***********************
Reloading configuration
***********************
After any changes made to the config file, we have to reload the configuration. We can do that:

1. From inside tmux, press :kbd:`Prefix + :` to  open the **tmux prompt**, then type::

    :source ~/.tmux.conf

.. note:: The default **prefix** is ``<Ctrl-b>``: While holding down the ``control`` key, press ``b``.

2. From outside tmux, run the ``source-file`` command::

    $ tmux source-file ~/.tmux.conf

Bind it to a key
================
If we want to reload the configuration with a **shortcut**, we can add one to our configuration file. For example, we can bind the command to the :kbd:`prefix + r` shortcut::

    bind r source ~/.tmux.conf\\; display "~/.tmux.conf sourced!"

Killing the server
==================
**Tmux** reads the configuration file everytime the server is started, so another way of reloading the configuration file is killing the server. Again, this can be done:

1. From outside ``tmux``:

    $ tmux kill-server

2. From inside ``tmux``, press :kbd:`Prefix + :` to  open the **tmux prompt**, then type::

    :kill-server

.. note:: Note that we'll lose all of our **sessions**.

Essential settings
==================
Here we'll mention several settings to help you keep your sanity.

Default shell
-------------
If when you start ``tmux``, some important keys such as **tab**, **escape** or the **arrow keys** send their raw keycodes, such as ``^[[A``, ``^[[B``, etc. The reason for that is that ``tmux`` may not be using your default shell, so we have to tell them to do so adding the following line to the config file::

    set-option -g default-shell /bin/bash

If your prompt doesn't look right either, that may be another clue pointing to this same issue. The setting above should solve it.

Changing prefix key
-------------------
Another essential setting for me is to modify the default :kbd:`Prefix` key to something more to our taste. In my case I chose to change it to :kbd:`Ctrl + Space`, so this is what I added to my config file::

    # Prefix key: Ctrl+Space
    unbind C-b
    set -g prefix C-Space
    bind Space send-prefix 

.. _`Tmux`: https://github.com/tmux/tmux