.. _tmux_status_line_label:

***************
Tmux status bar
***************
To check the current settings related to the status bar, we could run::

    $ tmux show-options -g | grep status
    status on
    status-interval 15
    status-justify left
    status-keys emacs
    status-left "[#S] "
    status-left-length 10
    status-left-style default
    status-position bottom
    status-right " \"#{=21:pane_title}\" %H:%M
    status-right-length 40
    status-right-style default
    status-style fg=black,bg=green

To experiment with these settings, we can use the ``set-option`` command::

    $ tmux set-option status-justify centre

This is what the **status bar** looks like after applying the setting above:

.. figure:: /_images/tmux_status_bar.png
   :scale: 75%
   :align: center

Let's go over its contents:

1. **Status left**: The ``[0]`` part is the **session name**. It is displayed inside the **status-left** by the string ``"[#S]"``.
2. ``0:bash*`` is the **window list component** that shows the ``window number:program``; the ``*`` denotes the current window. By default this component appears justified to the ``left``, but we can put if in the ``centre`` or to the ``right``.
3. **Status right**: contains the **hostname**, **time** and **date** by default.

The following table contains a summary of options:

+-------------------------+---------------------------------+
|         Option          |             Values              |
+=========================+=================================+
| ``status``              | ``on`` or ``off``               |
+-------------------------+---------------------------------+
| ``status-position``     | ``top`` or ``bottom``           |
+-------------------------+---------------------------------+
| ``status-justify``      | ``left``, ``centre``, ``right`` |
+-------------------------+---------------------------------+
| ``status-left-length``  | (In characters)                 |
+-------------------------+---------------------------------+
| ``status-right-length`` | (In characters)                 |
+-------------------------+---------------------------------+

Git status
==========
A good use of the status bar is to automatically show the **git status** of our projects, if any. There are different ways to do that, but probably the easiest solution would be using a plugin.

Installing GITMUX
-----------------
`Gitmux`_ is a plugin written in Go, so we could compile and install it using the Go compiler, but it's easier to just download a **binary release** for our system, uncompress it, and place it somewhere in our ``PATH``.

.. note:: At the moment or writing this, **Gitmux** couldn't be installed using the `Tmux Plugin Manager`_.

So let's add the following line to our Tmux config file::

    set -g status-right '#(gitmux "#{pane_current_path}")'

Don't forget to **restart** the thing to get it up and running.

.. _`Tmux`: https://github.com/tmux/tmux
.. _`Tmux Plugin Manager`: https://github.com/tmux-plugins/tpm
.. _`Gitmux`: https://github.com/arl/gitmux