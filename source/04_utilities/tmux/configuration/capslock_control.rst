*******************
Capslock to control
*******************
By default, `Tmux`_ uses :kbd:`Ctrl-b` as the **prefix key** (aka **leader key**). I found that combination quite unconvenient, so decided to change it to :kbd:`Ctrl-space`, so added the following in my **tmux configuration**::

    # Prefix key: Ctrl+Space
    unbind C-b
    set -g prefix C-Space
    bind Space send-prefix

That mapping is particularly convenient when our system's `Caps Lock`_ key is remapped to the `control key`_. The downside to this, is losing the Caps Lock functionality, but that can be solved in several ways:

* Pressing both `shift keys`_ simultaneously toggles on and off the **Caps Lock**. This is our favourite.
* Holding down `Alt`_ and pressing the Caps Lock key, which is enabled until Shift is pressed, like Chromebooks do.
* Pressing :kbd:`Shift + Caps Lock` to toggle the functionality. Not convenient, since the combination :kbd:`Ctrl + Shift` is used abundantly as part of other shortcuts.

macOS
=====
In **macOS** we can use `Karabiner-Elements`_ to implement the double Shift to toggle on and off the Caps Lock functionality. This is the configuration file we have to create:

.. code-block:: json

    {
    "title": "Toggle caps-lock with both Shift keys",
    "rules": [
        {
        "description": "Toggle caps_lock by pressing left_shift + right_shift at the same time",
        "manipulators": [
            {
            "type": "basic",
            "from": {
                "key_code": "left_shift",
                "modifiers": {
                "mandatory": [
                    "right_shift"
                ],
                "optional": [
                    "caps_lock"
                ]
                }
            },
            "to": [
                {
                "key_code": "caps_lock"
                }
            ],
            "to_if_alone": [
                {
                "key_code": "left_shift"
                }
            ]
            },
            {
            "type": "basic",
            "from": {
                "key_code": "right_shift",
                "modifiers": {
                "mandatory": [
                    "left_shift"
                ],
                "optional": [
                    "caps_lock"
                ]
                }
            },
            "to": [
                {
                "key_code": "caps_lock"
                }
            ],
            "to_if_alone": [
                {
                "key_code": "right_shift"
                }
            ]
            }
        ]
        }
    ]
    }

The second part of the solution is map **Caps Lock** to :kbd:`Ctrl`, which is easily done enabling a simple modification included by default in Karabiner.

Linux
=====


.. _`Tmux`: https://github.com/tmux/tmux
.. _`Caps Lock`: https://en.wikipedia.org/wiki/Caps_Lock
.. _`Control key`: https://en.wikipedia.org/wiki/Control_key
.. _`shift keys`: https://en.wikipedia.org/wiki/Shift_key
.. _`Alt`: https://en.wikipedia.org/wiki/Alt_key
.. _`Karabiner-Elements`: https://karabiner-elements.pqrs.org/