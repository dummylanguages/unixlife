**********
tmuxinator
**********
`tmuxinator`_ is is a ruby gem that allows you to easily manage tmux sessions by using yaml files to describe the layout of a tmux session, and open up that session with a single command.

Installing
==========
We can install it with the **package manager** of our Linux distribution, it shouldn't be a problem. In macOS though...

macOS
-----
Here we have a couple of options, both of them involve using `homebrew`_:

1. Install the `tmuxinator formula`_::

    $ brew install tmuxinator

2. Install it as a Ruby `gem`_. In this case, we must have an up to date version of Ruby::

    $ brew install ruby

Then add ``ruby`` to our ``PATH``, so in our Bash configuration::

    export PATH="/usr/local/opt/ruby/bin:$PATH"

And the **gems directory** too::

    export PATH="/usr/local/lib/ruby/gems/2.7.0/bin:$PATH"

Now we can install the gem::

    $ gem install tmuxinator

If we install tmuxinator this way, we'll need to put the **completion script** where it'll be loaded by your shell. I use Bash, and I keep my completion scripts in the local directory so::

    $ curl https://raw.githubusercontent.com/tmuxinator/tmuxinator/master/completion/tmuxinator.bash -o \
    ~/.local/share/bash_completion/completions/tmuxinator

.. warning:: The shell **completion files** also include the ``mux`` **alias** that can be used in place of the ``tmuxinator`` full name.

Doctor
======
The first command we should run would be::

    $ tmuxinator doctor
    Checking if tmux is installed ==> Yes
    Checking if $EDITOR is set ==> Yes
    Checking if $SHELL is set ==> Yes

Creating projects
=================
Now we're ready to `create a project`_. Tmuxinator projects are just `yaml`_ files than contains the definition of the layout. For example::

    $ tmuxinator new test

That opens our default ``$EDITOR`` with a file named ``test.yml``, located in the ``~/.config/tmuxinator`` directory. We can edit the file, save it and exit. Now, if we run::

    $ tmuxinator start test

We'll start a **tmux session** named ``test``, with the layout defined in the ``~/.config/tmuxinator/text.yml`` file. Centralized projects can be started no matter where we are in the filesystem, meaning we don't have to change to the project folder.

 If you open such a file you'll see that the first line is a **comment** like this::

    # /Users/bob/.config/tmuxinator/test.yml

Local projects
--------------
I prefer to keep my **tmuxinator projects** in the same folder as the **project** itself lives. For example, if I'm working in a project named ``foo``, I just move to that directory and create a **local tmuxinator project** file::

    $ mkdir foo && cd foo
    $ tmuxinator new --local foo

That will create the ``.tmuxinator.yml`` file, inside the ``foo`` directory. Projects created this way, can't be listed.

.. note:: This feature is really useful for sharing tmux configurations in complex development environments.

If you open a ``.tmuxinator.yml`` file, you'll see that the first line contains something like::

    # ./.tmuxinator.yml

All **local configuration** files are named this way.

Moving a project
----------------
If for some reason we created a project in the centralized location (``~/.config/tmuxinator``) and we want to move it to the project folder, we can do so. To start such a project we would move to the project's folder an run::

    $ tmuxinator start -p test.yml

As you can see, the ``-p`` flag takes as an argument the path to the local configuration file. If you try to start such a project with::

    $ tmuxinator start
    Project file at ./.tmuxinator.yml doesn't exist.

So it's a good idea to **rename** your configuration file to ``.tmuxinator.yml``, the **default** for *local configurations*.

Starting a session
==================
1. If you have a **centralized config** file, run::

    $ tmuxinator start project_name

2.If you created a **local config** for your project, move to your project folder and run::

    $ tmuxinator start

``tmuxinator`` will automatically look for a config file named ``.tmuxinator.yml``.

3. If your **local config** has a name other than ``.tmuxinator.yml``, you'll have to use the ``-p`` flag::

    $ tmuxinator start -p test.yml

.. note:: As we said before, if you installed the **completion files** for your shell, don't forget to use ``mux``, the handy alias for ``tmuxinator`` command, as well as ``s``, shorthand for the ``start`` subcommand.

Delete a project
================
This command only works with centralized config files::

    $ tmuxinator delete test

It's convenient because we can delete a  withouth having to change to the directory where the config file is. Apart from that, deleting a ``tmuxinator`` project is just a matter of deleting the config file, no biggie.

Layouts
=======
Each window section has a ``layout`` subsection where we can define the size and number of panes. The procedure is as follows:

1. Start your session as usual and setup a **layout** you like, with as many **windows** and **panes** as you need. You can resize the panes dragging with the mouse (if enabled) or the ``resize-panes`` command.
2. Run the ``list-windows`` command, which should produce an output sort of like this one::

    0: editor* (2 panes) [173x45] [layout 4536,173x45,0,0{75x45,0,0,0,97x45,76,0,3}] @0 (active)
    1: server (1 panes) [173x45] [layout c1be,173x45,0,0,1] @1
    2: logs- (1 panes) [173x45] [layout c1bf,173x45,0,0,2] @2

Each line represents a **window**. Each line contains useful information; let's focus for example on the first line:

* The **number** (``0:``) and **name** (``editor``) of the **window**.
* Amount of panes, ``(2 panes)``.
* Size of the window ``[173x45]``.
* Also, the window's **layout**::

    [layout 4536,173x45,0,0{75x45,0,0,0,97x45,76,0,3}]

This is the part we should copy to our **tmuxinator** config file. Assuming we're using a **local config**, we would add the following in the ``.tmuxinator.yml`` file::

    - editor:
        layout: 4536,173x45,0,0{75x45,0,0,0,97x45,76,0,3}
        panes:
            - nvim
            - #empty, will just run plain bash

Note that in the ``panes`` section we should have an amount of panes that matches the amount in the ``layout`` section, even if some panes are just an empty dash ``-``.

Running commands in windows or panes
====================================
We can pass a **list of commands** to be run in **windows** or **panes**. For example:

.. code-block:: yaml

    windows:
      - editor:
        layout: even-horizontal
        panes:
          - docker exec -it ubuntu-cont nvim
          - bash:
            - docker exec -it ubuntu-cont bash
            - clear
      - tests:
        - docker exec -it ubuntu-cont bash
        - cd exercism
        - clear

In the configuration above we have two windows:

1. One named ``editor``, which contain **two panes**:

    * A **nameless pane** where we run the command ``docker exec -it ubuntu-cont nvim``
    * A **pane** named ``bash`` where we run two commands.

2. The other **window** is named ``tests`` and it doesn't have any pane. We're running three commands in it.


.. _`tmuxinator`: https://github.com/tmuxinator/tmuxinator
.. _`homebrew`: https://brew.sh
.. _`tmuxinator formula`: https://formulae.brew.sh/formula/tmuxinator
.. _`gem`: https://guides.rubygems.org/what-is-a-gem/
.. _`create a project`: https://github.com/tmuxinator/tmuxinator#create-a-project
.. _`yaml`: https://yaml.org/