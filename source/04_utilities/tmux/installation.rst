********
Terminfo
********
When I was trying to configure **Tmux** I set up my terminal emulator (iTerm2) to declare itself as ``xterm-256color`` but whenever I was in a Tmux session, the Home and End keys weren't producing the expected behaviour. So I changed the Tmux configuration to::

    set -g default-terminal "screen-256color"

But that worsened the situation making my terminal unresponsive. The Tmux official documentation recommends to set up the terminal to ``tmux`` or ``tmux-256color``, but after looking into ``/usr/share/terminfo`` I didn't find any of these terminal definitions. So somehow I had to get one in my system.

A bit of history
================
Back in the day of physical `computer terminals`_, almost all manufacturers of video terminals (meaning computer terminals featuring a video display to output information) added vendor-specific `escape sequences`_ to perform operations such as placing the cursor at arbitrary positions on the screen.

As these sequences were different for different terminals, a couple of libraries were created to offer a common API so programs could be written without having to implement support for each and every terminal model:

* The first iteration of `Termcap`_ ("terminal capabilities") was written in 1978.
* Soon after, in 1982, the first version of `Terminfo`_ was written as an improvement over Termcap.

Terminfo is both a **library** and also a **database** that contains descriptions of hundreds of different display terminals. Each description must contain the canonical name of the terminal, and optionally one or more aliases. Either the name or aliases are the keys by which the library searches the terminfo database.

.. note:: The `Text-Terminal-HOWTO`_ includes an interesting section dedicated to `Terminfo and Termcap`_.

ncurses
=======
According to `Wikipedia`_:

    `ncurses`_ (new curses) is a programming library providing an application programming interface (API) that allows the programmer to write text-based user interfaces in a **terminal-independent** manner.

As you may have guessed, the ncurses API is independent from any terminal models thanks to **Terminfo**. As of January 2020, MacOS provide **ncurses 5.7** (from 2008) whereas the current version is **6.2** (from 2020). Because of that, the **Terminfo database** available in our system is a bit old and doesn't include a terminal description for **Tmux**.

Installing the latest Ncurses
-----------------------------
The easiest way would be to use the **package manager** of your distribution. In macOS we would do::

    $ brew install ncurses

But if you installed **Tmux** using Homebrew (or other package manager), you already should have an up to date version of **Ncurses**. During installation try, Homebrew warns us that::

    [...]
    ncurses is keg-only, which means it was not symlinked into /usr/local,
    because macOS already provides this software and installing another version in
    parallel can cause all kinds of trouble.
    [...]

That means that we need to manually symlink the new library in order to use the included tools::

    $ brew link --overwrite ncurses

There's no need for that though, if we keep in mind that Homebrew installs its formulae to ``/usr/local/Cellar``. In this case, the library is symlinked to ``/usr/local/opt/share/ncurses``, so in any of these two directories we can access the utilities included in **Ncurses** without having to modify our PATH.

Printing a terminfo description
-------------------------------
If we go to any of the two directories mentioned before, under ``ncurses/share/terminfo`` we'll find the database of terminal descriptions. In the folder named ``74`` I found two terminal descriptions for Tmux: ``tmux`` and ``tmux-256color``.

.. note:: The original (and most common) implementation of the **terminfo library** stores the terminal descriptions in a directory hierarchy.

Now we have to use the `infocmp`_ command included in one of the aforementioned directories, to print a terminfo description::

    $ /usr/local/Cellar/ncurses/6.2/bin/infocmp tmux-256color > ~/tmux-256color.info

Here we are basically decompiling the executable version of ``tmux-256color`` into a human readable version. It's not advisable to use the preinstalled version of the ``infocmp`` command to *print* a description from an updated database.

Compiling the description
-------------------------
To compile the description into an executable file we'll use the `tic`_ command::

    $ tic -x -o ~/.terminfo ~/tmux-256color.info

.. warning:: In this case we have to use the system's ``tic`` command.

That would automatically create the ``~/.terminfo`` folder and compile inside it a ``tmux-256color`` terminal description. We can check out that the description was added to the database running::

    $ infocmp tmux-256color
    Reconstructed via infocmp from file: /Users/bob/.terminfo/74/tmux-256color
    [...]

That should print the description, and tells us where the system is picking it up from.

Declare the terminal in Tmux configuration
==========================================
Finally we're ready to declare::

    set -g default-terminal "tmux-256color"

.. note:: Read here: https://github.com/neovim/neovim/issues/7209

Compiling from source
=====================
Sometimes the version of the package provided by the repositories of our Linux distribution is not up to date with the latest features offered by the developer under the project repositories. Fortunately compiling and installing Tmux is not that difficult, except for the small hiccup along the way.

Build dependencies
------------------
We need to install::

$ sudo apt install autoconf automake pkg-config

Compiling
---------
First of all, let's get the source code::

    $ git clone https://github.com/tmux/tmux.git

Let's change to the source directory and run the ``autogen.sh`` script::

    $ cd tmux
    $ sh autogen.sh

At this point I got the error ``./autogen.sh: aclocal: not found``, which I solved installing::

    $ sudo apt install autotools-dev
    $ sudo apt install automake

After that ``autogen.sh`` run smoothly, and I could compile::
    
    $ sh autogen.sh
    $ ./configure && make

Installing
----------
Just a matter of placing the resulting ``tmux`` executable anywhere in our path.

.. _`computer terminals`: https://en.wikipedia.org/wiki/Computer_terminal
.. _`escape sequences`: https://en.wikipedia.org/wiki/Escape_sequence
.. _`Termcap`: https://en.wikipedia.org/wiki/Termcap
.. _`Terminfo`: https://en.wikipedia.org/wiki/Terminfo
.. _`Text-Terminal-HOWTO`: https://www.tldp.org/HOWTO/Text-Terminal-HOWTO.html
.. _`Terminfo and Termcap`: https://www.tldp.org/HOWTO/Text-Terminal-HOWTO-16.html
.. _`Wikipedia`: https://en.wikipedia.org/wiki/Ncurses
.. _`Tmux`: https://github.com/tmux/tmux
.. _`ncurses`: http://invisible-island.net/ncurses/
.. _`infocmp`: https://invisible-island.net/ncurses/man/infocmp.1m.html
.. _`tic`: https://invisible-island.net/ncurses/man/tic.1m.html