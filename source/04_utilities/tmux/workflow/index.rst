########
Workflow
########
.. toctree::
    :maxdepth: 3

    sessions
    windows
    panes


`Tmux`_ workflow is organized around three basic concepts:

* Sessions
* Windows
* Panes

The following diagram sums it up:

.. figure:: /_images/tmux_workflow.png
   :scale: 75%
   :align: center

A terminal window/tab can show **one session** at a time, but inside a single session, we can access to several windows. The active one shows an ``*``.

.. _`Tmux`: https://github.com/tmux/tmux/wiki
