*****
Panes
*****

Create a pane
=============
:kbd:`Prefix + %`, to split a vertical pane.
:kbd:`Prefix + "`, to split a horizontal pane.

Show pane numbers
=================
:kbd:`Prefix + q` (lower case Q), to **show pane numbers**.

Move between panes
==================
:kbd:`Prefix + l` (lower case L), to switch to the **last-used** window.
:kbd:`Prefix + o`, to go to the **next pane**.
:kbd:`Prefix + ;`, to **toggle** between the **current** and the **previous** pane.

Swap panes
==========
:kbd:`Prefix + {`, to **swap** the **current** pane with the one to the **left**.
:kbd:`Prefix + }`, to **swap** the **current** pane with the one to the **right**.

If we need a more precise control, we can run the command::

    $ swap-pane -s 3 -t 5

Resize panes
============
We have three ways of resizing panes:

* Using a shortcut.
* Using commands.
* With the mouse (didnt' work for me in macOS)

Using shortcut
--------------
The following shortcut worked flawlessly:

1. Press ``Prefix`` and release.
2. **Press and hold** ``Meta`` (**Option** key on macOS) and use the arrow keys repeatedly to resize current pane.

Using commands
--------------
To resize panes we can open a **tmux prompt** and use the ``resize-pane`` command. This command will resize the **current panel**, the one we were in when we issued the prompt. This command accepts four options:

* ``-U`` will resize the pane **upward** by one **cell**.
* ``-D`` will do it **downward**.
* ``-L`` to the **left**.
* ``-R`` to the **right**.

The default is **one cell**, but we can specify a number of cells, for example::

    :resize-pane -L 5

The command above resize the **current panel** 5 cells to the right.

Close a pane
============
:kbd:`Prefix + x`, to **close** current pane.

Respawn a pane
==============

.. _`entry`: https://en.wikipedia.org/wiki/Unix_philosophy