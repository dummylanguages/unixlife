*******
Windows
*******
``tmux`` shows the window list in the **status bar**, which by default, appears justified to the **left**. It's easy to center it with::

    set status-justify centre

Windows are **zero-based** numbered, and they are marked with two labels:

* The **current one** is marked with an ``*`` (next to the right).
* Whereas the **previous one** is labeled with a ``-``.

Create a new window
===================
There are several ways to do this:

1. The **most efficient** way is to use the shortcut :kbd:`Prefix + c`, write the name and :kbd:`Enter`. The problem with this approach is that our window won't have a name.
2. Once we are attached to a **tmux session** we can run the command::

    $ tmux new-window

3. Or launch the **tmux prompt** (:kbd:`Prefix + :`) and after the ``:`` prompt, run ``new-window``.

Move between windows
====================
Several commands to move between windows:

* :kbd:`Prefix + l` (lower case L), to switch to the **last-used** window.
* :kbd:`Prefix + p` , to switch to the **previous** window.
* :kbd:`Prefix + n` , to switch to the **next** window.
* :kbd:`Prefix + 0...9` , to switch to **window number** ``0`` to ``9``.

Swap windows
============
There's a command to swap a window's position. Enable **command mode** with :kbd:`Prefix + :` and run:

* ``swap-window -s 2 -t 3``, will swap the window number ``2`` (the source) with window number ``3`` (the destination)
* ``swap-window -t -1``, will swap the **current window** one position to the left (``-1``).

Kill a window
=============
There are several ways to do this:

1. Once we are attached to a **tmux session** we can close the current window::

    $ tmux kill-window

.. note:: In this case we can specify a window number using ``-t n``, where ``n`` represents the **number** or the **name** of the window we want to kill.

2. Or use the shortcut :kbd:`Prefix + &`, and confirm with ``y`` in the tmux prompt. This is the **most efficient** way to kill the **current window**.
3. Or launch the **tmux prompt** (:kbd:`Prefix + :`) and after the ``:`` prompt, run ``kill-window`` and confirm. Here we can specify the window name or number too.

Typing ``exit`` on our shell prompt also works.

Don't kill windows
==================
Sometimes windows become unresponsive, but killing them would mess up our carefully crafted layout. In these situations is way more appropriate to just **respawn** the **pane**. Just launch the **tmux prompt** (:kbd:`Prefix + :`) and run::

    :respawn-window

Rename a window
===============
Again, several ways. Let's start with the most efficient one:

1. The shortcut :kbd:`Prefix + ,` opens the prompt with the ``(rename-window)`` command, so we just have to write the **new name** and press :kbd:`Enter`.
2. Once we are attached to a **tmux session** we can rename the **current window**::

    $ tmux rename-window foo

Or we can rename other than the current window; in that case we have to specify its name after the ``-t`` option::

    $ tmux rename-window foo new-foo

3. Launch the the **tmux prompt** (:kbd:`Prefix + :`) and after the ``:`` prompt, run any of the commands above.


.. _`entry`: https://en.wikipedia.org/wiki/Unix_philosophy