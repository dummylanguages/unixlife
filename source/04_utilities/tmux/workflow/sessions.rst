********
Sessions
********
Invoking the ``tmux`` command from our terminal is equivalent to::

    $ tmux new-session

By default, our **session name** will be given a **number**, which isn’t too descriptive.

Listing sessions
================
We can list our existing sessions:

1. From outside, running::

    $ tmux ls
    0: 1 windows (created Thu Apr 16 11:09:26 2020) [173x44]
    1: 1 windows (created Thu Apr 16 14:10:03 2020) [173x44]

2. From inside ``tmux`` using the shortcut :kbd:`Prefix + s`, use the arrow to select the session you want, and :kbd:`Enter` to switch.

As you can see, the output shows the amount of windows in each session.

Naming and renaming
===================
It's a good idea to name our sessions at the same time we create them::

    $ tmux new-session -s'my rails project'

But if you forgot to do so, you can always go ahead and rename your session:

1. From inside ``tmux`` using the shortcut :kbd:`Prefix + $`, which will open

2. From outside, running::

    $ tmux rename-session -t 0 "first one"

.. note:: Note that sessions are **zero-based** numbered; so the first one is number ``0``, the second one number ``1``, and so on...

Detach from a session
=====================
When we are inside a session and want out, we gotta **dettach** from it using:

1. The shortcut :kbd:`Prefix + d`
2. Open the **tmux prompt** (:kbd:`Prefix + :`) and run ``detach``.

Attach to a running session
===========================
To attach to session ``fist one``::

    $ tmux attach-session -t primerisima

Switch to a different session
=============================
Open the **tmux prompt** (:kbd:`Prefix + :`) and run::

    attach -t session-name

We can also use the session number.

Shortcuts
---------
If we only have two sessions:

* :kbd:`Prefix + (` to switch to the **previous session**.
* :kbd:`Prefix + )` to switch to the **next session**.

Killing a session
=================
Open the **tmux prompt** (:kbd:`Prefix + :`) and run::

    :kill-session -t session-name

Or if we are detached from any session, in our shell::

    $ tmux kill-session -t session-name

.. _`Tmux`: https://github.com/tmux/tmux/wiki