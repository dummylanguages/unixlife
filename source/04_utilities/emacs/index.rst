#####
Emacs
#####

.. toctree::
    :maxdepth: 3
    :hidden:

    install
    switching_configs
    spacemacs/index
    doom/index

According to `Wikipedia`_:

    Emacs or EMACS (Editor MACroS) is a family of text editors that are characterized by their extensibility.

The most widely used variant, is `GNU Emacs`_. To learn more about this distribution check:

* `GNU Emacs manual`_
* `An Introduction to Programming in Emacs Lisp`_
* `Emacs Lisp Reference Manual`_
* `Other Emacs manuals`_



.. _`Wikipedia`: https://en.wikipedia.org/wiki/Emacs
.. _`GNU Emacs`: https://www.gnu.org/software/emacs/
.. _`GNU Emacs manual`: https://www.gnu.org/software/emacs/manual/html_node/emacs/index.html
.. _`An Introduction to Programming in Emacs Lisp`: https://www.gnu.org/software/emacs/manual/html_node/eintr/index.html
.. _`Emacs Lisp Reference Manual`: https://www.gnu.org/software/emacs/manual/html_node/elisp/index.html
.. _`Other Emacs manuals`: https://www.gnu.org/software/emacs/manual/index.html
