************
Installation
************
The main page of `GNU Emacs`_ provides links with instructions for download and installation for all major operating systems.

Installing in macOS
===================
There are several ways  to install GNU Emacs in macOS, but some are preferable for our goals.

Emacs for OSX (Nope)
--------------------
The `Emacs for OSX`_ website provides universal binaries, just **download** and **install**.

.. warning:: Don't follow this route.

Homebrew
--------
If you prefer to use the Homebrew package manager, there are several recipes. The one that works at the time of writing this is::

    $ brew tap d12frosted/emacs-plus
    $ brew install emacs-plus
    $ ln -s /usr/local/opt/emacs-plus/Emacs.app /Applications/Emacs.app

.. warning:: Don't use ``brew cask install emacs``!!

Configuration
=============
Not much to configure, only creating an **alias** in our shell configuration file, so we can launch **GUI Emacs** from our command line. (In the next section we'll see why this is useful.)

So in my case I added the line::

    alias gemacs='open -a /Applications/Emacs.app --args $1'

This way I still have available ``emacs`` in case I want to launch it inside the terminal, instead of the GUI.

.. _`GNU Emacs`: https://www.gnu.org/software/emacs/
.. _`Emacs for OSX`: https://emacsformacosx.com/
