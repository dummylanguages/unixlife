*************
Configuration
*************
`Doom Emacs`_  relies upon several configuration files that are located under the ``~/.doom.d`` directory:

* ``config.el``, where we add our custom settings (fonts, modules configuration, etc). Run ``doom sync`` after changes.
* ``init.el`` it's where we enable or disable Emacs modules. Don't forget to run ``doom refresh`` after changes.
* ``packages.el``


.. _`Doom Emacs`: https://github.com/hlissner/doom-emacs
