************
Installation
************
To install `Doom Emacs`_ we just have to clone the repo into our Emacs configuration folder, but first of all let's back up the original configuration::

    $ mv ~/.emacs.d ~/.emacs.d.bak

Now we can clone::

    $ git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
    $ ~/.emacs.d/bin/doom install

Adding ``doom`` binaries to PATH
================================
Since the framework contains the ``doom`` executable, it's a good idea to add its location to our PATH::

    export PATH="${HOME}/.emacs.d/bin:${PATH}"

The line above makes possible to invoke any of the following subcommands from anywhere:

* ``doom sync`` to synchronize your private config with Doom. Installs new packages, removes orphaned packages and regenerates caches. Run this whenever you modify your private ``init.el`` or ``packages.el``, or install/remove an Emacs package through your OS package manager (e.g. mu4e or agda).
* ``doom upgrade`` to update Doom to the latest release & all installed packages.
* ``doom doctor`` to diagnose common issues with your system and config.
* ``doom env`` to dump a snapshot of your shell environment to a file that Doom will load at startup. This allows Emacs to inherit your PATH, among other things.
* ``doom build`` to recompile all installed packages (use this if you up/downgrade Emacs).

Dependencies
============
There are several **dependencies** that need to be satisfied:

* `GNU Emacs`_, obviously. Version 26.1+ (27 is recommended) with GNUTLS support
* Git 2.23+
* ripgrep 11.0+
* GNU find



.. _`Doom Emacs`: https://github.com/hlissner/doom-emacs
.. _`GNU Emacs`: https://www.gnu.org/software/emacs/
