*************
Configuration
*************
When we install/clone `Spacemacs`_, we're basically replacing the ``~/.emacs.d`` folder with all the configuration files and folders that conform Spacemacs. For any configuration settings we wanted to add, we shouldn't touch any of these files or folders, instead there are two locations where these should be placed:

* A `dotfile`_ named ``~/.spacemacs``, should be enough if we are happy enough with most of the Spacemacs defaults.
* A `dotdirectory`_ named ``~/.spacemacs.d``, in case a single file is not enough for our configuration needs.

.. note:: These names can be changed if we use `Chemacs`_; read what we wrote :ref:`here <chemacs_label>` about it.

Dotfile
=======
If we need to edit ``~/.spacemacs`` there's no need to open a file tree explorer or even doing a file fuzzy search for it, we just have to use the shortcut ``SPC f e d``.

There are several sections inside this file:

* The **layers** function, which contains the ``dotspacemacs-configuration-layers`` section.
* The **initialization** function.
* The **initialization** function for user code.

Configuration layers
--------------------
In this section we just have to **uncomment** the lines (``SPC c l``) of the layers we want to use, and install their packages using any of the shortcuts:

* ``SPC f e R``, Vim style.
* ``M-m f e R``, Emacs style.

.. note:: To check the list of configuration layers we can press ``SPC h l``.


Creating a dotdirectory
=======================
By default, after installing Spacemacs we're given just a single **dotfile**, but we can easily turn it into a **dotdirectory**. We just have to create the ``~/.spacemacs.d`` folder with a ``init.el`` file in it::

    $ mkdir ~/.spacemacs.d
    $ touch ~/.spacemacs.d/init.el
    $ cat ~/.spacemacs > ~/.spacemacs.d/init.el
    $ rm .spacemacs # Now we can remove the dotfile.

Layers
======
In Spacemacs, a **layer** is typically a set of one or more packages, as well as the glue configuration code required to make them play well with each other and Spacemacs in general. A layer is simply a **folder** somewhere in Spacemacs' **layer search path** that usually contains these files (listed in loading order):

* ``layers.el``, declare additional layers
* ``packages.el``, which contains the packages list and configuration.
* ``funcs.el``, where all functions used in the layer should be declared.
* ``config.el`` contains layer specific configuration.
* ``keybindings.el`` for general key bindings.

The load path
-------------
The load path is a list of paths where elisp files can be found, and you can inspect it through ``SPC h d v load-path`` in Spacemacs. We can add additional directories to the **load path** by adding::

    (push "/some/path/" load-path)



.. _`Spacemacs`: https://www.spacemacs.org/
.. _`dotfile`: https://www.spacemacs.org/
.. _`dotdirectory`: https://www.spacemacs.org/
.. _`Chemacs`: https://github.com/plexus/chemacs
