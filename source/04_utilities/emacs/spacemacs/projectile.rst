**********
Projectile
**********
`Projectile`_ is a project interaction library for Emacs. All project related stuff is handled in `Spacemacs`_ is handled by this extension.

What is a project?
==================
A project is just a root folder that serves as a reference to limit certain operations such as file searchs. Projects are really useful because they are accessible through a list, and once we have switched to one, they allow us to limit our file/text search to files inside the project folder.

Adding a project
================
There are two ways of **adding a project** folder to the list of projects:

* Once we've opened a folder or a file that is in a directory under **source control**, Spacemacs automatically adds it to the list.
* A more explicit way is just to add an empty ``.projectile`` file inside the folder (``touch .projectile``), and open a file or subdirectory.

Projectile commands
===================
All project related commands start with the **shortcut prefix** ``SPC p``, which will open the Projectile menu. There we have:

* ``p`` to list our projects.
* ``SPC p f`` fuzzy search for **files** inside the project folder.
* ``SPC p t`` file explorer search (Neotree or Treemacs).
* ``SPC p /`` fuzzy search for **text** within files located inside the project folder.

In all these cases, our search is narrowed down to the root folder of the **current project**.

.. _`Spacemacs`: https://www.spacemacs.org/
.. _`Projectile`: https://github.com/bbatsov/projectile
