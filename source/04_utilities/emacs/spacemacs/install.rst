************
Installation
************
The `Github`_ page contains detailed instructions to install `Spacemacs`_:

1. Basically we just have to clone the repo into our Emacs configuration folder, but first of all let's **back up** the original configuration::

    $ mv ~/.emacs.d ~/.emacs.d.bak

2. Now we can **clone**::

    $ git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

**Optionally**, if we want to enjoy the latest features, we can ``git checkout`` the **develop** branch::

    $ cd ~/.emacs.d
    $ git checkout develop

3. After that, we just have to **launch** Emacs, and Spacemacs will automatically install the packages it requires.

4. Finally, **restart** Emacs to complete the installation.


.. _`Github`: https://github.com/syl20bnr/spacemacs
.. _`Spacemacs`: https://www.spacemacs.org/
.. _`GNU Emacs`: https://www.gnu.org/software/emacs/
