#########
Spacemacs
#########

.. toctree::
    :maxdepth: 3

    install
    configuration
    using
    windows
    dired
    terminal
    org_mode
    projectile

`Spacemacs`_ is a `GNU Emacs`_ distribution. It uses, by default, `Evil`_ which is a Vim emulation layer that emulates the main features of the Vim editor and provides facilities for writing custom extensions.

.. note:: The `Emacs wiki`_ contains a page dedicated to this extension, where are listed several plugins for **Evil**.

.. _`Spacemacs`: https://www.spacemacs.org/
.. _`GNU Emacs`: https://www.gnu.org/software/emacs/
.. _`Evil`: https://github.com/emacs-evil/evil
.. _`Emacs wiki`: https://www.emacswiki.org/emacs/Evil

