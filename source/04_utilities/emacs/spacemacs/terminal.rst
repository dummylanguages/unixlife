********
Terminal
********
In `Spacemacs`_ all stuff related to the terminal is configured in the `Shell layer`_. To use it we just have to make sure that the layer is enabled in our Spacemacs configuration. Under ``dotspacemacs-configuration-layers`` we should have an entry named ``shell``; it's as simple as that.

.. note:: Don't forget to reload your configuration with ``SPC f e R``.

With that we can open the **default shell** in a horizontal split at the bottom, by using the shortcut ``SPC '``.

.. note:: ``ansi-term`` is the default on Linux/macOS, whereas in Windows is ``eshell``.

To exit the mini shell, just kill its buffer or type ``exit``.

Shell layer configuration
=========================
Instead of just enabling the layer with ``shell``, I added a minimal configuration:

.. code-block:: lisp

    (shell :variables
            shell-default-height 30
            shell-default-position 'bottom
            shell-default-shell 'eshell)


Open a terminal in its own window
=================================
Sometimes we may need more vertical space for a terminal, for example when running tests for some code. In that case we can switch to whatever window we want to open the terminal in, and enter Vim command mode (press ``:``). Then type the name of any of the available terminals:

* ``eshell``. We can even run ELisp commands in this shell (Default on Windows).
* ``shell``, the **inferior shell**.
* ``term``, the **terminal emulator**.
* ``ansi-term``, term (default on Linux/macOS)
* ``vterm``, the new addition. Check instructions here about `how to install it`_.

Opening multiple terminals
--------------------------
We can open and move between multiple terminal buffers by preceding the basic shortcut with a number. For example, to move to terminal 2 we would type ``2 SPC '``.

Opening the terminal in the root of our project
-----------------------------------------------
Just press ``SPC p '``.

.. _`Spacemacs`: https://www.spacemacs.org/
.. _`Shell layer`: https://develop.spacemacs.org/layers/+tools/shell/README.html
.. _`how to install it`: https://develop.spacemacs.org/layers/+tools/shell/README.html#install-vterm

