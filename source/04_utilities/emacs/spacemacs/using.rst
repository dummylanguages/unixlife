*****
Using
*****
Let's talk about the bare minimum to start using `Spacemacs`_.

Font is too small
=================
Pressing ``SPC z f`` will allow us to temporarily zoom in and out, pressing the ``+`` and ``-`` keys.

.. note:: That shortcut opens the **Zoom Frame Transient State**. Press ``q`` to exit the transient state.

To modify the font size permanently, we must edit our configuration file.

Opening the configuration file
==============================
In Spacemacs, the user configuration is located, by default, in ``~/.spacemacs``, but if for some reason that's not the case, the shortcut ``SPC f e`` (File Emacs/Spacemacs) leads ``which-key`` to the section related to configuration files. Once there we press ``d`` to open the Spacemacs dotfile. Once the file is open, we just have to look for font settings and adjust the size.

.. note:: To **save** the **buffer**: ``SPC f s``, or use the traditional Vim command ``:w``.

Changes to some settings require Emacs to be restarted. Again, we can use the Vim way, ``:q``, or the Spacemacs way ``SPC q q``.

Reloading configuration
-----------------------
Once the config file is open, we may want to enable some `layers`_, such as Markdown or any other languages you use. Most of the times this is just a matter of uncommenting a line.

.. note:: To **toggle comment** a line: ``SPC c l``.

Once we've made changes to our configuration, we can **reload** it with ``SPC f e R``.

Opening files
=============
Apart from opening Emacs/Spacemacs configuration files, the shortcut prefix ``SPC f`` opens up a rich set of options related to files.

.. note:: We can also open files the Vim way, ``:e PATH/filename``.

Some useful ones:

* ``SPC f s`` to ``save-file``. (Vim way, ``:w``)
* ``SPC f d`` to ``kill-this-buffer``. (Vim way, ``:bd``)


Fuzzy search
------------
``SPC f f`` opens up **HELM Find Files** which is a files fuzzy finder. We can start typing the name of a folder, press ``Tab`` for **autocomplete** its name, or ``Ctrl h`` to go to the **previous folder**. (Or the Emacs equivalent, ``Alt Backspace``). Pressing ``Enter`` will open a file in the current window, or the **folder** in **Dired**, the directory editor.

New file
--------
After pressing ``SPC f f``, we can type a **new name** and press ``Enter``. That will create a **new file** in our filesystem and will put an empty buffer in our current window.

.. note:: We can also create a file the Vim way, ``:e PATH/filename``.

Directory Editor
================
Once we have opened `Dired`_ (Directory Editor) in the current window, we can navigate the list of files and folders with the cursor or Vim keys, and press ``Enter`` to open a file or enter a directory.

.. note:: Even though we can use Evil mappings to interact with Dired, it's probably a better idea to use its original mappings which you can check in `GNU Emacs manual`_.

Some Spacemacs-based Dired shortcuts are:

* ``..`` to go to the **parent folder**.

Buffers
=======
Let's say we have a folder in our filesystem that contains a project that we have under source control, i.e., it contains a ``.git`` subdirectory. We can navigate to this folder using ``SPC f f``, pressing ``Ctrl + j/k`` to navigate the directories, or start typing their names and pressing ``Tab`` to autocomplete them, until we finally press ``Enter``.

Once we open a buffer, it remains open until we manually delete it. Once we open ``SPC b``, some commands/shortcuts related to buffers are:

* ``b`` opens **Helm Mini buffer** where we can search all the open buffers.
* ``h`` will take us to the Spacemacs **welcome screen**.
* ``d`` will **delete** the current buffer. (V
* To alternate between the two last opened buffers we use ``SPC Tab``.

.. note:: We can mark several buffers in **HELM Mini** using ``Ctrl + SPC``; **delete** them all together with ``M + D`` (that's capital ``D``)

Buffer list
-----------
All the files we open, as well as the **Dired** itself, are opened in **buffers**. To list the buffers we use ``SPC b b`` and then ``Ctrl j`` or ``Ctrl k`` to navigate the list; ``Enter`` to open the buffer in the current window.

Fuzzy file search
-----------------
Once we have switched to a project, we can fuzzy search for files inside the project folder using the shortcut:

* ``SPC p f``, which opens **HELM Projectile**.

File explorer
-------------
Aside from fuzzy search for files, we can open a file explorer pressing ``SPC p t``, which depending on your Spacemacs version, will open either `Neotree`_ or `Treemacs`_ on a vertical side split.

These are some shortcuts for **Treemacs**:

* Pressing the ``?`` will **toggle** a useful **help**.
* Navigating through **Treemacs** uses the same Vim keys.
* To **open** a file or folder, ``Enter``.
* To move between the file and explorer windows ``SPC w w`` is the fastest choice if you have only two windows. Otherwise ``SPC w h`` and ``SPC w l`` also work.
* To **quit** the explorer, ``q``.

If during our file explorations we open a file for which there's no extension available, will be asked to install a **layer** to deal with that filetype.

Fuzzy text search
-----------------
If we choose the ``SPC p /`` we can grep the whole project for any text instance. If we installed any other file searcher such as `Ripgrep`_ or `The Silver Searcher`_, that would be automatically used by **Helm**.

Using ``SPC *`` is even more convenient, since we don't have to do any typing, it automatically searches for the definition under the cursor.

Jump to code definitions
========================
Another cool feature of **Helm** is the ability to **jump** to any function within the current file.

* ``, g g``, and type the name of the function; ``Enter`` will take you to where it's defined.
* ``SPC j i`` will **list the functions** in a file.
* ``SPC s s`` will open **HELM Swoop**, which it's a fuzzy search limited to the current file. Type words separated by spaces and you will fly to the text.

.. note:: We can also use the Vim ``*``, which will open the **Symbol Highlight Transient State** where we can select where we can go.

Don't forget the **Vim-based** available mappings; to search for words in our file we can use Vim's ``/``; to turn off the highlight results, ``SPC s c`` (equivalent to ``:nohl``.

The ``avy`` package
-------------------
If you have the `avy`_ package installed, we can jump to any word on the screen (no matter the window) by pressing:

* ``SPC j j``, the screen will gray out, when we type the string we want to search for, different **labels** will appear; typing any of them will take our cursor there.

Also, if we add the following lines to our configuration::

    (define-key evil-normal-state-map (kbd "M-s") #'avy-goto-char-timer)
    (define-key evil-normal-state-map (kbd "M-w") #'avy-goto-word-1)

We'll have available the following shortcuts:

* ``M s``, as in **search**.
* ``M w``, as in **word**.

Tags tools
==========
Jumping to definitions may get a little slow with large codebases. That's where you can use tools to generate tags, such as:

* `GNU global`_
* `ggtags`_, an Emacs package.

With these tools installed, pressing ``SPC p G`` (**Project Generate tags**) will generate a static database of your code, giving you instant search for variables, classes, and functions in your entire codebase.


.. _`Spacemacs`: https://www.spacemacs.org/
.. _`layers`: https://develop.spacemacs.org/doc/VIMUSERS.html#layers
.. _`Dired`: https://en.wikipedia.org/wiki/Dired
.. _`Neotree`: https://github.com/jaypei/emacs-neotree
.. _`Treemacs`: https://github.com/Alexander-Miller/treemacs
.. _`Helm`: https://github.com/emacs-helm/helm
.. _`Ripgrep`: https://github.com/BurntSushi/ripgrep
.. _`The Silver Searcher`: https://github.com/ggreer/the_silver_searcher
.. _`GNU Emacs manual`: https://www.gnu.org/software/emacs/manual/html_node/emacs/Dired.html
.. _`avy`: https://github.com/abo-abo/avy
.. _`GNU global`: https://www.gnu.org/software/global/
.. _`ggtags`:  https://github.com/leoliu/ggtags
