********
Org mode
********
`Org mode`_ is for keeping notes, maintaining TODO lists, planning projects, and authoring documents with a plain-text markup language.

Here we'll see how to use this mode in `Spacemacs`_, using the shortcuts provided by `Evil Org mode`_

Enabling the layer
==================
The first thing we should do in `Spacemacs`_ in order to use **Org mode** is to enable the `org layer`_.

Creating a file
===============
We just have to create a file with the ``.org`` extension using:

* The Vim way: ``:e test.org``
* The Spacemacs way: ``SPC f f``, type the file name and press ``Enter``.

Headlines
=========
We can create **headlines** of several levels::

    * Top level
        ** Level 2
            ** Level 3

.. note:: Pressing ``SPC SPC`` (or ``M-x``) will open the Emacs `minibuffer`_ where we can change to **text mode** typing ``text-mode``; run ``org-mode`` to go back to **org mode**.

Collapsing/expanding
--------------------
The main feature of headlines is that they're collapsable:

1. To collapse the headings we can use the ``Tab`` key, which will collapse all the contents under a given header.
2. Pressing ``Shift Tab`` once will collapse everything; twice will expand only the headlines; thrice will expand everything.

Indenting levels
----------------
We can type ``*`` to increase the headline levels, or just use the shortcuts ``Alt h`` or ``Alt l`` and save some typing (We can also use ``Alt`` with the arrow keys).

Searching
=========
In Spacemacs we can use ``SPC j i`` to open a minibuffer underneath where we can search for links, images, etc.

.. _`Spacemacs`: https://www.spacemacs.org/
.. _`Org mode`: https://orgmode.org
.. _`org layer`: https://www.spacemacs.org/layers/+emacs/org/README.html
.. _`Evil Org mode`: https://github.com/Somelauw/evil-org-mode
.. _`minibuffer`: https://www.gnu.org/software/emacs/manual/html_node/emacs/Minibuffer.html#Minibuffer
