*******
Windows
*******
In `Spacemacs`_ (and Emacs), a frame can be split in several windows. By default a frame has a single window, the one where we edit the current buffer. To access the **windows menu**, press ``SPD w``. That gives us access to the commands:

* ``v`` or also ``/``, creates a **vertical split**.
* ``s`` to create a **horizontal split**.
* ``m`` to **toggle maximization** of a split.
* ``d`` to **delete** a split.
* ``h``, ``j``, ``k`` and ``l`` to navigate to other windows.
* ``SPC 0-9`` will take us to any window using its number; for example, ``SPC 2``, to go to window **number 2**.

We can also click on a window to enable it.

.. _`Spacemacs`: https://www.spacemacs.org/
