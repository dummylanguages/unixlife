************************
Directory Editor (dired)
************************
`Dired`_ is an Emacs builtin extension to show a list of directories and files in a read-only buffer.

There are a couple of ways of launching Dired:

* ``SPC a d`` will **launch** the Dired ``application`` in a **HELM Mini buffer**.
* If we already are in the **HELM Mini buffer**, hitting ``Enter`` when some directory name ends with a ``/``, Dired will open in a dedicated buffer.

Dired dedicated buffer
======================
Some shortcuts here:

* ``q`` to **quit**.
* We can move around with the **arrow keys**, **Vim keys**.
* ``Enter`` to enter a subdirectory or **edit** a file.
* ``!`` to open a file with an **external application** (will use the ``open`` command in macOS).
* ``Shift-c`` to make ``dired-do-copy``. It will open a minibuffer to select destination.
* ``Shift-r`` to move/rename a file (``dired-do-rename``). It will open a minibuffer to select destination.
* ``Shift-d`` to delete a file. It will ask for confirmation (y or n).


.. _`Dired`: https://en.wikipedia.org/wiki/Dired
.. _`Spacemacs`: https://www.spacemacs.org/
