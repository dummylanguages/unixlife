.. _chemacs_label:

*****************************
Switching Emacs distributions
*****************************
`Chemacs`_ is an Emacs profile switcher, it makes it easy to run multiple Emacs configurations side by side.

Installation
============
1. Clone the Git repository, and run install.sh::

    $ git clone https://github.com/plexus/chemacs.git
    $ cd chemacs
    $ ./install.sh
    OK      Creating symlink ~/.emacs -> /home/arne/chemacs/.emacs

2. Download the Chemacs’ startup script to ``~/.emacs``::

    $ wget -O ~/.emacs https://raw.githubusercontent.com/plexus/chemacs/master/.emacs

.. note:: The `doom-emacs`_ github page warns that the ``~/.emacs.d`` directory must not exist for this to work. Hmm..

Configuration
=============
We create launch profiles in the file ``~/.emacs-profiles.el``. The main thing to configure is the ``user-emacs-directory``, for example::

    (("default" . ((user-emacs-directory . "~/.emacs.d")))
    ("spacemacs" . ((user-emacs-directory . "~/.emacs.spacemacs.d"))))

* The ``~/.emacs.d`` directory is the folder where we can keep our configuration for **vanilla Emacs**.
* The ``~/.emacs.spacemacs.d`` directory is the folder where we clone the **Spacemacs** framework.

Configuring Spacemacs
---------------------
**Chemacs** offers the ``SPACEMACSDIR`` environment variable to point at a directory with customizations that are applied on top of the base install. `Spacemacs`_ is typically installed by cloning the `Spacemacs repo`_ to ``~/.emacs.d``, and doing **extra customization** from ``~/.spacemacs`` or ``~/.spacemacs.d/init.el``. The aforementioned variable, needs a **directory** to work, so if you have just a ``~/.spacemacs`` file, you can::

    $ mkdir ~/.stable-spacemacs-config
    $ touch ~/.stable-spacemacs-config/init.el
    $ cat ~/.spacemacs > ~/.stable-spacemacs-config/init.el
    $ rm .spacemacs

I named my configuration folder ``~/.stable-spacemacs-config``, because I'm gonna create another one for experimentations with the ``develop`` branch of Spacemacs, but that's not necessary.

Then we'll modify the ``~/.emacs-profiles.el`` to add::

    (("default" . ((user-emacs-directory . "~/.emacs.d")))

     ("space"   . ((user-emacs-directory . "~/.emacs.space.d")                ;; stable branch.
                  (env . (("SPACEMACSDIR". "~/.stable-spacemacs-config")))))  ;; Configuration folder.

Usually the directory of the **base installation** is not duplicated, but the Spacemacs ``develop`` branch is at version **0.300** whereas the ``stable`` is still at **0.2ish**. We can create as many profiles as we want, specially with different **configuration folders** and people do so to experiment with different languages that have different needs. (Although I don't know if this can be achieve with layers...?

Configuring doom-emacs
----------------------

Default profile
---------------
For those scenarios where we want to launch Emacs GUI without opening a terminal, we can create an additional file named ``~/.emacs-profile``, containing the name of the profile you’d like to be used when none is given on the command line::

    $ echo 'spacemacs' > ~/.emacs-profile

We can change the **default** by simply changing the contents of this file.

Usage
=====
Basically consists in calling ``emacs`` or ``gemacs`` passing the ``--with-profile`` option, and the name of a profile that we have defined in the ``~/.emacs-profiles.el`` file.

.. _`Chemacs`: https://github.com/plexus/chemacs
.. _`doom-emacs`: https://github.com/hlissner/doom-emacs/blob/develop/docs/getting_started.org#install-doom-alongside-other-configs-with-chemacs
.. _`Spacemacs`: https://www.spacemacs.org/
.. _`Spacemacs repo`: https://github.com/syl20bnr/spacemacs
