*************
Installing it
*************
There are several ways of installing `fzf`_

macOs
=====
Using homebrew::

    $ brew install fzf

And if you want to install the included useful key bindings and fuzzy completion::

    $(brew --prefix)/opt/fzf/install

To upgrade it::

    $ brew update; brew reinstall fzf

Using git (recommended)
=======================
Alternatively, you can ``git clone`` this repository to any directory and run install script::

    $ git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    $ ~/.fzf/install

.. note:: If you work with several systems, this solution is way more portable; it only requires ``git``.

To upgrade it::

    $ cd ~/.fzf && git pull && ./install

Running the install script
--------------------------
Move to the folder where we cloned **fzf** and::

    $ . ./.fzf/install
    Downloading bin/fzf ...
    [...]
    Do you want to enable fuzzy auto-completion? ([y]/n)
    Do you want to enable key bindings? ([y]/n)

    Generate /Users/fito/.fzf.bash ... OK
    Generate /Users/fito/.fzf.zsh ... OK

    Do you want to update your shell configuration files? ([y]/n)

    Update /Users/fito/.bashrc:
    - [ -f ~/.fzf.bash ] && source ~/.fzf.bash
        + Added

    Update /Users/fito/.zshrc:
    - [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
        + Added

    Finished. Restart your shell or reload config file.
    source ~/.bashrc  # bash  (.bashrc should be loaded from .bash_profile)
    source ~/.zshrc   # zsh

    Use uninstall script to remove fzf.

    For more information, see: https://github.com/junegunn/fzf

Running the script above download the **autocompletion** and **key bindings** scripts, and add the following line at the end of our **shell configuration**, in my case ``~/.bashrc``::

    [ -f ~/.fzf.bash ] && source ~/.fzf.bash

This is sourcing a Bash script which is doing three things:

1. Adding ``fzf`` to **PATH**
2. Sourcing the **autocompletion** script.
3. Sourcing the **key bindings** script.

Uninstall
---------
It includes an ``uninstall`` script::

    $ . ./.fzf/uninstall

Vim/Neovim integration
======================
Read the official documentation about `FZF Vim integration`_, it's really well written, but I just added these lines to my ``init.vim``::

    Plug 'junegunn/fzf', { 'dir': '~/.fzf' }
    Plug 'junegunn/fzf.vim'

The second one because I wanted the **latest Vim plugin** available in Github, rather than the one in the package.

.. warning:: Don't forget to ``source`` your **Vim/Neovim configuration** and run ``:PlugInstall`` after.

.. _`fzf`: https://github.com/junegunn/fzf
.. _`FZF Vim integration`: https://github.com/junegunn/fzf/blob/master/README-VIM.md
