################################
fzf: A command-line fuzzy finder
################################

.. toctree::
    :maxdepth: 3

    install
    configuration
    shell_use

`fzf`_ is a general-purpose command-line fuzzy finder. The **fzf package** consists of the following components:

* The ``fzf`` executable, which can be `downloaded`_ alone, if that's the only thing you need.
* The ``fzf-tmux`` script for launching fzf in a **tmux pane**.
* Shell extensions:

    * Key bindings (CTRL-T, CTRL-R, and ALT-C) (bash, zsh, fish)
    * Fuzzy auto-completion (bash, zsh)
    * Vim/Neovim plugin

.. _`fzf`: https://github.com/junegunn/fzf
.. _`downloaded`: https://github.com/junegunn/fzf-bin/releases