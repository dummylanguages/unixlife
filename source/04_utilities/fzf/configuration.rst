*************
Configuration
*************
There are several ways of installing `fzf`_. If you installed it using ``git clone`` to your **home directory**, and run the install script::

    $ git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    $ ~/.fzf/install

That creates a couple of **shell configuration** files for Bash and Zsh in the middle of our **home directory**:

* ``~/.fzf.bash``
* ``~/.fzf.zsh``

In the case of the one for Bash, this file is only **sourcing** the **autocompletion** and **key bindings** scripts, check it out::

    $ cat ~/.fzf.bash
    # Setup fzf
    # ---------
    if [[ ! "$PATH" == */Users/bob/.fzf/bin* ]]; then
    export PATH="${PATH:+${PATH}:}/Users/bob/.fzf/bin"
    fi

    # Auto-completion
    # ---------------
    [[ $- == *i* ]] && source "/Users/bob/.fzf/shell/completion.bash" 2> /dev/null

    # Key bindings
    # ------------
    source "/Users/bob/.fzf/shell/key-bindings.bash"

Moving config files to XDG Base Directory
=========================================
1. The first thing I'd like to do is to move all **fzf** related config files to a subfolder under the standard `XDG_CONFIG_HOME`_::

    $ mkdir ~/.config/fzf
    $ mv ~/.fzf.bash ~/.config/fzf/fzf.bash

.. note:: I've also removed the ``.`` from its name, since the file is gonna be nested in a **hidden folder**. Also, I'm a Bash user so I'll overlook the **Zshell files**, but the procedure is analogous.

2. Then in my **Bash configuration**, I'll source this file in the new location::

    [ -f ~/.config/fzf/fzf.bash ] && source ~/.config/fzf/fzf.bash

3. Since I want to customize both the **auto-completion** and **key-binding** scripts,  I'll **copy** them also to the new folder::

    $ cp ~/.fzf/shell/*.bash ~/.config/fzf

4. And finally, update the ``~/.config/fzf/.fzf.bash``, to point to the new location of these files:

.. code-block:: bash

    # Auto-completion
    # ---------------
    [[ -e "${HOME}/.config/fzf/shell/completion.bash" ]] &&
    source "${HOME}/.config/fzf/shell/completion.bash" 2> /dev/null # && echo 'FZF completions - sourced!'

    # Key bindings
    # ------------
    [[ -e "${HOME}/.config/fzf/shell/key-bindings.bash" ]]&&
    source "${HOME}/.config/fzf/shell/key-bindings.bash" 2> /dev/null # && echo 'FZF keybindings - sourced!'


.. note:: Apart from this I'll also **backup** this new folder to my **dotfiles** folder, and ``stow`` them.

Now I can freely modify these copies, while keeping a backup under the ``~/.fzf`` folder, just in case I mess up.

Vim/Neovim integration
======================
Read the official documentation about `FZF Vim integration`_, it's really well written, but I just added this line to my ``init.vim``::

    Plug 'junegunn/fzf.vim'

Because I wanted the **latest Vim plugin** available in Github, rather than the one in the package.

.. warning:: Don't forget to ``source`` your **Vim/Neovim configuration** and run ``:PlugInstall`` after.

.. _`fzf`: https://github.com/junegunn/fzf
.. _`FZF Vim integration`: https://github.com/junegunn/fzf/blob/master/README-VIM.md
.. _`XDG_CONFIG_HOME`: https://www.freedesktop.org/wiki/Software/xdg-user-dirs/
