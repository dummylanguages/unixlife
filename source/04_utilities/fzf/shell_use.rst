*********************
Using it in the shell
*********************
There are several ways of using `fzf`_ on the **shell**:

* We can use `fuzzy patterns`_
* Or `key bindings`_

Even though its primary use is to search for **files and directories**, it can also be used to search our **command history**, **process IDs**, **enviroment variables** and even **hostnames** (for ``ssh`` or ``telnet``).

Searching for files and directories
===================================
Fuzzy completion for files and directories can be triggered if the word before the cursor ends with the **trigger sequence** which is by default ``**``. The synopsis would be something like this::

    $ COMMAND [DIRECTORY/][FUZZY_PATTERN]**<TAB>

For example, imagine you are somewhere into an ultra deep nested subdirectory in our filesystem, and want to change to the directory where you keep your **Neovim configuration**, and the only thing you remember is the name of the subdirectory, ``nvim``, but not sure where is located::

    $ cd ~**<TAB>
    > 'nvim
    19/48057
    > /Users/fito/.config/nvim
    /Users/fito/.local/share/nvim
    /Users/fito/.local/share/nvim/swap
    [...]

You would use ``cd ~``, because you know the ``nvim`` folder was somewhere in your home directory, then you would type ``**`` and press ``<TAB>`` start a **fuzzy search** under ``~``. In the **fzf prompt** you will start a search for an **exact match** using ``'nvim`` (note the single ``'`` apostrophe preceding the search term). You would use the **arrow keys** or ``<Ctrl>`` together with the traditional **vim keys** to navigate to the result you want; finally press ``<ENTER>`` to end the fuzzy search.

.. note:: If you don't remember if the folder you're looking for is named ``gvim`` or ``nvim`` or something that ends in ``vim``, you should search for a **fuzzy match**; meaning, at the **fzf prompt** don't use the ``'`` at the beginning of your **search term**, instead you could anchor the search term with a ``$`` sign (``vim$``) to signify that the folder name ends in *vim*.

You could press ``<Escape>``, or the **vim style shortcut** ``Ctrl - [`` to stop the **fuzzy search** at any point.

Using keybindings
-----------------
In the same scenario we've just described, we could use a slightly different approach:

1. First let's change to the **home directory** as the base for our search::

    $ cd ~

2. Then use the **default keybinding** ``Ctrl - t`` to start a **fuzzy search**::

    $ cd ~
    > 'nvim

This may not work as expected if the directory we are looking for is nested inside a **hidden directory**. The reason for that is that the shortcut is bound to a **fzf function definition** that doesn't search under hidden directories. That can be changed




.. _`fzf`: https://github.com/junegunn/fzf
.. _`fuzzy patterns`: https://github.com/junegunn/fzf#fuzzy-completion-for-bash-and-zsh
.. _`key bindings`: https://github.com/junegunn/fzf#key-bindings-for-command-line
