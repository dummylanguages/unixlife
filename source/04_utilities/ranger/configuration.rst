Configuration
=============
By default, after installing **ranger** we won't have any **configuration files**. It's easy to generate some **defaults**:
    $ ranger --copy-config=all
    creating: /home/bob/.config/ranger/rifle.conf
    creating: /home/bob/.config/ranger/commands.py
    creating: /home/bob/.config/ranger/commands_full.py
    creating: /home/bob/.config/ranger/rc.conf
    creating: /home/bob/.config/ranger/scope.sh


The files
---------
Let's go in more detail about the files we just generated:

1. ``rc.conf`` is the **Ranger** configuration read everytime it starts. It contains:

    * Options
    * Keybindings
    * Aliases

2. ``commands.py`` contains custom commands.
3. ``rifle.conf`` contains the **rules** for the file opener.
4. ``scope.sh`` is the script that generates file previews.

Apart from these files there are a couple of important but **optional** extra subdirectories:

1. ``plugins``, some examples can be found in ``/usr/share/doc/ranger/examples``.
2. ``colorschemes``. 

The ``rc.conf`` file
--------------------