Tabs
====
Ranger also incorporates tabbed functionality to allow multiple viewports into the filesystem. Here are the most common tab control keys:

+------------------+-------------------+
|      Hotkey      |      Result       |
+==================+===================+
| ``<ctrl> + n``   | Create new tab    |
+------------------+-------------------+
| ``<ctrl> + w``   | Close current tab |
+------------------+-------------------+
| ``<tab>``        | Next tab          |
+------------------+-------------------+
| ``<shift><tab>`` | Previous tab      |
+------------------+-------------------+

