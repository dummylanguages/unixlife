************************
Ranger: the file manager
************************
`Ranger`_ is a console file manager with VI key bindings. It's written in Python and uses the `curses`_ library to provide a minimalistic interface to interact with the directory hierarchy. It ships with rifle, a **file launcher** that is good at automatically finding out which program to use for what file type.

It's even possible to use it from within Vim/Neovim as a file manager, instead to the default **netrw** or **nerdtree**.

.. toctree::
    :maxdepth: 3
    :hidden:

    installation
    configuration
    options
    basic_commands
    basic_keybindings
    tabs
    bookmarks
    preview_images

.. _`Ranger`: https://github.com/ranger/ranger
.. _`curses`: https://en.wikipedia.org/wiki/Curses_(programming_library)