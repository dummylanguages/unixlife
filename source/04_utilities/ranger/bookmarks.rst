Bookmarks
=========
Ranger also offers bookmarking functionality:

* To **bookmark** a directory press ``m<bookmark_name>``.
* To **go to a bookmarked location** press ``'`` or `````, and select the bookmark from the list below.
* After jumping to a bookmark, we can go back to the last directory by typing `````` or ``''``.
* To **delete** a bookmark use ``um<bookmark_name>``

.. note:: The **single quote** key (``'``) and the **backtick** key (`````) are equivalent when bookmarking in Ranger.

Bookmarks are stored in the ``bookmarks`` file, located in ``~/.local/share/ranger/bookmarks``