More keybindings
================
There are more commands and hotkeys that may come in handy.

Ranger help
-----------
+-------------+------------------+
| Ranger help |                  |
+=============+==================+
| 1?          | Keybindings help |
+-------------+------------------+
| 2?          | Command help     |
+-------------+------------------+
| 3?          | Settings help    |
+-------------+------------------+

Navigation
----------

+-------------+---------------------------------------+
| Ranger help |                                       |
+=============+=======================================+
| ``[``       | Move to the **parent directory**      |
+-------------+---------------------------------------+
| ``]``       | Move to the **parent directory**      |
+-------------+---------------------------------------+
| ``S``       | Open a shell in the current directory |
+-------------+---------------------------------------+


Working with Files
------------------
Basic operations we may want to do with files are:
* Copying or cutting them.
* Moving them.
* Deleting them.
* Renaming them.


Selecting files
---------------
Once you have selected the files and directories you want, you have to press ``<Esc>``, 
+----------------+---------------------------+
| File selection |                           |
+================+===========================+
| ``<SpaceBar>`` | Non contiguous selection  |
+----------------+---------------------------+
| ``V``          | Contiguous selection      |
+----------------+---------------------------+
| ``v``          | Reverse current selection |
+----------------+---------------------------+

+----------------+------------------------+
| Renaming files |                        |
+================+========================+
| ``cw``         | Rename selection (one) |
+----------------+------------------------+
| ``I``          | Beginning of filename  |
+----------------+------------------------+
| ``A``          | End of filename        |
+----------------+------------------------+

Once we have selected one or several files or directories we have available several operations:

+---------------------------+-----------------+
| File/directory operations |                 |
+===========================+=================+
| ``yy``                    | Yank (Copy)     |
+---------------------------+-----------------+
| ``dd``                    | Delete (Cut)    |
+---------------------------+-----------------+
| ``pp``                    | Paste selection |
+---------------------------+-----------------+
| ``r``                     | Open with...    |
+---------------------------+-----------------+