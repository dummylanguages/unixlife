Previewing images
=================
One of the main attractives of using Ranger is the possibility to preview images, quite cool for a terminal based file manager. To preview images we have to:
1. First of all, set ``preview_images`` to ``true`` in ``~/.config/ranger/rc.conf``.
2. Then we need to select a method in ``set preview_images_method``:

    * The **default** is using the ``w3mimgdisplay`` command, available when we have installed a console web-browser named ``w3m``. Works with **xterm** and **urxvt** (without tmux)
    * If we are in macOS we can use the ``iterm2`` option. (Only for **iterm2** version **2.9**, and if it was compiled with image preview support)
    * ``terminology`` terminal emulator **Terminology**.
    * ``urxvt``, if it was compiled with **pixbuf** support.
    * ``urxvt-full``, same as above. It will use the whole terminal window.

macOS
-----
If you using **macOS** and **iterm2**, it's super easy to preview images. Just add the following lines to your ``~/.config/ranger/rc.conf.``::

    set preview_images true
    set preview_images_method iterm2

Check this article about `preview images`_ (I think it's old)

.. _`preview images`: https://www.everythingcli.org/ranger-image-preview-on-osx-with-iterm2/