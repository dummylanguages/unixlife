Ranger options
==============
The options can be set permanently in ``~/.config/ranger/rc.conf``, or enabled/disabled temporarily using the ``set`` command and hotkeys. For example, to see the **hidden files** we would run::

    :set show_hidden true

Or just toggle the setting with ``zh`` or ``<Ctrl + H>``.

Basic options
-------------
There are some basic **options** that can be toggled with a hotkey combo:

+-------------------+------------------------+
|    Ranger help    |                        |
+===================+========================+
| ``zh``, ``<C-H>`` | Toggle ``show_hidden`` |
+-------------------+------------------------+
| 2?                | Command help           |
+-------------------+------------------------+



Titlebar Settings
-----------------
There are several settings related to the **titlebar** that Ranger puts at the top of the window:

+--------------------------------+----------------------------------------------------+
|            Setting             |                                                    |
+================================+====================================================+
| ``status_bar_on_top``          | Show/hide the status bar at the top of the window  |
+--------------------------------+----------------------------------------------------+
| ``hostname_in_titlebar``       | Show hostname in titlebar                          |
+--------------------------------+----------------------------------------------------+
| ``show_selection_in_titlebar`` | Highlights the **filename** at the end of the path |
+--------------------------------+----------------------------------------------------+
| ``tilde_in_titlebar``          | Abbreviate $HOME with ~ in the titlebar            |
+--------------------------------+----------------------------------------------------+


Preview column Settings
-----------------------
preview_files [bool] <zp> Preview files in the preview column?
 
preview_images [bool]  Draw images inside the console with the external program w3mimgpreview?
 
preview_images_method [string] Set the preview image method. Supported methods: w3m, iterm2, urxvt, urxvt-full, terminology. See PREVIEWS section.

preview_max_size [int] Avoid previewing files that exceed a certain size, in bytes.  Use a value of 0 to disable this feature.

preview_script [string, none] Which script should handle generating previews?  If the file doesn't exist, or use_preview_script is off, ranger will handle previews itself by just printing the content.