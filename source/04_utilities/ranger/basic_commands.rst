Basic Hotkeys
=============
Since **Ranger** is inspired in Vim, we have available Vim-like shortcuts for basic navigation:

+--------+------------------------------+
| Hotkey |            Action            |
+========+==============================+
| ``h``  | Out of the directory.        |
+--------+------------------------------+
| ``j``  | **Down** the directory tree. |
+--------+------------------------------+
| ``k``  | **Up** the directory tree.   |
+--------+------------------------------+
| ``l``  | Like pressing ``<Enter>``    |
+--------+------------------------------+

The ``l`` shortcut is like pressing the ``<Enter>`` key. It has two meanings:

1. Open the **file** under the cursor, with the **default** app.
2. Enter the **directory** under the cursor.

The **default** app it's determined by the settings of **Rifle**, the **app launcher**. These settings are in the ``rifle.conf`` file.

Basic commands
==============
In order to be able to enter **Ranger commands**, you need to open the **Ranger console** by pressing ``:``. Once the console is open, here's a list of basic commands:

+-----------------+------------------------+-------------------------------------------------+
|     Hotkey      |        Command         |                     Result                      |
+=================+========================+=================================================+
| ``?``           | ``:help``              | View ranger documentation.                      |
+-----------------+------------------------+-------------------------------------------------+
|                 | ``:mk[dir] dirname``   | Creates a directory with the name dirname.      |
+-----------------+------------------------+-------------------------------------------------+
|                 | ``:tou[ch] filename``  | Creates an empty file, if it doesn't exists.    |
+-----------------+------------------------+-------------------------------------------------+
| ``E``           | ``:e[dit] [filename]`` | Edit file under the cursor or ``filename``.     |
+-----------------+------------------------+-------------------------------------------------+
| ``dD``          | ``:delete name``       | Delete **current** file or dir. or ``name``.    |
+-----------------+------------------------+-------------------------------------------------+
| ``cw``          | ``:rename newname``    | Rename the current file or directory.           |
+-----------------+------------------------+-------------------------------------------------+
| ``<CR>``, ``l`` | ``:open_with [app]``   | Open with **default** or ``app``.               |
+-----------------+------------------------+-------------------------------------------------+
| ``q``           | ``:quit``              | Closes current tab (And Ranger if last tab.)    |
+-----------------+------------------------+-------------------------------------------------+
| ``/``           | ``:find name``         | Search files in ``CWD``; ``<CR>`` to open it.   |
+-----------------+------------------------+-------------------------------------------------+
| ``/``           | ``:find dirname``      | Search folder in ``CWD``; ``<CR>`` to enter it. |
+-----------------+------------------------+-------------------------------------------------+
| ``!``           | ``:shell``             | Run commands in the shell.                      |
+-----------------+------------------------+-------------------------------------------------+

.. warning:: The ``:delete`` command is quite powerful. By **default** it will delete without any warning.

**Autocompletion** works fine when typing commands, for example, typing ``:mk`` and hitting `<Tab>`, expands to ``:mkdir``.

Mappings and aliases
====================
Most of the **hotkeys** we have seen above, are defined using **mappings** and **aliases**. These are defined in the `rc.conf` file.

Aliases
-------
Some commands like ``:e[dit]`` have a handy **alias**. An alias is defined::

    alias e edit

In this case, ``e`` is the **new command** and ``edit`` the **original command**, which can still be used in its verbose form.

Mappings
--------
Mappings are defined like this::

    map q quit

After the ``map`` command, the first part is the hotkey, in this case ``q``. The second one is the command, in this case ``quit``.

We can also create several mappings for the same command using ``copymap``::

    copymap q ZZ ZQ

Above we are creating two additional hotkeys, ``ZZ`` and ``ZQ``, using the hotkey ``q`` which points to the ``quit`` command.

Some directory shortcuts
^^^^^^^^^^^^^^^^^^^^^^^^
There are some useful mappings to navigate fast to some useful directories:

+---------+---------------------------+
| Mapping |         Change to         |
+=========+===========================+
| ``gh``  | ``$HOME``                 |
+---------+---------------------------+
| ``gr``  | To the root dir ``/``     |
+---------+---------------------------+
| ``gR``  | Ranger's config directory |
+---------+---------------------------+
| ``ge``  | ``/etc``                  |
+---------+---------------------------+
| ``gu``  | ``/usr``                  |
+---------+---------------------------+
| ``gd``  | ``/dev``                  |
+---------+---------------------------+
| ``go``  | ``/opt``                  |
+---------+---------------------------+
| ``gv``  | ``/var``                  |
+---------+---------------------------+
| ``gm``  | ``/media``                |
+---------+---------------------------+
| ``gM``  | ``/mnt``                  |
+---------+---------------------------+