Installation
============
There are several ways of installing **Ranger**.

Using your OS package manager
-----------------------------
The easiest way would be to use the **package manager** of your distribution:

* macOs: $ brew install ranger
* Debian: $ apt install ranger
* Arch: $ pacman -S ranger

Not installing It
-----------------
Another option is to **not** really install it. For example, I just download it to my ``~/Downloads`` folder, unzipped and change the folder name to ``.ranger`` so it was hidden.

Then I created a **symbolic link** in my ``~/bin`` folder::

    $ ln -s ~/Downloads/.ranger/ranger.py ~/bin/ranger

.. note:: The only downside to this, it's that we don't have access from the **command line** to the `man ranger`_ pages. Although it's not a big deal since the pages are available inside Ranger. (Hit ``?`` and then ``m``)

Install as a Python package
---------------------------
Since `Ranger`_ is written in Python, it also can be `installed as a Python package`_. Here we have several options:

* Installing to the **global site-packages** directory.
* Using another installation scheme, for example, install to the **user site-packages** directory.

I decided to install it following the Python's `user Scheme`_::

    $ pip3 install --user ranger-fm

Following this scheme, we just have to make sure that the ``userbase/bin`` folder is on our PATH, so you should have something like this in your shell configuration file::

    export PATH="/Users/javi/Library/Python/3.7/bin:${PATH}"

That way we can launch **Ranger** from our command line using the ``ranger`` command.

Problems installing (macOS)
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Apparently, I had some problem with the ``locale`` on my system, it wasn't set, so I had to add the following lines in my shell configuration::

    export LC_ALL=en_US.UTF-8
    export LANG=en_US.UTF-8

.. _`man ranger`: https://ranger.github.io/ranger.1.html
.. _`Ranger`: https://github.com/ranger/ranger
.. _`installed as a Python package`: https://github.com/ranger/ranger#installing
.. _`user scheme`: https://dummylanguages.gitlab.io/python-notes/src/distribution_packages/installing_packages/user_scheme.html