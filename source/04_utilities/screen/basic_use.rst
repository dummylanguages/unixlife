*********
Basic Use
*********
`GNU Screen`_ can be launched from the command line using ``screen``; that would create and start an **unnamed session**. The following options can be useful:

* ``-S session_name`` allows us to create a named session.
* ``-r session_name`` allows us to **reattach** a named session.
* ``-r session_id`` allows us to **reattach** to an unnamed session, using its PID.
* ``-ls`` for listing existing sessions.
* ``-X`` for quitting all existing sessions.
* ``-wipe`` for deleting all **dead sessions**.

Once inside a session, we can run commands; these usually start with an **initial shortcut**, which defaults to ``Ctrl + a``. Since this shorcut can be changed to something else, from now on, we'll refer to it with the name ``prefix``. These are some commands to run from inside sessions:

* ``prefix + :`` to change **session name**.
* ``prefix + c`` to create a **window**.
* ``prefix + A`` to change the **window's name**.
* ``prefix + n`` to switch to the **next** window.
* ``prefix + p`` to switch to the **previous** window.
* ``prefix + ""`` to **list** the existing windows.

Configuration File
==================
We can fix user settings using the ``~/.screenrc`` file. For example, to get rid of the **startup message** we would add::

    startup_message off

A good idea is to add information to the **status line**. The very minimal information that we'll need is relative to windows. The following line sets the position of the status line (at the bottom) and adds information relative to the windows (list of their names, and active one):

    hardstatus alwayslastline "%w"

.. _`GNU Screen`: https://en.wikipedia.org/wiki/GNU_Screen
.. _`terminal multiplexer`: https://en.wikipedia.org/wiki/Terminal_multiplexer
.. _`login sessions`: https://en.wikipedia.org/wiki/Login_session