######
Screen
######

.. toctree::
    :maxdepth: 3

    basic_use

`GNU Screen`_ is an old `terminal multiplexer`_ which was first released in 1987. A terminal multiplexer allows a user maintain several `login sessions`_ using the same terminal (or terminal emulator).

.. _`GNU Screen`: https://en.wikipedia.org/wiki/GNU_Screen
.. _`terminal multiplexer`: https://en.wikipedia.org/wiki/Terminal_multiplexer
.. _`login sessions`: https://en.wikipedia.org/wiki/Login_session