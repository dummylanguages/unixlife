.. _stow-label:

********
GNU Stow
********
`GNU Stow`_ is a symlink farm manager. It is used mainly for two things:

* For keeping track of **system-wide** and **per-user** installations of software built from source.
* For **managing our dotfiles**, especially when used in combination with some `VCS`_ such as `Git`_.

Installing Stow
===============
Just use your distribution's package manager.

Managing dotfiles
=================
Let's create a folder where we'll put the dotfiles we want to back up::

    $ mkdir ~/dotfiles_macOS

Now let's create a folder specifically for Bash files::

    $ cd dotfiles_macOS && mkdir Bash
    $ mv ~/.bash_profile ~/.bashrc ~/.bash_aliases Bash

.. note:: It's a good idea to create **a folder per application** we want to manage files for. It helps keeping things organized.

This is the structure we have inside our ``dotfiles_macOS`` folder::

    macOS_dotfiles
    └── Bash
        ├── .bash_aliases
        ├── .bash_profile
        └── .bashrc

All the Bash configuration files are neatly organized under a single directory.

The ``dot-`` prefix
-------------------
To avoid having a *package directory* full of **hidden files**, it's convenient to rename them with the ``dot-`` prefix. Let's do it in a *non boring* way::

    $ (shopt -s dotglob && for f in *; do mv $f $(echo "$f" | sed 's/^./dot-/'); done)

.. note:: This sets up the ``dotglob`` variable in a **subshell**, so the ``*`` expands to **hidden files**, so we can iterate over them to rename them.

The result of the command above is this::

    macOS_dotfiles
    └── Bash
        ├── dot-bash_aliases
        ├── dot-bash_profile
        └── dot-bashrc

You may be wondering about the purpose of that, and if Bash is gonna recognize this files with non-standard names. The next section answers it.

Creating the symlinks
---------------------
As of now, our dotfiles are not where they should be, so Bash won't be able to find them, plus they have weird names that start with the ``dot-`` prefix. Don't worry, we are just a command a way of creating symbolic links to this file where they belong, our ``$HOME`` and with the proper standard names::

    $ stow --dotfiles Bash

By default, the **target directory**, meaning the directory where the symbolic links will be created, it's assumed to be the parent of the current directory. Since the parent of the ``macOS_dotfiles`` directory it's our **home directory**, (``bob``), that's where the **symbolic links** are created::

    bob
    ├──macOS_dotfiles
    │  └── bash  # Real files here.
    ├── .bash_aliases   # symlink
    ├── .bash_profile   # symlink
    └── .bashrc         # symlink

That's it, ``stow`` has created symbolic links to these files, and name the **symlinks** with the standard names.

Specifying directories
----------------------
We could have easily specified the **home directory** using the ``--target`` option::

    $ stow --dotfiles Bash -t ~

We could have also specified the directory where the stow files are, using the ``--dir`` option::

    $ stow --dotfiles Bash -d ~/macOS_dotfiles bash -t ~

.. warning:: Note the space between the **stow base directory** ``~/macOS_dotfiles`` and the stow **package** we want to symlink.

A subdirectory
--------------
What if we have a file nested into a subdirectory? Imagine we want to back up the `Neovim`_ configuration file. Since this file is nested under a folder under our home directory (``~/.config/nvim/init.vim``), in our stow folder we'll mirror this same structure::

    $ mkdir -p ~/macOS_dotfiles/Neovim/dot-config/nvim

Now we can move the ``init.vim`` file to its stow folder::

    $ mv ~/.config/nvim/init.vim ~/macOS_dotfiles/Neovim/dot-config/nvim

Finally let's proceed to create the symlinks::

    $ stow --dotfiles Neovim

.. error:: At the time of writing this, there was a `bug #56727`_ which caused ``stow`` to fail when the ``--dotfiles`` option was used with **hidden directories**.

Fortunately, using the ``-t`` flag, allows us to specify the **target directory** and work around the bug::

    $ stow -t ~/.config Neovim

If you don't want to use the ``-d`` flag, make sure the **stow package** (the base directory where your dotfiles are), it's under your home, i.e. ``~/macOS_dotfiles``.

A script to rule them all
-------------------------
It may be a good idea to write a **shell script** to list the commands that each **stow package** requires, for example:

.. code-block:: bash

    #!/usr/bin/env bash

    # stow_dots.sh
    # Run: bash ./stow_dots to restore your dotfiles.

    stow --dotfiles Bash
    stow -t ~/.config Neovim
    stow -t ~/.config FZF
    [...]

Final step: version control
===========================
The whole point of this, was to keep our dotfiles under version control, so they can be restored whenever we format our system or buy a new MacBook ;)::

    $ cd macOS_dotfiles
    $ git init
    $ git remote add origin git@gitlab.com:your_name/macos_dotfiles.git
    $ git add .
    $ git commit -m "Initial commit"
    $ git push -u origin master

.. _`GNU Stow`: https://www.gnu.org/software/stow/
.. _`VCS`: https://en.wikipedia.org/wiki/Version_control
.. _`Git`: https://en.wikipedia.org/wiki/Git
.. _`Neovim`: https://neovim.io
.. _`bug #56727`: https://savannah.gnu.org/bugs/?group=stow