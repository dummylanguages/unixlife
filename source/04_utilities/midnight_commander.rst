******************
Midnight commander
******************
Installing it with homebrew is a breeze::

$ brew install mc

.. note::
	If you haven't used **homebrew** lately, don't forget to::
		$ brew doctor
		$ brew update   <== If needed.

Some shortcuts
^^^^^^^^^^^^^^
Here there are some shortcuts to control the **MC**::

	----- Esc -----
	Quick change directory: Esc + c
	Quick change directory history: Esc + c and then Esc + h
	Quick change directory previous entry: Esc + c and then Esc + p
	Command line history: Esc + h
	Command line previous command: Esc + p
	View change: Esc + t (each time you do this shortcut a new directory view will appear)
	Print current working directory in command line: Esc + a
	Switch between background command line and MC: Ctrl + o
	Search/Go to directory in active panel: Esc + s / Ctrl + s then start typing directory name
	Open same working directory in the inactive panel: Esc + i
	Open parent working directory in the inactive panel: Esc + o
	Go to top of directory in active pane: Esc + v / Esc + g
	Go to bottom of directory in active pane: Esc + j / Ctrl + c
	Go to previous directory: Esc + y
	Search pop-up: Esc + ?
	----- Ctrl -----
	Refresh active panel: Ctrl + r
	Selecting files and directories: Ctrl + t
	Switch active <-> inactive panels: Ctrl + i
	Switch active <-> inactive panels content: Ctrl + u
	Execute command / Open a directory: Ctrl + j
	----- F -----
	F1: help
	F2: user menu
	F3: read file / open directory
	F4: edit file
	F5: copy file or direcotry
	F6: move file or directory
	F7: create directory
	F8: delete file / directory
	F9: open menu bar
	F10: exit MC

Stay in the working directory after Midnight
--------------------------------------------
By default, after moving among directories in **mc**, when we quit it, we are taken back to the ...  If you want to avoid that, **mc** includes a cool feature. You just have to enable it in your ``.bash_profile``::

	alias mc=". /usr/local/opt/midnight-commander/libexec/mc/mc-wrapper.sh"

And don't forget to source ``.bash_profile`` after::

	source .bash_profile

This way, anytime you quit **MC** you will be left in the same directory you selected inside **MC**.

.. _`midnight commander`: https://mc
