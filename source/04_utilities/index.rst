#########
Utilities
#########
Here I will cover mostly command line utilities that run on any Linux/Unix operating system.

..note:: I should probably talk about what is known as the **Unix philosophy**, but Wikipedia has already an excellent `entry`_ about it.


.. toctree::
    :maxdepth: 3

    tmux/index
    screen/index
    powerline/index
    ranger/index
    midnight_commander
    stow
    fzf/index
    emacs/index

.. _`entry`: https://en.wikipedia.org/wiki/Unix_philosophy