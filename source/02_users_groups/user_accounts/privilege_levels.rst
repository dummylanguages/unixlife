.. _privilege levels:

****************
Privilege Levels
****************
Not all user accounts enjoy the same privileges when using the system. System administrators need more freedom than standard users, and the superuser has the maximum access level.

Super User
==========
In computing, the `superuser`_ is a special user account used for system administration. In Unix systems, the actual name of this account is ``root``. This account is used to perform administrative functions like managing users, configuring devices, configuring network settings, etc.

.. note:: The superuser always has a **user ID** of ``0``.

There are also some services that run with ``root`` privileges.

Principle of Least Privilege
----------------------------
It's a security best practice to never log in to the system as ``root``, but rather to log in as a **standard user** and elevate credentials when necessary. That's consequence of the `Principle of Least Privilege`_ which states that users should be given no more authority on the system than they need to perform their job. So, for example, if a user needs to be able to **read** but **not write** to a file, then she should be given **only read** permissions over that file.

Switching accounts: the ``su`` command
======================================
The ``su`` command, enables a user to switch their identity to that of another user. For example::

  $ whoami
  linda
  $ su bob
  Password: ******
  $ whoami
  bob

Anyone using ``su``, except ``root``, will be challenged for the password of the user account they are switching to. When used with a hyphen (``su - bob``) it can be used to start a **login shell**. Used this way, users can assume the **user environment** of the target user.

Escalating privileges
---------------------
A user can also use the ``su`` command to substitute a user with a higher level of access, even the ``root`` account. According to the **PoLP**, it's a best practice to log in with a standard user account, then elevate
your privileges to root as needed. There are different commands that allow a standard user to elevate her privileges to superuser:

* The `su`_ command (short for **substitute user**).
* The `sudo`_ command (short for **superuser do**).

We'll cover ``sudo`` in a separate section.

The ``wheel`` Group
-------------------
Some Unix-like systems implement the user group `wheel`_, and only allow members of this group to become root with ``su - root``. Many distributions disable the actual ``root`` account for users and instead allow administrative functions based on membership in the ``wheel`` group. In these systems, only members of the ``wheel`` can use the ``sudo`` command to temporarily elevate their privileges.

.. _`superuser`: https://en.wikipedia.org/wiki/Superuser
.. _`Principle of Least Privilege`: https://en.wikipedia.org/wiki/Principle_of_least_privilege
.. _`su`: https://en.wikipedia.org/wiki/Su_(Unix)
.. _`sudo`: https://en.wikipedia.org/wiki/Sudo
.. _`wheel`: https://en.wikipedia.org/wiki/Wheel_(computing)
