.. _user accounts label:

#############
User Accounts
#############
In a Unix system, user accounts are objects that represent users and services. There are three different types of accounts: 

* Standard user.
* Root, or super user.
* Service.

.. note:: Each person willing to use the system should have a unique user account.

Users are referenced by the system using a `user identifier`_ (aka **user ID** or **UID**), rather than a name (Names are also there just for the convenience of the users). A **user ID** is nothing but a **numeric value** used to represent a specific user.

.. note:: Users are also classified into `groups`_, which are represented in the system by a `group identifier`_, often abbreviated to **group ID** or **GID**.

We'll see the differences between these accounts, how to create, modify and deleting them, as well as how to switch from one account to another.


.. toctree::
   :maxdepth: 4
   :hidden:

   privilege_levels
   creating_users
   modifying_users
   deleting_users

.. _`user identifier`: https://en.wikipedia.org/wiki/User_identifier
.. _`group identifier`: https://en.wikipedia.org/wiki/Group_identifier
.. _`groups`: https://en.wikipedia.org/wiki/Group_(computing)

