.. _modifying users label:

***************
Modifying users
***************
There are a couple of utilities for modifying user accounts in a system:

* ``usermod``, which is command to modify settings for regular users.
* ``chage``, which is short for **change age**, is a command to control **password expiration**.
* ``passwd``, which can be used for **locking** and **unlocking** accounts, set password expiration dates, etc.

Password modifications
======================
The main modification we can do to an account is to **lock** password access, meaning that the user won't be able to log in with her password. The following table contains a summary of password related operations:

+--------+--------------+---------------------------------------+
| Short  |     Long     |              Description              |
+========+==============+=======================================+
| ``-d`` | ``--delete`` | Delete a password                     |
+--------+--------------+---------------------------------------+
| ``-l`` | ``--lock``   | Lock an account                       |
+--------+--------------+---------------------------------------+
| ``-u`` | ``--unlock`` | Unlock an account                     |
+--------+--------------+---------------------------------------+
| ``-S`` | ``--status`` | Displays an account's password status |
+--------+--------------+---------------------------------------+
| ``-e`` | ``--expire`` | Sets an account's password as expired |
+--------+--------------+---------------------------------------+

The ``-S`` flag shows information about a user's password, for example::

  $ passwd bob
  bob P 04/11/2021 0 99999 7 -1

Next to the user name we may found just a cryptic letter:

* ``P`` which means that the account has a password set.
* ``NP`` which means that the account doesn't have a password set.
* ``L`` which means that the account's password is lock.

The ``chage`` command displays password information in a more human-readable form::

  $ chage -l bob
  Last password change                                    : Apr 11, 2021
  Password expires                                        : never
  Password inactive                                       : never
  Account expires                                         : never
  Minimum number of days between password change          : 0
  Maximum number of days between password change          : 99999
  Number of days of warning before password expires       : 7

Other modifications
===================
The ``usermod`` command can also be used to **lock** accounts (use the ``-L`` flag), and many other things. The following has a summary:

+--------+------------------+------------------------------------------------------------------+
| Short  |       Long       |                           Description                            |
+========+==================+==================================================================+
| ``-c`` | ``--comment``    | Modify the GECOS field                                           |
+--------+------------------+------------------------------------------------------------------+
| ``-d`` | ``--home``       | Set a new user home folder. Add ``-m`` to move the current files |
+--------+------------------+------------------------------------------------------------------+
| ``-e`` | ``--expiredate`` | Change the account's expiration date                             |
+--------+------------------+------------------------------------------------------------------+
| ``-g`` | ``--gid``        | Changes an account's default group                               |
+--------+------------------+------------------------------------------------------------------+
| ``-G`` | ``--groups``     | Sets an account's additional groups                              |
+--------+------------------+------------------------------------------------------------------+
| ``-s`` | ``--shell``      | Changes an account's shell                                       |
+--------+------------------+------------------------------------------------------------------+
| ``-u`` | ``--uid``        | Changes an account's UID                                         |
+--------+------------------+------------------------------------------------------------------+

.. _`sudo`: https://www.sudo.ws/