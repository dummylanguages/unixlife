.. _deleting users label:

**************
Deleting users
**************
In case of mistake, there are several commands that may help when dealing with **users** and **groups**:

* ``userdel``
* ``groupdel``

Note: ``userdel`` comes in handy in those cases when you forgot to use the ``-m`` option, and you end up without a home directory structure for your new user.


.. _`Perl`: https://www.perl.com/
.. _`sudo`: https://www.sudo.ws/
.. _`passwd`: https://en.wikipedia.org/wiki/Passwd
.. _`/etc/passwd`: https://en.wikipedia.org/wiki/Passwd#Password_file
.. _`/etc/shadow`: https://en.wikipedia.org/wiki/Passwd#Shadow_file

.. _`Network Information Service`: https://en.wikipedia.org/wiki/Network_Information_Service
.. _`Lightweight Directory Access Protocol`: https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol
