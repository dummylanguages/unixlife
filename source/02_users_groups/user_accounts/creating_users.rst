.. _creating users label:

**************
Creating users
**************
There are a couple of utilities for creating new users in a system:

* ``useradd``, which is a native binary compiled with the system.
* ``adduser``, which is a `Perl`_ script which uses ``useradd`` binary in back-end.

.. note:: There are no differences regarding features, although ``adduser`` is more user friendly and interactive, the problem is that is not included in all distros.

Besides creating the user, we'll have to make sure to set a password for the new account. The `passwd`_ command is used to set the **initial password** for a user after creating the account with the
useradd command.

The ``useradd`` command
=======================
Let's say we want to add a new user named **bob** and include him in the ``wheel`` group::

    # useradd -m bob -G wheel,input

* The ``-m`` option (short for ``--create-home``) creates a **home** folder for the user at ``/home/bob``.
* The ``-G`` (short for ``--groups``) adds him to the groups ``wheel`` and ``input`` groups. Note that each group is separated from the next by a **comma**, with no intervening spaces. The default is for the user to belong only to the initial group.

To find out if we need to use the ``-m`` option, we could run::

  $ grep CREATE_HOME /etc/login.defs

If we don't get any output, it means the directive is not set, and by default the home directory is not created for new users.

The ``/etc/login.defs`` file
----------------------------
This configuration file is typically installed by default on most Linux distributions. It contains directives to control several aspects of newly created accounts such as:

* Password expiration date and password length.
* Method used to encrypt passwords.
* If a **home directory** should be automatically for every new user (``CREATE_HOME`` directive).

The ``/etc/default/useradd`` file
---------------------------------
This is another configuration file that controls the process of creating accounts. We can check the contents of this file running::

    $ useradd -D
    GROUP=100
    HOME=/home
    INACTIVE=-1
    EXPIRE=
    SHELL=/bin/sh
    SKEL=/etc/skel
    CREATE_MAIL_SPOOL=no

.. note:: Most of the directives are self-explanatory. Check the ``man`` pages of ``useradd`` for more information.

If you need to modify the ``/etc/default/useradd`` file’s directive settings, instead of using a text editor, you can employ the ``useradd -D`` command. For example, to modify the ``SHELL`` directive to point to the Bash shell, use super user privileges and run::

  $ sudo useradd -D -s /bin/bash

The ``/etc/skel`` file
----------------------
This is known as the **skeleton directory**, and it holds the files that will be copied when a home directory is created for a new user account.

Setting a password
==================
The `passwd`_ command is used by root to **set** or **reset** a password for any user. A user
can use the passwd command themselves to reset their own password. For example, let's protect the newly created user **bob** with a password. To do so we just have to run::

    # passwd bob
    Changing password for user bob.
    Changing password for bob.

.. warning:: An **essential** step after creating a new user account is to set a **password** for it.

The ``/etc/passwd`` file
------------------------
Traditionally, the `/etc/passwd`_ file has been used to keep track of the list of users that have access to a system. Each entry in this file contains the followig colon-separated information:

* **User name**: used by the user to log in.
* Encrypted password (represented in the record by an ``x``.
* **UID**: User ID number.
* **GID**: User's group ID number.
* **Comment**: It contains the `GECOS`_ field, used to record general information about the account or its user(s) such as their real name and phone number (traditionally just the **full name** of the user).
* User home directory
* **Login shell**: Path to the shell that is launched when the user logs in.

This file is owned by ``root``; standard users can only read it.

The ``/etc/shadow`` file
------------------------
Contrary to what its name may suggest, the ``/etc/passwd`` file doesn't store any password at all. Back in the day that used to be the case, but since all users have **read** permissions for that file, nothing stop them from trying to crack the hashed password of another users. Nowadays, all modern Unix-like systems keep the encrypted passwords in a separate file named `/etc/shadow`_, to which only ``root`` have access.

Both of these files (and many others) have been used by services as the now-obsolete `Network Information Service`_, or the more modern `Lightweight Directory Access Protocol`_.

The ``adduser`` command
=======================
As we said, ``adduser`` is a friendlier frontend to ``useradd``. A tipical interactive session would look like this::

    $ sudo adduser bob
    [sudo] password for javi:
    Adding user `bob' ...
    Adding new group `bob' (1001) ...
    Adding new user `bob' (1001) with group `bob' ...
    Creating home directory `/home/bob' ...
    Copying files from `/etc/skel' ...
    New password:
    Retype new password:
    passwd: password updated successfully
    Changing the user information for bob
    Enter the new value, or press ENTER for the default
        Full Name []: Robert
        Room Number []: 123
        Work Phone []: 123456789
        Home Phone []: 123456789
        Other []:
    Is the information correct? [Y/n] Y

As a result, a new user ``bob`` is created, including a **home directory** by default, and a **password** for the new user. Quite verbose as you can see.

.. _`Perl`: https://www.perl.com/
.. _`sudo`: https://www.sudo.ws/
.. _`passwd`: https://en.wikipedia.org/wiki/Passwd
.. _`/etc/passwd`: https://en.wikipedia.org/wiki/Passwd#Password_file
.. _`/etc/shadow`: https://en.wikipedia.org/wiki/Passwd#Shadow_file
.. _`GECOS`: https://en.wikipedia.org/wiki/Gecos_field
.. _`Network Information Service`: https://en.wikipedia.org/wiki/Network_Information_Service
.. _`Lightweight Directory Access Protocol`: https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol
