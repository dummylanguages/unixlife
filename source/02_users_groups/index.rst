.. _user and groups label:

###############
User and Groups
###############
`Unix`_ (and Unix-like) operating systems are `multi-user`_, meaning that the system can be accessed and used by several users simultaneously. Each **user** is represented in the system by a **user account**, and each user account enjoy different privileges regarding access to files, directories, and commands on the system.

.. note:: Not all **user accounts** represent users; some **services** are also represented by a user account.

To manage access levels in an easier way, user accounts with the same privilege level are put into `groups`_. In this section we'll go into detail about both mechanisms, explaining as well how to manage **user accounts** and **groups**.

.. warning:: Check the ``my_drafts`` folder!


.. toctree::
   :maxdepth: 4
   :hidden:

   user_accounts/index
   sudo/index
   groups/index
   querying_system
   macos/index

.. _`Unix`: https://en.wikipedia.org/wiki/Unix
.. _`multi-user`: https://en.wikipedia.org/wiki/Multi-user_software
.. _`groups`: https://en.wikipedia.org/wiki/Group_(computing)
