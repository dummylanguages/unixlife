*****
Draft
*****

Commands			| Action
--------------------|-------
`who`				| Displays who is logged in
`whoami`			| Displays **user id**
`finger` [username]	| Displays info about the system users

## Permissions
Mac OS X uses permissions to restrict users the access to files and directories. 

In this context, users fall into 3 categories:
* The **user** who owns the file.
* The **group** who owns the file/directory.
* The **other** users.
* **All** of the above.

Usually, every user has total access to his own files and also can limit the access of other people to his files. 
(usually, because the **root** can modify that)


Every file and folder on our Macs has a configurable set of permissions. Permissions control three types of access:
* reading. 
* writing.
* executing.

## Viewing permissions
To view the permissions of files and directories, we can use the command `ls -l`. That will list all files and subdirectories in **long format**, giving us info about the permissions:

```
d rwx --- ---+ 19 javi  staff    646 Mar 16 17:30 Downloads/
```
* The first letter `d` tells us that is a directory.
* Then we have 3 groups:
	* rwx, the **owner** can read, write and execute this directory.
	* ---, the users in the same **group** that the owner, can't do anything.
	* ---, the **other** users, can't do anything either.
* The `+` means that this file has extended security information. Right now, don't worry about it. 
* It contains 19 files. 
* The **owner** is javi.
* The **group** is staff.

The other stuff is not relevant at the moment.

## The chmod command
Recursive chmod Tricks

by Chris Gilligan on July 5, 2011 in Wordpress
Recursively chmod only directories
find . -type d -exec chmod 755 {} \;

Similarly, recursively set the execute bit on every directory
chmod -R a+X *
The +X flag sets the execute bit on directories only

Recursively chmod only files
find . -type f -exec chmod 644 {} \;

Recursively chmod only PHP files (with extension .php)
find . -type f -name '*.php' -exec chmod 644 {} \;

## Setting permissions by text

## Setting permissions by number

I found this info here. It may be important.
[http://stackoverflow.com/questions/2001881/correct-owner-group-permissions-for-apache-2-site-files-folders-under-mac-os-x][]
his is the most restrictive and safest way I've found, as explained here for hypothetical ~/my/web/root/ directory for your web content:

For each parent directory leading to your web root (e.g. ~/my, ~/my/web, ~/my/web/root):
* chmod go-rwx DIR (nobody other than owner can access content)

* chmod go+x DIR (to allow "users" including `_www` to "enter" the dir)

* sudo chgrp -R _www ~/my/web/root (all web content is now group _www)
All the files and directories will still be owned by you (the user) and still be writeable. The chgrp only allows the "_www" group to read the files.

* chmod -R go-rwx ~/my/web/root (nobody other than owner can access web content)

* chmod -R g+rx ~/my/web/root (all web content is now readable/executable/enterable by _www)

All other solutions leave files open to other local users (who are part of the "staff" group as well as obviously being in the "o"/others group). These users may then freely browse and access DB configurations, source code, or other sensitive details in your web config files and scripts if such are part of your content. If this is not an issue for you, then by all means go with one of the simpler solutions.

## Changing ownership of a file
The `chown` command
