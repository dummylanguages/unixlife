.. _sudo-label:

########
``sudo``
########

.. figure:: /_images/sudo.png
   :align: right

According to `Wikipedia`_, `sudo`_ is a program for Unix-like computer operating systems that allows users to run programs with the security privileges of another user, by default the superuser. The older versions of ``sudo`` were designed to allow normal users to run commands as the **superuser**, hence its name was understood to be short for *"superuser do"*. However, the later versions added support for running commands not only as the superuser but also as other (restricted) users, and thus it is also commonly expanded as *"substitute user do"*.

There are two ways to allow a user to run the ``sudo`` command:

* Add her to the ``sudo`` group.
* Add her to the ``/etc/sudoers`` file.

.. toctree::
   :maxdepth: 4
   :hidden:

   installing
   sudo_group
   sudoers_file
   editing_as_root

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Sudo
.. _`sudo`: https://www.sudo.ws/
.. _`wheel group`: https://en.wikipedia.org/wiki/Wheel_(computing)