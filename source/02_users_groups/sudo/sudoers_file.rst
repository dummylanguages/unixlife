.. _sudoers file:

*************************
The ``/etc/sudoers`` file
*************************
The ``sudo`` command is configured through a file located at ``/etc/sudoers``. It's strongly discouraged to edit this file directly, since using the wrong syntax can leave us with a broken system where it is impossible to obtain elevated privileges. Instead, it is recommended to use the ``visudo`` command, which validates the syntax of the file upon saving.

.. warning:: Don't edit the ``/etc/sudoers`` file directly! Use the ``visudo`` command.

The ``visudo`` command
======================
Ironically, if we haven't logged in as ``root``, we'll need to use ``sudo`` to run the ``visudo`` command::

    $ sudo visudo

.. note:: Needless to say, if your user account does not belong to 

That will open the ``/etc/sudoers`` file in the configured editor, which by default used to be ``vi`` but in some cases is ``nano``. To change the **default editor** we can run::

    $ sudo update-alternatives --config editor

And select one of the editors offered in the list.

For a one time command we also could::

    $ EDITOR=/usr/bin/nvim sudo visudo

Or you could export the ``EDITOR`` variable for the whole session or set it permanently in your shell configuration.

Default lines
=============
At the beginning of the ``/etc/sudoers`` file, there usually go the ``Defaults`` directives::

    Defaults        env_reset
    Defaults        mail_badpass
    Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"

1. In the first line, ``env_reset`` means that
2. In the second one, ``mail_badpass``, tells the system to mail notices of bad sudo password attempts to the configured mailto user. By default, this is the root account.
3. The third line, specifies the PATH that will be used for sudo operations. This prevents using user paths which may be harmful.

User privilege specification
============================
Then we have the line where the privilege for the **root** user are specified::

    root ALL=(ALL:ALL) ALL

We'll rarely have to modify this line, but we can use it to analyze the fields that we have to use when specifying privileges for other users:

+----------+---------+----------+-----------+----------+
| Username |  Hosts  |  Users   |   Groups  | Commands |
+==========+=========+==========+===========+==========+
| ``root`` | ``All`` | ``(ALL`` | ``:ALL)`` | ``All``  |
+----------+---------+----------+-----------+----------+


Group privilege lines
=====================
Sudo privileges can also be specified for groups, which allows busy system administrators saving time by taking care of all the users in a group simultaneously. Usually, ``/etc/sudoers`` files also contains commented lines for two groups:

* The `wheel group`_, which is legacy from the classic old UNIX days.
* The ``sudo`` group, only that using this second one (not completely sure about this), logging incidents leave a trail that can be audited later on.

.. note:: We can add more groups to this file.

For example, let's say I want any user in the ``wheel`` group to be able to use the ``sudo`` command, without having to introduce a password everytime they do. We just have to uncomment the line::

    %wheel  ALL=(ALL) NOPASSWD: ALL

The ``ALL`` after ``NOPASSWD`` means that users in the ``wheel`` group don't need password to run **any** command as sudo. We could be more specific and put here a **comma-separated list** of the full path executables we want to allow without password, for example::

    %wheel  ALL=(ALL)   NOPASSWD: /usr/bin/shutdown, /usr/bin/mount, /usr/bin/umount

This can also be done for individual users.

The ``/etc/sudoers.d/`` directory
=================================
Once we open the ``/etc/sudoers`` file, right at the top a comment advices::

    # Please consider adding local content in /etc/sudoers.d/ instead of
    # directly modifying this file.

Also, at the end of the file there's another line which sometimes is commented, sometimes is not::

    includedir /etc/sudoers.d


.. _`Wikipedia`: https://en.wikipedia.org/wiki/Sudo
.. _`sudo`: https://www.sudo.ws/
.. _`wheel group`: https://en.wikipedia.org/wiki/Wheel_(computing)