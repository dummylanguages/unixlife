.. _editing files as root:

*************************
Editing files as ``root``
*************************
At some point, we all need to allow users to edit some configuration files owned by ``root``, and we may be tempted to allow::

  $ sudo vim some.config

The problem with allowing that is that we're allowing the user to run the ``vim`` editor as ``root``, and that's very dangerous. She may decide to run some dangerous command (``!dangerous_command``) or even spawn a new shell (using ``:sh``) which will run with ``root`` privileges.

.. warning:: Remember the **principle of least privilege**, if you need a user to be able just to edit a file owned by ``root``, don't give her more leash than that.

The ``sudoedit`` command
========================
This command permits a user to edit a file with their own credentials, even if the file is only available to the ``root`` user. In addition, the user can use their preferred text editor, which has to be defined in the ``EDITOR`` **environment variable**.

.. note:: If ``EDITOR`` is set to ``vim``, the editor will start with the user's configuration settings.

Add entry to ``sudoers`` file
-----------------------------
To use ``sudoedit``, you must make an entry in the ``sudoers`` file. For example, the following line could be added to the ``sudoers`` file:

  %editors ALL = sudoedit /path/to/file

Any member of the ``editors`` group could then enter the following command to edit a file in particular, which we have to indicate in ``/path/to/file``. From then on, a user wishing to edit that file, can do so running::

  $ sudoedit /path/to/file

That will create a copy with a unique name in ``/tmp`` with permissions locked down to only that user, and then spawn her editor normally, without ``root`` privileges, on that copy.

Once she exits the editor, it will compare the temporary file and original file, and safely replace the original with your edit if it changed.

In this scenario, it becomes possible to allow a user to edit a system file, but not allow them to run random binaries as ``root`` or poke everywhere on the file system.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Sudo
.. _`sudo`: https://www.sudo.ws/
.. _`wheel group`: https://en.wikipedia.org/wiki/Wheel_(computing)