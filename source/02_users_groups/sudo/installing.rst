*******************
Installing ``sudo``
*******************
If the first time we try to run sudo we get something like::

    $ sudo
    bash: sudo: command not found

We may need to install the program, which it's trivial since it exists in packaged form in all Linux distros. The problem is, that we need superuser privileges to install software, so we'll have to **log in** as ``root``::

    $ su --login root
    # apt install sudo

.. note:: Believe it or not, ``sudo`` is not always installed in some distributions. 
