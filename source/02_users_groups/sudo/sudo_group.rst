***********************************
Adding a user to the ``sudo`` group
***********************************
One of the ways to allow a user to run the ``sudo`` command is to make sure she belongs to the ``sudo`` group. The problem is that adding users to groups require ``root`` privilege, so we'll have to **log in** as the ``root`` user::

    $ su --login root

.. note:: The ``--login`` option (or either ``-l``, or ``-`` for short) is important here, without it we inherit the user's environment, whose ``PATH`` does not include the path ``/usr/sbin``, where the ``usermod`` command is installed (This was a change introduced in Debian).

Once we're logged in as **root**, let's add our user to the sudo group::

    # usermod -a -G sudo bob

* The ``-a`` (short for ``--append``) flag is essential. Otherwise, the user will be removed from any groups, not in the list.
* The ``-G`` (short for ``--Groups``) option takes a (comma-separated, without whitespace at all) list of additional groups to assign the user to.
* Note that the user ``bob`` is the last argument.

Now we keep exiting the shell sessions (``exit``) until were completely logged out of the system.

.. warning:: Sometimes **loging out** is not enough for the changes to take effect. We have to **reboot** the system.
