*****
Users
*****
To see the groups of a user::

    $ id

Directory services command line
===============================
In OS X, there is no command for adding users. To do that, we have to to use an utility called the **directory services command line** or in short **dscl** and specify all the info manually. This command allows us to:

* Create a username::

    dscl . -create /Users/$USERNAME

* Set a shell for the new user::

    dscl . -create /Users/$USERNAME UserShell /bin/bash

* Set the real name of the user::

    dscl . -create /Users/$USERNAME RealName "$FULLNAME"

* Set the user's ID::

    dscl . -create /Users/$USERNAME UniqueID "$USERID"

* Set his **primary group**::

    dscl . -create /Users/$USERNAME PrimaryGroupID 20

* Where is gonna be his **home directory**::

    dscl . -create /Users/$USERNAME NFSHomeDirectory /Users/$USERNAME

* Set his password::

    dscl . -passwd /Users/$USERNAME $PASSWORD


.. _`Homebrew`:
.. _`Fink`:		http://www.finkproject.org/		
	
