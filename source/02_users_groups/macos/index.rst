#########################
Users and Groups in macOS
#########################

.. toctree::
    :maxdepth: 3
    :hidden:

    users
    groups

Managing users and groups in macOS is usually done through the GUI, although it can be done through the command line.

Super useful info: https://blog.travismclarke.com/post/osx-cli-user-management/

.. _`Darwin`: https://en.wikipedia.org/wiki/Darwin_(operating_system)

