******
Groups
******
to add the username bob into wheel, will it be::

    $ sudo dseditgroup -o edit -a fito -t user _mysql

This -a(dds) ``bob``, which is an object of -t(ype) ``user``, to the group “admin”.

I had to add -p as well so I could enter my password.

.. note:: ``$ man dseditgroup`` for more info.

.. _`Homebrew`:
.. _`Fink`:		http://www.finkproject.org/		
	
