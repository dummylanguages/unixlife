.. managing groups label:

***************
Managing Groups
***************
A `group identifier`_ (**GID**)  is a **numeric value** used to represent a specific group. The range of values for a **GID** varies amongst different systems; at the very least, a **GID** can be between ``0`` and ``32,767``, with one restriction: the login group for the **superuser** must have ``GID 0``.

THE ``/etc/group`` FILE
=======================
The ``/etc/group`` file is the storage location for all groups in the system. Each line in this file contains four fields of information separated by a colon. The fields are not necessarily all populated. These fields are:

* **Group name**: The user-friendly name of the group.
* **Password**: The password required to enter the group.
* **Group ID**: The group ID by which the system references the group.
* **Group list**: The list of all group members (empty by default).

This file should not be edited directly; instead, its contents can be modified using the commands:

* ``groupadd``
* ``groupmod``
* ``groupdel``

Adding users to a group: ``groupadd``
=====================================
The ``groupadd`` command creates a group. By default, the group has **no members** and **no password**. The general syntax for this command is::

  $ sudo groupadd [options] <groupname>

Regarding the **options**, some of them are:

* ``-g`` to specify a **GID**.
* ``-o`` to allow the group to be created with a non-unique **GID**.
* ``-f`` to return a **success status** if the group already exists (used to check the existence of a group).

Modifying a group: ``groupmod``
===============================
The ``groupmod`` command is used to change the group's own attributes. It will change the contents of the ``/etc/group`` file. The general syntax for this command is::

  $ sudo groupmod [options] <groupname>

Regarding the **options**, some of them are:

* ``-g`` to change the **GID**.
* ``-n`` to rename the group.

Deleting a group: ``groupdel``
==============================
The ``groupdel`` command is used to **delete** groups from the ``/etc/group`` file. The general syntax for this command is::

  $ sudo groupdel [options] <groupname>

Deleting a group does not delete the **user accounts** that were member of the deleted group.

.. _`group identifier`: https://en.wikipedia.org/wiki/Group_identifier
