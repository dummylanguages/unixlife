.. _groups label:

######
Groups
######
In a Unix system, user accounts are objects that represent users and services. There are three different types of accounts: 

* Standard user.
* Root, or super user.
* Service.

Each type of account enjoys different privileges when using the system. When granting access to users, `groups`_ simplify tremendously administrative tasks. For example, imagine a computer science department has a network which is shared by **students** and **staff** members. Students should have limited access to directories related with the staff. Without groups, every time a new student joined the network, administrators would have to give that particular student permissions to the right directories on a per directory basis.

.. note:: User accounts with the same privilege level are classified into `groups`_

With groups, the task is much simpler: create a ``student`` group and a ``staff`` group, and grant the entire group access to the appropriate directories. After that, giving access to a particular student it's just a matter of adding its user account to the ``student`` group. To give access to a student or staff member to the proper directories, the administrator only has to add her to the group. Revoking access would be a matter of just deleting a person from a group.

.. toctree::
   :maxdepth: 4
   :hidden:

   managing_groups

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Sudo
.. _`sudo`: https://www.sudo.ws/
.. _`wheel group`: https://en.wikipedia.org/wiki/Wheel_(computing)