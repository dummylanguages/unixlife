*******************
Querying the System
*******************
Administrators often need to use tools to gather about the users and groups in a system. Users may also want to gather details about their identity on the system for troubleshooting access problems or just verifying what account the user is currently logged on with. In this section we'll take a look to some of the commands to achieve that.

THE ``whoami`` COMMAND
======================
Sometimes, after we log in to a system we may need to switch to a different user. In these situation we may need to remind ourselves of the user account we're currently logged in. The `whoami`_ command give us that information::

  $ whoami
  bob

The ``id`` command
==================
In POSIX-compliant environments, the command-line command ``id`` gives the current **user's ID**, as well as more information such as:

* The **user name**.
* The name and ID of the **primary user group**.
* The list of groups a user belongs to.

For example, to see the **user name** of the current account we would run::

  $ id -un

This is equivalent to the ``whoami`` command.

The ``who`` Command
===================
``who`` is a standard command in Unix systems, used to determine the details of users currently logged in to a system. The output of the who command includes:

* User name.
* The name of the system from which the user is connected.
* The date and time that the user has been connected since.

The ``w`` Command
=================
The `w`_ command provides provides a quick summary of every user logged into a computer:

* The **first line** of the output displays the status of the system.
* The **second line** of the output displays a table with the first column listing the users logged in to the system and the last column indicating the current activities of the users.
* The remaining columns of the table show different attributes associated with the users.

The ``last`` command
====================
The ``last`` command displays the history of user login and logout actions, along with the actual time and date.

The ``getent`` command
======================
The `getent`_ command allows us to view account records in the ``/etc/passwd`` and ``/etc/shadow`` files. The general syntax of this command is::

  getent <file> <user>

Where ``file`` refers to the file we want to check, and ``user`` the name of the user account we want to check. For example, the following command will give us the record in ``/etc/passwd`` of the user ``bob``::

  $ getent passwd bob
  bob:x:1002:1002::/home/bob:/bin/bash

If we want to check information about ``bob`` (or any other user) in the ``/etc/shadow`` file, we'll need to run this command as ``root``::

  $ getent shadow bob
  $
  $ sudo getent shadow bob
  bob:!:17806:0:99999:7:::

.. _`whoami`: https://en.wikipedia.org/wiki/Whoami
.. _`who`: https://en.wikipedia.org/wiki/Who_(Unix)
.. _`w`: https://en.wikipedia.org/wiki/W_(Unix)
.. _`getent`: https://en.wikipedia.org/wiki/Getent
