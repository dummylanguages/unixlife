****************
Installing fonts
****************
Every major **desktop environment** includes some software to install and manage **fonts** using a GUI. But if you prefer to do it manually, keep reading.

.. note:: By the way, a good idea before installing a new font would be to check if it's not already installed. We can list all the fonts installed in our system using ``fc-list``.

We assume that you have `Fontconfig`_ installed in your system.

Fonts location
==============
Installing a font manually is just a matter of placing it in the proper folder, and update our font cache files (we'll explain how). So what's the proper folder? Well, we can install fonts that will be available for the whole system or just for a particular user.

System wide fonts
-----------------
According to ``/etc/fonts.conf`` the directories for **system-wide fonts** are:

* /usr/share/fonts
* /usr/local/share/fonts

User fonts
----------
Regarding **user fonts**, they used to go in ``~/.fonts``, but that location is now deprecated in favor of ``~/.local/share/fonts``. If you haven't installed any font on your system, chances are that the directories mentioned above don't even exist. In that case you'll have to create them manually.

Adding new font locations
-------------------------
The above mentioned paths can be customized in the fontconfig configuration file at ``/etc/fonts/fonts.conf`` – you can also include subdirectories or links, which is useful if you have a directory of fonts on a separate hard drive (or partition or other location). 

.. note:: `Fontconfig`_ will look for fonts in all of the above mentioned directories.

There's the option of settting your **user font configuration** manually in ``~/.config/fontconfig/fonts.conf``. That's an **xml** file, (analogous to ``/etc/fonts/fonts.conf``?) where we can set all the fonts that we install on our system.

Setting the right permissions
-----------------------------
Once we've downloaded some **font files**, we have to make sure that their permissions are set to ``644`` (``-rw-r--r--``), otherwise they may not be usable.

Examples
========
For example, let's download the **Yosemite San Francisco** font::

    $ git clone https://github.com/supermarin/YosemiteSanFranciscoFont.git
    $ cd YosemiteSanFranciscoFont
    $ install -m0444 *.ttf ~/.local/share/fonts

The install command places the fonts in ``~/.local/share/fonts``, and at the same time set the proper permissions. After placing a font in one of the proper directories, we have to update the **font cache**::

    $ fc-cache -v

The ``-v`` option is for verbose output.

Using our package manager
-------------------------
If the font is available as a package for our distribution, we can use our **package manager** too. For example, let's imagine we want to install the **terminus** font::

    $ sudo apt install terminus-font
    $ pacman -S terminus-font

That should install the font in the proper directory.

.. note:: The **Terminus font** is very interesting for 2 reasons:

  1. The font itself is awesome and has a lot of fans. It's designed for long (8 hours and more per day) work with computers.
  2. It comes in the 2 old bitmap formats: **PSF** for the console, and **PCF** for everything else.

.. _`Fontconfig`: https://www.freedesktop.org/wiki/Software/fontconfig/

