Vector fonts
============
Outline fonts are also called **vector fonts**, and as you may have already imagine, they are scalable to any size. Popular formats in this category are:

* **TrueType fonts** aka **TTF**. Truetype is a font standard developed by Apple and Microsoft in the late 1980s. It has become the most common format for fonts on the macOS, and Microsoft Windows operating systems.

* **OpenType** fonts, aka **OTF**. Opentype is a registered trademark of Microsoft Corporation.

* The **FreeType** project is an independent implementation of the TrueType standard which is included in many Linux distributions. 

Stroke fonts
============


.. _`PC Screen Font`: https://en.wikipedia.org/wiki/PC_Screen_Font
