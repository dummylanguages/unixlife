*****
Fonts
*****

.. toctree::
	:maxdepth: 3
	:hidden:

	console_fonts
	X11_font_systems
	xlfd
	font_config
	vector_fonts
	installing_fonts

In traditional typography, a `font`_ was a particular size, weight and style of a `typeface`_. For example, `Times New Roman`_ is a **typeface** whereas **Times New Roman 12-point** and **Times New Roman 14-point** are just **fonts**.

.. figure:: /_images/movable_types.jpg
	:scale: 70 %
	:align: center

Nowadays, with the advent of **desktop publishing** the term **font** has become synonymous with **typeface**. Thus, all sizes of the `Comic Sans MS`_ typeface are considered the same font, regardless of their weight (bold, italic or whatever).

.. note:: `Desktop publishing`_ is the creation of documents using **page layout software** on a **desktop computer**.

Here we're gonna be dealing, obviously with `computer fonts`_, which instead of sets of movable metal type pieces, are implemented as digital file formats. These files are usually compressed formats that contain all the `glyphs`_ in a single file.

There are two basic kinds of computer font file formats:

1. `Bitmap fonts`_ consist of a matrix of pixels representing the image of each glyph.
2. **Vector fonts**, also known as `Outline fonts`_, are a set of lines and curves instead of pixels that can be scaled without causing pixellation.

TO DO:

1. FINISH IT.
2. Check about Pango

https://www.pango.org

.. _`font`: https://en.wikipedia.org/wiki/Font
.. _`typeface`: https://en.wikipedia.org/wiki/Typeface
.. _`computer fonts`: https://en.wikipedia.org/wiki/Computer_font
.. _`glyphs`: https://en.wikipedia.org/wiki/Glyph
.. _`Comic Sans MS`: https://en.wikipedia.org/wiki/Comic_Sans
.. _`Times New Roman`: https://en.wikipedia.org/wiki/Times_New_Roman
.. _`Desktop publishing`: https://en.wikipedia.org/wiki/Desktop_publishing
.. _`Bitmap fonts`: https://en.wikipedia.org/wiki/Computer_font#Bitmap_fonts
.. _`Outline fonts`: https://en.wikipedia.org/wiki/Computer_font#Outline_fonts