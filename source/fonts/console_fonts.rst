.. _console-fonts-label:

Linux console fonts
===================
The `Linux console`_ is the system console which outputs all kernel messages and warnings when the system is booting and which allows us to login in single user mode. Just to be clear, the Linux console is implemented inside the kernel and does not depend on the X Window System.

.. note:: The Linux console is an **optional** kernel feature, and most embedded Linux systems do not enable it.

There are two main implementations of the Linux console: framebuffer and text mode. 

1. The legacy text mode implementation was used in PC-compatible systems with CGA, EGA, MDA and VGA graphics cards.
2. The framebuffer implementation is the default in modern Linux distributions, 

Here we're gonna cover useful topics related with the fonts used by the Linux console.

Packages that deal with console fonts
-------------------------------------
As we just said, the Linux console is a kernel thing and does not depend on the X Window System. That means that the utilities available in X11 for managing fonts are not useful when dealing with console fonts.

The ``console-setup`` package
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The `console-setup`_ package includes the ``setupcon`` command. This command allows us to set up **font** and the **keyboard** on the console in a fast and easy way. This command works reading from several configuration files:

Regarding the **font configuration**, it is specified in the files:

* ``~/.console-setup``
* ``/etc/default/console-setup``

The **keyboard configuration** is specified in ``~/.keyboard`` or ``/etc/default/keyboard``. In Debian systems the default keyboard layout is shared between X and the console.

These files contain the **default configuration** of the font and keyboard respectively, but we can add several alternative configuration files. For example, we can configure the **default** files to be used with a latin font and English keyboard layout and add another configuration in the files ``~/.console-setup.russian`` and ``~/.keyboard.russian``. Then, if we need to change to the Russian configuration files we would run::

    $ setupcon russian

And to go back to the default configuration we would run the command without any arguments::

    $ setupcon

Check the manual pages of `setupcon`_ for additional options.

The ``console-setup`` file
""""""""""""""""""""""""""
This file contains the configuration for the **console font**. Optionally, we could use this file to configure the **keyboard layout** but it is not recommended to do so. Instead, use the ``keyboard`` files mentioned in the section before.

This is how the ``/etc/default/console-setup`` looks in my system::

    ACTIVE_CONSOLES="/dev/tty[1-6]"
    CHARMAP="UTF-8"
    CODESET="guess"
    FONTFACE="TerminusBold"
    FONTSIZE="12x24"

In this case we a a minimal configuration with the essential options, but the man page of the `console-setup file`_ contains information about all the available settings. Let's explain the ones we have here: 

* ``ACTIVE_CONSOLES`` allows us to specify the **virtual terminals** (tty) to be configured.
* ``CHARMAP`` specifies the desired **encoding** on the console.
* ``CODESET`` determines which symbols are supported by the font. The **special value** ``guess`` can be used in which case a suitable codeset will be guessed.
* ``FONTFACE`` is where we specify the typeface. The fonts with font face **TerminusBold** are optimized for use with the **framebuffer** video modes.
* ``FONTSIZE`` is self explanatory but notice that in the filename of the font I chose ``Uni2-TerminusBold24x12`` the numbers are reversed.

Graphical menu
""""""""""""""
In Debian there's a utility named `dpkg-reconfigure`_ which allows us to reconfigure packages after they have already been installed. We just have to pass it the name of the package we want to reconfigure, in this case::

    $ sudo dpkg-reconfigure console-setup

This will open an `ncurses`_ based graphical menu where we'll use the **vertical arrow keys** to navigate the options, the **horizontal arrow keys** to select “OK”, and press **ENTER** when ready.

This is by far the most comfortable method, and there's no risk of making mistakes choosing the wrong font name or size.

The ``kbd`` package
^^^^^^^^^^^^^^^^^^^
There is another package that contains a lot of tools to set up the keyboard and the fonts in the console (and also in X). The `kbd project`_ aka the **Linux keyboard tools**, contains tools for managing Linux console, mainly loading console fonts and keyboard maps.

This distribution contains no binaries because the sources depend on the kernel version, but almost every Linux distro includes a compiled and packaged version of the tools. In Debian we can find themin the `kbd`_ package.

One of the included utilities is the  ``setfont`` command, which allows us to load console fonts in our tty. We can call this command passing a font name as an **argument**, that's why is a good idea to list all the console fonts to choose the proper name::

    $ ls /usr/share/consolefonts
    $ setfont Uni2-TerminusBold24x12
    
Note that the changes will be gone when we restart the system, so the point of this command is to test fonts on the fly until we find the one we're looking for, and then set it up permanently in the configuration files with the other utilities mentioned before.

The manpage of the `setfont`_ command contains useful information about the Linux console fonts in general. Such as the locations:

* ``/usr/share/consolefonts`` is the default font directory.
* ``/usr/share/unimaps`` is the default directory for Unicode maps.
* ``/usr/share/consoletrans`` is the default directory for screen mappings.

It also mentions:

    The standard Linux font format is the PSF font.

PSF fonts
---------
`PC Screen Font`_ is a bitmap font format currently employed by the Linux kernel for **console fonts**. They are installed into the ``consolefonts`` directory, which location varies depending on your distro:

    * In Debian is found in ``usr/share/consolefonts/``.
    * In Arch Linux is found in ``usr/share/kbd/``.



Font configuration in Systemd
-----------------------------
Most Linux distributions that use `systemd`_ as their init system, don't use console-setup file (nor the package either), to keep the console configuration. They use instead the ``/etc/vconsole.conf`` file.

.. note:: Some OS like **Debian** use **Systemd** but still use the ``console-setup`` file for keeping the console configuration.

A minimal configuration could be::

    KEYMAP=us
    FONT=Uni2-TerminusBold24x12

By the way, ``us`` is the default, no need to set it up explicitely. Check more options in the manual page of `vconsole.conf`_.

Keyboard mappings (keymaps) for virtual console, **console fonts** and **console maps** are provided by the **kbd package** (a dependency of systemd), which also provides many low-level tools for managing virtual console. In addition, systemd also provides the `localectl`_ tool, which can control both the system locale and keyboard layout settings for both the virtual console and Xorg.

For a list of all the available **keymaps**, use the command::

    $ localectl list-keymaps

We can set the keymap to American English using::

    $ localectl set-keymap us

Interesting links
-----------------
`The Hackers Guide for console-setup`_


.. _`Linux console`: https://en.wikipedia.org/wiki/Linux_console
.. _`PC Screen Font`: https://en.wikipedia.org/wiki/PC_Screen_Font
.. _`The Hackers Guide for console-setup`: http://download.vikis.lt/baltix/baltix-ubuntu-packages/edgy/ubiquity-installer/old/d-i/source/console-setup/doc/console-setup.html/index.html#contents
.. _`console-setup`: https://manpages.debian.org/buster/kbd/setfont.8.en.html
.. _`setupcon`: https://manpages.debian.org/buster/console-setup/setupcon.1.en.html
.. _`console-setup file`: https://manpages.debian.org/buster/console-setup/console-setup.5.en.html
.. _`dpkg-reconfigure`: https://manpages.debian.org/stretch/debconf/dpkg-reconfigure.8.en.html
.. _`ncurses`: https://en.wikipedia.org/wiki/Ncurses
.. _`kbd`: http://kbd-project.org
.. _`kbd project`: http://kbd-project.org
.. _`setfont`: https://manpages.debian.org/buster/kbd/setfont.8.en.html
.. _`systemd`: https://systemd.io/
.. _`vconsole.conf`: https://www.freedesktop.org/software/systemd/man/vconsole.conf.html#
.. _`localectl`: https://www.freedesktop.org/software/systemd/man/localectl.html#