**********
Fontconfig
**********
`Fontconfig`_ is a library for configuring and customizing font access. When we installed automatically, removing a common source of configuration problems.
`Wikipedia`_ has some information about it
perform font name substitution, so that appropriate alternative fonts can be selected if fonts are missing.

It ships with eight command line utilities:

* ``fc-list``: Lists all fonts available on the system.
* ``fc-match``: Matches font-pattern (empty pattern by default) using the normal fontconfig matching rules to find the most appropriate font available.
* ``fc-cache``: Creates a cache of all FreeType readable fonts in a specified directory or create a cache of all FreeType readable fonts from all directories specified in Configuration files.
* ``fc-cat``: Reads the font information from cache files or related to font directories and emits it in ASCII form.
* ``fc-query``: Querys font files and reports resulting pattern(s).
* ``fc-scan``: Scans font files and directories and reports resulting pattern(s).
* ``fc-pattern``: Lists best font(s) matching the supplied pattern(s).
* ``fc-validate``: Validate font file(s) and reports the results.

In Debian we can also install a **Python** based graphical utility named **font-manager**, to preview all installed and available fonts. 

Some useful command examples
----------------------------
For ``fc-list``::

    $ fc-list : family | sort | uniq
    $ fc-list : family style
    $ fc-list | grep "Source Code Pro"

This one avoids listing bitmap only fonts::

    fc-list :outline -f "%{family}\n"

To find a list of scalable fixed-pitch fonts which may be used for the faceName resource value::

    $ fc-list :scalable=true:spacing=mono: family


.. _`Fontconfig`: https://www.freedesktop.org/wiki/Software/fontconfig/
.. _`Wikipedia`: https://en.wikipedia.org/wiki/Fontconfig

