****************
X11 font systems
****************


Bitmap fonts
============
Bitmap fonts: The main feature of this type of font is that it's **not scalable**. There are lots of formats inside this category, the most popular ones are:

Two different font systems are used by **X11**:

1. The old core **X Logical Font Description**, aka XLFD. (Same as the original core X11 fonts system???)
2. The newer **XFT** system, short for **X FreeType**.

SNF fonts
---------
`Server Normal Format`_ (SNF) is a bitmap font format used by X Window. It is one of the oldest X Window font formats. Nowadays it is rarely used, however it is still supported by the latest X.org server. SNF fonts had the problem of being platform dependent, therefore they needed to be compiled on each system. In 1991, X11 moved away from SNF fonts to Portable Compiled Format, which could be shared between systems.[

BDF fonts
---------
The `Glyph Bitmap Distribution Format`_, (abreviated as **BDF**) is a file format for storing bitmap fonts that was created by Adobe. In 1988, the `X Consortium`_ adopted BDF 2.1 as a standard for X Window **screen fonts**, but this is not the case anymore, since X Windows has largely moved to other font standards such as PCF, Opentype, and Truetype.

PCF fonts
---------
The `Portable Compiled Format`_ are the **X core fonts**, in other words, the font format used by **X Window System** in its **core font system**, and has been used for decades. PCF fonts are usually installed, by default, on most Unix-based operating systems, and are used in terminals such as xterm.


Bitmap fonts are being replaced by **scalable fonts** such as **OpenType** and **TrueType**.



XLFD
====
In this system each font has a description which contains 14 fields, each prefixed by a dash. The position of the field determines its meaning: foundry, family, weight, etc. A field can contain the value ``*`` to allow the server to pick any. For example::

    -xos4-terminus-medium-*-*-*-14-*-*-*-*-*-iso8859-1

Since dealing with this description format is quite complicated and error prone, there is a graphic tool named ``xfontsel`` which help us generating the font description, which we later will use for setting the font in programs that support the XLFD system.

There is another interesting program to deal with **bitmap fonts**, ``xlsfonts``. We can find this command:

* Archlinux: In the ``xorg-xlsfonts`` package.
* Debian: In the ``x11-utils`` package.

 [-options] [-fn pattern]

A list of XLFD names and font alias names available on your system is displayed. Bitmap fonts show values in all fourteen XLFD fields. Scalable typefaces show zeros in the PixelSize, PointSize, ResolutionX, and ResolutionY positions.

XFT: The X Font Library
=======================
The **X11 core fonts system** is directly derived from the fonts system included with X11R1 in 1987, which could only use monochrome bitmap fonts. This system is maintained for backwards compatibility with legacy X11 applications that have not yet updated. Toolkit authors are encouraged to switch to Xft as soon as possible.

The Xft system was designed and implemented in the early 2000's. This system sits alongside the legacy core font system. Xft was designed from the start to provide good support for scalable fonts, and it supports features such as anti-aliasing giving applications full control over the way glyphs are rendered.

Xft is not compatible with the core fonts system. While X.Org will continue to maintain the core fonts system, 

### Xft configuration
Xft relies upon the **fontconfig** library to configure and customise fonts. Fontconfig is not specific to the X Window system.



https://www.x.org/releases/X11R7.5/doc/libXfont/fontlib.html
https://keithp.com/~keithp/talks/xtc2001/paper/
https://www.freedesktop.org/wiki/Software/Xft/


.. _`Server Normal Format`: https://en.wikipedia.org/wiki/Server_Normal_Format
.. _`Glyph Bitmap Distribution Format`: https://en.wikipedia.org/wiki/Glyph_Bitmap_Distribution_Format
.. _`Portable Compiled Format`: https://en.wikipedia.org/wiki/Portable_Compiled_Format
.. _`X Consortium`: https://www.x.org/wiki/XConsortium/

