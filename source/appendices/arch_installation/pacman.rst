********************
Arch User Repository
********************
`Pacman`_ is the default **package manager** used by `Arch Linux`_, and other Arch-based distributions (Manjaro). Here we'll go over the basic features.

.. note:: For detailed information check the page in the Arch Wiki `about pacman`_

Listing Packages
================
We can check all the packages installed on our system by checking the **local database**::

    $ sudo pacman -Q

Checking if some package is installed
-------------------------------------
We can query the database to see if a particular package is installed on our system by passing the name of the package after the ``-Q`` flag::

    $ sudo pacman -Q rofi

Additionally, we can add the ``-i`` flag to get detailed information about the package.

The sync database
=================
The **sync database** is the list of all possible packages, determined by the list of **official repositories** enabled in ``/etc/pacman.conf``. We can refresh this database using:

* ``-Syy``: it's useful to do this from time to time to get the latest updates of the whole database.
* ``-Sl``: view a list of the available packages in the sync database.

Installing packages
===================
``pacman`` installs packages from the Arch Linux **official repositories**.

* ``-S``: install a package.
* ``-Sy``: install a package without asking for confirmation.
* ``-Su``: upgrade **all** installed packages.

Other package managers
======================
``yaourt`` is a ``pacman`` wrapper with extended features and **AUR support**. **AUR** stands for the **Arch User Repository**, a community-driven repository for Arch users.

.. warning:: `yaourt`_ is not maintained anymore.

pkgfile
=======
pkgfile:  maintains a database of which packages own which file even when not installed, which is updated every day by cron by default. To use it, for example::

    $ pkgfile startx


.. _`Arch Linux`: https://www.archlinux.org/
.. _`pacman`: https://www.archlinux.org/pacman/
.. _`about pacman`: https://wiki.archlinux.org/index.php/pacman
.. _`yaourt`: https://github.com/archlinuxfr/yaourt
