******************************
Installation and configuration
******************************
Here is where we start installing stuff to disk. After that, we'll do some essential configuration.

Installation
============
For the installation we'll use a script named ``pacstrap``, to install the ``base`` package, the ``linux`` kernel and ``linux-firmware`` for common hardware::

    # pacstrap /mnt base linux linux-firmware

Optionally you can also add other packages such as ``base-devel``, text editors, etc.

Configuration
=============
We have to do several things at this stage.

fstab
-----
Generate an fstab file (use ``-U`` or ``-L`` to define by **UUID** or **labels**, respectively)::

    # genfstab -U /mnt >> /mnt/etc/fstab

Check the resulting ``/mnt/etc/fstab`` file, and edit it in case of errors. 

.. warning:: You may run into another problem her if your computer for some reason goes to sleep mode. Running the ``genfstab`` script will throw SQUASHFS error. Reboot, mount the disk partitions and try again.

chroot
------
`Change root`_ into the new system::

    # arch-chroot /mnt

We do this so the configuration changes and the packages we are going to install go into the SSD of the computer, not in the live system. Remember, we have mounted all the physical partitions under ``/mnt``, so the command above means that ``/mnt`` is gonna become the ``/`` directory.

.. warning:: This step is really important, so make sure that the partitions were mounted as explained in the last section.

At this stage, you may want to run::

    # mkinitcpio -p linux

In case it wasn't run during the pacstrap stage.

Time zone
---------
To check the current zone defined for your system::

    $ timedatectl status

If the date, time or timezone are not properly set, you should start by checking all the available ones::

    $ timedatectl list-timezones

.. note:: The list of timezones is kept in the folder ``/usr/share/zoneinfo``.

To set a timezone::

    $ timedatectl set-timezone Europe/Moscow

This will create an ``/etc/localtime`` **symlink** that points to a zoneinfo file under ``/usr/share/zoneinfo/``.

.. warning:: During chroot where ``timedatectl`` will not work, so we'll have to create the link **manually**.

Alternatively, we can create the symbolic link **manually**::

    # ln -sf /usr/share/zoneinfo/Europ/Moscow /etc/localtime

Localization
------------
Edit the file ``/etc/locale.gen`` and uncomment your locale, for example::

    en_US.UTF-8
    en_US ISO-8859-1
    
Uncomment other locales if you need to (You can have multiple ones). Now we have to generate the locales by running::

    # locale-gen

Now we have to create the ``/etc/locale.conf`` file and add::

    LANG=en-US.UTF-8

Hostname
--------
The hostname during installation is usually ``archiso``. To create a hostname we just have to create the file::

    # nvim /etc/hostname

Inside we can write whatever name we want. We also have to edit the ``/etc/hosts`` file, where we'll add::

    127.0.0.1	localhost
    ::1		    localhost
    127.0.1.1	myhostname.localdomain	myhostname

.. note:: If you gave your system a **static IP address**, that static IP address should be used instead of ``127.0.1.1``. 

Network configuration
---------------------
The network configuration can be done after we reboot. For now, let's install a package called ``networkmanager``::

    # pacman -S networkmanager

And also enable a systemd service so it starts automatically when we reboot our machine::

    # systemctl enable NetworkManager

Boot loader
-----------
Let's install a well known boot manager::

    # pacman -S grub efibootmgr

Now we install grub on the ``/dev/sda`` disk (don't choose any partition here, just the whole disk::

    # grub-install /dev/sda

And generate a ``configuration`` file for the manager::

    # grub-mkconfig -o /boot/grub/grub.cfg

.. warning:: If after running this command you don't see something like ``Found linux image(s) in /boot`` your system won't be able to boot.

Console font
------------
A small detail that may seem unimportant is the console font. When we finish the installation and reboot, the temporary font that we set at the beginning, so let's install the font::

    # pacman -S terminus-font

And let's add it in the file::

    # cat FONT=ter-132b >> /etc/vconsole.conf

Setting a root password
-----------------------
Easily done running::

    # passwd

Finishing the installation
--------------------------
Creating a new initramfs is usually not required, because mkinitcpio was run on installation of the kernel package with pacstrap. But just in case we can recreate the initramfs image::

    # mkinitcpio -P

* Exit the **chroot environment** by typing ``exit`` or pressing Ctrl+d.

* Optionally manually unmount all the partitions with ``umount -R /mnt``: this allows noticing any "busy" partitions, and finding the cause with fuser.

* Finally, restart the machine by typing ``reboot``: any partitions still mounted will be automatically unmounted by systemd. Remember to remove the installation medium and then login into the new system with the root account. 

Configuring the WiFi
====================
Once we reboot in our new Arch system, we may find that the WiFi connection is not working. In my case ``nmcli`` let me know that the ``NetworkManager.service`` wasn't running so I had to::

    # systemctl enable NetworkManager.service --now

Then we can run the ``nmtui`` command, included in the NetworkManager.

.. _`here`: https://www.happyassassin.net/posts/2014/01/25/uefi-boot-how-does-that-actually-work-then/
.. _`Ubuntu forum post`: https://askubuntu.com/questions/928161/is-there-sense-in-mounting-efi-partition-at-boot
.. _`Arch Wiki`: https://wiki.archlinux.org/index.php/EFI_system_partition#Typical_mount_points
.. _`Change root`: https://wiki.archlinux.org/index.php/Change_root
