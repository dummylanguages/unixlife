********************
Arch User Repository
********************
One of the main reasons for the popularity or `Arch Linux`_ is the huge amount of packages available in the `Arch User Repository`_. The **AUR** is a community-driven repository not only not recommended but also discouraged officially, but... who gives a fuck?

**AUR packages** can’t be installed using the standard package manager `pacman`_. We have two options to install packages from AUR:

1. The manual build process:

    * Acquire build files (PKGBUILD and others)
    * Build the package(mkpgk).
    * Install the package(pacman).

2. Use an **AUR helper** such as `yay`_ or **Yaourt** (unmaintained?). AUR helpers are not supported by Arch Linux either, whatever.

Manual build and install process
================================
This is the officially recommended way to install **AUR** packages. Let's describe in mode detail.

0. Prerequisites: We have to make sure that ``base-devel`` package is installed. It's also convenient to have  an appropriate **build directory**, where the package will be compiled into an executable. It can be any directory, for example, ``~/builds``:
    $ mkdir ~/builds

1. Acquire build files: usually packages in the **AUR** provide a **Git Clone URL** that we can use to download the build files:

    $ cd ~/builds
    $ git clone https://aur.archlinux.org/polybar.git

An advantage of this method is that in the future we can easily get updates to the package via ``git pull``. But we can also download the **tarball** using our browser or some command(``curl``).

2. Build the package: once the package is downloaded (and extracted if it was a tarball), we have to cd where the **PKGBUILD** file is:
    $ cd yay

    Note: At this point, it's officially recommended to carefully check the **PKGBUILD**, for any ``.install`` files, and any other files in the package's git repository for malicious or dangerous commands. We can do that with:

        $ less PKGBUILD

    In the case we are **updating** a package, we may want to look at the changes since the last commit with ``git show``.

Once we are ready, let's make the package:

    $ makepkg -si

* The ``-s/`` flag (--syncdeps) automatically resolves and installs any dependencies with ``pacman`` before building. But if the package depends on other **AUR** packages, you will need to install them first.
* The ``-i/``--install installs the package if it is built successfully. Alternatively, the built package(a tarball) can be **installed** with:
    $ pacman -U package.pkg.tar.xz 

Installing an AUR helper: yay
=============================
Yay it's in the **AUR**, so let's install it the official way::

    $ cd ~/builds
    $ git clone https://aur.archlinux.org/yay.git
    $ cd yay
    $ makepkg -si

Using yay
---------
We can search for packages in the **AUR**::

    $ yay -Ss <package_name>

And install it with::

    $ yay -Ss <package_name>

.. _`Arch Linux`: https://www.archlinux.org/
.. _`Arch User Repository`: https://aur.archlinux.org/
.. _`AUR helper`: https://wiki.archlinux.org/index.php/AUR_helpers
.. _`pacman`: https://wiki.archlinux.org/index.php/pacman
.. _`yay`: https://github.com/Jguer/yay