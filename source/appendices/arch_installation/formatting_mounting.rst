***********************
Formatting and mounting
***********************
Once the partitions have been created, each must be formatted with an appropriate file system. After that we'll see how to mount them.

Formatting
==========
In the last section we created four partitions:

1. The first one is the **boot partition**, EFI type, so it has to be formatted as ``FAT32`` using the mkfs command::

    # mkfs.fat -F32 /dev/sda1

2. The second one is the **root partition**, so we'll choose ``Ext4``::

    # mkfs.ext4 /dev/sda2

3. The third one is the **swap partition**, so we'll initialize it with ``mkswap``::

    # mkswap /dev/sda3
    # swapon /dev/sda3

4. The fourth one is the **home partition**, so we'll choose ``Ext4`` again::

    # mkfs.ext4 /dev/sda4

If for some reason you get an error of non-existing partitions while formatting, run::

    # lsblk

If you don't see the partitions you created, you'll have to reboot your system. If you run ``lsblk`` again you should see the partitions you created in the last section, and you will be able to run the commands above in order to format them.

.. note:: After rebooting you'll have to connect to the Wi-Fi again.

Mount the partitions
====================
Let's start with the **root partition**, which was mounted straight to ``/mnt``::

    # mount /dev/sda2 /mnt

.. warning:: It's important that the **root partition** is the first one you mount at ``/mnt``.

Since my **boot partition** it's EFI, I wasn't sure where to mount it. Apparently there are different points to mount it:

* In the `Arch Wiki`_ they suggest either ``/efi`` or ``/boot``. If you choose the first option, we'll have to create that directory (``mkdir /efi``) since it doesn't exist.
* At ``/boot/efi`` like in Fedora and Ubuntu, as suggested `here`_ and by Rod Smith in the `Ubuntu forum post`_.

.. warning:: Whatever you choose, since we have mounted all our partitions under the ``/mnt`` directory don't forget to prepend it in the next step.

So I went ahead and chose ``/boot``, so I had to create this folder inside ``/mnt``::

    # mkdir /mnt/boot
    # mount /dev/sda1 /mnt/boot

.. note:: The ``SWAP`` partition doesn't have to be mounted.

For the **home partition** we have to create a folder inside ``/mnt``::

    # mkdir /mnt/home
    # mount /dev/sda4 /mnt/home


.. _`Arch Wiki`: https://wiki.archlinux.org/index.php/EFI_system_partition#Typical_mount_points
.. _`here`: https://www.happyassassin.net/posts/2014/01/25/uefi-boot-how-does-that-actually-work-then/
.. _`Ubuntu forum post`: https://askubuntu.com/questions/928161/is-there-sense-in-mounting-efi-partition-at-boot
