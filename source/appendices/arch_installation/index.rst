*********************
Installing Arch Linux
*********************

.. toctree::
    :hidden:

    disk_partitioning
    formatting_mounting
    installation_configuration
    after_minimal_install
    pacman
    aur

Here we'll see how to set up `Arch Linux`_ in a laptop. The easy part:

1. Downloaded an image from the `Arch Linux Downloads`_ page using **BitTorrent**. 
2. Then flashed the image into a pendrive using a multiplatform tool named `Balena Etcher`_.
3. **Reboot** the laptop, press ``Enter`` and then ``F12`` to enter the boot order configuration. Once there chose the pendrive.
4. Configured the WiFi using one of the `iwd`_ commands::

    # iwctl
    [iwd]# device list
    [iwd]# station wlan0 get-networks 
    [iwd]# station wlan0 connect my_wifi_ssid
    Type the network passphrase for my_wifi_ssid psk.
    Passphrase: ************* 

5. I couldn't see shit, so I changed the font to a reasonable size::

    $ cd /usr/share/kbd/consolefonts && ls
    $ setfont ter-132b.psf.gz

.. note:: The console font change is temporary: it won't survive a reboot.

.. _`Arch Linux`: https://www.archlinux.org/
.. _`Balena Etcher`: https://www.balena.io/etcher/
.. _`Arch Linux Downloads`: https://www.archlinux.org/download/
.. _`iwd`: https://iwd.wiki.kernel.org/
