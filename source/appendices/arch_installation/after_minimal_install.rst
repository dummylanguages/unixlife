****************************
After a minimal installation
****************************
Here we are going to cover the next steps towards a more functional Arch Linux installation. Here we'll learn how to:

* Add a normal user
* Give her sudo access
* Install a window manager.

Users and groups
================
So far our system only have the **root** user. Let's create a new one, and set up a password::

    # useradd -m bob -g sudo
    # passwd bob
    New password:
    Retype new password:
    passwd: password updated successfully

The ``-m`` option creates a **home** folder under /home/bob, and ``-g`` adds him to the group ``sudo``.

.. note:: You may want to use ``adduser``, which is a friendlier frontend to ``useradd`` written in Perl. It does things like create user directories by default, prompting us for additional information and a password for the new user.

Delete user
-----------
In case of mistake, for example, let's say you forgot to use the ``-m`` option, and you end up without a home directory structure for your new user, you can delete a user with::

    # userdel bob

sudo access
===========
Assuming that ``sudo`` is installed on our system, the first thing we should do is make sure that the user belongs to the ``wheel`` or ``sudo`` groups::

    # usermod -a -G wheel bob

.. note:: We could also use the **sudo group**, but apparently, in Arch there's a preference for the `wheel group`_ (legacy from the classic old UNIX days).

Once that's done, we have to edit the ``/etc/sudoers`` file using the ``visudo`` utility instead::

    # visudo

This will open the sudoers file in our ``$EDITOR``, where we can make the desired changes. If we get an error about ``visudo`` not finding an editor, run::

    # EDITOR=/usr/bin/nvim sudo visudo

That will set the ``EDITOR`` variable, and ``visudo`` will use neovim (or whatever editor we chose to) to edit the ``/etc/sudoers`` file.

.. note:: You can also the set ``EDITOR`` variable for the environment by adding a line in your shell confiration file.

We have three options:

1. Allow all members in the ``wheel`` group to run **all** commands using sudo.
2. The same but being asked for the user's password.
3. Limit the commands that can be run with ``sudo`` to a few.

No password please
------------------
In my case I want any user in the ``wheel`` group to be able to use the ``wheel`` command, without having to introduce its password everytime they do. We just have to uncomment the line::

    %wheel  ALL=(ALL) NOPASSWD: ALL

.. note:: For more information, read the other :ref:`section about sudo <sudo-label>`.

A Window manager
================
After any Arch installation, when we boot our system we are faced with a useful but simple **tty**. How about adding some GUI sauce? Here we are faced with the choice of a **desktop environment** or a **window manager**, and we'll go with the second option.

Anyway, whatever's your choice you're gonna need a display server protocol, which at this point in time is either `X11`_ or `Wayland`_. Since we are fans of minimalistic window managers and there are not a lot of them for **Wayland** (just one named `Sway`_) our choice is obvious.

.. note:: Read more about **window managers** and **display server** protocols in our section about X11.

We need to install a particular implementation of the X11 protocol named `X.Org`_. In Arch we need to install 2 packages:

* xorg-server, which is the server itself.
* xorg-xinit includes the commands to start the server: ``xinit``, ``startx``.

We have to install them as a **normal user** (using sudo), so we don't have permission problems down the line::

    $ sudo pacman -S xorg-server xorg-xinit
    :: There are 2 providers available for libgl:
    :: Repository extra
        1) libglvnd 2) nvidia-340xx-utils
    Enter a number (default=1):

Apparently, all current drivers have been adapted to support ``libglvnd``, whereas ``nvidia-340xx-utils`` is only for legacy nvidia cards where the driver doesn't implement libglvnd. Choose **1**, the default.

Problems launching the X server
-------------------------------
Once we finished our installation, when we try to launch the X server, two things may happen:

* The X server starts and terminates inmediately after.
* We may also get the ``xterm: command not found`` error.

These things happen because an **X11 session** consists of an **X server** and some **client**. When we start the server, it tries to find some client, and if there's none, it automatically shuts down. The second error happens for almost the same reason, if the server doesn't find any client it tries to launch the **default terminal emulator** for the **X window system**, which is **xterm**, but we don't have it installed.

A fast fix to make sure the X server works fine would be to install any client program, let's say a terminal emulator::

    $ sudo pacman -S alacritty

The ``startx`` command takes an optional client program as an argument but we have to pass the full path, for example::

    $ which alacritty
    /usr/local/bin/alacritty
    $ startx /usr/local/bin/alacritty

.. note:: The two commands above can be shortened to a one-liner: ``startx $(which alacritty)``.

You should see a window with Alacritty running on it. Type ``exit`` to exit the shell session; the X server will terminate again.

A window manager
================
Instead of passing an X client everytime we start an X session, let's install a `window manager`_ named `Bspwm`_::

    $ sudo pacman -S bspwm sxhkd

Read about how to configure **bspwm** in our :ref:`section about it <bspwm-label>`, and :ref:`sxhkd <sxhkd-label>`, the daemon that **bspwm** uses by default as a shortcut manager.

Other useful stuff
==================
Once I finished installing the GUI, I realized that some things werent' working, namely:

* **Multimedia keys** to adjust the sound, screen brightness, etc. Read our section about :ref:`multimedia keys <multimedia_keys-label>` to learn how to deal with it.
* The **touchpad**, although functional, lacked of multigesture functionality. Fortunately, we also wrote about how to :ref:`set up your touchpad <touchpad-label>` and add :ref:`add multigestures <multigesture-label>`.

Also a good idea to read the other sections about how to install and configure the several programs:

* :ref:`rofi <rofi-label>`, a pretty neat program launcher.
* :ref:`polybar <polybar-label>`, a super configurable status bar.
* If you keep your configuration files under source control, you may also want to read :ref:`section about stow <stow-label>`

.. _`wheel group`: https://en.wikipedia.org/wiki/Wheel_(computing)
.. _`X11`: https://en.wikipedia.org/wiki/X_Window_System
.. _`X.Org`: https://en.wikipedia.org/wiki/X.Org_Server
.. _`Wayland`: https://wayland.freedesktop.org/
.. _`Sway`: https://swaywm.org/
.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
.. _`Bspwm`: https://github.com/baskerville/bspwm
