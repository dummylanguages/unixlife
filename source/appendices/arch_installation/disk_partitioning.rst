*********************
Partitioning the disk
*********************
This step is a bit more involved. The chosen sizes for the partitioning scheme is a polemic thing, so after reading for a while I tried to make some informed decisions.

.. warning:: Be sure you have **backed up all your data** in the disk you're about to partition.

We'll use the ``fdisk`` command to do this job. First thing is get the **label** of the disk we want to partition by running::

    # fdisk -l

.. note:: A more graphical solution is to use ``cfdisk`` command.

The output of this command shows us information about all the disks connected to the system. We have to make sure we choose the right disk, and for that we can pay attention to the ``Disk model`` and the size of the disk. But if 

.. note:: In case you have two disks of the same brand and sized connected you have to be super-extra-careful because it will be difficult to tell them apart. A good idea would be to previously format the disk with a different format, and read the ``Disklabel type`` field.

Deleting existing partitions
----------------------------
Before creating new partitions it's advisable to delete the old ones. To do that we have to select the proper disk, in my case the one labeled as ``/dev/sda``::

    # fdisk /dev/sda

That will open the fdisk prompt, where we can type ``m`` to see all available options. In our case we need to use the ``d`` command. If we have more than one partitions on that disk(``/dev/sda1``, ``/dev/sda3``, etc) we'll have to select the partition number we want to delete. I had three of them, so I went ahead and delete them all.

.. note:: If you change your mind but can't get out of the partition number menu, press any number and don't worry, that partition won't be deleted unless you type ``w`` to write changes to disk.

Creating new partitions
-----------------------
Once you have the entire disk space free, it’s time to create new partitions with command ``n``. This is the partitioning scheme I chose for a **500GB** SSD disk:

+--------------------+---------------+------------------------------+---------------------------------+
|        Name        |   Partition   | Mount point (Once installed) |              Size               |
+====================+===============+==============================+=================================+
| **Boot partition** | ``/dev/sda1`` | ``/boot`` or ``/efi``        | Between **260GB** and **512GB** |
+--------------------+---------------+------------------------------+---------------------------------+
| **Root partition** | ``/dev/sda1`` | ``/``                        | Between **23GB** and **32GB**   |
+--------------------+---------------+------------------------------+---------------------------------+
| **Swap partition** | ``/dev/sda2`` | ``[SWAP]``                  | **16GB**                         |
+--------------------+---------------+------------------------------+---------------------------------+
| **Home partition** | ``/dev/sda3`` | ``/home``                    | The remainder of the disk.      |
+--------------------+---------------+------------------------------+---------------------------------+

.. note:: In the table above we are assuming that the disk we want to partition has been labeled as ``/dev/sda``, but in other systems may be labeled differently.

All the sizes are optional, and different people recommend different things. For example, for the boot partition **Red Hat Linux** recommends 250MB.

Boot partition
^^^^^^^^^^^^^^
The Boot partition contains the necessary stuff to boot our system. To create a new partition we have to select the proper disk again::

    # fdisk /dev/sda

And once in the fdisk prompt, use the command ``n`` to create a new partition::

    Command (m for help): n
    Partition number (1-128, default 1): 1
    First sector (34-976773134, default 2048):
    Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-976773134, default 976773134): +512M

For the **first sector** accepts the default (just press Enter), and for the **last sector** type ``+512M``. That will create a new partition of type ``Linux filesystem``, but in my case, I have a laptop that uses UEFI firmware (not BIOS) so I had to change the type to ``EFI system``::

    Command (m for help): t
    Partition type or alias (type L to list all): uefi

And boom, partition created. Use the ``p`` comand to print the results on your screen.

Root partition
^^^^^^^^^^^^^^
The Root partition is where all the programs we install go. I went with the maximum size recommended, 32GB::

    Command (m for help): n
    Partition number (2-128, default 2):
    First sector (1050624-976773134, default 1050624):
    Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-976773134, default 976773134): +32G

Again, use the ``p`` comand to see how things are developing.

Swap partition
^^^^^^^^^^^^^^
The Swap partition is space on a disk that is used by the system when it runs out of physical RAM memory. In that case, inactive memory pages are moved from the RAM to the swap space. To use hibernation (a.k.a suspend to disk) it is advised to create the swap partition at the size of RAM, 16GB in my system.

.. note:: 
    
    **Suspend/Sleep** Put your computer at very low power state, screen off but everything else is on but at very low power so that you can resume your work where you left off but if battery die you loose all your unsaved data.

    **Hibernate**, (aka suspend to disk) saves the state of your computer to the hard disk and completely powers off. When resuming, the saved state is restored to RAM.

Let's get to it::

    Command (m for help): n
    Partition number (3-128, default 3):
    First sector (68159488-976773134, default 68159488):
    Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-976773134, default 976773134): +16G

We have to change the type to ``Linux swap``, so we use the ``swap`` alias::

    Command (m for help): t
    Partition type or alias (type L to list all): swap

As usual, use the ``p`` command to make sure of your progress.

Home partition
^^^^^^^^^^^^^^
The Home partition is where we put all of our shit::

    Command (m for help): n
    Partition number (4-128, default 4):
    First sector (101713920-976773134, default 101713920):
    Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-976773134, default 976773134):

Since we want to dedicate the remaining space in our disk to this partition, we just have to accept the defaults three times.

Writing table to disk
^^^^^^^^^^^^^^^^^^^^^
Finally, we'll write the partition table to disk and exit using the ``w`` command::

    Command (m for help): w
    The partition table has been altered.
    Syncing disks.

.. note:: If for some reason you get an error when writing, run ``fdisk`` again, print to make sure you have the table you just made and write.

