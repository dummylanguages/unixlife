*********************
Motherboard firmwares
*********************
https://www.happyassassin.net/2014/01/25/uefi-boot-how-does-that-actually-work-then/

UEFI vs BIOS
============
UEFI and BIOS are two different types of motherboard firmware.

Nowadays, most computers use **UEFI firmware** but still refer to it as the “BIOS”, which it's wrong. Other people, refer to UEFI firmware as **UEFI BIOS**, and BIOS is called **Legacy BIOS**, which is also wrong but we get the point.

BIOS
----
The BIOS must only has **1 MB** of space to execute in, for this reason it has trouble initializing multiple hardware devices at once, leading to a slower boot process when it has to deal with all the hardware interfaces and devices on a modern PC.

When a computer starts up, the BIOS loads and wakes up all the hardware components, making sure they are working properly. Then it loads the boot loader to initialize Windows or any other operating system you have installed.

MBR
^^^
The `master boot record`_ (MBR) is the first sector at the very beginning of any HDD or SSD that contains:

* Information about how the logical partitions, containing file systems, are organized on that disk.
* The `boot loader`_, which is executable code that loads the operating system.

UEFI
----
`UEFI`_ (Unified Extensible Firmware Interface) stores all the information about initialization and startup in a ``.efi`` file, stored on a special partition called **ESP** (short for EFI System Partition). The ESP partition will also contain the boot loader programs for the operating system installed on the computer.

.. note:: UEFI is pronounced like "unify" without the n.

It is because of this partition, UEFI can directly boot the operating system and save the BIOS self-test process, which is an important reason for UEFI faster booting.

One of the partitions on a GPT disk must be a special system partition known as the ESP, formatted as FAT and containing the files required for booting the computer.
A computer equipped with UEFI will, by default, boot using the information provided in the system partition but it also has the capability to boot a legacy MBR disk by enabling the Compatibility Support Module (CSM) in the settings, or by defining a boot menu item as a BIOS Compatibility item.

A partition structure defines how information is structured on the partition, where partitions begin and end, and also the code that is used during startup if a partition is bootable.

ESP
^^^
`ESP`_ stands for EFI System Partition



GPT
^^^
`GPT`_ (short for **GUID Partition Table**) is a standard for the layout of the partition table on a physical hard disk, using **GUID** (which stands for Globally Unique Identifier). Even though this standard forms part of the **UEFI standard**, it is also used on some BIOS systems.

.. note:: **GUID** is the term used by Microsoft to refer to `UUID`_.

GPT uses modern `LBA`_ (Logical Block Addressing) where:

* **LBA 0** is the first sector of the disk, and contains Legacy MBR information, so that programs which only understand MBR don't think the disk is unformatted and corrupted.
* **LBA 1** contains the GPT header. is in LBA 1, and the partition table itself follows. 
* In 64-bit Windows operating systems, **32 sectors**, are reserved for the GPT.
* **LBA 34** is the first usable sector on the disk.

Mounting the ESP partition
==========================
In Windows 10
-------------
We can mount the ESP partition with the `mountvol`_ command. We must run this command inside an **administrator command prompt**::

    mountvol P: /s

The command above mounts the **ESP partition** (``/s``) at the ``P:`` mount point, but we can choose any other letter as long as it's not already used. Now we can change to the new mount point typing ``P:`` and ``Enter``::

    C:\WINDOWS\system32>P:
    P:\>

If we ``dir`` the contents of drive **P** it has the filesystem::

    P:\
        EFI\
            Microsoft\
                Boot\
                    bootmgfw.efi
                Recovery\
            Boot\
                bootx64.efi
            debian\
                shimx64.efi
                grubx64.efi
                mmx64.efi
                fbx64.efi
                BOOTX64.CSV
                grub.cfg
                fw\
                fwupdx64.efi


When we boot the system, the UEFI firmware will look for:

1. The **Windows Boot Manager**, on ``EFI\Microsoft\Boot\bootmgfw.efi``, and if it doesn't find it...
2. As an **EFI fallback**, it will look for the file at ``EFI\Boot\bootx64.efi``. I heard that this is the Windows boot loader that Windows installs.
3. Under the ``debian`` directory there's also a bootloader named ``grubx64.efi`` that loads the **GRUB boot manager**, which allows us to select other boot loaders. We could get of it if we installed another boot manager that uses the Linux in-kernel EFI stub boot loader.

.. _`coreboot`: https://www.coreboot.org/
.. _`bootsec`: https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/bootsect-command-line-options
.. _`master boot record`: https://en.wikipedia.org/wiki/Master_boot_record
.. _`boot loader`: https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/bootsect-command-line-options
.. _`UEFI`: https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface
.. _`ESP`: https://en.wikipedia.org/wiki/EFI_system_partition
.. _`GPT`: https://en.wikipedia.org/wiki/GUID_Partition_Table
.. _`UUID`: https://en.wikipedia.org/wiki/Universally_unique_identifier
.. _`LBA`: https://en.wikipedia.org/wiki/Logical_block_addressing
.. _`mountvol`: https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/mountvol
.. _`bcdedit`: https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/bcdedit-command-line-options
