######################
UEFI and Boot managers
######################

.. toctree::
	:maxdepth: 3
	:hidden:

	motherboard_firmwares
	rEFInd


.. _`rEFInd`: https://www.rodsbooks.com/refind/
