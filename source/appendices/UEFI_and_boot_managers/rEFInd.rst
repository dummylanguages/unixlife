******
rEFInd
******

Installing rEFInd
=================
`rEFInd`_ is usually available as a package in the repositories of a lot of Linux distros.

From debian
-----------
Just install the `refind package`_ running::

    $ sudo apt install refind

And installing to the **ESP** running::

    $ sudo refind-install

After rebooting you'll be presented with the **rEFInd Boot Manager**. That's all.

Manually
--------

.. warning:: Don't even try. It didn't work.

If we prefer to install **rEFInd** manually, we can download a binary zipfile from the `Getting rEFInd`_ page. Once the zip file is in our Downloads folder to install it we just have to unzip it, and copy the ``refind`` folder to ``/boot/efi/EFI/refind``::

    # cp -r refind /boot/efi/EFI/

It's a good idea to get rid of the **binary files** and folders with **drivers** that doesn't apply to our system. For example, if our system's architecture is `x86-64`_, we can get rid of the **binary files** and **subdirectory drivers** for other CPU types such as:

* `IA-32`_(Intel Architecture, 32-bit), ``refind_ia32.efi`` and ``tools_ia32`` respectively.
* `AA64`_(AArch64 or ARM 64), ``refind_aa64.efi`` and ``tools_aa64`` respectively.

Editing the rEFInd configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Adding rEFInd to the list of boot loaders
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Now we have to add rEFInd to our EFI's list of available boot loaders. This list is stored in NVRAM. For that we'll use the command::

    # efibootmgr -c -d /dev/sda2 -l '\EFI\refind\refind_x64.efi' -L rEFInd

Let's go over the options:

* ``-c`` is to ``--create`` a new ``bootnum`` variable and add to ``BootOrder`` list.
* ``-d`` is to point to the ``--disk`` that contains the boot loader. We have to use it because it **defaults** to ``/dev/sda``. Use **gparted**, **Disks** or any other app to check the device that contains the EFI System.
* ``-l`` specifies the ``--loader``. Note the use of **backslashes** to specify the path to the boot loader. We could have written this path without quotes, as ``\\EFI\\refind\\refind_x64.efi``, but we have to escape the backslashes.
* ``-L`` allows us to ``--label`` the boot loader as wish, defaults to ``Linux``.

That command will automatically put the new boot manager at the top of the ``BootOrder`` list. We could run ``efibootmgr -v`` to verify that the rEFInd boot loader is at the top.

.. note:: Run `man efibootmgr`_ for more info about all the possible options.

rEFInd doesn't show up
^^^^^^^^^^^^^^^^^^^^^^
What we've done above should be enough to have rEFInd running, but if that's not the case you may need a workaround. Check the following diagram about the UEFI boot flow:


1. **UEFI Firmware**: Performs CPU and Chipset initialization, load drivers, etc.
2. **UEFI Boot Manager**: Loads UEFI device drivers and loads boot application.
3. **Windows Boot Manager** (``EFI\Microsoft\Boot\bootmgfw.efi``): Is responsible for loading the **Windows Boot Loader** (C:\Windows\System32\winload.efi) chosen by the user (in case there’s more than one Windows installed).

If rEFInd is not showing up it's because the UEFI firmware looks for the **Windows Boot Manager** at ``EFI\Microsoft\Boot\bootmgfw.efi`` and if it doesn’t find it, it’ll look at the **fallback path** which is at ``EFI\Boot\bootx64.efi``.

1. We want to **rename** the **Windows Boot Manager** so the **UEFI Boot Manager** can't find it, and has to look in the fallback path.
2. The fallback path directory will also be renamed as ``EFI/Boot.old`` for example, so the **UEFI Boot Manager** can't find it either.
3. We'll also rename the **rEFInd** directory as the fallback path so the **UEFI Boot Manager** loads it:

    * Rename the file ``EFI/refind/refind_x64`` as ``boot_x64.efi``.
    * And the directory ``EFI/refind`` as ``EFI/Boot``.

Now we can **delete** the old entry::

    # efibootmgr --bootnum 0003 --delete-bootnum

* The ``--bootnum`` option(``-b``) takes a bootnum as an argument, in this case ``0003```.
* The ``--delete-bootnum`` option (``-B``) **deletes** the bootnum.

Messing up the EFI partition
============================
If at some point you end up unable to boot Windows we can **format** and **rebuild** the partition. Boot into your `Windows 10 installation media`_, launch a command prompt and execute the following:

    mountvol B: /s
    format B: /FS:FAT32
    bcdboot C:\Windows /s B: /f UEFI

We are mounting the EFI partition at ``B``, then formating the partition, and rebuilding it. We should be able to boot into Windows at least.

.. _`rEFInd`: https://www.rodsbooks.com/refind/
.. _`refind package`: https://packages.debian.org/buster/refind
.. _`Getting rEFInd`: https://www.rodsbooks.com/refind/getting.html
.. _`x86-64`: https://en.wikipedia.org/wiki/X86-64
.. _`IA-32`: https://en.wikipedia.org/wiki/IA-32
.. _`AA-64`: https://en.wikipedia.org/wiki/AArch64
.. _`Windows 10 installation media`: https://support.microsoft.com/en-us/help/15088/windows-10-create-installation-media
.. _`man efibootmgr`: https://manpages.debian.org/buster/efibootmgr/efibootmgr.8.en.html
