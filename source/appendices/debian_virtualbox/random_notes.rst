************
Random notes
************

Installing some extra packages (not needed)
===========================================
Before being able to install the Guest Additions, we'll need some extra packages:

* The **linux headers** for your distribution.
* The package **build-essential**.

Install linux headers::

    $ sudo apt update && sudo apt upgrade && sudo apt dist-upgrade
    $ sudo apt install linux-headers-(uname -r)

Install the package **build-essential**::

    $ sudo apt install build-essential

Run the installation script
---------------------------
Once the CD image is mounted, we can install the Guest Additions by running the script::

    $ sudo sh /media/cdrom/VBoxLinuxGuestAdditions.run

Then we'll have to restart the machine running ``sudo reboot``.

Shared folders: Adding user to the ``vboxsf`` group
===================================================
We have to add our user to the ``vboxsf`` group, so we can access **shared folders**::

    $ sudo usermod -a -G sudo bob

Don't forget to **log out** and log back in so the changes take effect.

.. _`framebuffer`: https://en.wikipedia.org/wiki/Framebuffer
.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`dpkg-reconfigure`: https://manpages.debian.org/stretch/debconf/dpkg-reconfigure.8.en.html

