********
Host key
********
When we start a **virtual machine**, we activate it by clicking inside its screen. From that point on, the virtual machine automatically will capture all keyboard and mouse events so all keystrokes (including the system ones like **Alt+Tab**) will be directed to the virtual machine.

.. note:: If we have the **Auto capture keyboard** option turned on, that will happen automatically.

To return ownership of the keyboard and mouse to the **host OS**, **VirtualBox** reserves a **special key** on our keyboard known as the **Host key**. By default, this key is:

* On a Mac host, the default Host key is the **Left-Command** key.
* **Right-Ctrl** key on Windows and Linux hosts.

Defining the host key problem
=============================
The problem was that in a lot of desktop managers, the **Super key*** (which corresponds to the **Command key** in Apple keyboards) is used for a lot of useful things.

VirtualBox offers the option of changing the **host key** under:
    
    *Input->Keyboard->Keyboard Settings*

That seemed to work, except when we press ``Left-Command + Shift + Q`` to quit an app in the **guest**, the host, **OSX** always asked me if I wanted to *Quit all applications and log out*. The reason for that is that apparentely VirtualBox, no matter what, reserves the shortcuts **Cmd + Q** and **Cmd + H**.

Workaround
----------
To solve this problem I decided to use a **macOS** program named `Karabiner`_ that allows to remap key presses to whatever key event we want. The solution involved using 2 remappings:

1. In OSX: Map the **Left Command key** to produce the key event of another key, preferably of some unused key; I opted to remap it to **Scroll Lock**, since I don't remember having used that key ever, and, as far as I know, it doesn't have a useful purpose in macOS.

This remapping only had to work when using **VirtualBox**. If I'm using any other app, **Left Command** will still produce the **Left Command** event. (We don't want to use the **essential** functionality of the **Command** keys in macOS.)

2. In our virtual machine: Remap the **Scroll Lock** key, back to **Right Command** (Right Super in linux), so I can use it freely as my **Super key** in any desktop manager.

In the host: Karabiner
^^^^^^^^^^^^^^^^^^^^^^
**Karabiner** has available a lot of pre-built remappings under the **Complex Modifications** tab. If we click on the button **Add rule** (it's at the bottom-left corner), that opens a **website** with lots of karabiner rules. From there I chose a group that seemed to contain stuff that I could use: **Application Specific**, meaning it would only work when some application has the focus. 

In that group chose a rule named **Don't exit fullscreen when pressing ESC in Safari**. After some examination of the **json** markup, I clicked on the button **Import**. Automatically a pop-up window offered me to open the link with **Karabiner**, what I did. Automatically a json file was downloaded into ``~/.config/karabiner/assets/complex_modifications``. The plan was to modify that file to make it suitable to my needs.

Since, I only wanted this remap working when any of the **VirtualBox** windows (the manager or the virtual machine) had the focus, I needed a piece of information named **bundle identifier**. So I launched the **Eventkeyviewer** and clicked on both:

* The **VirtualBox VM** window.
* The **VirtualBox manager** window.

To get the bundle identifiers of each window: ``org.virtualbox.app.VirtualBoxVM`` and ``org.virtualbox.app.VirtualBox`` respectively. I finish editing the file and chose a rule name for it, saved it, and added the rule from Karabiner elements.

.. note:: In Karabiner we don't have to edit the main configuration file located in ``~/.config/karabiner/karabiner.json``.

I had to add a new file under the ``~/.config/karabiner/assets`` directory, that I chose to name :download:`leftCommand_to_scroll_lock.json </_code/appendices/leftCommand_to_scroll_lock.json>`:

.. literalinclude:: /_code/appendices/leftCommand_to_scroll_lock.json
   :language: json
   :linenos:
   :tab-width: 2

As a result, when any of these two windows had the focus, pressing the **Left Command** key would have the effect as pressing the **Scroll Lock** key, even when using the combinations **Left Command + Q** or **Right Command + H**.

In the guest: Debian
^^^^^^^^^^^^^^^^^^^^
The second part of the job was to make the Linux virtual machine to interpret all **Scroll Lock** key events as **Super key** events. In other words, we wanted to remap the keycode (``scroll_lock``) that I was getting from pressing the **Right Command** key, so that my linux machine would interpret it as  **Right Super**.

Finding the keycode and action
""""""""""""""""""""""""""""""
We need to find two things:

1. The keycode of the **Scroll Lock** event that the physical **Command** key is sending to the Linux virtual machin, when any of the VirtualBox windows has the focus. Don't forget to enable the **rule** we created in karabiner!

2. The name of the **action** that **Right Command** would send normally, when is not remapped. For that we have to disable the karabiner **rule**.

There is a command named ``xev`` that prints the contents of X events, such as keypresses::

    $ xev

We'll see a lot of info, but we are just interested in the **keycode number**, which we'll write down to use it later.

1. So, from the karabiner menu we enabled the **rule** and after running ``xev`` on linux, we get the keycode **78**.
2. With the karabiner rule **disabled**, we get the name of **action** for the **Right Command** key, ``Super_R``.

Testing the remap
^^^^^^^^^^^^^^^^^
So what we want to do, is tell linux that when the keycode for **Scroll Lock** (78) is received, it must be interpreted as the ``Super_R`` action. We'll do that with the command::

    $ xmodmap -e "keycode 78 = Super_R"

This way we can check if the remap works, but the problem is that the modification will not survive after we reboot our **guest** system.

Permanently remapping
^^^^^^^^^^^^^^^^^^^^^
Once we test out that everything works according to plan, we have to make the remapping **permanent** writing it down into some configuration file. Here we have several options:

1. Some **display managers** such as lightdm, read the ``~/.Xmodmap`` when they are loaded, so we can create the file and add the command in it::

    $ xmodmap -e "keycode 78 = Super_R" > ~/.Xmodmap


2. Use the ``.xinitrc`` file, called by ``xinit`` (``startx`` is just a front-end script for ``xinit``),  when starting up X sessions. But some **display managers** (aka login managers) don't use this configuration file. For me it's not a problem since I don't use any display manager at all, just plain ``logind`` (the login screen on tty)

3. Use the configuration file in our desktop manager (in i3wm is ``~/.config/i3/config``), which makes a lot of sense, since I won't probably need this remapping in other window managers.

3. Configuring the **XKB** X11 extension, read this section to see how. This is the proper way to do it in a real system, the problem with this approach is that the Virtual Machine manager intercepts the **Super_L** keypress.

What I did
^^^^^^^^^^
I opted for the **third option** and added the following line at the end of my ``~/.config/i3/config``::

    exec_always xmodmap -e 'keycode 78=Super_L'

.. note:: The keycode for Super_L is 133.

Also, I repeated the process for my **Left Command**.


TODO: Create another file so I could use the **Pause** key when on **Virtual Manager** as the **Host** key. 

.. _`Karabiner`: https://pqrs.org/osx/karabiner/
