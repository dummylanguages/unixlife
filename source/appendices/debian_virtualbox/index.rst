######################################
Setting up Debian in a virtual machine
######################################

.. toctree::
	:maxdepth: 3
	:hidden:

	installing_guest_additions
	console_configuration
	desktop_environment
	super_key
	random_notes

Here we'll see how to set up **Debian** as a virtual machine using **VirtualBox** in a **macOS** host. Let's assume:

1. That `VirtualBox`_ is already installed in our **host** system, macOS in this case.
2. We have also installed the right version of the **extension pack**, the one with the same version number as our VirtualBox software.
3. And that we have created a Debian virtual machine and did a **minimal installation**, without the X Window System, just the Linux Console.

Here we're gonna take care of a few in our **guest** (the Debian virtual machine):

1. Install the Guest additions.
2. Change the framebuffer resolution.
3. Adjust the Font size.

.. _`VirtualBox`: www.virtualbox.org