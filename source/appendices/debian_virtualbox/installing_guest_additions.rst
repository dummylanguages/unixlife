******************************
Installing the Guest Additions
******************************
To make sure we're on the same page, let's summarize where we are:

* Our **host** operating system is **macOS**
* We have installed the **VirtualBox** software and its corresponding **extension pack**.
* We also have created a **virtual machine**, and completed a **Debian** installation.

What are the Guest Additions
============================
The **Oracle VM VirtualBox Guest Additions** is a set of software (device drivers and system applications) that provides closer integration between host and guest, such as:

* Better video support (aka **full screen**), so the video resolution in the guest will be automatically adjusted to the size of our screen.
* **Shared folders** between the host and guest systems.
* **Shared clipboard**, so we can copy and paste text in both directions.

The Guest Additions are designed to be installed inside a **virtual machine** after the **guest operating system** has been installed. Every installation of the **Oracle VM VirtualBox** platform includes this software as a single CD-ROM image file which is called ``VBoxGuestAdditions.iso``.

1. Insert Guest Additions CD image (host)
-----------------------------------------
In our **host** system, meaning the system where you have installed VirtualBox (in this case **macOS**), we have to start the Debian virtual machine. Once it's running, in the **VirtualBox VM** window, (not the VirtualBox Manager) place the cursor over the toolbar, under **Devices** select **Insert Guest Additions CD image**.

.. figure:: /_images/insert_guest_additions.png

2. Mounting the Guest Additions CD image (guest)
------------------------------------------------
Usually, the **iso image** will be already mounted and accessible from our **file manager**. For example, in **GNOME Files** (the **file manager** used in GNOME), we should be able of seeing it mounted:

.. figure:: /_images/already_mounted.png

If for some reason the **iso image** is **not mounted**, we could run the following command::

    # mount /dev/cdrom /media/cdrom

Note the the prompt looks like a `#` We can mount it only if we have **root privileges**.

.. warning:: Again, we'll doing all this in our **guest** system (the Debian virtual machine).

3. Install the necessary packages
---------------------------------
In order to run installation script, we'll need to install some packages in our **guest system** (Debian in this case)::

	sudo apt install build-essential dkms linux-headers-$(uname -r)

.. note:: `$(uname -r)` prints the running Kernel version. Headers are a bit different among different kernels.

4. Run the installation script
------------------------------
For this step, we can open the file manager, select the `VBoxLinuxGuestAdditions.run` script and click on the **Run Software** button:

.. figure:: /_images/run_software.png

.. warning:: If you run into issues when trying to run the scripts from the **file manager**, you may have to login as **root** (`su root`) and try to do so from the terminal.

If for some reason you don't have a GUI file manager, we can also run the script from a console::

    # sh /media/cdrom/VBoxLinuxGuestAdditions.run

.. warning:: You must **run** the script with `sh`, and not just with `source` or with `./`.

Then we'll have to restart the machine running ``sudo reboot``, and the Guest Additions will be installed.

.. note:: Here we have been running powerful commands as **root**, because sometimes the **normal user** is not in the `sudo` group (or `sudo` is not even installed). Generally speaking, it's more advisable to do system administration as a normal user using ``sudo`` to escalate privileges. Read our other :ref:`section about it <sudo-label>` to see how to add a user to the `sudo` group.

.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
