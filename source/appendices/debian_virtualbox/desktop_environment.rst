*********************
A Desktop environment
*********************
Here we'll take the necessary steps to install a `desktop environment`_ for our Debian virtual machine. But since we did a minimal installation, our system lacks a lot of the components that a desktop environment needs to function.

The main thing we are missing is a **display server**, so we're going to install the `X.Org server`_.

The X.Org server
================
There are several packages in Debian that install different parts of the X Window System, but since we are just interested in the **display server**, the `xorg`_ package is what we are looking for. This package contains the X libraries, the server, a set of fonts, and a group of basic X clients and utilities. Installing it, it's easy::

    $ sudo apt install xorg

Once the installation finishes we can start the server with the command::

    $ sudo startx

We'll be presented with a black screen and a smaller terminal emulator white screen (xterm). We won't be able of dragging the terminal window or doing almost anything, because our installation doesn't include **twm** or any window manager at all. Let's exit the X session with the ``exit`` command.

XFCE
====
In the last section we installed a **display server**, another important missing piece is a **window manager**. Usually, desktop environments include their own window manager, and `XFCE`_ is not an exception to this. In Debian there is a package named `xfce4`_ that includes the essentials we need to have a functional desktop, such as:

* The `xfwm4`_ **window manager**.
* A file manager named `thunar`_.
* A terminal emulator called `mlterm`_.
* The `lightdm`_, display manager.

We can install it running::

    $ sudo apt install xfce4

Once installed, as soon as we launch the X Window System running ``startx``, we should be presented with our new desktop environment.

If we reboot the system we won't have to log in using a tty, but a **graphical login manager**. To find out which one::

    $ cat /etc/X11/default-display-manager
    /usr/sbin/lightdm


.. note:: Graphical login managers are also known as `X display managers`_, or just display managers. `XDM`_ (short for, the X Display Manager) is the default display manager in the X Window System.

Shortcuts
---------
* Launch terminal: Command+Enter
* Close window: Command+Q

LightDM
=======

DWM
===

BSPWM
=====
Stop the XOrg server: ``Alt + PrtScn/SysReq + K``
A bit of a long winded keyboard shortcut, maybe too many people were pressing ``Ctrl + Alt + Backspace`` so they changed it to this.

Launch TTY: Ctrl+Alt+F1?

.. _`windowing system`: https://en.wikipedia.org/wiki/Windowing_system
.. _`WIMP`: https://en.wikipedia.org/wiki/WIMP_(computing)
.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`display server`: https://en.wikipedia.org/wiki/Display_server
.. _`login manager`: https://en.wikipedia.org/wiki/Login_manager
.. _`XDM`: https://en.wikipedia.org/wiki/XDM_(display_manager)
.. _`X.Org server`: https://en.wikipedia.org/wiki/X.Org_Server
.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
.. _`twm`: https://en.wikipedia.org/wiki/Xterm
.. _`xterm`: https://en.wikipedia.org/wiki/Xterm
.. _`xorg`: https://packages.debian.org/sid/xorg
.. _`XFCE`: https://www.xfce.org/
.. _`desktop environment`: https://en.wikipedia.org/wiki/Desktop_environment
.. _`xfce4`: https://packages.debian.org/buster/xfce4
.. _`lightdm`: https://github.com/canonical/lightdm
.. _`X display managers`: https://en.wikipedia.org/wiki/X_display_manager
.. _`XDM`: https://en.wikipedia.org/wiki/XDM_(display_manager)
.. _`xfwm4`: https://docs.xfce.org/xfce/xfwm4/start
.. _`thunar`: https://docs.xfce.org/xfce/thunar/start
.. _`mlterm`: http://mlterm.sourceforge.net/

