****************************
Setting up the Linux console
****************************
In the last section we installed the Guest Additions and rebooted our system. In this section we are going to:

1. Set up the console resolution.
2. Set up the console font.

.. note:: The stuff we're gonna be dealing with here has nothing to do with the `X Window System`_. We are trying to fix the screen resolution on the **Linux console** aka the `framebuffer`_, which is part of the Linux kernel.

If we've completed a **Debian minimal install**, meaning, we haven't installed the X Window System yet, when we boot up the virtual machine we are greeted by the **Linux console**, we can log in as root or as a normal user, in case we created one during the installation process. But, the **screen resolution** is very small, it doesn't extend to the borders of our monitor, it's like a small square in the middle, not good.

Fixing the console resolution: a first try
==========================================
In order to set up the resolution of the Linux console, we have to check what are the available resolutions. When we turn on the virtual machine and the bootloader screen shows up, we have to press the letter ``c`` to start the **GRUB console**. The following prompt should appear::

    grub>

A good idea is to run the command::

    grub> set pager=1

So that when we list the available resolutions they show up in paged form and we don't miss the first section of the output. Then, we can list the resolutions with the commands ``vbeinfo`` or ``videoinfo``. In my case I chose the biggest one in the list, ``1280x1024x24``. Then we have to press **Shift+Escape** to get back to the GRUB original screen and boot the system normally.

Once we log in, we have to edit the ``/etc/default/grub`` file and add::

    GRUB_GFXMODE=1280x1024x24
    GRUB_GFXPAYLOAD_LINUX=keep

The first variable ``GRUB_GFXMODE`` sets the resolution for the Grub menu that shows up while booting the system, whereas ``GRUB_GFXPAYLOAD_LINUX`` is the resolution that we'll enjoy once the system boots.

It's essential to run the following command, to write the changes to the ``/boot/grub/grub.cfg`` file by running::
    
    # update-grub

And finally **reboot** the system::

    # reboot

We'll notice that the resolution has improved, but we're not taking advantage of the total resolution our host system offer, in my case 1920x1080. That's the resolution we should use, the problem is that, as we saw, is not listed as a valid resolution.

To be able of setting up the native resolution of the host we have to take several steps:

1. Install the Guest Additions.
2. Setting a video mode in the host system, so that a new resolution will be available in the guest.
3. Change the resolution of the console as we just did, but this time with the right resolution.

.. note:: To be honest, for a headless system (no X Window System) the Guest Additions don't buy us much, just the ability of setting a proper resolution for the Linux console.

Fixing the console resolution
=============================
To properly set up the console resolution in our Debian virtual machine we have to take a two step process:

1. On the **host** we have to customize a new resolution and make it available for the virtual machine.
2. On the **guest** we'll take advantage of this new resolution in our GRUB configuration.

On the host
-----------
Here we have to do a small fine-grain adjustment that is not available in the GUI of the VirtualBox manager, so we have to use a couple of command-line utilities installed within VirtualBox.

.. warning:: Make sure you have installed the right version of the **extension pack**, otherwise none of the following steps will work!

1. Get the name of your virtual machine::

    $ vboxmanage list vms
    "debian" {6350388d-06dc-49af-b67c-a6e2f2080d91}

2. Let's add a CustomVideoMode::

    $ vboxmanage setextradata "debian" CustomVideoMode1 1920x1080x24

3. Let's verify the custom video mode::

    $ vboxmanage getextradata "debian" CustomVideoMode1
    Value: 1920x1080x32

4. And if we want to do it 'inmediately' (not sure this works)::

    $ vboxmanage controlvm "debian" setvideomodehint 1920 1080 24

On the guest
------------
On the Debian virtual machine we have to reboot again to verify that the custom resolution we just set in the host is available in the guest. Remember, when the bootloader screen shows up, we have to press the letter ``c`` to start the **GRUB console**, and list the available resolutions with the command ``vbeinfo`` or ``videoinfo``.

We should see the custom resolution showing up, most probably at the bottom of the list. If that's the case press **Shift+Escape** to get back to the GRUB original screen and boot the system normally.

Once the system has boot up, we'll edit again the ``/etc/default/grub`` file::

    GRUB_GFXMODE=1920x1080x24
    GRUB_GFXPAYLOAD_LINUX=keep

The first variable ``GRUB_GFXMODE`` sets the resolution for the Grub menu that shows up while booting the system, whereas ``GRUB_GFXPAYLOAD_LINUX`` is the resolution that we'll enjoy once the system boots.

It's essential to run the following command, to write the changes to the ``/boot/grub/grub.cfg`` file by running::
    
    $ sudo update-grub

Finally **reboot** the system::

    $ sudo reboot

The resolution of the console should look just fine.

Choosing a console font
=======================
Most Linux distributions that use `systemd`_ as their init system, keep the console configuration in the ``/etc/vconsole.conf`` file. Even though **Debian** uses **Systemd**, it still uses the ``console-setup`` file to keep the font configuration.

In Debian there's a called utility named `dpkg-reconfigure`_ which allows us to reconfigure packages after they have already been installed. We just have to pass it the name of the package we want to reconfigure, in this case::

    # dpkg-reconfigure console-setup

This will open a console-base graphic menu where we'll use the **vertical arrow keys** to navigate the options, the **horizontal arrow keys** to select “OK”, and press **ENTER** when ready.

To learn more about console fonts read our other :ref:`section about it <console-fonts-label>`.

.. _`framebuffer`: https://en.wikipedia.org/wiki/Framebuffer
.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`systemd`: https://systemd.io/
.. _`dpkg-reconfigure`: https://manpages.debian.org/stretch/debconf/dpkg-reconfigure.8.en.html

