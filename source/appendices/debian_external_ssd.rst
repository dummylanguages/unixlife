************************
Debian in a external SSD
************************
Recently had the luck of buying a second hand Lenovo T480 at a price I couldn't let go. Here I'm gonna keep notes about how to install Debian Linux in an external SSD.

Windows 10 Product Key
======================
When playing with **dual boots** it's always a good idea to keep at hand the **Windows 10 license number**, in case we have to reinstall again. Usually computers come with Windows 10 have the key in the UEFI's firmware, so if we are installing Windows in that same computer, our license will be activated automatically.

.. note:: **OEM keys** are shipped with a specific computer and can't be transferred to another machine.

But if for some reason the key is not there, or we want to install Windows in another computer, we have to find out the license number and writing it down somewhere. There are two methods to get the Windows 10 Product key:

1. Press ``Windows + R`` to open **Run** and type ``cmd`` to open the **Command Prompt**. To get the key type::

    >wmic path softwareLicensingService get OA3xOriginalProductKey
    OA3xOriginalProductKey
    XYBM5-NBQB7-YWMCR-27JP9-MPL31

2. Press ``Windows + R`` to open **Run**, type ``regedit`` to open the **Registry Editor**. There you have to click on::

    HKEY_LOCAL_machine\SOFTWARE\Microsoft\Windows NT\CurrentVersion

Find ``DigitalProductID``, the problem is that it's written in hexadecimal format.

Creating a bootable drive
=========================
We want to create a **bootable pendrive** so we can reboot the laptop into Linux, and from there, install it on the external SSD. These are the steps:

1. Download a `Debian iso`_. If we download a **small image**, it will be faster to write to the pendrive, but we'll get a small selection of packages in case our internet connection doesn't work.
2. Download `rufus`_, a Windows program to write the iso to the pendrive and make it bootable.
3. Plug in the pendrive and use **rufus** to write the **Debian iso** to the **pendrive**.

It take a while, go drink some tea or smoke some crack.

Booting from the pendrive
=========================
Once we have the bootable usb drive, we have to change some settings on the laptop's *BIOS*(we mean UEFI) so we can **boot** from the pendrive.

Change BIOS settings
--------------------
Restart the computer and when you see the **LENOVO** red screen, press ``Enter`` and then ``F1`` to enter the *BIOS*. Once inside, we have to:

1. Under the **Security** tab, we have to move down to **Secure Boot** and select **Disabled**.
2. Then under the **Startup** tab, and on **UEFI/Legacy boot** select **Both**. (UEFI first and CSM Support:yes)

Save changes and exit **F10**.

.. note:: This approach didn't work since the BIOS didn't allow me to enable **Both**, but I was able to access the **boot menu**.

Entering Boot menu
------------------
While the laptop is restarting press ``F12`` to enter the **boot menu** and select the pendrive.

Installing Debian to the SSD
============================
Once we boot from the pendrive, we should be welcomed by an **installation graphical menu**, the only thing worth mentioning here is the disk partitioning.

Disk partitioning
-----------------
In this stage we must be careful selecting the disk we want to install to, otherwise we risk formatting and installing to the internal hard drive. In my case I went with this partitioning scheme:

* A **250 MB** partition, Fat32, ``bootable`` flag. I labeled it **boot**.
* **30 GB** formatted as ext4 for the **root partition**, where we'll mount the ``/`` directory.
* **8 GB** for **swap space**, right click and ``swapon``.
* **125 GB** formatted also as ext4 for my ``/home`` directory. Labeled as **home**.

Fixing Boot Order
-----------------
After the installation finished, simply by rebooting the laptop with the SSD connected Debian was booted. This had the following problem:

* If for some reason the SSD was **unplugged** (I didn't bring it to Starbucks that day) booting the laptop would send me to the ``grub`` command line. Running ``exit`` would get me out of it, and back to the **boot menu** where I could select **Windows Boot Manager** and automatically boot into Windows. But I wanted to find a cleaner solution:

1. Whenever the SSD was **unplugged** (because either wasn't available, or wanted ot boot in Windows) **Windows 10** had to boot automatically.
2. To boot Debian, the SSD had to be **plugged in** and we had to access the **UEFI built-in boot manager** in order to boot into Debian.

.. note:: In another section we'll see how to install, `rEFInd`_ a good looking (themable) boot manager/boot loader.

The solution
^^^^^^^^^^^^
Finally, a solution to change the boot order was quite easy.

From Windows: easy way
""""""""""""""""""""""
I just have to download and install a program named `EasyUEFI`_. Once we launch the program, we have to click on **Manage EFI Boot Option**. Then under the Boot order tab, select **Windows Boot Manager** and used the green arrows to move it up to the top of the list.

Solved, from now on, every time we boot the laptop, **Windows 10**, or whatever OS we have put at the top will boot first. We can also edit the **description** of the **boot loader** entry in the UEFI's built-in **boot manager**.

From Windows: command line
""""""""""""""""""""""""""
Alternatively we can use a Windows command named `bcdedit`_ to manually set the path to the first boot manager::

    bcdedit /set "{bootmgr}" path EFI\Microsoft\Boot\bootmgfw.efi

We may use this command to write a description as well::

    bcdedit /set "{bootmgr}" description "Evil corp. OS"

That's the description that will pop up when using the UEFI's built-in boot manager

From Linux
""""""""""
Run `efibootmgr`_ without any options to identify::

    $ sudo efibootmgr
    BootCurrent: 0001
    Timeout: 2 seconds
    BootOrder: 0000, 0001, 0017...
    Boot0000* Evil Corp. OS
    Boot0001* debian
    [...]

To adjust the **boot order**, you must identify the entry and then use the ``-o`` option::

    $ sudo efibootmgr -o 0000,0001...

Run `man efibootmgr`_ for more information.

WiFi drivers
============
Once I booted into Debian, the WIFI card wasn't working. I had to go to the Debian packages site and search for `firmware-iwlwifi`_, downloaded it (in another computer) copy it to the Thinkpad using a pendrive. Once there I installed it with::

    $ sudo apt install ./firmware-iwlwifi.deb

.. note:: When installing from the local filesystem do not forget the ``./`` before the debian package, so ``apt install`` won't go looking for it to the repositories. Or use the old ``dpkg -i``.

After that, I run the `dmesg`_ command to check that the card was working. In case the iwlwifi module wasn't loaded::

    $ sudo modprobe iwlwifi

And if that is not enough, using the same command with the ``-r`` option, removes the module. After that I can reload it again.


.. _`Debian iso`: https://www.debian.org/CD/torrent-cd/
.. _`rufus`: https://rufus.ie/
.. _`rEFInd`: https://www.rodsbooks.com/refind/
.. _`EasyUEFI`: https://www.easyuefi.com/index-us.html
.. _`bcdedit`: https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/bcdedit-command-line-options
.. _`efibootmgr`: https://github.com/rhboot/efibootmgr
.. _`man efibootmgr`: https://manpages.debian.org/buster/efibootmgr/efibootmgr.8.en.html
.. _`firmware-iwlwifi`: https://packages.debian.org/stable/firmware-iwlwifi
.. _`dmesg`: https://en.wikipedia.org/wiki/Dmesg
