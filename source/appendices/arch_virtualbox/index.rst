************************************
Installing Arch in a virtual machine
************************************

.. toctree::
	:maxdepth: 3
	:hidden:

	arch_minimal_install
	arch_guest_additions

Here we'll see how to set up Arch inside VirtualBox in a macOS host. Let's assume:

* We have installed `VirtualBox`_ in our **host** system, macOS in this case.
* And that we have created an Arch virtual machine and did a minimal installation.

Here we're gonna take care of a few in our **guest** (the Arch virtual machine):

1. Install the Guest additions.
2. Change the framebuffer resolution.
3. Adjust the Font size.

.. _`VirtualBox`: www.virtualbox.org