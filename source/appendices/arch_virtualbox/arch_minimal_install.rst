Arch installation
=================
First thing is downloading an **Arch Linux image**, an iso file we can download from **archlinux.org**. Once we have the image, we'll boot our virtual machine with the image mounted on a **virtual optical drive**.

Note: for a real installatation we would have to put that image on a pendrive (Using ``dd``).

Booting
=======
Upon boot, we'll be face with a black screen, logged in as root (automatically). First thing we should do is make sure we have an internet connection::

    # ping archlinux.org

In this case we're going to assume we have it, if you don't, since we are installing Linux on a virtual machine, configure it in your host system.

Partitioning
============
Next thing is checking our **hdd** with either::

    # lbsblk

Or::

    # fdisk -l

Once we know the name of the **block device** (aka hard drive) where we want to install Linux. In my case, since I created a **8GB** fixed size virtual hdd I'll choose the device with that size, ``/dev/sda``. 

To actually create the partitions we have 2 options:
1. Using ``fsdisk``, text based interface.
2. Using ``cfdisk``, it offers a nice GUI.

Let's choose the 2nd option::

    # cfdisk /dev/sda

Labeling
--------
After typing the command above we'll be faced with a series of options about the **label type**:
* gpt, for UEFI boot.
* dos, for BIOS boot.
* sgi.
* sun.

Since we want to install **Arch** on a **virtual machine**, we'll select **gpt**. (U)EFI booting not enabled by **default** in virtualbox although we can enable it if we want under **System**, on the **Motherboard** tab, checking the **Enable EFI (special OSes only)**. 

Important: It must be noted that if we installed our system on our **virtual machine** using BIOS firmware, we can not switch over to EFI. On real systems it can be done though.

About gpt vs dos
----------------
dos is the **label type** for the BIOS old system. This system writes a MBR 

Most of the Operating Systems (except OSX) can boot from BIOS. W
Using MBR/BIOS, we cannot boot from a partition bigger than 2TB.

On a real system, we can verify the boot mode running::

    # ls /sys/firmware/efi/efivars

If UEFI mode is enabled on an UEFI motherboard, Archiso will boot Arch Linux accordingly via systemd-boot. To verify this, list the efivars directory. But if the directory does not exist, the system may be booted in BIOS or CSM mode.
If that directory is not empty, we may have a computer that require UEFI. It is recommended to always use **GPT** for UEFI boot, as some UEFI firmwares do not allow UEFI-MBR boot.

Refer to your motherboard's manual for details.

Partition scheme
----------------
1. Select **New** to create a partition.
2. In this case we'll keep it simple and create a single partition taking the whole available space. 
3. Primary.
4. Bootable: The boot column will be marked with an asterisk.
5. Select **Write**, to write the partition, and confirm with **yes**.
6. Now we can **Quit**.

A real world partition scheme
-----------------------------
That would be something like:

1. Boot partition, primary (``/dev/sda1``). Size is around 200MB. Red Hat Linux recommends 250MB.
2. Swap partition, primary (``/dev/sda2``). More than 512MB.
3. Root partition, primary (``/dev/sda3``). Where all the programs we install go. Size: 23–32 GiB.
4. Home partition, primary (``/dev/sda4``). Where we keep all our shit. The remainder of the disk.
5. A ``/boot/efi`` partition (EFI System Partition) - only on systems with UEFI firmware

Having a properly partitioned system, allows us to reinstall the system on the **root** partition, while all of our stuff is safe at **home**.

Note: All of these partions will use the **ext4** filesystem; there's no need to format the **swap** partition. For this last partition we'll have to initialize it::

    # mkswap /dev/sda2
    # swapon /dev/sda2

Formatting the partition
------------------------
Now we need to format the new partition we have just created. We'll use the **ext4** filesystem:

    # mkfs.ext4 /dev/sda1

Mounting the device
-------------------
Mount the device so we can write data to it, this would be needed for installing the base system onto it:

    # mount /dev/sda1 /mnt

It's tradition to mount the filesystem on the root partition on ``/mnt``.

Note: if we had more partitions we'd have to mount them too:

    # mount /dev/sda1 /mnt/boot
    # mount /dev/sda4 /mnt/home

Installing the base packages
----------------------------
Use the ``pacstrap`` script (included in the Arch Linux iso) to install the packages: 

    # pacstrap /mnt base base-devel

We are installing the **base** and the **base-devel** packages groups, but we could add additional packages if we wanted to. All the packages to be installed are downloaded from mirror servers, which are defined in ``/etc/pacman.d/mirrorlist``. On the iso image, all mirrors are enabled.

Writing a Filesystem Table
--------------------------
Before we mounted manually the filesystem located in the ``/dev/sda1`` device to the mount point (``/mnt``). What we need now is to create a file that mounts the filesystem automatically during the boot process. To generate this file we'll use the command:

    # genfstab -U -p /mnt >> /mnt/etc/fstab

The ``genfstab`` command outputs all the stuff mounted on ``mnt`` to the standard output, so we have to redirect it to some file, in this case ``/mnt/etc/fstab``. The ``-U`` option stands for **UUID**, and it's to give each filesystem a unique identifier. We can check the generated file opening it with an editor::

    $ nano /mnt/etc/fstab

Later on, we can check the contents of this file doing::

    $ cat /etc/fstab

Configuring the system
======================
Before start configuring the system, we have to do change the **root** of our system. In order to do that we'll use the command::

    # arch-chroot /mnt

This command starts an interactive shell, which treats the argument passed(``/mnt``) as the root directory. So now ``/mnt`` becomes ``/``.

Now we can start configuring stuff.

Time zone
=========
First of all we can check what time zones are available manually browsing the directories::

    # ls /usr/share/zoneinfo
    # ls /usr/share/zoneinfo/Europe

Or running the command::

    # timedatectl list-timezones

We'll get a list of results with the format **Region/City**, we just have to choose ours, and set it out with:

To set the timezone we have a couple of choices. We can run::

    # timedatectl set-timezone Europe/Moscow

Or **manually** create a symbolic link with::

    # ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime

Time
====
For the time, we have to enable the hardware clock and let the OS understand that it is running in UTC:

    # hwclock --systohc --utc

Localization
============
Before a **locale** can be enabled on the system, it must be generated. This can be achieved by uncommenting applicable entries in the ``/etc/locale.gen`` file, which contains the list of locales that are to be generated with the ``locale-gen`` command. I'm gonna go ahead and uncomment the line that says::

    en_US.UTF-8 UTF-8

Then generate the locales running the command::

    # locale-gen

Once the **locale generation** is complete, we can list the locales available in our system doing::

    # localedef --list-archive

Or::

    # localectl list-locales

Setting the system-wide locale
------------------------------
Now we have to choose from all the **available locales** the **system locale**, and we have to manually create the ``/etc/locale.conf`` adding the locale we want. I'm just gonna add: :

    # echo "LANG=en_US.UTF-8" > /etc/locale.conf

Once we have that file, we can modify it in the future running the command::

    # localectl set-locale LANG=en_US.UTF-8

To display the current system locale and keyboard layout settings we'll use::

    # localectl status
    System locale: LANG=en_US.UTF-8
    VC keymap: n/a
    X11 layout: n/a

From the output above we can see that our system locale is properly set up. The last two lines are for the layout of the keyboard in:
* The **Virtual Console**, meaning the TTY that we'll see when we log in.
* The **X11 layout** is the layout when we launch X11.

The default console keymap is **US**, so no worries. Available layouts can be listed with::

    # ls /usr/share/kbd/keymaps/**/

Overriding system locale per user session
-----------------------------------------
The system-wide locale can be overridden in each user session by creating or editing ``~/.config/locale.conf``.

Network configuration
=====================
We don't have to do much regarding our network when we install Arch in a virtual machine:

1. But this one is really important: We have to enable the **dhcp client** so it can talk to the host machine (or to our home router in a real installation):

    # systemctl enable dhcpcd

Note: without this, we won't be able of accessing the internet to install packages for example.

In a **real world installation** we would install **NetworkManager**:

    # pacman -S networkmanager

Once we have it installed we have to tell **Systemd** to start it when the system boots up:

    # systemctl enable NetworkManager

This will create a bunch of symlinks, which tell **Systemd** to start **NetworkManager** whenever we log in.

2. Choose a **hostname**, meaning the name that identifies our machine on a network; the same one you see for example in your prompt when you are on a shell. I'm gonna call this machine **black-coffee**:

    # echo 'black-coffee' > /etc/hostname

3. Related to the **hostname**, we also have to add a basic **hosts** file. Let's use a text editor this time:

    # nano /etc/hosts

Add in there:
    127.0.0.1   localhost
    ::1         localhost
    127.0.1.1   black-coffee

    About the hosts file: When we type a **hostname** in our browser (facebook.com), our system will check our **hosts** file, to find out what's the IP address of that site. If it doesn't find it there, it will look it up on the DNS servers that we have configured in our network. (That's why we don't have to have the hostname/IP of the whole internet on our hosts file)

    But we can use our hosts file for a couple of things:
        * Aliases for locations on our local network.
        * Block certain sites by associating their hostname with a fake IP.

Setting a root password
=======================
Easily done:

    # passwd

You have to re-enter it to confirm.

### Installing a bootloader
Finally, we will have to install a **bootloader**, responsible of booting our system. It would also be giving us  options to choose which OS to boot from, in case we had multiple operating systems installed. We have chosen **grub** because why not:

    # pacman -S grub

* Once the **grub** package is installed, let's make sure of the name of the partition so we don't write grub to the wrong one:

    # fdisk -l

Assuming the name of the name of our disk is ``/dev/sda``, then we have to install **grub** to the **Master Boot Record**:
    # grub-install /dev/sda

Note that we are pointing this command to ``/dev/sda``, and not to ``/dev/sda1``. 

The ``grub-install`` command accepts a ``--target`` option to specify which CPU/Platform to install. If unspecified, ``grub-install`` will make a guess: on **x86/amd64** it will use the **i386-pc** value by default. 

* Now, we have to generate a **grub configuration file** and save it in the **boot directory**:
    # grub-mkconfig -o /boot/grub/grub.cfg

This file will contain the location of the partition we want to boot from. When we are generating it, it is important that the system is either booted or chrooted into (Remember ``/mnt`` became ``/`` when we chrooted).

### Reboot
* Exit the **chroot environment** by typing:

    # exit

(You can also press ``Ctrl+D``.)

* Unmount all our filesystem with:

    # umount -R /mnt

* Properly reboot:

    # reboot

We will be greeted with a TTY and a very basic **login screen**. By the way in Arch linux, the getty(aka login manager) is **agetty**. Also, to swith TTYs, use the **left Alt** key, the right one is not configured to do that.

.. _`VirtualBox`: www.virtualbox.org