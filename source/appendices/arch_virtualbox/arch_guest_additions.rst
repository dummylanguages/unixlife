******************************
Installing the Guest Additions
******************************
Most probably after booting your new Arch Linux virtual machine, the resolution of the TTY doesn't take the full screen. First thing is to install the **VirtualBox Guest Additions**. 

At the moment I just want to install the Guest Additions **without X support**. We do that installing by installing the following package::

    $ pacman -S virtualbox-guest-utils-nox
    :: There are 2 providers available for VIRTUAL-BOX-GUEST-MODULES:
    :: Repository community:
       1) virtualbox-dkms  2) virtualbox-guest-modules-arch

    Enter a number (default 1):

According to the **Archlinux wiki** about Virtual box, since we have the **default kernel**, we'll choose the second option. That will prompt us for confirmation to install 2 packages::

* ``virtualbox-guest-modules-arch-6.0.4-14``
* ``virtualbox-guest-utils-nox-6.0.4-14``

We have to check that the versions of these 2 packages is the same as the version of **VirtualBox** on our host.

.. note::

    Arch offers two versions of the kernel:

    1. The ``linux`` package is thee default **linux kernel**. In Arch gets updated frequently. 
    2. The ``linux-lts`` package is a more stable version. **LTS** stands for **Long Term Support**,  

Framebuffer resolution
======================
After installing the **Virtualbox Guest Additions**, we want to set up an optimal resolution for our virtual consoles(TTYs). This is a 2 step process:

1. We have to configure VirtualBox in our **host**, so it passes this resolution to the **guest OS** (Arch)
2. Set up a framebuffer resolution in Arch.

Set a framebuffer resolution in the host (macos)
------------------------------------------------
Let's start on the **host** side. First of all, let's get the exact name of the virtual machine where we have installed Arch Linux, so just open a **terminal** window in macOS and type::

    $ VBoxManage list vms

This will give us the list of all our **virtual machines**. We need the exact name of the machine we want to configure. Once we have it we'll do::

    $ VBoxManage setextradata "Arch-5.0.2" "CustomVideoMode1" "1920x1080x32"

We have to **reboot** our virtual machine now.

Explanation
^^^^^^^^^^^
I tried these commands in macOS terminal::

    $ VBoxManage setextradata global GUI/MaxGuestResolution any
    $ VBoxManage setextradata "Arch-5.0.2" "CustomVideoMode1" "1920x1080x32"

* To check resolutions I found two ways::

    $ VBoxManage showvminfo 'Arch-5.0.2' | grep 'Video mode'
    Video mode:                  800x600x32 at 0,0 enabled

Which clearly tells me something I don't like. And also::

    $ vboxmanage getextradata "Arch-5.0.2" enumerate
    Key: CustomVideoMode1, Value: 1920x1080x32
    Key: GUI/Fullscreen, Value: true
    Key: GUI/LastCloseAction, Value: PowerOff
    Key: GUI/LastNormalWindowPosition, Value: 640,301,800,621

Which is good news. Problem is, which one is right?

* I read somewhere that I had to change::

    $ vboxmanage setextradata "Arch-5.0.2"/LastGuestSizeHint 1920,1080

But ``vboxmanage getextradata`` wasn't giving me ``LastGuestSizeHint``, only the window position (``LastNormalWindowPosition``)

* Another option would be change resolution while the VM is running::

    $ vboxmanage controlvm "Arch-5.0.2" setvideomodehint 1920 1080 32

It should switch to the new resolution immediately, but guess what, it didn't. I suspect it's because the **guest additions** weren't installed in Arch!

Check the available resolutions in the guest (Arch Linux)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
We can check the modes that our virtual graphic card supports via VBE. We do that with the ``vbeinfo`` command (``videoinfo`` also works), which is available only on the **GRUB shell**. 

Note: it seems that in **grub-efi** the command is ``videoinfo``.

So when your **GRUB boot menu** appears, press ``C`` to open the **GRUB command shell**. I did that and right at the bottom was available the configuration I customized in the host: 1920x1080x32. 

Add a parameter to the kernel
"""""""""""""""""""""""""""""
To do that, we have to add a parameter to the kernel, and there are 3 ways to do that:

1. When building the kernel.
2. When starting the kernel (usually, when invoked from a boot loader).
3. At runtime 

Parameters either have the format ``parameter`` or ``parameter=value``. In this case the parameter we want to pass is ``video=1920x1080``

* We are gonna choose the **2nd** option, and pass the parameter when **GRUB** invokes the kernel. To do that, we'll make sure the following lines are in ``/boot/grub/grub.cfg``::

    GRUB_GFXMODE=1920x1080x32
    GRUB_GFXPAYLOAD_LINUX=keep

**Important note** We shouldn't manually edit this file(``/boot/grub/grub.cfg``). Instead we have to edit ``/etc/default/grub``. Remember that ``/boot/grub/grub.cfg`` has to be re-generated after any change to ``/etc/default/grub`` or files in ``/etc/grub.d/``::

    # grub-mkconfig -o /boot/grub/grub.cfg

 and then regenerate ``grub-mkconfig -o`` to write the changes to ``/boot/grub/grub.cfg``.

* In Debian you use ``update-grub``, but this command doesn't still in Arch.

Load the VirtualBox kernel modules (in the guest, Arch)
-------------------------------------------------------
To load the modules we have 2 options:

1. Do it **automatically**, enable ``vboxservice.service`` which loads the modules and synchronizes the guest's system time with the host::

    # systemctl enable vboxservice.service

This will create a **symlink** from::

    /etc/systemd/system/multi-user.target.wants/vboxservice.service

To::

    /usr/lib/systemd/system/vboxservice.service

2. To load the modules **manually**, type::

    # modprobe -a vboxguest vboxsf vboxvideo

Guest additions with X support
------------------------------
If we have installed a Xorg, or another display manager, we are going to have to install the **VirtualBox Guest Additions** with X support::

    $ sudo pacman -S virtualbox-guest-utils

This package s in conflict with ``virtualbox-guest-utils-nox``, so we'll be prompted to uninstalling before continuing one.

It also replaces the old:

* virtualbox-archlinux-additions
* virtualbox-guest-additions

After the installation we have to do 2 things:

* Load the VirtualBox kernel modules (see above how to do that)
* Launch the VirtualBox guest services. The guest services are actually just a binary executable called VBoxClient which will interact with your X Window System. VBoxClient manages the following features:

    * shared clipboard and drag and drop between the host and the guest;
    * seamless window mode;
    * the guest display is automatically resized according to the size of the guest window;
    * checking the VirtualBox host version

All of these features can be enabled independently with their dedicated flags::

    $ VBoxClient --clipboard --draganddrop --seamless --display --checkhostversion

As a shortcut, the ``VBoxClient-all`` bash script enables all of these features. 

XDG Autostart
-------------
Launching the services will not survive a reboot. ``virtualbox-guest-utils`` installs ``/etc/xdg/autostart/vboxclient.desktop`` that launches ``VBoxClient-all`` on logon. If your **desktop environment** or **window manager** does not support XDG Autostart, you will need to set up autostarting yourself.

Since I don't know if my future **window manager** is gonna launch the **VirtualBox services** or not, I'll just do ir in ``~/.xinitrc`` adding the line::

    VBoxClient-all &
    some_other_program &
    exec urxvt

IMPORTANT: We have to run all processes except the **last** in the background by adding ``&`` to them. The ``exec`` prefix to ``urxvt`` tells the shell that this is the last command, so the shell does not need to wait to run a next command. If you don't send the programs into the background, ``some_other_program`` would only start when VBoxClient-all ends and so on.

Shared folder
-------------
To enjoy having a **shared folder** between the **host** and **Arch linux** we have to take a series of steps:

1. Assuming that we have created a shared folder using VirtualBox in the **host**, we have to include the user ``bob`` in the group ``vboxsf``::

    $ sudo gpasswd -a bob vboxsf

2. We have to check that ``media`` has group ownership ``vboxsf``, currently it's not the default behaviour when ``media`` it's created by the ``virtualbox-guest-utils``::
    
    $ chgrp -R vboxsf /media

3. Now, we can mount the folder::

    $ sudo mount -t vboxsf -o gid=109 macOS_sharing Shared


Where the first ``macOS_sharing`` is the name given to the folder in the **Virtual Box** GUI, and ``~/Shared`` the point in the **guest** where we want to mount it.

IMPORTANT: Without passing as an option(``-o``) the ``gid=109`` of the ``vboxsf`` group(my user is in that group) I could mount the folder, but not copy or open any of the file contents.

NOTE: On the **host** side, the following options were **OFF**::
* Read-only
* Auto-mount
* Mount point: BLANK
* Make Permanent

Automounting: NOT WORKING
^^^^^^^^^^^^^^^^^^^^^^^^^
IMPORTANT: Due to a **bug** in ``virtualbox-guest-utils`` Arch package, **automounting** doesn't work!
BUG: https://bugs.archlinux.org/task/61307

Automounting requires the ``vboxservice.service`` to be enabled/started. We can check that out doing::

    $ systemctl | grep vboxservice

Once we make sure it's running, the folder should be mounted in ``/media/sf_Shared``. That mean that we should be able of seeing the contents of ``/media/sf_Shared`` with::

    $ ls /media/sf_Shared

And I guess even through the **mounting point**::

    $ ls ~/Shared

But, see BUG above.

Mount at boot: NOT WORKING
^^^^^^^^^^^^^^^^^^^^^^^^^^
We can mount our shared folder with ``/etc/fstab``, adding the following line:

    sharedFolderName  /path/to/mntPtOnGuestMachine  vboxsf  uid=user,gid=group,rw,dmode=700,fmode=600,noauto,x-systemd.automount

Where:

* ``sharedFolderName`` is the name we gave it in the Virtual Machine settings.
* ``/path/to/mntPtOnGuestMachine``: where we want to mount the folder. If it doesn't exist, this directory should be created manually.
* ``dmode/fmode`` are the permissions for directories/files inside ``/path/to/mntPtOnGuestMachine``.

In my case this was the line::

    macOS_sharing ~/Shared vboxsf uid=1000, gid=109, rw, dmode=700, fmode=600, noauto, x-systemd.automount

Regarding the permissions set in ``dmode=700`` and ``fmode=600``:

* 600: owner can read and write
* 700: owner can read, write and execute

IMPORTANT: It didn't work either. Same bug?

My remapped key
---------------
At some point down the line, I'm gonna install a window manager, and some of them use the **Command/Windows key** as the modifier key. In **macOS** I had remapped my **Command** keys to **Scroll Lock** using an awesome remapping tool named Karabiner.

In Arch Linux, to find out the **key code** of some key we use the command ``showkey`` (by the way, this command is available in the console, not in the terminal). According to this command, the keycode for the **Scroll Lock** key is **78**.

So I will add the remapping in my ``~/.xinitrc``, so any **window manager** or **desktop environment** gets it right::

    VBoxClient-all &
    xmodmap -e 'keycode 78=Super_R' &
    exec urxvt

I can create a text file with my remappings in some file, ``~.Xmodmap`` for example, with the line::

    keycode 78 = Super_R

and source that file from ``~/.xinitrc``::

    xmodmap .Xmodmap &
