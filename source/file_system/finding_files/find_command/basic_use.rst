*********
Basic use
*********
The `POSIX standard for find`_ includes the following **synopsis** of this command::

    find [-H|-L] path ... [operand_expression...]

.. note:: The ``-H`` and ``-L`` options control how the find command should treat `symbolic links`_. We'll go in depth about **options** in a :ref:`dedicated section <options section>`.

For demonstration purposes, the following examples will be using the following directory tree::

    .
    ├── fileA
    ├── fileB
    └── folder
        ├── another_folder
        │   └── fileZ
        ├── fileC
        └── fileD

You can create it real fast pasting the following lines in your terminal::

    $ mkdir test && cd $_ && mkdir -p folder/another_folder
    $ touch $_/fileZ && touch file{A..B} && touch folder/file{C..D}

Path
====
The only required argument is ``path``, which represents the path to the **file** or **directory** we want too find.

Finding Files
-------------
We can ``find`` a **single file**::

    $ find fileA
    fileA

By default, **printing** is the **default action**, which means that the command above is equivalent to::

    $ find fileA -print
    fileA

If we use several **path** arguments, we can also look for **several files** at the same time::

    $ find fileA fileB
    fileA
    fileB

In the command above we're using two **path** arguments. As usual, a bare filename is gonna be searched in the **current directory**, but we can also look in other folders, for example::

    $ touch /tmp/foo
    $ find /tmp/foo
    /tmp/foo

If the file doesn't exist, we'll get the following output::

    $ find fileC
    find: fileC: No such file or directory

If we want to search for ``fileC`` in another location, for example a subdirectory one level down, we could do::

    $ find */fileC
    folder/fileC

We can also go two levels down::

    $ find */*/fileZ
    folder/another_folder/fileZ

Finding Folders
---------------
Looking for folders is not much different::

    $ find folder
    folder
    folder/another_folder
    folder/another_folder/fileZ
    folder/fileD
    folder/fileC

Note that since the **path** matches a folder, ``find`` will also list the **contents** of the found folder and subfolders **recursively** (later we'll see how to avoid that if needed). 

.. note:: The ``find`` utility **recursively** descends the directory tree for each matched **path**.

Path as a Starting Point
------------------------
Most of the times we'll be specifying a **single path** that will work as the **starting point** of the search. For example, if we want to see the contents of the current directory, we can use a ``.`` (single dot) as the **path** argument::

    $ find .
    [..]

The command above should print to standard output the contents (files and folders) of the **current directory** and all its **subdirectories**, an entry per line; as we mentioned before, ``find`` will **recursively** descend the directory tree for each path listed.

.. note:: In some implementations (like the one of **GNU** but not the one used in **macOS**) the **path** can be omitted, since it defaults to the **current directory** (``.``). In those cases, the command above is equivalent to::

    $ find

Once we have established the **starting point** of the search (usually the current directory) we can add filters to limit the search to what we're really looking for. For example, let's say we're looking for just **files** under the current directory whose names start with ``file`` and then a single character::

    $ find . -type f -name 'file?'
    ./folder/fileD
    ./folder/fileC
    ./folder/another_folder/fileZ
    ./fileB
    ./fileA

The result is an **expression** that allows us to fine tune our search; we're searching in the **current directory** for:

* Files (``-type f``)
* Whose names start with ``file`` and end with a single character (``-name 'file?'``). Yes, you can use `globbing`_.

.. note:: It is possible to specify **several directories** at the same time.

Non-recursive search
--------------------
Note that ``find`` will look **recursively** in all subfolders. We could limit the search to that same level adding another option::

    $ find . -type f -name 'file?' -maxdepth 1
    ./fileB
    ./fileA

.. note:: The ``-maxdepth`` option is a **GNU extension** that won't work in **macOS**. The **BSD** version of that option is ``-depth``.

.. _`POSIX standard for find`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/find.html
.. _`symbolic links`: https://en.wikipedia.org/wiki/Symbolic_link
.. _`globbing`: https://en.wikipedia.org/wiki/Glob_(programming)
