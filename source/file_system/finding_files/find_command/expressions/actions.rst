*******
Actions
*******

Print
-----
We could have written the command above as::

    $ find . -name 'file*' -print

In the example above, the ``-print`` option only adds **verbosity** since, by default, the found files are printed. In other ocasions, using ``-print`` is quite useful to test the output of a command, before using some destructive option such as ``-delete``.

Delete
------
The ``-delete`` option does exactly what its name suggests. For example, let's say we want to **delete** files whose names start with ``~``, and the ones that start or finish with ``*``::

    $ find . \( -name "*~" -o -name "#*" -o -name "*#" \) -delete -print
    ./file~
    ./#file
    ./file#

The filenames in this example were a bit contrived, but we wanted to showcase the use of **double quotes** around the wildcards to escape **shell expansion** (``~`` expands to the home directory and ``*`` to all files in the current directory).

.. note:: It's quite useful to use the ``-print`` option as well in order to get some feedback about the files being deleted.

Executing commands on files
---------------------------
The ``-exec`` option allows us to execute commands on the found files. For example, let's search all files with ``.txt`` extension that contains the word ``hello``::

    $ find . -name '*.txt' -exec grep -li 'hello' {} \;
    file1.txt
    file2.txt

There are several things to notice about the command above:

* The ``{}`` expands to the filename of each file or directory found with ``find``.
* At the end of the command we passed to ``-exec`` there's a ``\;``. The semicolon ``;`` ends the command executed by ``-exec``. It needs to be **escaped** with ``\`` so that the shell does not treat it as a special character, but rather passes it to ``find``.

Also, if we used ``+`` to end the command, ``find`` appends the found files to the end of the command rather than invoking it once per file (so that the command is run only once, if possible). That is useful for example if we needed to concatenate the contents of several files, for example::

    $ find . -name '*.txt' -exec cat {} +
    hello world
    hello world

.. _`POSIX standard for find`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/find.html
.. _`symbolic links`: https://en.wikipedia.org/wiki/Symbolic_link
