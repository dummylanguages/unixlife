*********
Operators
*********
Operators allow us to build complex expressions combining **tests** and **actions**.

AND
===
There are three ways of using the boolean **AND**:

* Using the ``-and`` operator, such as ``expr1 -and expr2``.
* Using the ``-a`` operator, such as ``expr1 -a expr2``.
* Writing two expressions next to each other, such as ``expr1 expr2``.

As usual, only when the two subexpressions are **true** the whole expression is.

OR
==
The ``-o`` option allows us to use the **OR** logical operator on our expression. For example::

    $ find . \( -type d -o -name fileA \)
    .
    ./folder
    ./folder/another_folder
    ./fileA

The command above is searching for directories **or** a file named ``fileA``.

.. warning:: In many shells the **parentheses must be escaped** with a backslash (``\(`` and ``\)``) to prevent them from being interpreted as special shell characters. Also, leave a **space** around the parentheses.

NOT
===
.. _`POSIX standard for find`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/find.html
.. _`symbolic links`: https://en.wikipedia.org/wiki/Symbolic_link
