*****
Tests
*****
These are the *primaries* that we can use to search for files according to different criteria. The most common tests allow us to find files:

* By ``-name``.
* By ``-type``.
* By ``-size``.
* By **modification time** using the ``-mtime`` option.
* By **permissions** using the ``-perm`` option.
* By **owner** or **group** using the ``-user`` or ``-group`` options respectively.

Name
====
The ``--name`` option allows us to find files (and folders) by name. The name doesn't have to be literal, we can also use **wildcards** and operators. For example::

    $ find . -name 'file*'
    ./fileB
    ./folder/another_folder/fileZ
    ./folder/fileD
    ./folder/fileC
    ./fileA

We're trying to match all the files whose filenames start with ``file``, such as ``fileA``, ``fileB``, etc. In order to do that, we write the characters that are common to all these filenames, followed by an ``*``. That's known as a **wildcard**; there are two of them:

* The ``*`` character replaces **any amount of characters** (even **no characters**).
* The ``?`` character replaces a **single character**.

.. warning:: Note that we had to use **quotes** (single or double) to avoid **shell expansion** of the ``*``.

Name (case insensitive)
-----------------------
Just use ``-iname`` instead.


Type
====
What if we're only interested in finding **folders**? Then we could use the ``-type`` option with the ``d`` value. For example::

    $ find . -name 'another*' -type d
    ./folder/another_folder

The following table contains all the types we can use with the ``-type`` option:

+-------+-------------------------------+
| Value |           File type           |
+=======+===============================+
| ``b`` | Block device (buffered)       |
+-------+-------------------------------+
| ``c`` | Character device (unbuffered) |
+-------+-------------------------------+
| ``d`` | Directory                     |
+-------+-------------------------------+
| ``f`` | Regular file                  |
+-------+-------------------------------+
| ``l`` | Symbolic link                 |
+-------+-------------------------------+
| ``p`` | Named pipe                    |
+-------+-------------------------------+
| ``s`` | Socket                        |
+-------+-------------------------------+
| ``D`` | Door                          |
+-------+-------------------------------+

By size
=======
The size can be specified using a number followed by a **suffix** for the unit. The following table summarizes the units:

+--------+--------------------------------------------------------------+
| Suffix |                             Unit                             |
+========+==============================================================+
| ``c``  | For **bytes**                                                |
+--------+--------------------------------------------------------------+
| ``w``  | For 2-byte **words**                                         |
+--------+--------------------------------------------------------------+
| ``b``  | For 512-byte blocks                                          |
+--------+--------------------------------------------------------------+
| ``k``  | For **kibibytes** (1024 bytes)                               |
+--------+--------------------------------------------------------------+
| ``M``  | For **mebibytes** (1024 * 1024 = 1,048,576 bytes)            |
+--------+--------------------------------------------------------------+
| ``G``  | For **gibibytes** (1024 * 1024 * 1024 = 1,073,741,824 bytes) |
+--------+--------------------------------------------------------------+

.. note:: A **kilobyte** is **1000** bytes, a **megabyte** is **1,000,000** bytes, and a **gigabyte** is **1,000,000,000** bytes (all powers of ten).

Size Prefixes
-------------
Since we rarely are going to remember the **exact size** of a file, there are two prefixes that allow us to match files **bigger** (``+``) or **smaller** (``-``) than a certain amount. For example, if we wanted to match a file whose size is bigger than **2 KiB** (2 * 1024) we would run::

    $ find . -size +2k

That would match files (and folders) starting with a size of **2049 bytes**.

.. warning:: When specifying the size using these prefixes, the size is **rounded up** to the next unit. So if were using ``-size -1M`` we'll be matching **empty files** (-1 rounded up is 0). To match a file smaller than **1 MiB** use a smaller unit, such as ``-size -1048576c``; this matches files from 0 to **1,048,575 bytes** (1 byte under 1 MiB).

By time
=======
There are a group of options to find files according to the files timestamps meaning:

* **Access time**, meaning the last time the file was read.
* **Change time**, meaning the last time the file or its attributes were modified.
* **Modify time**, lat time the file’s contents changed.

For example, to look for files and directories that have been modified in the last seven days, use the ``-mtime`` option::

    $ find . -mtime -7


.. _`POSIX standard for find`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/find.html
.. _`symbolic links`: https://en.wikipedia.org/wiki/Symbolic_link
