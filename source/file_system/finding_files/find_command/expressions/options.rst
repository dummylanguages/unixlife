.. _options section:

*******
Options
*******
Options affect the overall operation rather than the processing of a specific file.

Symbolic Links Options
======================
The ``-H`` and ``-L`` options control how the find command should treat `symbolic links`_. The default behaviour is never to follow symbolic links. 

* The ``-H`` flag will only follow symbolic links while processing the command line arguments.
* The ``-L`` flag will cause the ``find`` command to follow symbolic links. 

A common **extension** (non POSIX) is the ``-P`` flag, for **explicitly** disabling symlink following. Again, this option is not required, since the **default** behaviour is to not follow symbolic links.

.. _`symbolic links`: https://en.wikipedia.org/wiki/Symbolic_link
