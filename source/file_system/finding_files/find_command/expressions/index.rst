###########
Expressions
###########

.. toctree::
	:maxdepth: 3
	:hidden:

	options
	tests
	actions
	operators

The ``find`` command becomes really powerful when we start building **expressions**. An expression is built using **primaries**, which can be classified as:

* **Options**, which affect the overal behaviour of the whole expression.
* **Tests**, which return true or false.
* **Operators**, which allow us to use boolean logic to build more sophisticated tests.
* **Actions**, which specify the action to perform on the matched files.

.. _`find`: https://en.wikipedia.org/wiki/Find_(Unix)
.. _`POSIX standard`: https://pubs.opengroup.org/onlinepubs/009695399/utilities/find.html
.. _`Version 5 Unix`: https://en.wikipedia.org/wiki/Research_Unix
.. _`GNU findutils`: https://www.gnu.org/software/findutils/
.. _`fd command`: https://github.com/sharkdp/fd
