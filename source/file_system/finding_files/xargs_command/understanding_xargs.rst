***********************
Understanding ``xargs``
***********************
Basically, ``xargs`` takes the output of a command and passes it as **argument** of a second command.

.. note:: If no second command is specified, xargs just prints the output of the first command.

Let's start by putting some content into ``test.txt``::

  $ printf 'file1\nfile2\n' > test.txt
  $ cat test.txt
  file1
  file2

Now let's pipe the output of ``cat`` into ``xargs``::

  $ cat test.txt | xargs
  file1 file2

It doesn't seem a big deal to get the contents of ``test.txt`` all in the same line, but actually we could use this line as arguments of another command.

Two Ways of taking Input
========================
To better understand the role of ``xargs``, we gotta remember that there are commands that can take their input in two different ways:

* Either through **command line arguments**.
* Or through their **standard input**.

For example, the `grep`_ command needs two pieces of information to do its job:

* A **pattern** to search for.
* A **file** to search for the above mentioned **pattern**.

Let's use the ``test.txt`` file of the last example and search for the line that contains the word ``file2``. We have two options to do so:

1. We can feed ``grep`` the pattern and the file to search for as **command line arguments**; for example:

 .. code-block::

  $ grep '*2' text.txt
  file2

2. And we can also feed the file using a **pipe**; for example:

 .. code-block::

  $ cat test.txt | grep '*2'
  file2

One Way of taking Input
=======================
There are other commands that can't take input through their standard input stream. Let's put some contents into another file::

  $ printf 'hello world' > file1

Let's ``cat`` the contents of ``test.txt``::

  $ cat test.txt
  file1
  file2

If we needed to use this output of some command in order to create a copy of ``file1`` we couldn't run::

  $ cat test.txt | cp
  cp: missing file operand
  Try 'cp --help' for more information.

It wouldn't work because the ``cp`` command can only take information through command line arguments. Using ``xargs`` allows us to pass arguments to the ``cp`` command as if we were feeding it through its standard input. Let's try using ``xargs``::

  $ cat test.txt | xargs cp
  $ cat file2
  hello world

When we ``cat`` the contents of ``test.txt`` into ``xargs``, they're turned into arguments to ``cp``. As you can see, we've made a copy of ``file1`` named ``file2``. You can use the ``-t`` option to check what command we're running::

  $ cat test.txt | xargs cp
  cp file1 file2 

Remember that, by default, ``xargs`` prints to **standard output** its input, so another way of checking what arguments we'll be passing to a particular command would be::

  $ cat test.txt | xargs
  file1 file2

Controlling Point of Insertion
==============================
The ``xargs`` is frequently used together with commands that output lists of files such as ``find``, ``locate``, etc. Let's create some dummy files to play with::

  $ touch file{1..5}.png

.. note:: `Brace expansion`_ works in `Bash`_, `Zsh`_, and `ksh93`_.

Imagine that we want to move all files with the ``.png`` extension to the ``~/Pictures`` folder::

  $ find . -name '*.png' -type f | xargs -I% mv % ~/Pictures

Since the ``mv`` command will move all its arguments to the location passed as last argument, we need a way to insert the files found by ``find`` in between the ``mv`` command and the destination folder, in this case ``~/Pictures``. The ``-I`` option will replace an arbitrary string (``%`` is commonly used) with all the arguments produced by ``xargs``.

Reading from files
==================
The ``xargs`` command can also read items from a **file** instead of **standard input**. To do so, use the ``-a`` option (short for ``--arg-file``) followed by the file name. For example, imagine we have the following file::

  $ cat ip_addresses.txt
  127.0.0.1
  localhost

And we want to ``ping`` all of them, one after another::

  $ xargs -t -L1 -a ip_addresses.txt ping -c 1

The ``-L`` option (short for ``--max-lines``) allows us to specify the maximum number of lines we want to read at a time. Using ``-l`` this number defaults to ``1``, so a numeric argument is not mandatory.

Trimming Whitespace
===================
The ``xargs`` command can also be used to trim whitespace around and in between words. For example::

  $ printf '\thola\t\n\tmundo\n' | xargs
  hola mundo

The result is each word separated by a single space.

Delete Files with Embedded Spaces
=================================
Sometimes, some filenames include spaces; trying to delete them, may be tricky::

  $ touch 'a file' foo bar
  $ find . -type f | xargs rm
  $ ls
  'a file'

In this situation we have to use a ``find`` option named ``-print0``, paired with a ``xargs`` option named ``-0``::

  $ touch 'a file' foo bar
  $ find . -type f -print0 | xargs -0 rm
  $ ls

.. _`xargs`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/xargs.html
.. _`grep`: https://en.wikipedia.org/wiki/Grep
.. _`Brace expansion`: https://www.gnu.org/software/bash/manual/html_node/Brace-Expansion.html
.. _`Bash`: https://www.gnu.org/software/bash/
.. _`Zsh`: https://www.zsh.org/
.. _`ksh93`: http://www.kornshell.com/