#####################
The ``xargs`` command
#####################

.. toctree::
	:maxdepth: 3
	:numbered: 3
	:hidden:

	understanding_xargs

According to `Wikipedia`_, the `xargs`_ command converts input from standard input into **arguments** to a command.

.. note:: ``xargs`` is short for **eXtended arguments** and it's usually pronounced as *eks args*.

Its `man`_ page in the **BSD General Commands Manual** says that ``xargs`` first appeared in `PWB UNIX`_. Nowadays, `this command`_ is part of the `Single UNIX Specification`_ standard although some `BSD`_ flavours and the `GNU version`_ introduce several non-standard extensions.

.. note:: **macOS** includes the **BSD version** of ``xargs``.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Xargs
.. _`xargs`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/xargs.html
.. _`man`: https://en.wikipedia.org/wiki/Man_page
.. _`PWB UNIX`: https://en.wikipedia.org/wiki/PWB/UNIX
.. _`this command`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/xargs.html
.. _`Single UNIX Specification`: https://en.wikipedia.org/wiki/Single_UNIX_Specification
.. _`BSD`: https://en.wikipedia.org/wiki/Berkeley_Software_Distribution
.. _`GNU version`: https://www.gnu.org/software/findutils/manual/html_node/find_html/index.html
