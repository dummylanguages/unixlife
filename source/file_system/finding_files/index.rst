#############
Finding files
#############

.. toctree::
	:maxdepth: 3
	:hidden:

	find_command/index
	xargs_command/index

In Unix-like operating systems, the main tool when it comes to locate files and directories in the filesystem is the ``find`` command. According to Wikipedia, `find`_ is a command-line utility that locates files based on some user-specified criteria and then applies some requested action on each matched object. 

.. note:: The  ``find`` command has been part of Unix since `Version 5 Unix`_ (aka **Research Unix**), back in 1971, and it's part of the `POSIX standard`_.

Currently, there are several implementations of this tool:

* The `GNU findutils`_ package, includes utilities such as ``find``, ``locate``, ``xargs``, etc. This implementation includes a large number of additional features not specified by **POSIX**.
* The **BSD versions** of the ``find`` command, along with the other mentioned tools (``locate``, ``xargs``, etc).

.. note:: The new guy in the block is the `fd command`_, which is a simpler, and user-friendly alternative to ``find``. The price for this simplicity is the lack of support for all the powerful functionality that ``find`` offers.

.. _`find`: https://en.wikipedia.org/wiki/Find_(Unix)
.. _`POSIX standard`: https://pubs.opengroup.org/onlinepubs/009695399/utilities/find.html
.. _`Version 5 Unix`: https://en.wikipedia.org/wiki/Research_Unix
.. _`GNU findutils`: https://www.gnu.org/software/findutils/
.. _`fd command`: https://github.com/sharkdp/fd
