.. _magic file section:

**************
The magic file
**************
In the :ref:`section about the file command <file command>`, we explained how, in order to determine the type of a file, one of the tests performed by the ``file`` command, uses the **magic file** to look for **magic numbers** inside the file being tested. The **magic file** contains information about:

* What is the **position** of the magic number inside the tested file.
* What should be the **value** of this magic number.

.. note:: **Magic numbers** (aka **file signatures** are typically **not visible** using a simple editor, but they can be seen by using a `hex editor`_ or by using the ‘xxd’_ command.

If there is a match, the information contained in the magic file, regarding the type of the tested file will be reported as the output of the ``file`` command.

.. note:: Some files, such as **plain text files**, do **not** have **magic numbers**. In this case, ``find`` identifies them by checking the character sets used inside the file.

.. _magic database location:

Location of the magic database
==============================
As we mentioned in the preceding sections, the magic tests performed by ``file``, consists in checking the contents of the **magic database**. This database may consists of:

* A **compiled magic file**, usually located in ``/usr/share/misc/magic.mgc`` (in **macOS** is located in ``/usr/share/file/magic.mgc``)
* Or the text files in the **directory** located at ``/usr/share/misc/magic`` (in **macOS** in ``/usr/share/file/magic``) if the compiled file does not exist.

In addition, if ``$HOME/.magic.mgc`` or ``$HOME/.magic exists``, it will be used in preference to the system magic files.

Format of a magic file
======================
Here we'll describe the format of the **source fragment files** that are used to build the compiled database. According to `man magic`_  each line of the file specifies a **test** to be performed. A test compares the data starting at a particular **offset** in the file with a **value**. If the test succeeds, a message or MIME type is printed, along with additional information in some cases.

Each line consists of four fields: 

1. The first field is a **byte offset** that indicates the position in the tested file to look for the magic number.
2. The second field is the **type of the value**, which by the way, doesn't have to be a number.
3. The third field is the **value** itself, preceded by an **optional operator**.
4. The rest of the line is the **message string** to be printed if the magic number is found in the tested file.

For example, if we wanted to create a magic file to detect files type **foo**, we could write the following line::

    # Foo files
    41      string      "42"       File containing "42"

The line above contains **four fields**:

1. The first indicates the **offset**: we are saying that the magic number will be offset ``41`` bytes, which means it must be found at the **42nd byte**, that's why we indicate an  of ``41`` bytes.
2. **Type**: Here we're saying that the magic numbers is gonna be a **string**.
3. **Value**: its value is ``"42"``.
4. **Message**: Finally, ``File type 41`` is the message string that the ``file`` command should report when a file of type **foo** is tested and matched.

Offset
------
Most files have the magic numbers in the bytes at the beginning of the file, but some times the number can be offset.

Type
----
There are a lot of **types of data** that can be specified. The following table only contains just the names, so make sure to check the manual for details:

+---------------+------------+----------+------------+---------+----------+
| Types of data |            |          |            |         |          |
+===============+============+==========+============+=========+==========+
| byte          | short      | long     | quad       | float   | double   |
+---------------+------------+----------+------------+---------+----------+
| string        | pstring    | date     | qdate      | ldate   | qldate   |
+---------------+------------+----------+------------+---------+----------+
| beid3         | beshort    | belong   | bequad     | befloat | bedouble |
+---------------+------------+----------+------------+---------+----------+
| bedate        | beqdate    | beqldate | bestring16 | leid3   | leshort  |
+---------------+------------+----------+------------+---------+----------+
| lelong        | lequad     | lefloat  | ledouble   | ledate  | leqdate  |
+---------------+------------+----------+------------+---------+----------+
| leqldate      | lestring16 | melong   | medate     | meldate | indirect |
+---------------+------------+----------+------------+---------+----------+
| regex         | search     | default  |            |         |          |
+---------------+------------+----------+------------+---------+----------+

Value and operators
-------------------
The next field is a **value**, preceded by an **optional operator**.


Message string
--------------

The valid specifiers are listed as follows: 

Lines beginning with a ``>`` or ``&`` character represent **continuation lines** to a preceding **main entry**. For example::

>


Using a custom magic file
=========================
Let's say we've written a customized **magic file** named ``foo``, and we want to use it to test a file named ``test.foo``::

    $ file -m foo test.foo
    test.foo: File containing "42"

Compile a magic file
--------------------
If we're gonna be testing lots of files, it would be a good idea to compile our source file::

    $ file -C -m foo

Now our tests should run faster::
    
    $ file -m foo.mgc test.foo

Using several magic files
-------------------------
The problem now, is that since our compiled magic file doesn't contain all the definitions of the **system's magic file**, those types of files won't be detected by our magic file. The solution to that is to pass several magic files separated by a **semicolon**, for example::

    $ file -i -m foo.mgc:/usr/share/misc/magic.mgc test.*

.. _`hex editor`: https://en.wikipedia.org/wiki/Hex_editor
.. _`xdd command`: https://linux.die.net/man/1/xxd
.. _`man magic`: https://linux.die.net/man/5/magic