#####
Files
#####

.. toctree::
	:maxdepth: 3
	:numbered: 3
	:hidden:

	inodes
	unix_file_types
	magic_numbers
	file_command
	magic_file

From an organizational point of view, a **files** is the smallest unit in which the information is stored. Here we'll continue discussing other topics related to files:

* inodes.
* The file types that exist in Unix.
* How to check for the type of information contained into a file.
* Mime types.
* File extensions.
* File formats.

At a hardware level, the information contained in a file, is stored as a set of **ones** and **zeros**.

.. _`file types`: https://en.wikipedia.org/wiki/Unix_file_types
