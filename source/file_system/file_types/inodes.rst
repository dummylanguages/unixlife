######
inodes
######
In a Unix-style file system, an `inode`_ is  a data structure that describes a **file-system object** such as a **file** or a **directory**. Every **storage device** keeps a table like structure with the numbered list of inodes in a determined location of the device. This location is usually at the beginning of the partition. Every partition has its own inode table.

.. figure:: /_images/file_system/inode_partition.png
	:scale: 70 %
	:align: center

POSIX inode description
=======================
The POSIX standard describes the list of **attributes** stored in an inode:

* **Device ID** (this identifies the device containing the file)
* File serial numbers.
* The **file mode** which determines the **file type** and the file's **permissions**.
* A **link count** telling how many **hard links** point to the inode.
* The **User ID** of the file's **owner**.
* The **Group ID** of the file.
* The **device ID** of the file if it is a device file.
* The **size** of the file in bytes.
* **Timestamps** that give information about:

    * When the inode itself was last modified (``ctime``, which stands for **inode change time**)
    * When the file content last modified (``mtime``, **modification time**)
    * When it was last accessed (``atime``, **access time**)

* The preferred **I/O block size**.
* The **number of blocks** allocated to this file.

inode of a directory
====================
In the case of a **directory**, an inode stores a table that maps file names to their inode numbers.

.. figure:: /_images/file_system/directory_inode.png
	:scale: 70 %
	:align: center

inode of a file
===============
In the case of files, an inode stores:

* The exact disk location that contains the data belonging to the **file**. 
* It also stores **metadata** about the file.

Each inode is identified by an integer number known as **inode number**. 

.. figure:: /_images/file_system/file_inode.png
	:scale: 70 %
	:align: center

You can check the inode number of the files in a directory by running ``ls -i``.

File serial numbers
-------------------
**inodes** are also known as a **file serial numbers**. A file's inode stores both:

* The **serial number** of a file in a given device. This serial number is unique at a partition level, meaning that you can have two files with the same inode number given they are on different partitions.
* The **device ID** of the storage device containing the file.

These two numbers combined, **uniquely** identify the file within the whole system.

File Mode String
----------------
The **mode** of a file can be identified by using the ``ls -l`` command. For example::

    $ ls -l /usr
    drwxr-xr-x 11 root  wheel   352 Sep  7 20:03 usr

`POSIX`_ specifies the format of the output for the **long format** (``-l`` option). In particular, the **first field** (before the first space) is dubbed the **file mode string**. The first character of this string describes the **file type**. The rest of this string indicates the **file permissions**.

Therefore, in the example, the **mode string** is ``drwxr-xr-x``: 

* The **file type** is ``d`` (directory) 
* And the **permissions** are ``rwxr-xr-x``.

Link count: Hard links
======================
In computing, a `hard link`_ is a new filename for the same inode. For example, imagine we have a file named ``filex`` under the current directory. We can easily create a hard link to this file running::

    $ ln fileX hlfileX
    $ ls -i
    4399159 fileX
    4399159 hlfileX

As you can see, both names point to the same **inode number**. Because of that, we can delete the original filename and the data will still be available through the hard link. The data linked to this inode number will remain available until all names associated with it are deleted.

.. note:: A hard link does not create a new file. It only provides a new name for the same data. Deleting a file is basically deleting all the names linked with the inode.

The **second field** of the output of the ``ls -l`` command, is the **link count** of a file or directory::

    $ ls -li
    4399159 -rw-r--r--  2 bob  staff    0 Jan 25 20:00 file
    4399159 -rw-r--r--  2 bob  staff    0 Jan 25 20:00 hlfileX

If you create a **soft link** to a file, that doesn't affect the **link count**::

    $ ln -s fileX slfileX
    $ ls -li
    4399159 -rw-r--r--  2 bob  staff    0 Jan 25 20:00 file
    4399159 -rw-r--r--  2 bob  staff    0 Jan 25 20:00 hlfileX
    4447300 lrwxr-xr-x  1 javi  staff    4 Jan 26 20:27 slfileX -> fileX

.. note:: Note also that a **soft link** has its own inode.

The ``stat()`` system call
==========================
`stat()`_ is a `Unix system call`_ that returns file attributes about an inode. As an example, Unix command ``ls`` uses this system call to retrieve information on files that includes:

* ``atime``: time of last access (``ls -lu``)
* ``mtime``: time of last modification (``ls -l``)
* ``ctime``: time of last status change (``ls -lc``)

.. note:: ``stat`` appeared in `Version 1 Unix`_. It was modified in **Version 4** with the addition of **group permissions** and **larger file size**.

.. _`inode`: https://en.wikipedia.org/wiki/Inode
.. _`POSIX`: https://pubs.opengroup.org/onlinepubs/9699919799/
.. _`hard link`: https://en.wikipedia.org/wiki/Hard_link
.. _`stat()`: https://en.wikipedia.org/wiki/Hard_link
.. _`Unix system call`: https://en.wikipedia.org/wiki/System_call
.. _`Version 1 Unix`: https://en.wikipedia.org/wiki/Research_Unix#Versions
.. _`Version 1 Unix`: https://en.wikipedia.org/wiki/Research_Unix#Versions
