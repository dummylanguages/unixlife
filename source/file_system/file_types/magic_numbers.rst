*************
Magic numbers
*************
In programming, the term `magic number`_ has different meanings:

* Sometimes it refers to the use of `unnamed numerical constants`_ (such as ``3.14``) used in the source code of a program, which is considered as an `anti-pattern`_. A better solution is to replace all significant magic numbers with **named constants** such as ``float PI = 3.14``. That makes programs easier to read, understand and maintain.

* They are also use in **communication protocols**. For example, requests from **unencrypted BitTorrent trackers** begin with a single byte containing the value ``19`` representing the header length, followed immediately by the phrase ``BitTorrent protocol`` (19 characters). Another example is the first four bytes of a block in the Bitcoin Blockchain, which contain a **magic number** which serves as the **network identifier**.

* It may also refer to the use of **hexspeak words** for different purposes such as:

    * `Debug values`_ such as ``B16B00B5`` (*"Big Boobs"*), formerly required by Microsoft's Hyper-V hypervisor to be used by Linux guests as the upper half of their **guest id**.
    * To create or alter `Globally Unique Identifiers (GUIDs)`_ so that they are memorable. For example, Microsoft Windows **product ID** numbers for Microsoft Office products sometimes end with ``0000-0000-0000000FF1CE`` (*"OFFICE"*). Another example is the **GUID Partition Table** of the GPT partitioning scheme, BIOS Boot partitions use the special GUID ``{21686148-6449-6E6F-744E-656564454649}`` wich is ASCII translates as *"Hah!IdontNeedEFI"*. 

 .. note:: `Hexspeak`_ is a novelty form of variant English spelling using the hexadecimal digits.

* **Magic numbers** are also used in many operating systems, especially in Unix and its derivatives, to determine the `format of a file`_.

 .. note:: This last use is the one we'll be talking about in this section.

Magic numbers as file format indicators
=======================================
Regular files can store lots of different type of information: images, audio, video, plain text, etc. At a hardware level, a file is just a bunch of **ones** and **zeros**, that once decoded according to some `file format`_, will reveal its intended message: a beautiful landscape, that old song, or your buy list.

Different operating systems have traditionally taken different approaches to determining a particular file's format:

* Adding a **filename extension** is a popular method used by many operating systems, including Windows, macOS, etc. It consists in adding at the end of a filename a final period followed by some letters, for example, ``.txt`` for text files, ``.jpg`` for `JPEG` images, ``.mp3`` for compressed audio, etc.

* **Internal metadata** is another approach. It consist in including information regarding the **file format** inside the file itself. There are a couple of ways to do this:

    * **File headers** usually store this information at the start of the file, but might be present in other areas too, often at the very end.
    * Using **magic numbers** as `format indicators`_ is common practice across many operating systems, especially in Unix and its derivatives. It consists on storing a number inside the file, usually at the beginning. For example, gif files start with the hexadecimal ``47 49 46 38 39 61`` which in ASCII translates to ``GIF89a``.

.. note:: Once the file format is determined, the operating system will use it to associate it with some application able of interpret its contents.

Wikipedia has an entry with a `list of file signatures`_, aka magic numbers or magic bits.

Shebangs are magic numbers
--------------------------
As you may already know, in Unix, a `shebang line`_ is the first line of a script. This line always starts with the prefix ``#!`` followed with the path to the program that must interpret the script. This prefix is actually a human-readable instance of the **magic number** ``0x23 0x21``, which is the hexadecimal represention of the characters ``#!``, encoded in ASCII.

.. note:: If you open any **script** with a shebang in a **hexadecimal editor**, you should see that the first four digits are ``2321``.

When we run a script, an `exec`_ system call is issued, and if the magic numbers ``2321`` are there, then the rest is handled by the `loader`_ which parses the rest of the shebang line as an `interpreter directive`_.



.. _`magic number`: https://en.wikipedia.org/wiki/Magic_number_(programming)
.. _`unnamed numerical constants`: https://en.wikipedia.org/wiki/Magic_number_(programming)#Unnamed_numerical_constants
.. _`anti-pattern`: https://en.wikipedia.org/wiki/Anti-pattern
.. _`Debug values`: https://en.wikipedia.org/wiki/Magic_number_(programming)#Magic_debug_values
.. _`Globally Unique Identifiers (GUIDs)`: https://en.wikipedia.org/wiki/Globally_unique_identifier
.. _`Hexspeak`: https://en.wikipedia.org/wiki/Hexspeak
.. _`format of a file`: https://en.wikipedia.org/wiki/File_format
.. _`file format`: https://en.wikipedia.org/wiki/File_format
.. _`format indicators`: https://en.wikipedia.org/wiki/Magic_number_(programming)#Format_indicators
.. _`list of file signatures`: https://en.wikipedia.org/wiki/List_of_file_signatures
.. _`shebang line`: https://en.wikipedia.org/wiki/Shebang_(Unix)#Magic_number
.. _`exec`: https://en.wikipedia.org/wiki/Exec_(system_call)
.. _`loader`: https://en.wikipedia.org/wiki/Loader_(computing)
.. _`interpreter directive`: https://en.wikipedia.org/wiki/Interpreter_directive


