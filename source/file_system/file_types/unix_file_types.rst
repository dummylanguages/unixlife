.. _unix file types section:

***************
Unix File Types
***************
The seven standard `Unix file types`_ required by POSIX are:

* Regular files
* Directories
* Symbolic links, aka **symlinks**.
* FIFO special, aka **named pipe**.
* Block special
* Character special
* POSIX sockets.

.. note:: The original Unix file system supported three types of files: **ordinary files**, **directories**, and **special files**, also termed `device files`_. Then, the `Berkeley Software Distribution (BSD)`_ added a file type to be used for interprocess communication known as `Berkeley sockets`_. Later, `System V`_ added another file type for the same purpose named `FIFO files`_.

 **BSD** also added **symbolic links** to the range of file types. Symlinks were modeled after a similar feature in `Multics`_.
 
Different Unix operating systems may include additional types, such as `Doors`_ in `Solaris`_.

Regular files
=============


Directories
===========
Unix directories do not contain files, they are just tables that map **filenames** to **inodes**. 

.. note:: A **directory** is also a file, only special. In Unix `everything is a file`_.

A directory is marked with a ``d`` as the first letter of their **file mode string** (the first field of the output of ``ls -l``)

Symbolic links
==============
, and differ from hard links in that they may span filesystems and that their existence is independent of the target object.
When you create a soft link, you create a new file. In its metadata, it points to the target. For every soft link you create, you use one inode.

Device files: block devices and character devices
=================================================
In Unix, almost all things are handled as files and have a location in the file system, even hardware devices like hard drives.

* **Character devices** provide only a serial stream of input or accept a serial stream of output
* **Block devices** are randomly accessible


.. _`Unix file types`: https://en.wikipedia.org/wiki/Unix_file_types
.. _`Multics`: https://en.wikipedia.org/wiki/Multics
.. _`device files`: https://en.wikipedia.org/wiki/Device_file
.. _`Berkeley Software Distribution (BSD)`: https://en.wikipedia.org/wiki/Berkeley_Software_Distribution
.. _`Berkeley sockets`: https://en.wikipedia.org/wiki/Berkeley_sockets
.. _`System V`: https://en.wikipedia.org/wiki/UNIX_System_V
.. _`FIFO files`: https://en.wikipedia.org/wiki/Named_pipe
.. _`Doors`: https://en.wikipedia.org/wiki/Doors_(computing)
.. _`Solaris`: https://en.wikipedia.org/wiki/Solaris_(operating_system)
.. _`POSIX`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/ls.html
.. _`everything is a file`: https://en.wikipedia.org/wiki/Everything_is_a_file
.. _`JPEG`: https://en.wikipedia.org/wiki/JPEG
.. _`Magic numbers`: https://en.wikipedia.org/wiki/File_format#Magic_number

