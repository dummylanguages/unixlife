.. _file command:

****************
``file`` command
****************
The original version of file originated in `Unix Research Version 4`_ in 1973. Most major BSD and Linux distributions use a free, open source reimplementation which was written in 1986-87 by Ian Darwin from scratch. This new open source implementation has a home page `fine free file command`_.

POSIX Tests Description
=======================
`POSIX`_ describes the series of steps that the ``file`` utility takes in order to determine the type of a file:

1. If the file cannot be read, or its Unix file type is **undetermined**, the output should indicate that.
2. ``file`` must be able to determine all **Unix file types**.
3. **Empty files** must be determined as such.
4. ``file`` performs what the standard calls **position-sensitive tests** on the initial part of the file.
5. Next come the **context-sensitive tests**, in which the entire file is considered.
6. If at the end of the tests above the type of the file hasn't been determined, the file is identified as a **data file**.

The relevang part for this section is **point 4**. In most implementations, the ``file`` command compare the lead bytes of the file against the **magic database** which location we'll disclose a bit later.

.. note:: As we'll see, this location may be a bit different depending on the system.

``file`` command Tests Description
==================================
The **man page** of ``file`` available in **macOS**, describes the **version 5.04** of the ``file`` command. It mentions three set of tests performed in the following order:

* Filesystem tests.
* Magic tests.
* Language tests.

The first test that succeeds causes the **file type** to be printed. The type may be:

* ``text``, if the file contains only printing characters and a few common control characters and is probably safe to read on an ASCII terminal.
* ``executable``, if the file contains the result of compiling a program in a form understandable to some UNIX kernel or another.
* ``data`` meaning anything else (data is usually **binary** or non-printable.

Filesystem tests
----------------
They're based on examining the return from a `stat()`_ system call. These tests evaluate if the file is **emtpy** or if it's one of the **Unix file types** (check :ref:`this section <unix file types section>` to see which are those).

Magic tests
-----------
These are tests to check files with data in particular fixed formats. They consist on comparing a **magic number** stored inside the file, in a particular place near the beginning of the file. If a match for the magic number is found in the **magic database**.

.. note:: Check the :ref:`section about the magic database location <magic database location>` to know about where to find it.

Language tests
--------------
If a file does not match any of the entries in the **magic file**, another group of tests will be performed to determine if the file should be reported as:

* A **text file**
* Just **data**.

Text files
^^^^^^^^^^
The first group of tests look for different ranges and sequences of bytes that constitute printable text. If a file passes any of these tests, its **character set** is reported as:

* **Text file** if it uses one of the following character sets:

    * ASCII
    * ISO-8859-x 
    * UTF-8
    * extended-ASCII

 These character sets are considered as **text files** because they will be mostly readable on nearly any terminal.

* But if the character set used is UTF-16 or EBCDIC, it will be classified as **character data**, becasue it is text that will require translation before it can be read. 

In addition, another group of tests, will attempt to determine other characteristics of text-type files.  If the lines of a file are terminated by ``CR``, ``CRLF``, or ``NEL``, instead of the Unix-standard ``LF``, this will be reported. Files that contain embedded **escape sequences** or **overstriking** will also be identified.

Once ``file`` has determined the character set used in a **text-type** file, it will attempt to determine in what **language** the file is written.  The language tests look for particular strings that can appear anywhere in the first few blocks of a file. For example, the keyword ``.br`` indicates that the file is most likely a `troff`_ input file, just as the keyword ``struct`` indicates a **C program**.  These tests are less reliable than the previous two groups, so they are performed last.  The language test routines also test for some miscellany (such as tar(1) archives, JSON files).

Data
^^^^
Any file that cannot be identified as having been written in any of the character sets listed above is simply said to be **data**.

Options
=======



.. _`Unix Research Version 4`: https://en.wikipedia.org/wiki/Unix_Research_Version_4
.. _`fine free file command`: http://darwinsys.com/file/
.. _`POSIX`: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/file.html
.. _`stat()`: https://en.wikipedia.org/wiki/Hard_link
.. _`troff`: https://en.wikipedia.org/wiki/Troff
