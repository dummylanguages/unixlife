**************
Unix standards
**************
Nowadays, the term **Unix like** operating systems it is used to refer to both proper Unix but also Linux systems and even macOS. In a broad sense, any OS that behaves in a manner similar to a Unix system is usually thrown in that category; it doesn't even have to conform to, or being certified by any of the UNIX standards.

As of today, there are a couple of documents that pop out when referring to Unix standardization:

* `POSIX`_.
* The `Single UNIX Specification`_, known as the `The UNIX® Standard`_ or **"The Standard"**.

Although today the situation is clear and stable, back in the day, during the late 1980s and early 1990s different vendors of the Unix computer operating system were strugling to set the standard. This period is known as the `Unix wars`_.

The Unix wars
=============
In **1987** `AT&T Corporation`_ (the company where UNIX was created) started working together with `Sun Microsystems`_ on a unified UNIX system that would be promoted as the UNIX standard. This system was eventually released as `UNIX System V Release 4 (SVR4)`_.

.. note:: Shortly after the release of this system, **AT&T** sold all its rights to UNIX to Novell. 

Other UNIX vendors feared that this initiative would locked them out of the standardization process, so in **1988** the `Digital Equipment Corporation`_ hosted a by-invitation-only meeting for several UNIX system vendors (called the **Hamilton Group**, since the meeting was held at DEC's offices on Palo Alto's Hamilton Avenue). After the meeting, some members extended the idea of a joint development effort to **Sun** and **AT&T**, but both companies rejected it.

During that meeting, it was proposed the creation of a not-for-profit organization to create an open standard for an implementation of the UNIX operating system. That same year, the `Open Software Foundation`_ was founded and sponsored by just 7 companies, but later more than a hundred joined. The co-founder of Sun remarked that OSF really stood for *"Oppose Sun Forever"*. 

.. note:: The **Open Software Foundation** is **not** related to the `Free Software Foundation`_.

Later that same year, **AT&T** created an association named `Unix International`_ to promote open standards, especially the Unix operating system. Its most notable members were **AT&T** and **Sun Microsystems**, and in fact the commonly accepted reason for its existence was as a counterbalance to the **Open Software Foundation** (OSF).

**UI** and **OSF** thus represented the two sides of the Unix wars in the late 1980s and early 1990s. During the wars, the **X/Open Company** tried to hold a middle ground.

By 1993, it had become clear that the greater threat to UNIX system vendors was not each other as much as the increasing presence of **Microsoft** in enterprise computing, which led to the major members of both UI and OSF announced the `Common Open Software Environment`_. The **COSE** initiative was not created in opposition to another set of Unix vendors, it was oriented towards creating  standards of existing interfaces already in use by Unix software vendors of the time. This resulting list, originally known as **Spec 1170**, evolved to become what is now known as the Single Unix Specification.

The X/Open Company
==================
The `X/Open Company, Ltd.`_ was a consortium founded by several European manufacturers in **1984** to identify and promote open standards in the field of information technology, and more specifically, to define a single specification for operating systems derived from UNIX, to increase the interoperability of applications and reduce the cost of porting software.

The group published its specifications under the name X/Open Portability Guide (or XPG):

* **Issue 1** was published in 1985, and covered basic operating system interfaces.
* **Issue 2** was published in 1987, extended the coverage to include Internationalization, Terminal Interfaces, and the programming languages C, COBOL, FORTRAN, and Pascal, as well as data access interfaces for SQL, etc.
* **XPG3** followed in 1988, its primary focus being convergence with the POSIX operating system specifications. This was probably the most widely used and influential deliverable of the X/Open organisation.

In **1993**, `Novell`_, the company that owned the rights to `UNIX®`_ at the time, transferred the trademarks of Unix to the **X/Open Company**. X/Open managed the **UNIX trademark** from 1993 to 1996, when it merged with the `Open Software Foundation`_ to form `The Open Group`_.


The Austin Group
================
The `Austin Common Standards Revision Group`_, more commonly known as the `Austin Group`_, is a joint technical working group formed to develop and maintain a common revision of POSIX.1 and parts of the Single UNIX Specification.

The group currently has approximately 500 participants and the resulting standard is published by three entities:

* The `ISO/IEC (Joint Technical Committee 1, subcommittee 22)`_
* The  IEEE PASC (Portable Applications Standards Committee)
* `The Open Group`_

POSIX
=====
The **Portable Operating System Interface** (`POSIX`_) is a family of standards specified by the `IEEE Computer Society`_ for maintaining compatibility between `Unix-like`_ operating systems. There are several POSIX specifications for Unix-like operating systems:

* POSIX.1: Core Services (IEEE Std 1003.1-1988)
* POSIX.1b: Real-time extensions (IEEE Std 1003.1b-1993)
* POSIX.1c: Threads extensions (IEEE Std 1003.1c-1995)
* POSIX.2: Shell and Utilities (IEEE Std 1003.2-1992)

.. note:: The family of POSIX standards is formally designated as **IEEE 1003** and the international standard name is **ISO/IEC 9945**. 

After **1997**, the Austin Group became in charge of developing and maintaining the specifications under the name of `Single UNIX Specification`_. Once a SUS is approved, it becomes a POSIX standard when formally approved by the ISO. 

* POSIX.1-2001 (IEEE Std 1003.1-2001)
* POSIX.1-2004 (IEEE Std 1003.1-2004)
* POSIX.1-2008 (IEEE Std 1003.1-2008, 2016 Edition)

As of today, *December 26th 2019*, the **current standard** is `POSIX.1-2017`_ which is simultaneously:

* The `IEEE Std 1003.1-2017`_
* And the `Single UNIX® Specification, Version 4, 2018 Edition`_ 

Single UNIX Specification
=========================
The `Single UNIX Specification`_ (SUS for short) is the collective name of a family of standards required by operating system to qualify for using the **UNIX** trademark. The core specifications of the **SUS** are developed and maintained by the **Austin Group**.

Spec 1170
---------
In the early **1990s**, a separate effort known as the **Common API Specification** or **Spec 1170** was initiated by several major vendors, who formed the `COSE`_ alliance in the wake of the Unix wars. This specification became more popular because it was available at no cost, whereas the IEEE charged a substantial fee for access to the POSIX specification.

Management over these specifications was assigned to **X/Open** who also received the Unix trademark from **Novell** in 1993. `Unix International`_ merged into Open Software Foundation (OSF) in 1994 only to merge with X/Open to form The Open Group in 1996. 

The UNIX Standard
=================
According to the official site of `The UNIX® Standard`_, its latest version, **UNIX V7**, aligns with the **Single UNIX Specification Version 4, 2018 Edition**. This is an open standard owned and managed by `The Open Group`_, which owns the **UNIX® Trademark**.

According to this site, only the **core volumes** of this standard are simultaneously:

* The IEEE Portable Operating System Interface (POSIX) standard.
* And the ISO/IEC 9945 standard. 

Which implies that the **Single UNIX Specification** (The UNIX® Standard), is a superset of **POSIX**, and obviously the **ISO/IEC 9945 standard**.
  

.. _`AT&T Corporation`: https://en.wikipedia.org/wiki/AT%26T_Corporation
.. _`Sun Microsystems`: https://en.wikipedia.org/wiki/Sun_Microsystems
.. _`UNIX System V Release 4 (SVR4)`: https://en.wikipedia.org/wiki/UNIX_System_V#SVR4
.. _`Digital Equipment Corporation`: https://en.wikipedia.org/wiki/Digital_Equipment_Corporation
.. _`Open Software Foundation`: https://en.wikipedia.org/wiki/Open_Software_Foundation
.. _`Free Software Foundation`: https://en.wikipedia.org/wiki/Free_Software_Foundation
.. _`Unix International`: https://en.wikipedia.org/wiki/Unix_International
.. _`Single UNIX Specification`: https://en.wikipedia.org/wiki/Single_UNIX_Specification
.. _`POSIX`: https://en.wikipedia.org/wiki/POSIX
.. _`POSIX.1-2017`: https://standards.ieee.org/standard/1003_1-2017.html
.. _`Unix wars`: https://en.wikipedia.org/wiki/Unix_wars
.. _`IEEE Computer Society`: https://en.wikipedia.org/wiki/IEEE_Computer_Society
.. _`Unix-like`: https://en.wikipedia.org/wiki/Unix-like
.. _`The UNIX® Standard`: https://www.opengroup.org/membership/forums/platform/unix
.. _`The Open Group`: https://www.opengroup.org
.. _`X/Open Company, Ltd.`: https://en.wikipedia.org/wiki/X/Open
.. _`Novell`: https://en.wikipedia.org/wiki/Novell
.. _`UNIX®`: https://en.wikipedia.org/wiki/Unix
.. _`Austin Group`: https://en.wikipedia.org/wiki/Austin_Group
.. _`Austin Common Standards Revision Group`: https://www.opengroup.org/austin/
.. _`ISO/IEC (Joint Technical Committee 1, subcommittee 22)`: https://en.wikipedia.org/wiki/ISO/IEC_JTC_1/SC_22
.. _`IEEE Std 1003.1-2017`: https://standards.ieee.org/standard/1003_1-2017.html
.. _`Single UNIX® Specification, Version 4, 2018 Edition`: https://publications.opengroup.org/t101
.. _`Common Open Software Environment`: https://en.wikipedia.org/wiki/Common_Open_Software_Environment
.. _`COSE`: https://en.wikipedia.org/wiki/Common_Open_Software_Environment