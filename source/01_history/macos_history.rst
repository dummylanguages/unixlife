##########################
macOS: UNIX under the hood
##########################
One of the reasons why a lot of software developers use a **Mac** computer as a preferred tool, is the fact that under the hood, macOS packs a powerful and open-source UNIX operating system (mostly compliant with the `Single UNIX Specification`_) named `Darwin`_.

.. image:: /_images/macos/hexley.png

In this section we'll go about the history of this operating system.

******************************
macOS: the evolution of a name
******************************
`macOS`_ is the name of the **operating system** for the `Mac computers`_ designed, manufactured, and sold by `Apple`_. The **name** of this operating system has undergone a series of changes along the years.

The Classic Mac OS era
======================
The `Classic Mac OS`_ period begun in **1984** when Apple released its first personal computer, the original `Macintosh`_. This computer used to run an operating system known as `System 1`_. After System 1 came **Systems 2, 3, 4**.

**System 5**: Towards the end of **1987**, Apple introduced a package titled "Apple Macintosh System Software Update 5.0". For the first time, the Macintosh operating system was offered as a distinct retail product that included four 800K disks and three manuals, at a cost of $49 USD.

`System 6`_ also known as **System Software 6** was released in 1988, and it's worth mentioning because of some unstability problems.

`System 7`_ codenamed **Big Bang** was released in **1991**. With the release of version **7.6** in 1997, Apple officially renamed the operating system **Mac OS**, a name which had first appeared on **System 7.5.1**'s boot screen. With Mac OS 7.6 Apple dropped the **System** moniker as a more trademarkable name was needed in order to license the OS to the growing market of third-party Macintosh clone manufacturers.

`Mac OS 8`_ was released on **1997**, almost 6 years after the first version of System 7. It was one of Apple's most commercially successful software releases. As it came at a difficult time in Apple's history, even pirate groups refused to traffic in the new OS, encouraging people to buy it instead. That same year Steve Jobs came back to Apple the company he had cofounded.

`Mac OS 9`_ was the ninth and final major release of Apple's classic Mac OS operating system. The **8.0** version was released in **1997**. The **8.1** version was the last version of the Mac OS to run on `Motorola 68000`_ series processors, starting a transition to `PowerPC`_ processors. Mac OS **8.5** was the first version of the Mac OS to run solely on `Macs`_ equipped with a PowerPC processor.

Mac OSX
=======
After the `Mac OS X Public Beta`_ released in **2000**, `Mac OS X 10.0`_ (code named Cheetah) was the first publicly available version of Apple's **Mac OS X** operating system. It was was a radical departure from the classic Mac OS and was Apple’s long-awaited answer for a next generation `Macintosh operating systems`_.

In Mac OS X Apple introduced the new `Darwin`_ core, an open-source Unix-like operating system. It also was the first one to feature the new `Aqua user interface`_. It supported both the `Carbon API`_ and the `Cocoa API`_

Parts of the OS
---------------
If we look closely at Apple's OS X operating system we can tell apart several components:

    * The `XNU`_ kernel
    * The great Aqua Graphic User Interface
    * A command line interface.

About the APIs
--------------
The `Carbon API`_ was introduced in incomplete form in **2000**, with backward-compatibility with 1997's **Mac OS 8.1**. In **2012**, with the release of **OS X 10.8 Mountain Lion**, most Carbon APIs were considered deprecated, so the `Cocoa API`_ became the only supported API.

macOS
=====
In **2016**, with the release of `macOS 10.12 Sierra`_, the name was changed from **OS X** to **macOS** to streamline it with the branding of Apple's other primary operating systems: iOS, watchOS, and tvOS.

At macOS's core is a POSIX compliant operating system built on top of the XNU kernel, with standard Unix facilities available from the command line interface. Apple has released this family of software as a free and open source operating system named **Darwin**. On top of Darwin, Apple layered a number of components, including the **Aqua interface** and the Finder, to complete the GUI-based operating system which is **macOS**.

.. _`XNU`: https://en.wikipedia.org/wiki/XNU
.. _`Single UNIX Specification`: https://en.wikipedia.org/wiki/Single_UNIX_Specification
.. _`Darwin`: https://en.wikipedia.org/wiki/Darwin_(operating_system)


.. _`macOS`: https://en.wikipedia.org/wiki/MacOS
.. _`Mac computers`: https://www.apple.com/mac/
.. _`Classic Mac OS`: https://en.wikipedia.org/wiki/Classic_Mac_OS
.. _`Apple`: https://en.wikipedia.org/wiki/Apple_Inc.
.. _`Macintosh`: https://en.wikipedia.org/wiki/Macintosh
.. _`Macintosh 128K`: https://en.wikipedia.org/wiki/Macintosh_128K
.. _`Macintosh 512K`: https://en.wikipedia.org/wiki/Macintosh_512K
.. _`Macintosh Plus`: https://en.wikipedia.org/wiki/Macintosh_Plus

.. _`System 1`: https://en.wikipedia.org/wiki/System_1
.. _`System 6`: https://en.wikipedia.org/wiki/System_6
.. _`System 7`: https://en.wikipedia.org/wiki/System_7

.. _`Mac OS 8`: https://en.wikipedia.org/wiki/Mac_OS_8
.. _`Mac OS 9`: https://en.wikipedia.org/wiki/Mac_OS_9
.. _`Steve Jobs`: https://en.wikipedia.org/wiki/Steve_Jobs
.. _`Motorola 68000`: https://en.wikipedia.org/wiki/Motorola_68000_series
.. _`PowerPC`: https://en.wikipedia.org/wiki/PowerPC
.. _`Macs`: https://en.wikipedia.org/wiki/Macintosh
.. _`Mac OS X Public Beta`: https://en.wikipedia.org/wiki/Mac_OS_X_Public_Beta
.. _`Mac OS X 10.0`: https://en.wikipedia.org/wiki/Mac_OS_X_10.0
.. _`Aqua user interface`: https://en.wikipedia.org/wiki/Aqua_(user_interface)
.. _`Macintosh operating systems`: https://en.wikipedia.org/wiki/Macintosh_operating_systems
.. _`Darwin`: https://en.wikipedia.org/wiki/Darwin_(operating_system)
.. _`macOS 10.12 Sierra`: https://en.wikipedia.org/wiki/MacOS_Sierra
.. _`Carbon API`: https://en.wikipedia.org/wiki/Carbon_(API)
.. _`Cocoa API`: https://en.wikipedia.org/wiki/Cocoa_(API)
