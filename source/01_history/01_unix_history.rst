*********************
A bit of Unix history
*********************
The development of the first UNIX operating system started in the 1970's at the `Bell labs`_ by `Ken Thompson`_ and `Dennis Ritchie`_ among others. Originally it was a proprietary system intended for use to be a convenient platform for programmers developing software, rather than for non-programmers.

.. note:: Wikipedia has an interesting page about the `history of Unix`_.

In the late 1970's `AT&T`_ started licensing the use of Unix to other companies and the `University of California, Berkeley`_. This is important because this University started studying and developing the original Unix source code and created their own software distribution named `BSD`_. The permissive nature of the `BSD license`_ has allowed many other operating systems, both open-source and proprietary, to incorporate BSD source code.

Several **open-source** operating systems are based on BSD, including:

    * `FreeBSD`_
    * `OpenBSD`_
    * `NetBSD`_
    * `DragonFly BSD`_

Also some **proprietary** operating systems based their code in BSD Unix, among them is worth mentioning `Darwin`_, wich in 2000 became the core of `macOS`_.


.. _`history of Unix`: https://en.wikipedia.org/wiki/Unix
.. _`Bell labs`: https://en.wikipedia.org/wiki/Bell_Labs
.. _`Ken Thompson`: https://en.wikipedia.org/wiki/Ken_Thompson
.. _`Dennis Ritchie`: https://en.wikipedia.org/wiki/Dennis_Ritchie
.. _`AT&T`: https://en.wikipedia.org/wiki/AT%26T
.. _`University of California, Berkeley`: https://en.wikipedia.org/wiki/University_of_California,_Berkeley
.. _`BSD`: https://en.wikipedia.org/wiki/Berkeley_Software_Distribution
.. _`BSD license`: https://en.wikipedia.org/wiki/BSD_licenses
.. _`FreeBSD`: https://en.wikipedia.org/wiki/FreeBSD
.. _`OpenBSD`: https://en.wikipedia.org/wiki/FreeBSD
.. _`NetBSD`: https://en.wikipedia.org/wiki/NetBSD
.. _`DragonFly BSD`: https://en.wikipedia.org/wiki/DragonFly_BSD
.. _`Darwin`: https://en.wikipedia.org/wiki/Darwin_(operating_system)
.. _`macOS`: https://en.wikipedia.org/wiki/MacOS
