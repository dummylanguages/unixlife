*****************************
The Linux Directory Structure
*****************************
The `directory structure`_ and directory contents in Linux distributions is defined by the `Filesystem Hierarchy Standard`_

/: The root directory

/root: The home folder for the root user.

/bin: Where Linux core commands reside like ls, mv.

/boot: Where boot loader and boot files are located.

/dev: Where all physical drives are mounted like USBs DVDs.

/etc: Contains configurations for the installed packages.

/home: Where every user will have a personal folder to put his folders with his name like /home/likegeeks.

/lib: Where the libraries of the installed packages located since libraries shared among all packages unlike windows, you may find duplicates in different folders.

/media: Here is the external devices like DVDs and USB sticks that are mounted and you can access their files from here.

/mnt: Where you mount other things Network locations and some distros you may find your mounted USB or DVD.

/opt: Some optional packages are located here and this is managed by the package manager.

/proc: Because everything on Linux is a file, this folder for processes running on the system, and you can access them and see much info about the current processes.

/sbin: Like /bin, but binaries here are for root user only.

/tmp: Contains the temporary files.

/usr: Where the utilities and files shared between users on Linux.

/var: Contains system logs and other variable data.


https://en.wikipedia.org/wiki/Path_(computing)

.. _`directory structure`: https://en.wikipedia.org/wiki/Directory_structure
.. _`Filesystem Hierarchy Standard`: https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard
