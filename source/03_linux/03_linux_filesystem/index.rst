####################
The Linux Filesystem
####################
Here I'll keep notes about the linux fs

.. toctree::
	:maxdepth: 3

	01_linux_directory_structure

From `Wikipedia`_:

	In computing, a **file system** or **filesystem** controls how data is stored and retrieved. Without a file system, information placed in a storage medium would be one large body of data with no way to tell where one piece of information stops and the next begins.

We can talk about a filesystem at different levels, for example:

* At a **low level**, a filesystem may refer to a Disk file system such as FAT, exFAT, ext...
* At a **high level**, the filesystem refers to the `directory structure`_ followed by a given operating system when organizing files and directories.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/File_system
.. _`directory structure`: https://en.wikipedia.org/wiki/Directory_structure
