###############
freedesktop.org
###############
`freedesktop.org`_ is a project focused in providing standards for the interoperability between **desktop environments** for Unix and Linux systems. They do not produce themselves any desktop environment, but host a lot of projects related to them. Some of the `specifications`_ they release are quite useful, for example:

* `XDG Base Directory Specification`_, defines several base directories for configuration files and other files.
* `Desktop Entry Specification`_ defines a standard for configuration files (aka ``.desktop`` files) that describe how a particular program is to be launched, how it appears in menus, etc.
* `Desktop Menu Specification`_ defines defines how to construct application menus.
* `Autostart`_, which defines how applications can be started automatically after the user has logged in, and how removable media can request a specific application to be executed or a specific file on the media to be opened after the media has been mounted.

.. toctree::
    :maxdepth: 3
    :hidden:

    desktop_entry

.. _`freedesktop.org`: https://www.freedesktop.org/wiki/
.. _`specifications`: https://www.freedesktop.org/wiki/Specifications/
.. _`XDG Base Directory Specification`: https://specifications.freedesktop.org/basedir-spec/latest/
.. _`Desktop Entry Specification`: https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html
.. _`Desktop Menu Specification`: https://specifications.freedesktop.org/menu-spec/menu-spec-latest.html
.. _`Autostart`: https://specifications.freedesktop.org/autostart-spec/autostart-spec-latest.html