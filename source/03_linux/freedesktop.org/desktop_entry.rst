###########################
Desktop Entry Specification
###########################
The `XDG Desktop Entry Specification`_ defines a standard for configuration files (aka ``.desktop`` files) that describe how a particular program is to be launched, how it appears in menus, etc.

Application entries (aka ``.desktop`` files)
============================================
In order for applications to integrate with the menus of the desktop environment, they must have a metadata file that defines the appearance of the app in the launching menu, where is the executable file, the category of apps where it fits, etc. These metadata files must finish with the ``.desktop`` extension and usually reside in a couple of locations:

* For applications installed **system-wide** they are under: 

    * ``/usr/share/applications/`` or
    * ``/usr/local/share/applications/``.
    
* For **user-specific** applications they go under the ``~/.local/share/applications/`` folder. User entries take precedence over system entries. 

A ``.desktop`` file example
---------------------------
Nothing like an example to see what a ``.desktop`` file is about. Let's create an empty file under the user-specific directory and save it with the name ``firefox-dev``. Let's write the following inside::

    [Desktop Entry]

    # This is a comment; they're allowed ;-)
    Version=1.0
    Type=Application
    Name=Firefox Developer Edition
    Comment=Firefox Developer Edition
    Path=/opt/firefox-dev
    Exec=firefox %u
    Icon=/opt/firefox-dev/browser/chrome/icons/default/default64.png
    Terminal=false
    Categories=Internet;Browser;

At the top of the file we have a **group header** named ``[Desktop Entry]``. Now let's explain the meaning of the **keys** we've used:

* ``Version`` specifies the version of the **desktop entry specification** not the version of the application itself.
* The ``Type`` key may have three values:
* ``Name`` is the name of the app, and ``Comment`` just a description.
* ``Path`` specifies the path to the folder where the executable is located.
* ``Exec`` is the name of the executable itself; it can have **arguments**.
* Under ``Icon`` we've specified the absolute path to the icon.
* ``Terminal`` describes whether this application needs to be run in a terminal or not.
* ``Categories`` describes the category in the applications menu in which this entry should be shown.

Validation
----------
As some keys have become deprecated over time, you may want to validate your desktop entries using the ``desktop-file-validate`` utility, which is part of the **desktop-file-utils package**. To validate, run:

    $ desktop-file-validate firefox-dev.desktop

Installation
------------
Use ``desktop-file-install`` to install desktop file into a **target directory**. For example:

    $ desktop-file-install --dir=$HOME/.local/share/applications firefox-dev.desktop

Update database of desktop entries
----------------------------------
Finally, to make desktop entries defined in ``~/.local/share/applications`` work, run the following command::

    $ update-desktop-database ~/.local/share/applications

That should be all. If we open the application menu** of our desktop environment, we should be able to lauch Firefox. 

.. _`XDG Desktop Entry Specification`: https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html
