*******************
Pimping the Console
*******************
The **virtual console**, (aka **virtual terminal** or **VT** for short) is a conceptual combination of the keyboard and display for a computer user interface. In Linux we have available 7 of them, accessible by some shortcuts:

* ``Alt + Fn`` (where n is a number from 1 to 7) In Arch only **left Alt** works for this.
* ``Alt + Left/Right arrow`` to switch to the previous/next console.

These virtual consoles are represented in the system as **device files** (/dev/tty1, /dev/tty2...) which serve as an interface so other programs can write and read to and from these files. The graphical **X Window System** usually starts in the seventh virtual console, but in **Arch** it does in the 1st one.

Here we are gonna take care of the **keyboard mappings** and the **font**. Both console fonts and console maps are provided by the **kbd package** (a dependency of `systemd`_), which also provides many low-level tools for managing virtual console. In addition, ``systemd`` also provides the ``localectl`` tool, which can control both the system locale and keyboard layout settings for both the virtual console and Xorg.

http://kbd-project.org/

Keyboard mappings
=================
The **keymap files** available in our system are located in the ``/usr/share/kbd/keymaps/`` directory. To check all the available layouts we run::

    $ ls /usr/share/kbd/keymaps/**/*.map.gz

The first level directory after ``keymaps``, contains the types of processor architecture(amiga, atari, mac...). In **i386** there are available other **layouts** such as colemak, dvorak, etc.

For a list of all the available keymaps, use the command::

    $ localectl list-keymaps

* During installation, we had a first chance to set up our **keyboard layout**, but if we didn't do it, the **default** is:
* **qwerty layout**
* ``en`` **language code** 
* ``us`` **country code**

The keymap file for this combination is located in::

    /usr/share/kbd/keymaps/i386/qwerty/us.map.gz

* To **temporarily** change the keymap we can **list** all the keymaps to find out the one we want, then use::

    $ loadkeys es

**Important:** If we load the wrong keymap and got lost, we can load the **default** using the ``-d`` option.

* To **permanently** change the keymap we have to add them to the file ``/etc/vconsole.conf``, wich is the file that configures the **virtual console**, under the settings:

* ``KEYMAP=`` To set up the key mapping table for the keyboard. It defaults to "us" if not set. 
* ``KEYMAP_TOGGLE=`` can be used to configure a second toggle keymap and is by default unset.

Note: this file usually doesn't exist, we have to create it.

For convenience, ``localectl`` may be used to set **console keymap**. It will change the ``KEYMAP`` variable in ``/etc/vconsole.conf`` and also set the keymap for current session::

    $ sudo localectl set-keymap --no-convert us

The ``-no-convert`` option must be explained. We can use ``localectl`` to set the keymap for both:
1. The **virtual console**, using the sub-command ``set-keymap``.
2. The **X window manager**, using the sub-command ``set-x11-keymap``.

If we set the keymap for the **VC**, ``localectl`` will try to apply the closest matching keymap to **X**; and if we are setting the keymap for **X**, ``localectl`` will try to find the nearest match for **VC**. Using ``-no-convert`` avoids both scenarios.

Now if we run ``localectl`` we can see the ``VC Keymap: us``, whereas before it was set to ``n/a``.

Console fonts
=============
Arch Linux has several directories for fonts, but the one for **console fonts** is::

    /usr/share/kbd/consolefonts/

In this folder there are a lot of files with the ``psfu`` extension. These are fonts with the **PC Screen Font** format (PSF), a **bitmap** font format currently employed by the Linux kernel for **console fonts**. 

* To **temporarily** change the font we would do::

    $ setfont lat4a-19

With no parameter, ``setfont`` returns the console to the **default** font.

* To make our changes **permanent**, we have to add them to the file ``/etc/vconsole.conf``, under the settings:

* ``FONT=`` sets the font of the console.
* ``FONT_MAP=``, the console map.
* ``FONT_UNIMAP=`` the unicode font map.

We'll set up just the font opening the file in some editor and adding ``FONT=lat4a-19``, or whatever font we choose.

Terminus: my favourite console font
-----------------------------------
In the default font directory there's already a Terminus font (``Lat2-Terminus16``), but only the size 16, and I would like something bigger. 

The Terminus font is a monospace bitmap font, for both X11 and console. So let's install the font::

    $ sudo pacman -S terminus-font

This will also installed some dependencies if they weren't already on our system. The fonts will be installed to a couple of locations, namely ``/usr/share/fonts/misc/`` and ``usr/share/kbd/consolefonts``.

To set it up(temporarily), check the **console fonts directory**, and choose the one you like. My fav is ``ter-v20b``::

    $ setfont ter-v20b

To make it permanent, add ``FONT=ter-v20b`` to your ``/etc/vconsole.conf``.


.. _`systemd`: https://systemd.io/

