#################
The Linux Console
#################

.. toctree::
	:maxdepth: 3
	:hidden:

	draft


According to `Wikipedia`_:

	The Linux console is a system console internal to the Linux kernel which provides a way for the kernel and other processes to send text output to the user, and to receive text input from the user. The user typically enters text with a computer keyboard and reads the output text on a computer monitor.

That Wikipedia page is a gold mine of knowledge about the console.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Linux_console
