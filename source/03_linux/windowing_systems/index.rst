#################
Windowing Systems
#################
In modern operating systems, the `windowing system`_ (or just window system) is all the software that provides a `graphical user interface`_ (GUI) which implements the `WIMP`_ (windows, icons, menus, pointer) paradigm for a user interface.

We can differentiate several parts in a window system:

* At the **core** of the window system we have the `display server`_ also known as the **window server** or **compositor**, which is responsible of dealing with video hardware, mice, or keyboards.
* In some window systems, such as X11, there's a clear distinction between the display server and the `window manager`_. The latter controls the placement and appearance of the windows within the windowing system.
* On top of that sits the `desktop environment`_.
* Finally we have all the **application software** that provides a GUI, such as for example, LibreOffice, Blender, etc.

.. toctree::
    :maxdepth: 3
    :hidden:

    01_display_servers
    02_window_managers
    03_desktop_managers
    screen_resolution


.. _`windowing system`: https://en.wikipedia.org/wiki/Windowing_system
.. _`graphical user interface`: https://en.wikipedia.org/wiki/Graphical_user_interface
.. _`WIMP`: https://en.wikipedia.org/wiki/WIMP_(computing)
.. _`display server`: https://en.wikipedia.org/wiki/Display_server
.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
.. _`desktop environment`: https://en.wikipedia.org/wiki/Desktop_environment