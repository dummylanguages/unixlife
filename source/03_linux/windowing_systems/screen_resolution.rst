*****************
Screen resolution
*****************
Sometimes, after an installation we get a low resolution in our screens, and can't change it using the GUI, or if we do, the changes don't persist a reboot.

If you use `X Window System`_ aka **X11**



.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
