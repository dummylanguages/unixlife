***************
Window managers
***************
In some windowing systems such as the `X Window System`_, there is a clear distinction between the **display server**, which interacts with video hardware, mice, or keyboards and the `window manager`_, which controls the placement and appearance of windows. So the window manager sit in between the **display server** and the **desktop environment**. 

Some examples of window managers are:

* `Compiz`_
* `AwesomeWM`_
* `dwm`_
* `i3`_
* In Microsoft Windows operating systems, starting from Vista up to Windows 10, there's the `Desktop Window Manager`_

https://en.wikipedia.org/wiki/Widget_toolkit

.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`window manager`: https://en.wikipedia.org/wiki/Window_manager
.. _`Compiz`: https://en.wikipedia.org/wiki/Compiz
.. _`AwesomeWM`: https://en.wikipedia.org/wiki/Awesome_(window_manager)
.. _`dwm`: https://en.wikipedia.org/wiki/Dwm
.. _`i3`: https://en.wikipedia.org/wiki/I3_(window_manager)
.. _`Desktop Window Manager`: https://en.wikipedia.org/wiki/Desktop_Window_Manager
