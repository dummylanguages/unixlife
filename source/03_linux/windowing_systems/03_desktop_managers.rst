****************
Desktop managers
****************
Even though the **display server** is only the **core** of a **windowing system**, most of the times, when people talk about the windowing system they are referring to the display server.



As it may be evident from its name, display servers are designed following a `client-server model`_

`X Window System`_ aka **X11**



.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`client-server model`: https://en.wikipedia.org/wiki/Client%E2%80%93server_model
