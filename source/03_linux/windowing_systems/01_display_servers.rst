***************
Display servers
***************
Even though the **display server** is only the **core** of a **windowing system**, most of the times, when people talk about the windowing system they are referring to the display server.

As it may be evident from its name, display servers are designed following a `client-server model`_, where the server communicate with their clients using a `communication protocol`_.

Some examples of display servers are:

* The `X Window System`_ aka **X11**
* `Wayland`_
* The `Quartz compositor`_, which fulfills the tasks of both a **display server** and of a **window manager** in the windowing system in **macOS** operating systems.

We'll mostly focus on **X11** in separate sections, and here we'll talk a bit about Wayland.

Wayland
=======
Wayland is a `display server protocol`_ that specifies the communication between a display server and its clients. A display server using the Wayland protocol is called a `Wayland compositor`_, because it additionally performs the task of a compositing window manager. 

.. _`X Window System`: https://en.wikipedia.org/wiki/X_Window_System
.. _`client-server model`: https://en.wikipedia.org/wiki/Client%E2%80%93server_model
.. _`communication protocol`: https://en.wikipedia.org/wiki/Communication_protocol
.. _`Quartz compositor`: https://en.wikipedia.org/wiki/Quartz_Compositor
.. _`display server protocol`: https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)
.. _`Wayland compositor`: https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)#Wayland_compositors