#########
Bluetooth
#########

.. toctree::
	:maxdepth: 3
	:hidden:

	bluez


According to `Wikipedia`_:

**Bluetooth** is a wireless technology standard used for two main purposes:

1. **Exchanging data** between fixed and mobile devices over short distances.
2. Building **personal area networks**.

It was originally conceived as a wireless alternative to `RS-232`_ data cables, in case you are old enough to remember those.

Some interesting terminology:

* What are `radio waves`_.
* `UHF`_ radio waves. 
* The `ISM radio bands`_. 

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Bluetooth
.. _`RS-232`: https://en.wikipedia.org/wiki/RS-232
.. _`radio waves`: https://en.wikipedia.org/wiki/Radio_waves
.. _`UHF`: https://en.wikipedia.org/wiki/Ultra_high_frequency
.. _`ISM radio bands`: https://en.wikipedia.org/wiki/ISM_band
.. _`Bluetooth`: https://www.bluetooth.com/
