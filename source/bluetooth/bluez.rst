******************
Bluetooth in Linux
******************
In Linux, the official implementation of the Bluetooth protocol stack is `BlueZ`_, so we should make sure that is installed in our system.

Installation
============
We shouldn't have trouble finding packaged versions of all what we need in the repositories of any Linux distro.

Debian packages
---------------
In Debian we would do::

    $ sudo apt list --installed | grep -i bluez

Aside from the `bluez package`_, in the Debian repositories we can find another one named `bluez-tools`_ which provides command line tools to control our Bluetooth adapter, basically the ``bluetoothctl`` command.  are available .

.. note:: In Debian there's also a **metapackage** named `bluetooth`_ which provides all of the necessary stuff to work with the Bluez bluetooth stack.

If for some reason, running the ``bluetoothctl`` command leaves you with an iddle prompt, just ``ctrl + c``; Next we'll explain how to troubleshoot that.

Checking if the module is loaded
================================
Probably the most low level action we can perform to troubleshoot our Bluetooth connection is to check if the kernel module is loaded. We can do that with::

    $ lsmod | grep -i bluetooth

That should produce a list of modules related with bluetooth. The one named ``btusb`` is the driver, so that one should be in the list. To see more detailed information about a particular module::

    $ sudo modinfo btusb

If for some reason the aforementioned module is not loaded, we can do so with::

    $ sudo modprobe btusb

The ``-r`` option unloads the module, which can be loaded again running the command without that option.

Checking if the service is running
==================================
Most of the times this is the issue with a non-responsive Bluetooth connection. We can check the status of the service with::

    $ systemctl status bluetooth

If for some reason the service is not running, we can **start** it with::

    $ systemctl start bluetooth

.. note:: If you don't use `systemd`_, feel free to use the agnostic ``service``, which is a wrapper command for whatever init system being used::

    $ sudo service bluetooth start
    $ sudo service bluetooth status

Autostart
---------
If our service wasn't running that may be because it wasn't enabled to do so. We can check that out with::

    $ systemctl is-enabled bluetooth

To **enable** a service so that it starts automatically every time we boot our system::

    $ sudo systemctl enable bluetooth

Autoenable
----------
Imagine the case scenario where you've paired up some bluetooth device to your machine and successfully connected to it. Then after **rebooting** your system, even though the device is right there in the list of paired devices, you have to connect to it manually. That's specially problematic if you have to move your cursor to the password box in the login screen, and a bluetooth trackpad is the only input device with a pointer connected to your machine.

A way to solve that situation is by editing the ``/etc/bluetooth/main.conf`` file and make sure the ``Autoenable`` option is set to ``true``::

    [Policy]
    AutoEnable=true

This setting is essential if you want to enable your bluetooth adapter/controller as soon as the system found it; don't forget about it.

Fixing connection drops
-----------------------
After pairing my **Apple Magic Trackpad** and using it successfully for a while, I was experiencing some drops in the connection. It was easily fixable just by pressing the trackpad button; about 30 seconds it was again connected. It doesn't sound like much, but when you're in the zone, and your trackpad stops working it takes more than those 30 seconds to get back on track. Next I'll go over the solutions I tried.

TLP
^^^
Surfing the internet found a possible explanation for the issue: `TLP`_, which is a feature-rich command line utility for Linux, saving laptop battery power without the need to delve deeper into technical details.

.. note:: When I run ``sudo pacman -S tlp`` to check if the `tlp package`_ was installed, it seemed it wasn't installed; nonetheless, the configuration file was under ``/etc/tlp.conf``.

The proposed fix was to disable the power management features that apply to my  `Intel Dual Band Wireless-AC 8265 adapter`_ (dual band because it controls both WiFi and Bluetooth). First thing I needed to find the **ID** of the card::

    $ lsusb | grep -i bluetooth
    Bus 001 Device 004: ID 8087:0a2b Intel Corp. Bluetooth wireless interface

From the output above, it seems the Bluetooth interface has the **ID** ``8087:0a2b``. With that information, we can edit the TLP configuration file, located at ``/etc/default/tlp``, to include the following line to blacklist the USB device from being managed by TLP::

    USB_BLACKLIST=”8087:0a2b”

Now, save and exit.  Reboot for the new settings to take effect.

.. note:: Source: https://serenity-networks.com/bluetooth-mouse-disconnects-on-ubuntu-16-04-with-intel-7260/

.. warning:: After doing that and rebooting, drops in the connection kept happening.

Disable USB Autosuspend
^^^^^^^^^^^^^^^^^^^^^^^
The Arch Wiki offered another solution under `Disable USB Autosuspend`_. The explanation is that the **Linux kernel** can automatically suspend USB devices when they are not in use. This can sometimes save quite a bit of power, however some USB devices are not compatible with USB power saving and start to misbehave (common for USB mice/keyboards). **udev rules** based on whitelist or blacklist filtering can help to mitigate the problem. 
 
So I decided to create a **udev rule** under ``/etc/udev/rules.d`` which I named ``50-usb_power_save.rules`` in order to **blacklist** my bluetooth card::

    $ sudoedit /etc/udev/rules.d/50-usb_power_save.rules

The contents of the file::

    # blacklist Magic Touchpad
    ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="0x8087", ATTR{idProduct}=="0x0a2b", GOTO="power_usb_rules_end"
    ACTION=="add", SUBSYSTEM=="usb", TEST=="power/control", ATTR{power/control}="auto"
    LABEL="power_usb_rules_end"

And rebooted the machine.

.. warning:: After doing that and rebooting, drops in the connection kept happening.

Disable enhanced SCO mode
^^^^^^^^^^^^^^^^^^^^^^^^^
Another thing I tried was to disable the `eSCO mode`_. To check the status of the mode::

    $ cat /sys/module/bluetooth/parameters/disable_esco
    N

It was **Not** disabled, meaning it was enabled. To disable it temporarily::

    $ echo 1 | sudo tee /sys/module/bluetooth/parameters/disable_esco
    $ sudo /etc/init.d/bluetooth restart

And permanently::

    $ echo "options bluetooth disable_esco=1" | sudo tee /etc/modprobe.d/bluetooth-tweaks.conf

``bluetoothctl``
================
When you run the command ``bluetoothctl`` you should be taken to a prompt that looks like this::

    [bluetooth]#

Running the ``help`` command will show the list of available commands. The most useful ones are:

* ``list`` will list all available Bluetooth adapters, (generally just one) identified by a MAC address.
* ``show`` will show information about our Bluetooth adapter. If you have more than one, you can pass the hexadecimal MAC address of the one you want to know about; as in ``show AC:ED:5C:B6:5F:4D``.
* ``power on/off`` turn the controller(adapter) on/off.
* ``pairable on/off`` self-explanatory.
* ``info`` will show information about connected devices or about a particular one if we pass its MAC address, ``info [def]``.
* ``quit`` or ``exit`` will close the prompt.

Seriously, it's a really easy tool to work with, just type ``help`` to get a description of the available subcommands.


.. _`BlueZ`: https://en.wikipedia.org/wiki/RS-232
.. _`bluez package`: https://packages.debian.org/bullseye/bluez
.. _`bluez-tools`: https://packages.debian.org/buster/bluez-tools
.. _`bluetooth`: https://packages.debian.org/bullseye/bluetooth
.. _`systemd`: https://systemd.io/
.. _`TLP`: https://linrunner.de/tlp/
.. _`tlp package`: https://wiki.archlinux.org/index.php/TLP
.. _`Intel Dual Band Wireless-AC 8265 adapter`: https://www.intel.com/content/dam/www/public/us/en/documents/product-briefs/dual-band-wireless-ac-8265-brief.pdf
.. _`eSCO mode`: https://en.wikipedia.org/wiki/List_of_Bluetooth_protocols#SCO

