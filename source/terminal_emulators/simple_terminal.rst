***************
Simple terminal
***************
`Suckless.org`_ makes available an open source terminal emulator called `st`_ (*simple terminal*). Even though we may find a packaged binary version, it's much more practical download the source code and compile in our system.

Download the source code
========================
First we have to download the **source code** either from https://st.suckless.org/, by clicking on the download link. Once downloaded, we'll change to the Downloads directory and untar the file::

    $ cd ~/Downloads
    $ tar -zxvf st-0.8.2.tar.gz
    $ cd st-0.8.2

We can also clone the code from their repository in github::

    $ cd ~/Downloads
    $ git clone https://git.suckless.org/st
    $ cd st

Compiling and installing
========================
It should be as easy as::

    $ sudo make install

But in my case I was missing a couple of libraries which in **Debian** we can find in the following packages::

* `libxft-dev`_: FreeType-based font drawing library for X (development files)
* `libx11-dev`_: X11 client-side library (development headers)

So::

    $ sudo apt install libxft-dev libx11-dev

Default bindings
================
Increase font size: Ctrl+Shift+PageUp
Decrease font size: Ctrl+Shift+PageDown
Reset to default font size: Ctrl+Shift+Home

Copy to the clipboard selection: Ctrl+Shift+C
Paste from the clipboard selection: Ctrl+Shift+V

Patching
========
1. Download the patch. 
2. The ``patch`` command knows what file to patch ;-)
3. Check the files being patched, because you may have to smash the config::

    $ cp config.def.h config.h
    $ make clean
    $ make
    $ sudo make install

.. _`Suckless.org`: https://suckless.org/
.. _`st`: https://st.suckless.org/
.. _`libxft-dev`: https://packages.debian.org/buster/libxft-dev
.. _`libx11-dev`: https://packages.debian.org/buster/libx11-dev