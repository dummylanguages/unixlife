*****
Xterm
*****
The `xterm`_ program is the standard terminal emulator for the `X Window System`_.

Fonts
=====

Bitmap fonts
------------
The **default console font** that **xterm** will use is a font that goes under two aliases: ``fixed`` and ``6x13``. This font has the XLFD description::

    -misc-fixed-medium-r-semicondensed--13-120-75-75-c-60-iso8859-?

To print out all the **console fonts** available in our system we can use the command::

    $ xlsfonts

Before permanently setting a font in our configuration, we can launch ``xterm`` with the option ``-fn`` (for **bitmap fonts**) followed by the font/size we want to try::

    $ xterm -fn '-xos4-terminus-medium-*-*-*-18-*-*-*-*-*-*-*'

As you can see, we have to use the XLFD description of the font.

$ xrdb -query

TrueType fonts
--------------
The **default TrueType font** that **xterm** will use is the **14‑point** font matched by the name ``mono``. To find out what is the default in our system we can use this command::

    $ fc-match mono

To print out all the **fixed width** fonts that are available in our system::

    $ fc-list :scalable=true:spacing=mono: family

Before permanently setting a font in our configuration, we can launch ``xterm`` with the option ``-fa`` followed by the font/size we want to try:

    $ xterm -fa 'Source Code Pro' -fs 20




Colorscheme
===========
There is a site named https://terminal.sexy that allows us to create online our own colorscheme and download it in our prefered format. A colorscheme is just a list of color definitions. When writing a theme, pay attention to **capitalization**::

    xterm*vt100*Background: #161719
    xterm*vt100*Foreground: #c5c8c6
    xterm*vt100*color0: #000000

Using ``.XTerm.`` instead of ``.xterm`` will not work! Also the ``vt100`` setting is for applying colors only to the text space of the screen, not also to the menus. Also, even though other settings can be written separated with dots (``.``), in the case of **colors**, it has to be **asterisks** (``*``).

Select a colourscheme
---------------------
We can have our colorscheme in a separate file, and import it our configuration using an ``#include`` directive. For example, in my ``~/.config/xterm/config`` file I have::

    #include "./themes/Solarized.xterm"

The ``.xterm`` extension is unnecessary but descriptive, to differentiate from other themes.

xrdb files
----------
Sometimes themes are files with the ``.xrdb`` extension, that contain just a **list of color definitions** like this::

    #define Ansi_0_Color #000000
    #define Ansi_1_Color #fd5ff1
    #define Ansi_2_Color #87c38a
    ...
    #define Background_Color #161719
    #define Bold_Color #c5c8c6
    #define Cursor_Color #d0d0d0
    #define Cursor_Text_Color #151515
    #define Foreground_Color #c5c8c6
    #define Selected_Text_Color #c5c8c6
    #define Selection_Color #444444

To use these themes, we need a file that works as a **theme template**, that use the same variable names defined in the ``.xrdb`` file. Let's call it ``xterm_template``::

    xterm*color0:      Ansi_0_Color
    xterm*color1:      Ansi_1_Color
    ...
    xterm*colorBD:     Bold_Color
    xterm*colorIT:     Italic_Color
    xterm*colorUL:     Underline_Color
    xterm*foreground:  Foreground_Color
    xterm*background:  Background_Color
    xterm*cursorColor: Cursor_Color

In our **xterm configuration** we would import the wished ``.xrdb`` theme (list of colors)::

    #include "$HOME/.config/xterm/themes/xrdb/Blazer.xrdb"

Finally we would just merge the ``xterm_template`` using::

    $ xrdb -merge xterm_template

Very bold colors
----------------
There is an option named ``veryBoldColors`` (0 by default) that specifies whether to **combine video attributes** with colors specified by:
* colorRV
* colorUL
* colorBD
* colorBL
* colorIT

The **resource value** is the sum of values for each attribute:
* 1 for reverse,
* 2 for underline,
* 4 for bold,
* 8 for blink, and
* 512 for italic

When using small font sizes, **bold** or **italic** characters may be difficult to read. One solution is to turn off bolding and underlining or italics and use color instead. This example does just that::

    ! disable bold font faces, instead make text light blue.
    xterm.vt100.colorBDMode: true
    xterm.vt100.colorBD: rgb:82/a4/d3
    ! disable underlined text, instead make it white.
    xterm.vt100.colorULMode: true
    xterm.vt100.colorUL: rgb:e4/e4/e4
    ! similarly use colorIT for italics


.. _`xterm`: https://invisible-island.net/xterm/
.. _`X Window System`: https://www.x.org/wiki/
