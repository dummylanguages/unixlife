************
rxvt-unicode
************
As the author explains in the official `rxvt-unicode`_ website:

    **rxvt-unicode** is a fork of the well known terminal emulator rxvt.

Installing it is super easy, since it's available in the repositories of most Linux distros. To install it in **Arch** we would run::

    $ sudo pacman -S rxvt-unicode

In Debian or Debian-based::

    $ sudo apt install rxvt-unicode

Now we'll create a ``~/.xinitrc`` file, and we'll add the following line::

    exec urxvt

Now when we start a session in X11::

    $ startx

The client will be the **urxvt terminal**. Problem is the font is small, as well as the screen size. Let's fix that next.

Copy and Paste shortcuts
========================
By default, in **urxvt** there are 2 groups of shortcuts for **copy and paste**:

One that only works within the terminal. This are for the PRIMARY::

* Copy: ``Ctrl + Insert``
* Paste: ``Shift + Insert``

Another one that allows us to copy-paste from the terminal to other apps bidirectionally::

* Copy: ``Ctrl + Alt + C``
* Paste: ``Ctrl + Alt + V``

In other terminal emulators, like **Gnome Terminal** these shortcuts are ``Ctrl+Shift+C`` and ``Ctrl+Shift+V`` respectively. Using simply ``Ctrl+ V`` would interfere with other terminal programs like Vim for example.

In **urxvt** ``Ctrl + Shift`` can't be used, because it's already use to enable two special modes:

* **ISO 14755 mode**
* **Picture Insert Mode**

The ISO 14755 mode can be disabled at compile time using the line::

    $ ./configure --disable-iso14755
    
We can also add the following lines to your ``.XDefaults`` or ``.Xresources`` file::

    URxvt.iso14755: false
    URxvt.iso14755_52: false

Configuration
=============
**X resources** are configuration options for applications that depend on the **X11** server, such as the **rxvt-unicode** terminal emulator. X resources usually are set in the `~/.Xresources` file.

While most display managers will automatically load this configuration file on startup, it is possible to load the configuration manually by running:
    $ xrdb ~/.Xresources

To check all the available configuration options for **rxvt-unicode**, we can filter the manual pages with:
    $ urxvt --help 2>&1 | grep desired_topic

To apply any configuration changes, we have to manually **reload** this file with:
    $ xrdb ~/.Xresources

Structuring configuration files
===============================
Once you start tuning your **urxvt** configuration, your ``~/.Xresources`` file may grow more than you want, specially if you keep there the configuration of other apps that also depend on the **X11** server. So I prefer to keep different files for each application by using ``#include`` in the main ``~/.Xresources`` file.

We can choose any directory we want to keep our files, such as ``~/.Xresources.d``, in that case we would include:
    #include ".Xresources.d/xterm"

Or in my case I chose ``~/.config`` a directory that was already in my home, being used by some apps such as my window manager:
    #include ".config/urxvt"

Note: You need to have a C preprocessor, such as GNU CPP, installed to use this functionality.

Inside the this folder directory I keep:
* The file with the proper configuration: ``urxvt``
* A folder with **themes**. I link the them from the ``urxvt`` using a ``#include`` directive as well.

Themes
======
These are just list of colors.

FINISH IT

Transparency
============
Urxvt offers two types of transparency for its background:

* What is known as **fake transparency**, that copies the wallpaper to the background of the terminal. (more exactly, the contents of Xorg's root window) Settings for **fake transparency** would be::

    urxvt*transparent: true
    urxvt*shading: 30

Using **fake transparency** would be better on a system low in resources.

* **Real transparency**, using some **composite manager**. The settings for this would be::

    urxvt*depth: 32
    urxvt*background: rgba:0000/0000/0200/c800

If we use these settings but no composite manager is running, there will be no transparency at all. But if we enable the two types of settings, the fake transparency will take the precedence over the real transparency (composite).

Font size in the terminal
=========================
Before anything, we have to query the fonts installed in our system in order to get their names to use them in our configuration. The most simple but useful queries are:

* ``fc-list : family`` lists only font family names.
* ``fc-list : family style`` lists font family names with their available styles.
* ``fc-list :lang=es`` lists Spanish fonts.

See the man page to see how to use the ``--format`` option with this command. For example::

    $ fc-list --format="%{family[0]}\n" | sort | uniq

1. Create an ``~/.Xresources`` file and an ``~/.Xresources.d`` folder.

2. Inside ``~/.Xresources`` link the configuration file:
    #include ".Xresources.d/urxvt/config"

3. Create the configuration file for ``urxvt`` and set the font:
    URxvt.font:     xft:xos4 Terminus:size=16:style=normal
    URxvt.boldFont: xft:xos4 Terminus:size=16:style=bold

4. Run::

    $ xrdb ~/.Xresources

.. note:: If you are using a custom ``.xinitrc`` add the following line::

    [[ -f ~/.Xresources ]] && xrdb -merge -I$HOME ~/.Xresources

Note: Do not background the xrdb command within ``~/.xinitrc``. Otherwise, programs launched after ``xrdb`` may look for resources before it has finished loading them.

.. note:: for the changes in the ``~/.Xresources`` to take place, we have to run ``xrdb`` and restart the terminal. But since we don't have so far any other client for the **X session** to go on, restarting our terminal it's equivalent to restarting our **X session**.

More info
---------
At the beginnning I was experimenting some issue with the huge font-spacing, which I try to solve using::

    URxvt.letterSpace: -1

But that didn't solve it. Only when I switch to another font that was my system(freemono) the spacing problem dissapeared. Finally, regarding the font I chose **inconsolata**. We could install it using::

    $ sudo apt-get install fonts-inconsolata

But I prefer just going to their website, download the compressed file, and put the fonts in my ``~/.fonts`` directory, which we may want to back up, to keep a nice font collection for the future. 

And enabled it adding the following line to my configuration::

    URxvt.font: xft:inconsolata:size=16

!! TRY SOURCE CODE PRO

Setting up urxvt screen size
============================
The screen size can be adjusted with the ``geometry`` option, either through the command line::

    $ urxvt --geometry COLSxLINES

Useful for adjusting dinamically the screen size by trial and error. Once we get to the desired resolution, we can set it up permanently on the configuration::

    urxvt.geometry: COLSxLINES

* It would be faster to use an **urxvt extension** named **fullscreen**. The problem with this extension is that it does its magic by using a program named ``wmctrl``, and this program only works with **window managers** compliant with the EWMH specification (Extended Window Manager Hints), but we don't have installed a window manager at all, lol.

IMPORTANT: Once we install a **window manager**, it will take care of the size of all our windows, including the one for our terminal emulator. So don't try to set it up now, IT'S NOT WORTHTY!

Some links:

https://github.com/bookercodes/awesome-urxvt
https://en.wikipedia.org/wiki/Rxvt
http://rxvt.sourceforge.net/

Extensions
==========
Most of the times, extensions are just **perl** scritps. Since rxvt-unicode **9.14** the **default location** for the extensions is ``$HOME/.urxvt/ext``, meaning that we can put the scripts there and they'll be recognized. 

But we can also define a **custom location** using the following option in the configuration file:

    urxvt.perl-lib: $HOME/.config/urxvt/ext

I chose ``~/.config/urxvt``, but we can choose other directory if we want.

The resize-font extension
-------------------------
The best way to see how extensions work, is installing one. I downloaded the ``resize`` Perl script and place it in ``~/.config/urxvt/ext``. 

Enabling the extension
----------------------
Once we have the extension in the right location, we have to enable it::

    URxvt.perl-ext-common:  resize-font

Adding the shortcuts
--------------------
This extension works with some customizable shortcuts::

    ! resize-font shortcuts
    URxvt.keysym.C-minus:   resize-font:smaller
    URxvt.keysym.C-plus:    resize-font:bigger
    URxvt.keysym.C-equal:   resize-font:reset

After that, reload the changes with ``$ xrdb ~/.Xresources``. The default shortcuts were working fine:

* `Ctrl + +` to increase the size.
* `Ctrl + -` to decrease it.
* `Ctrl + =` to the default size.




.. _`rxvt-unicode`: http://software.schmorp.de/pkg/rxvt-unicode.html