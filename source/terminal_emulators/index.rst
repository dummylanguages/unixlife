##################
Terminal emulators
##################

.. toctree::
	:maxdepth: 3
	:hidden:

	alacritty
	xterm
	simple_terminal
	urxvt

According to `Wikipedia`_:

	A terminal emulator is a computer program that emulates a video terminal within some other display architecture.

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Terminal_emulator
