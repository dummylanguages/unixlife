###################
Encryption software
###################

.. toctree::
	:maxdepth: 3
	:hidden:

	pgp/index
	tor/index

According to Wikipedia, `encryption software`_ refers to a category of programs that uses `cryptography`_ to prevent unauthorized access to digital information. Cryptography is used to encrypt `data at rest`_, meaning the data that exists on some storage device, as well as to proctect `data in transit`_, meaning the digital information that is sent to other computers over the Internet.

.. _`encryption software`: https://en.wikipedia.org/wiki/Encryption_software
.. _`cryptography`: https://en.wikipedia.org/wiki/Cryptography
.. _`data at rest`: https://en.wikipedia.org/wiki/Data_at_rest
.. _`data in transit`: https://en.wikipedia.org/wiki/Data_in_transit
