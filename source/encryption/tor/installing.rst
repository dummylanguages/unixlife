**************
Installing Tor
**************
We shouldn't have any trouble finding the Tor package in the official repositories of our distribution::

	$ sudo apt update && sudo apt install tor

.. _`web of trust`: https://en.wikipedia.org/wiki/Web_of_trust
.. _`Electrum`: https://electrum.org
.. _`GPG`: https://gnupg.org/

