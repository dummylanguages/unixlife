###
Tor
###

.. toctree::
	:maxdepth: 3
	:hidden:

	installing
	ssh_tor

According to `Wikipedia`_, Tor (short for **The Onion Router**) is free and open-source software for enabling **anonymous communication**. Using `Tor`_ makes it more difficult to trace the Internet activity to the user. Tor's intended use is to protect the personal privacy of its users, as well as their freedom and ability to conduct confidential communication by keeping their Internet activities unmonitored. 

.. _`Wikipedia`: https://en.wikipedia.org/wiki/Tor_(network)
.. _`Tor`: https://www.torproject.org
