****************************
Configuring ``ssh`` over Tor
****************************
In this section we'll learn how to set up an ``ssh`` connection over Tor. Assuming we have Tor installed both in the **server** and in the **client**, next we'll explain the configuration on each of them.

Server Configuration
====================
First thing is to ``start`` and ``enable`` the ``tor`` service::

  $ sudo systemctl start tor
  $ sudo systemctl enable tor

And do the same for the ``sshd`` daemon::

  $ sudo systemctl start ssh
  $ sudo systemctl enable ssh

In the file ``/etc/tor/torrc`` add/uncomment the lines::

  HiddenServiceDir /var/lib/tor/hidden_service
  HiddenServicePort 22 127.0.0.1:22           # port for ssh

Restart the "tor" service::

  $ sudo systemctl restart tor

Getting the onion address
-------------------------
We have configured a tor hidden ssh service that is listening on ``127.0.0.1:22``. Now we need to get the onion address; for that we'll need to switch to ``root`` and cat the contents of the ``hostname`` file::

  $ sudo su
  # cat /var/lib/tor/hidden_service/hostname 
  f2rq533h57a4obsfhommm7g6w4x5zkc3mb555hshflova4svutx75hqd.onion

Client Configuration
====================
Just install tor, ssh-client and `filezilla`_. Now we need to do two things:

* First, you must be running tor on your client machine as well.
* Then, you must tell your ssh client to proxy through tor.

.. warning:: incomplete!!

.. _`filezilla`: https://filezilla-project.org/

