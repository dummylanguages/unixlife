###
PGP
###

.. toctree::
	:maxdepth: 3
	:hidden:

	verifying_signatures

**PGP** (short for **Pretty Good Privacy**) is an encryption program that provides several features for encrypt and decrypt digital information as well as to determine the authenticity of the message.

.. note:: **PGP** was originally developed by `Phil Zimmermann`_ in **1991**. The Wikipedia entry for `Pretty Good Privacy`_ is a pretty good source of information regarding its history.

`OpenPGP`_ is described in its official website as the most widely used **email encryption standard**. It was born out of the PGP software mentioned at the beginning.

.. note:: The standard is defined by the `RFC4880`_. (**RFC** stands for `Request For Comments`_ and it's is a publication from the **Internet Society** (ISOC) and its associated bodies, most prominently the **Internet Engineering Task Force** (IETF), the principal technical development and standards-setting bodies for the Internet. 

`GnuPG`_ (also known as **GPG**) is a complete and free implementation of the OpenPGP standard. This software allows us to encrypt and sign your data and communications.

.. _`Phil Zimmermann`: https://en.wikipedia.org/wiki/Phil_Zimmermann
.. _`Pretty Good Privacy`: https://en.wikipedia.org/wiki/Pretty_Good_Privacy
.. _`OpenPGP`: https://www.openpgp.org/
.. _`RFC4880`: https://www.ietf.org/rfc/rfc4880.txt
.. _`Request For Comments`: https://en.wikipedia.org/wiki/Request_for_Comments
.. _`GnuPG`: https://gnupg.org/
