*****************************
Verifying a digital signature
*****************************
Plenty of times we see how developers provide **GPG signatures** along with their software. This is to make sure that the executable you're downloading is the one that has been signed by the original developer with his public key. Even if the official website was compromised, and the original files had been replaced, **signature verification** would fail, because the attacker would not be able to create valid signatures.

For example, imagine you are planning to purchase some bitcoin and want to store it in a **hot wallet** such as `Electrum`_. In such scenario, you don't want to download and install a version of the program that may have been altered by hackers, since they could easily stole your bitcoin. 

.. note:: In the following sections we'll be describing the **signature verification** process using `GPG`_, which is usually include in all Linux distributions. In **macOS** you'll have to use ``brew`` or any other package manager to get ``gpg`` in your system.

Downloading the necessary files
===============================
To verify the digital signature of a file, you want to download:

* The file itself, in this case the **Electrum** executable.
* The **public key** of the publisher, which also includes the ``.asc`` extension.
* Its **signature file**, recognizable by the ``.asc`` extension.

The publisher's public key
--------------------------
The **public key** of the publisher (ThomasV) was also available at the official site. Again, it can be downloaded using your browser or the command::

	$ wget https://raw.githubusercontent.com/spesmilo/electrum/master/pubkeys/ThomasV.asc

This file was quite long, it looked something like this::

	-----BEGIN PGP PUBLIC KEY BLOCK-----

	mQINBE34z9wBEACT31iv9i8Jx/6MhywWmytSGWojS7aJwGiH/wlHQcjeleGnW8HF
	[...]
	ZFA8xDodsofQEmlv+I/xyEZ7na6nxbpZVuPC3B0JFtY=
	=sUYl
	-----END PGP PUBLIC KEY BLOCK-----

Once we have the **public key** in our file system, we have to **import** it::

	$ gpg --import ThomasV.asc

.. note:: If you have the **key ID** but not a path to download the file, use this command to get the key::

	$ gpg --recv-keys KEYID

 The command above also imports the key into our **public keyring**.

The signature file
------------------
For example, in my case I wanted to download the executable for macOS, so I had to download its specific signature. To download it we could either::

	$ wget https://download.electrum.org/4.0.9/Electrum-4.0.9.tar.gz.asc

Or just click on the link and save the file to our file system. Once downloaded, this is what that file looked like::

	-----BEGIN PGP SIGNATURE-----

	iQIzBAABCgAdFiEEZpTY3nvo7lYxvtlQK9WCS3+UcOYFAl/c/ekACgkQK9WCS3+U
	[...]
	5cr+T8SDg6FimxBMuUjE9sKX2WVoOVBU0l4QpThAkxv+/ra4RKw=
	=/Mwm
	-----END PGP SIGNATURE-----

Once we have the **signature** in our file system, we just have to verify it with::

	$ gpg --verify electrum-4.0.9.dmg.asc electrum-4.0.9.dmg

Where the **first argument** is the key and the **second argument** is the file we want to verify. The output of the command above was as follows::

	gpg: Signature made Fri Dec 18 21:07:21 2020 EET
	gpg:                using RSA key 6694D8DE7BE8EE5631BED9502BD5824B7F9470E6
	gpg: Good signature from "Thomas Voegtlin (https://electrum.org) <thomasv@electrum.org>" [unknown]
	gpg:                 aka "ThomasV <thomasv1@gmx.de>" [unknown]
	gpg:                 aka "Thomas Voegtlin <thomasv1@gmx.de>" [unknown]
	gpg: WARNING: This key is not certified with a trusted signature!
	gpg:          There is no indication that the signature belongs to the owner.
	Primary key fingerprint: 6694 D8DE 7BE8 EE56 31BE  D950 2BD5 824B 7F94 70E6

Checking the fingerprint of a public key
========================================
When you import someone's **public key**, you should check its fingerprint using independent sources. In this case, the **Electrum** official site, included a link to a Youtbe video where the ThomasV was giving a talk. The footage shows the **fingerprint** of the key. That serves as proof that the key belongs to the owner.

.. note:: In cryptography, a `web of trust`_ is a concept used in PGP, GnuPG, and other OpenPGP-compatible systems to establish the authenticity of the binding between a public key and its owner.

Importing Public Key from a Trusted Source
==========================================
Sometimes, the software author tells you his/her **public key ID** on the website, we can import it with::

	$ gpg --recv-keys <key-ID>

Then display the **fingerprint** with:

	$ gpg --fingerprint <key-ID>

And compare the fingerprint from output with that published on website. This is more secure because the public key is imported from a public key server, which by **default** is set to ``hkp://keys.gnupg.net`` in ``~/.gnupg/gpg.conf`` file. Since all of the major keyservers communicate with each other and synchronize keys, so you don’t need to change the default.

.. _`web of trust`: https://en.wikipedia.org/wiki/Web_of_trust
.. _`Electrum`: https://electrum.org
.. _`GPG`: https://gnupg.org/

