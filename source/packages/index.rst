####################################
The Debian package management system
####################################

.. toctree::
	:maxdepth: 3
	:hidden:

	package_types
	dkpg
	apt_tools/index
	apt_front_ends/index
	draft

In the past, many Linux programs were distributed as `source code`_, which a user would build into the required program or set of programs, along with the required man pages, configuration files, and so on. Nowadays, most Linux distributions offer what are known as `package management systems`_, a practical and comfortable way of distributing software.

The Debian's package management system has several levels:

* At the base is `dpkg`_.
* At a higher level we have the `Advanced Package Tool`_ (APT for short), which can be considered a **front-end** to ``dpkg``, while at the same time provides new features.
* Then we have **graphical front-end tools** to ease their use:

	* `dselect`_ is the oldest front-end to ``dpkg``, and today has become obsolete.
	* `aptitude`_ is a front-end to **APT**. It provides a graphical interface based on `ncurses`_.
	* `Synaptic`_ is another graphical user interface for the APT package manager. It's based on `GTK`_, and can also be used on systems based on `RPM`_ packages (used by Red Hat and derivatives).

There are many more graphical front-ends to **APT**, basically each desktop manager offers their own.

.. note:: The Debian's package management system is used by other Debian-based distributions such as: `Ubuntu`_, `Kali Linux`_, `Tails`_, etc.

For more information about the Debian's packaging system check:

* The `Debian Administrator's Handbook`_, especially:

	* `Chapter 5 - Packaging System`_ 
	* `Chapter 6 - The APT Tools`_.

.. _`source code`: https://en.wikipedia.org/wiki/Source_code
.. _`package management systems`: https://en.wikipedia.org/wiki/Package_manager
.. _`dpkg`: https://en.wikipedia.org/wiki/Dpkg
.. _`Advanced Package Tool`: https://en.wikipedia.org/wiki/APT_(software)
.. _`dselect`: https://en.wikipedia.org/wiki/Dselect
.. _`aptitude`: https://en.wikipedia.org/wiki/Aptitude_(software)
.. _`Synaptic`: https://en.wikipedia.org/wiki/Synaptic_(software)
.. _`ncurses`: https://en.wikipedia.org/wiki/Ncurses
.. _`GTK`: https://en.wikipedia.org/wiki/GTK
.. _`RPM`: https://en.wikipedia.org/wiki/RPM_Package_Manager
.. _`Ubuntu`: https://ubuntu.com/
.. _`Kali Linux`: https://www.kali.org/
.. _`Tails`: https://tails.boum.org/
.. _`Debian Administrator's Handbook`: https://www.debian.org/doc/manuals/debian-handbook/
.. _`Chapter 5 - Packaging System`: https://www.debian.org/doc/manuals/debian-handbook/packaging-system.en.html
.. _`Chapter 6 - The APT Tools`: https://www.debian.org/doc/manuals/debian-handbook/apt.en.html
