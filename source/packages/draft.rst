###################
Install from source
###################
Some software may not available for our Linux distribution, in that case we may have to download the source code, compile it into a binary file, and install it in our system.

For example, let's see how we would install a terminal emulator called `st`_, which is short for **simple terminal**.

Download the source code
========================
First we have to download the **source code** either from their website, by clicking on the **download link**. Once downloaded, we'll change to the Downloads directory and untar the file::

    $ cd ~/Downloads
    $ tar -zxvf st-0.8.2.tar.gz
    $ cd st-0.8.2

We can also clone the code from their repository in github::

    $ cd ~/Downloads
    $ git clone https://git.suckless.org/st
    $ cd st

Dependencies
============
According to the Debian's description of the `stterm`_ package, simple terminal has the following dependencies:

* libc6: GNU C Library. Shared libraries that are used by nearly all programs on the system.
* libx11-6: The X11 client-side library
* libfontconfig1: generic font configuration library (runtime)
* libfreetype6: FreeType 2 font engine (shared library files)
* libxft2: FreeType-based font drawing library for X
* ncurses-term: additional terminal type definitions 

In my case, when I try to build the system complain about `pkg-config`_, which is is a helper tool used when compiling applications and libraries.



As it happens, I needed to install:

* `libxft-dev`_: FreeType-based font drawing library for X (development files)
* `libx11-dev`_: X11 client-side library (development headers)


Compiling
=========
Once we have the source code in our filesystem, make sure you have changed to the directory where we have cloned the code. Now we have to check our system for the required software needed to build the program using the ``configure`` command::

    $ sudo make clean install

Errors
======
Apparently, I couldn't install **st** because it couldn't find some libraries, so I had to set up the ``PKG_CONFIG_PATH`` environment variable that ``pkg-config`` uses when informing the compiler about the location of the libraries.

Looking for files
-----------------
apt-file is used to do a search of which package provides a file. Consider it the offline and somewhat limited version of http://packages.ubuntu.com.

First thing we should do with it is to update the cache with::

    $ sudo apt-file update

We must use ``apt-file update`` with ``sudo``, so it can do an update with a system-wide directory search (typically /var/cache/apt/apt-file), and doesn't use instead a directory in our home (~/.cache/apt-file).

Now we can look for the file we are looking for::

    $ apt-file search fontconfig.pc
    libfontconfig1-dev: /usr/lib/x86_64-linux-gnu/pkgconfig/fontconfig.pc

And set the ``PKG_CONFIG_PATH`` environment variable properly::

    export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig

.. _`st`: https://st.suckless.org/
.. _`pkg-config`: https://www.freedesktop.org/wiki/Software/pkg-config/
.. _`stterm`: https://packages.debian.org/buster/stterm
.. _`libxft-dev`: https://packages.debian.org/buster/libxft-dev
.. _`libx11-dev`: https://packages.debian.org/buster/libxft-dev