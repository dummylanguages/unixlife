*****************
Types of packages
*****************
In Debian we can differentiate between two types of packages:

1. `Source packages`_, which we are not gonna discuss here. Read `here`_ about it.
2. `Binary packages`_, which are what almost everybody have in mind when talking about **Debian packages**.

Binary packages
===============
Debian binary packages are easily recognizable by their ``.deb`` extension. They contain applications, documentation, source code, etc that we can install in our system. They are called **binary packages**, not so much because they include executable files but because they are not character encoded as human readable text. For example, a compiled library is a binary file that we can not run.

Structure of a binary package
-----------------------------
Imagine we go to https://code.visualstudio.com/docs/setup/linux and download the latest version of Visual Studio Code. Let's move to our Downloads folders and extract the contents of the ``code_1.43.0-1583783132_amd64.deb`` file::

    $ ar vx code_1.43.0-1583783132_amd64.deb
    x - debian-binary
    x - control.tar.gz
    x - data.tar.xz

.. note:: We used two options with the ``ar`` command: ``x`` for extract and ``v`` stands verbose. Check the `ar manual page`_ for more info about it.

Inside every ``.deb`` file, there are 3 files:

* ``debian-binary`` is a text file which simply indicates the version of the ``.deb`` file used.
* ``control.tar.gz`` contains all of the available meta-information, like the name and version of the package.
* ``data.tar.gz`` contains all of the files to be extracted from the package; this is where the executable files, documentation, etc., are all stored.


.. _`Source packages`: https://wiki.debian.org/Packaging/SourcePackage
.. _`here`: https://www.debian.org/doc/manuals/debian-handbook/sect.source-package-structure.en.html
.. _`Binary packages`: https://wiki.debian.org/Packaging/BinaryPackage
.. _`ar manual page`: https://manpages.debian.org/buster/binutils-common/ar.1.en.html
