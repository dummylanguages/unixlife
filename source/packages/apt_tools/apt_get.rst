***********
``apt-get``
***********
``apt-get`` was the first front end tool available to interact with `APT`_. ``apt-get`` was developed within the APT project, and it's often used as **back-end** by other tools that interact with the APT library..

``apt-get`` works in the command-line, next we're gonna go over the things we can do with it. For more information:

* Run `man apt-get`_.
* Check `chapter 2.2`_ of the **Debian Reference**.
* Or `chapter 6.2`_ of **The Debian Administrator's Handbook**.
* Also `chapter 8.1.2`_ of the **The Debian GNU/Linux FAQ**.

Updating the packages list
==========================
Before doing any work with APT, it's a good idea to update the list of available packages::

    $ sudo apt-get update

This command fetches the latest indexes of available packages from the location(s) specified in ``/etc/apt/sources.list``. The **update** operation should always be performed before a system **upgrade** or **dist-upgrade** operations.

Installing and removing packages
================================
To **install** one or more packages, we just have to pass their names as arguments to the ``install`` subcommand::


    $ sudo apt-get install foo bar

That will install the ``foo`` and ``bar`` packages plus all their necessary dependencies we run.

Package in our filesystem
-------------------------
If the package ``foo.deb`` is already in our filesystem (we just downloaded it), we can indicate we want to install it passing the path to the file as argument::

    $ sudo apt-get install ./foo

Installing a specific version
-----------------------------
When we install a package using its package name, we are installing the latest version. If we want to install a specific version, we can use the ``=`` sign to specify the version number::

    $ sudo apt-get install foo=1.2

Reinstalling
------------

Simulate the installation
-------------------------

    $ sudo apt-get install -s foo

Removing
--------
If we want to **remove** the package we run::

    $ sudo apt-get remove foo

Note that this will remove the package's dependencies but won't remove its configuration files. For a complete uninstallation we can run::

    $ sudo apt-get purge foo

That gets rid of configuration files too.

We can install/remove several packages at the same time, we just have to pass several arguments to the respective command::

    $ sudo apt-get install package1 package2 package3

Installing and removing simultaneously
--------------------------------------
We can use the install command to remove packages too, by appending the package we want to remove with a ``-`` sign::

    $ sudo apt-get install package1 package2 package3-

When we are removing packages, we can install one of them appending a plus sign ``+`` to its name::

    $ sudo apt-get remove package1 package2+ package3+

Cleaning caches
===============
When we remove a package, the ``.deb`` file we used to install it, it's kept in ``/var/cache/apt/archives/`` to avoid downloading them again if we decide to reinstall them. If we want to free space in our hard drive, we can clean this cache of retrieved packages using the command::

    $ sudo apt-get clean

That would delete **all** packages in that folder.

Autocleaning
------------
If we want to clear the cache only for packages that can't be downloaded anymore because they are no longer in the repositories, we would run::

    $ sudo apt-get clean

Autoremove
----------
If we want to remove just the packages that were installed as dependencies but are not dependencies anymore, we would run::

    $ sudo apt-get autoremove

This command won't remove a package as long as it's a dependency of any other installed package.

System upgrade
==============
If we want to install the newest versions of **all** packages currently installed on the system from the sources enumerated in ``/etc/apt/sources.list``, we run::

    $ sudo apt-get upgrade

This command looks for installed packages which can be upgraded without removing any packages. In other words, the goal is to ensure the least intrusive upgrade possible.

dist-upgrade
============
Imagine that we have package ``foo`` installed in our system, and package ``bar`` was also installed as a dependency. The new version of ``foo`` does not have ``bar`` as a dependency, so if we want to upgrade to the new version and remove ``bar`` and any other dependencies that are not needed anymore, we would run::

    $ sudo apt-get dist-upgrade


.. _`APT`: https://www.debian.org/doc/manuals/debian-handbook/apt.en.html
.. _`man apt-get`: https://manpages.debian.org/stable/apt/apt-get.8.en.html
.. _`chapter 2.2`: https://www.debian.org/doc/manuals/debian-reference/ch02.en.html#_basic_package_management_operations
.. _`chapter 6.2`: https://www.debian.org/doc/manuals/debian-handbook/sect.apt-get.en.html
.. _`chapter 8.1.2`: https://www.debian.org/doc/manuals/debian-faq/pkgtools.en.html#apt-get
.. _`synaptic`: http://savannah.nongnu.org/projects/synaptic
.. _`GTK`: https://www.gtk.org/
.. _`wajig`: https://togaware.com/wajig/
