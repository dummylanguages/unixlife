#############
The APT tools
#############

.. toctree::
	:maxdepth: 3
	:hidden:

	apt_sources
	apt_get
	apt_cache
	apt

**APT** is the abbreviation for `Advanced Package Tool`_. This system allows operations such as:

* Install or remove packages.
* Upgrade the ones already installed.
* Check for broken dependencies.

APT is just a library with a core application and it requires of some front-end tool to allow user-interaction. Here we're gonna see the tools included with APT:

* `apt-get`_ and `apt-cache`_ were the first tools developed within the project.
* `apt`_, which is a newer command-line tool also provided by **APT** to overcome some design mistakes of ``apt-get``.

For more information, check:

* Check the manual `Using APT offline`_.


.. _`Advanced Package Tool`: https://www.debian.org/doc/manuals/debian-handbook/apt.en.html
.. _`apt`: https://manpages.debian.org/stable/apt/apt.8.en.html
.. _`apt-get`: https://manpages.debian.org/stable/apt/apt-get.8.en.html
.. _`apt-cache`: https://manpages.debian.org/buster/apt/apt-cache.8.en.html
.. _`Using APT offline`: https://www.debian.org/doc/manuals/apt-offline/index.en.html

