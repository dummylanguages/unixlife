***************
The APT sources
***************
APT needs a list of repositories to do its job, which is maintained on the file ``/etc/apt/sources.list``. Repositories are usually URL's to web or ftp servers all around the world that APT uses to download packages from before installing them. A `Debian repository`_ may also be:

* A network server.
* A local directory.
* A CD/DVD.

A repository contains a set of Debian binary or source packages organized in a special **directory tree**. The root directory of a repository has a directory ``dists`` which in turn has a directory for each **release** (stretch, buster, etc) and **suite**
(stable, testing, etc). Client computers can connect to the repository to download and install the packages using APT or any of the package management tools based on APT.

.. note:: https://debgen.simplylinux.ch/ is an online sources.list generator, check it out ;-)

Sources
=======
Every active line in the ``/etc/apt/sources.list`` describes a **source**, and has the following format::

    deb http://site.example.com/debian/ distribution component1 component2 component3

Let's go over each field separately to fully understand what a source is.

1. Type of package
------------------
The first field represents the type of package, either:

* ``deb`` if it's a **binary package**.
* ``deb-src`` in the case of a **source package**.

2. Repository URL
-----------------
Next entry is the URL to the repository where the packages will be downloaded from. We have several options here:

cdrom
^^^^^
Since a CD-ROM may not always be inserted into the drive, these sources are managed through the ``apt-cdrom`` program.

Debian Fast Server Select service
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
We can use the **Debian Fast Server Select service**, which automatically will choose for us the fastest mirror server. The URL is::

    http://deb.debian.org/debian/

Official mirrors
^^^^^^^^^^^^^^^^
We can choose one of the mirrors listed at the official list maintained at https://www.debian.org/mirror/list. This list contains two types of mirrors:

    * **Primary mirrors**, the ones with good bandwidth which usually carry packages for all architectures (amd64, arm64, etc)
    * **Secondary mirrors**, due to space restrictions they may not carry all architectures, but that doesn't mean they are slower than primary mirrors.

Security updates no mirror
^^^^^^^^^^^^^^^^^^^^^^^^^^
Debian does not recommend to their users to use mirrors to obtain `security updates`_, instead it's encouraged to directly download these updates from::

    deb     http://security.debian.org/debian-security buster/updates main
    deb-src http://security.debian.org/debian-security buster/updates main

Actually there are several official mirrors for security updates, but they are implemented through DNS aliases, so they all point to the url above.

3. Distribution
---------------
Here things are a bit complicated, attention must be paid.

Base repository
^^^^^^^^^^^^^^^
The base repository can be either the release **codename** or **alias**, based on the characters of the Toy story movies:

* `stretch`_, which is the alias of Debian 9.
* `buster`_, which is the alias of Debian 10.
* `bullseye`_
* `sid`_

Or the release **suite**:

* `stable`_ (buster).
* `testing`_ (bullseye).
* `unstable`_ (sid).

It's a good idea to track packages that correspond to a Debian point release using the **codenames**. Otherwise, using **stable** may result in nasty surprises and broken systems when the next stable release is Debian 11(assuming the current stable is Debian 10). For example, using the Debian fast server, the sources for the base repo of **buster** would be::

    deb     http://deb.debian.org/debian/ buster main contrib non-free
    deb-src http://deb.debian.org/debian/ buster main contrib non-free


Security updates
^^^^^^^^^^^^^^^^
As we said before, there are no mirrors for these packages, just one URL. The repository will be named with the format ``base/updates``. For example, if we want to add a repo for **buster**::

    deb     http://security.debian.org/debian-security buster/updates main
    deb-src http://security.debian.org/debian-security buster/updates main

Note it's ``buster/updates``, as opposed to ``buster-updates`` for the normal updates.

Stable updates
^^^^^^^^^^^^^^
Stable updates are not security sensitive but are still important. This repository will typically contain fixes for critical bugs which could not be fixed before the official release or which have been introduced by subsequent updates.

    deb     http://deb.debian.org/debian/ buster-updates main contrib non-free
    deb-src http://deb.debian.org/debian/ buster-updates main contrib non-free

Note I've used the codename ``buster-updates``, instead of ``stable/updates. Also, compare to ``buster/updates`` for the security updates, where a backslash is used.

Proposed updates
^^^^^^^^^^^^^^^^
Once published, the Stable distribution is only updated about once every 2 months. The `proposed-updates`_ repository is where the expected updates are prepared. Anyone can use this repository to test those updates before their official publication. To use these packages with APT, you can add the following lines to your sources.list file::

  # proposed updates for the next point release
  deb http://ftp.us.debian.org/debian buster-proposed-updates main contrib non-free

In this case we are using the repo ``buster-proposed-updates``, we could use ``bullseye-proposed-updates``. We are using the server ``ftp.us.debian.org`` just as an example, this repo is available in the mirrors.

Stable backports
^^^^^^^^^^^^^^^^
`Backports`_ are recompiled packages from testing (mostly) and unstable (in a few cases only, e.g. security updates) in a stable environment so that they will run without new libraries (whenever it is possible) on a Debian stable distribution.

We can add this repository to our sources list adding the line::

    deb http://deb.debian.org/debian buster-backports main

Instead of ``http`` we can use ``https`` when the `apt-transport-https`_ package is installed

Other sources
^^^^^^^^^^^^^
Check the `sources list wiki`_ to learn about other sources:

4. Components
-------------
The components field has three sections to differentiate packages according to the licenses chosen by the authors of each work:

* **Main** gathers all packages which fully comply with the Debian Free Software Guidelines.
* The **non-free** archive is different because it contains software which does not (entirely) conform to these principles but which can nevertheless be distributed without restrictions.
* **Contrib** is a set of open source software which cannot function without some non-free elements.

.. _`APT`: https://www.debian.org/doc/manuals/debian-handbook/apt.en.html
.. _`Debian repository`: https://wiki.debian.org/DebianRepository
.. _`Debian release`: https://www.debian.org/releases/
.. _`stretch`: https://www.debian.org/releases/stretch/
.. _`buster`: https://www.debian.org/releases/buster/
.. _`bullseye`: https://www.debian.org/releases/bullseye/
.. _`sid`: https://www.debian.org/releases/sid/
.. _`stable`: https://www.debian.org/releases/stable/
.. _`testing`: https://www.debian.org/releases/testing/
.. _`unstable`: https://www.debian.org/releases/unstable/
.. _`security updates`: https://www.debian.org/security/faq#mirror
.. _`Backports`: https://backports.debian.org/
.. _`apt-transport-https`: https://packages.debian.org/stable/apt-transport-https
.. _`proposed-updates`: https://www.debian.org/releases/proposed-updates
.. _`sources list wiki`: https://wiki.debian.org/SourcesList
