***********************
``apt`` management tool
***********************
The `Advanced Package Tool`_ provides as part of its package a new **front end** tool named ``apt``, which overcomes some design mistakes of ``apt-get`` and other backend tools. It is basically a wrapper of ``apt-get``, ``apt-cache`` and similar commands, that now can seen as backend tools.

Since ``apt`` was originally intended for **interactive** use, it's recommended to be used in that fashion, whereas the backend tools are still the recommended commands in shell scripts.

A couple of ``apt`` new features are:

* It provides a friendly **progress bar** when installing packages.
* It removes cached ``.deb`` packages by default after sucessful installation.

For more information:

* Run `man apt`_
* Check the `apt CLI wiki`_

Syntax comparison
=================
For the most part, ``apt`` it's just a command-line interface to the backend tools(``dpkg`` and APT tools), providing an improved output for interactive use. The following table contains a syntax comparison:

+--------------------------+----------------------+
|         Back-end         |   ``apt`` commands   |
+==========================+======================+
| ``apt-get update``       | ``apt update``       |
+--------------------------+----------------------+
| ``apt-get upgrade``      | ``apt upgrade``      |
+--------------------------+----------------------+
| ``apt-get dist-upgrade`` | ``apt full-upgrade`` |
+--------------------------+----------------------+
| ``apt-get install``      | ``apt install``      |
+--------------------------+----------------------+
| ``apt-get remove``       | ``apt remove``       |
+--------------------------+----------------------+
| ``apt-get autoremove``   | ``apt autoremove``   |
+--------------------------+----------------------+
| ``apt-get clean``        | ``apt clean``        |
+--------------------------+----------------------+
| ``apt-get autoclean``    | ``apt autoclean``    |
+--------------------------+----------------------+
| ``apt-cache show``       | ``apt show``         |
+--------------------------+----------------------+
| ``apt-cache search``     | ``apt search``       |
+--------------------------+----------------------+


.. _`Advanced Package Tool`: https://en.wikipedia.org/wiki/APT_(software)
.. _`man apt`: https://manpages.debian.org/buster/apt/apt.8.en.html
.. _`apt CLI wiki`: https://wiki.debian.org/AptCLI
