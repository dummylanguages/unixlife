*************
``apt-cache``
*************
The ``apt-cache`` command gives us access to the information stored in APT's internal database. This information is gathered from the sources listed in the ``sources.list`` during an **update operation** (``apt-get update`` or ``apt update``).

.. note:: In computing, a `cache`_ is a hardware or software component that stores data so that future requests for that data can be served faster. It would be very ineffective to go through the network for every package search.

A repository consists of at least one directory with some ``.deb`` packages in it, and two special files:

* ``Packages.gz`` for the binary packages(if we have the ``deb`` keyword in our ``sources.list`` file)
* ``Sources.gz`` for the source packages (if we have the ``deb-src`` keyword).

Every time we run an **update operation** APT will fetch these files, and will store a copy in ``/var/lib/apt/lists/``, that's the cache. Package searches are done within those local files.

For more information:

* Run `man apt-cache`_.
* Check `chapter 6.3`_ of the **Debian Administrator's Handbook**.
* The `Debian Repository HOWTO`_ is considered obsolete, but still holds valuable information.

Search for packages
===================
If we need to search for a package in the repositories we can use::

    $ sudo apt-cache search foo

This command basically implements ``grep`` on package's descriptions. It often returns too many results or none at all when you include too many keywords. The same functionality is provided by:

* ``apt search``
* ``aptitude search``

.. note:: The **Debian Administrator's Handbook** recommends using the command ``axi-cache search``, included in the `apt-xapian-index package`_.

Web search
----------
We can also point our browser to https://www.debian.org/distrib/packages and search for packages online.

Display information about a package
===================================
We can get information about an installed package and also about all the available versions in the repositories, using the ``policy`` command::

    $ apt-cache policy vim
    vim:
    Installed: (none)
    Candidate: 2:8.1.0875-5
    Version table:
        2:8.1.0875-5 500
        500 http://deb.debian.org/debian buster/main amd64 Packages

About the meaning of the ``500``, it's a priority number which corresponds to installable; ``100`` would mean installed. To learn more about priority numbers check `man apt_preferences`_.

The ``madison`` command will show information about the package, but won't tell us if it's installed or not::

    $ apt-cache madison vim
    vim | 2:8.1.0875-5 | http://deb.debian.org/debian buster/main amd64 Packages
    vim | 2:8.1.0875-5 | http://deb.debian.org/debian buster/main Sources

.. _`cache`: https://en.wikipedia.org/wiki/Cache_(computing)
.. _`APT`: https://www.debian.org/doc/manuals/debian-handbook/apt.en.html
.. _`man apt-cache`: https://manpages.debian.org/stable/apt/apt-cache.8.en.html
.. _`chapter 6.3`: https://www.debian.org/doc/manuals/debian-handbook/sect.apt-cache.en.html
.. _`Debian Repository HOWTO`: https://www.debian.org/doc/manuals/repository-howto/repository-howto.en.html
.. _`apt-xapian-index package`: https://packages.debian.org/stable/apt-xapian-index
.. _`man apt_preferences`: https://manpages.debian.org/stable/apt/apt_preferences.5.en.html
