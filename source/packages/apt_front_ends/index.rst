##############
APT front-ents
##############

.. toctree::
	:maxdepth: 3
	:hidden:

	aptitude

In Debian and Debian-based systems, package management operations can be performed by  different tools based on `APT`_, the **Advanced Package Tool**. In the last section we explained the ones included within APT system, but there are more:

* `Aptitude`_, which includes both a text mode interface and a graphical one.
* `Synaptic`_, which provides a **graphical interface** based on `GTK`_.
* `wajig`_. 


.. _`APT`: https://www.debian.org/doc/manuals/debian-handbook/apt.en.html
.. _`Aptitude`: https://wiki.debian.org/Aptitude
.. _`Synaptic`: http://savannah.nongnu.org/projects/synaptic
.. _`GTK`: https://www.gtk.org/
.. _`wajig`: https://togaware.com/wajig/
