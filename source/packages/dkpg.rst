****
dkpg
****
`dkpg`_ is the tool used for installing ``.deb`` packages that are already in our filesystem. ``dpkg`` checks dependencies and will refuse to install a package whose dependencies aren't met, but it won't help us find and install those dependencies. That's why ``apt`` was introduced, to overcome these limitations.

.. note:: ``dpkg`` is considered a medium-level tool; below it, there's ``dpkg-deb``, which performs lower-level actions on ``.deb`` files, such as reading the control information and directly extracting the contained files. 

Installing packages
===================
Once we have a ``.deb`` package in our filesystem, we can install it using the ``--install`` option::

    $ sudo dpkg -i foo_1.2.deb

The command above will unpack the file and configure/install it.

Separate unpacking and configuration
------------------------------------
We can also specify these stages ourselves::

    $ sudo dpkg --unpack foo_1.2.deb
    ...
    $ sudo dpkg --configure foo_1.2.deb
    ...

Force installation
------------------
Sometimes ``dpkg`` will fail to install a package and return an error. We can ignore it and force the installation using any of the ``--force-*`` options. Run ``dpkg --force-help`` for a full list.

Of all these options, only  ``--force-overwrite`` is likely to be used regularly. When a package contains a file that is already installed by another package, ``dpkg`` will refuse to install it showing us the error::

    Unpacking foo (from .../foo_1.2.3-4_amd64.deb) ...
    dpkg: error processing /var/cache/apt/archives/foo_1.2.3-4_amd64.deb (--unpack):
    trying to overwrite '/usr/bin/foobar', which is also in package baz 1.2.3-4

A careless use of the ``--force-*`` options can lead to a system where the APT family of commands will refuse to function. In that case, we could edit ``/var/lib/dpkg/status`` to delete/modify the dependency, or conflict, so we can keep using APT tools. But this manipulation is an ugly hack, and should never be used, except in the most extreme case of necessity.

Package removal
===============
Invoking dpkg with the ``-r`` or ``--remove`` option, followed by the name of a package, removes that package::

    # dpkg -r foo-pak

This removal is, however, not complete: all of the configuration files, maintainer scripts, log files (system logs) and other user data handled by the package remain. To completely remove everything associated with a package, use the ``-P`` or ``--purge`` option, followed by the package name::

    # dpkg -P foo-pak

Querying ``dpkg``'s Database
============================
If we want to list the files that a package has installed in our system, we would  use the ``--listfiles`` or ``-L`` option followed by the name of the package::

    $ dpkg -L some-pkg


When we want to find the package(s) that contain a certain file, we use the ``--search`` or ``-S`` option, followed by the name of the file::

    $ dpkg -S some-file

To display the headers of an installed package we'd use the ``--status`` or ``-S`` option, followed by the name of a package::

    $ dpkg -L some-pkg

The ``--list`` or ``-l`` option don't need any additional argument. They display the list of packages known to the system and their installation status::

    $ dpkg -l

Inspecting ``.deb`` Files
=========================
If we want to list the files in a given Debian package we have to use the ``--contents`` or ``-c`` option, followed by the name of the ``.deb`` file::

    $ dpkg -c foo.deb

When we just want to display the headers, the ``--info`` or ``-i`` option::

    $ dpkg -i foo.deb

Find out which package provides a certain file
==============================================
We can use the ``dpkg`` command to find out which installed package owns a file:

    $ dpkg -S /bin/ls
    coreutils: /bin/ls

There's a program name `apt-file`_ which does something similar but even with packages that are not installed.

``dpkg``'s Log File
===================
``dpkg`` keeps a log of all of its actions in ``/var/log/dpkg.log``. This log is extremely verbose, since it details every one of the stages through which packages handled by ``dpkg`` go.

.. _`dkpg`: https://wiki.debian.org/dpkg
.. _`apt-file`: https://wiki.debian.org/apt-file
